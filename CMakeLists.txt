cmake_minimum_required(VERSION 3.19)

project(brain-motifs LANGUAGES CXX)
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()


set(CMAKE_CXX_COMPILER)

add_compile_options(--std=c++17 -g -O3)

include_directories(include)
add_subdirectory(src/data_structure)
add_subdirectory(src/config)
add_subdirectory(src/graphlet_census)
add_subdirectory(src/network_measures)
add_subdirectory(src/graphlet_measures)
add_subdirectory(src/encoding)
add_subdirectory(src/graph_generator)

set(MAIN
    main.cpp)
add_executable(brain-motifs ${MAIN})



target_link_libraries(  brain-motifs
                        mpi_cxx
                        mpi
    		            boost_serialization
    		            boost_mpi
                        nauty
                        encoding
                        graphlet_census
                        network_measures
                        graphlet_measures
                        graph_generator)