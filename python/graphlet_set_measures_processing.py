from motif_analysis.plot.graphlet_set_measures import GraphletSetMeasures 
import sys 
import json


datasets  = json.load(open("param/log/datasets_all.json"))

datasets  = [ (dataset["category"],dataset["subcategory"]) for dataset in datasets ]

baseModels = [ "erdos-renyi", 
               "reciprocal_erdos-renyi",
               "configuration",
               "reciprocal_configuration" ]

measures   = ["density",
              "number_of_simple_cycles",
              "reciprocity",
              "symmetry_index",
              "orbit_polynomial_root"]

groupname = "all"

plot = GraphletSetMeasures(groupname,datasets,baseModels=baseModels,measures=measures)
plot.saveprocessing()