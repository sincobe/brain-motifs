

from motif_analysis.plot.motif_set_inference import MotifSetInference
import sys 
import json



idx      = int(sys.argv[1])
datasets = json.load(open("param/log/datasets_A2.json"))
dataset  = (datasets[idx]["category"],datasets[idx]["subcategory"])


baseModels = [ "erdos-renyi",
               "reciprocal_erdos-renyi",
               "configuration",
               "reciprocal_configuration" ]

plot = MotifSetInference(dataset,baseModels,mode="make",extension='.pdf') 
plot.load_panels()
plot.make()

