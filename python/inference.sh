#!/bin/bash
#SBATCH  -o console_plot/log_inference_%a.out -e console_plot/log_inference_%a.err
#SBATCH --array=0-5
#SBATCH --mem=30GB
#SBATCH -p dbc

python3 python/inference_processing.py $SLURM_ARRAY_TASK_ID
python3 python/inference_plot.py       $SLURM_ARRAY_TASK_ID

exit 0
