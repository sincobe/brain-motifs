from motif_analysis import plot_comparative as pcomp
import json 
import sys

j       = json.load(open('../param/datasets.json','r'))
idx     = int(sys.argv[1])

dataset = (j[idx]["category"],j[idx]["subcategory"])

method       = "planted_motif_model"
refs         = ["reciprocal_configuration"]
gens         = ["reciprocal_configuration"]

plot = pcomp.PlotComparative(method,refs)
for gen in gens:
    print("Test for randomized model:",gen)
    plot.test_compression_evolution(dataset,gen)
