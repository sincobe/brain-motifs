from motif_analysis.plot.compressibility import Compressibility
import sys 

idx = int(sys.argv[1])
groups = {
    "witvliet":[
        ("celegans","witvliet_2020_1"),
        ("celegans","witvliet_2020_2"),
        ("celegans","witvliet_2020_3"),
        ("celegans","witvliet_2020_4"),
        ("celegans","witvliet_2020_5"),
        ("celegans","witvliet_2020_6"),
        ("celegans","witvliet_2020_7")
    ],
    "larva":[
        ("drosophila_larva","antennal_lobe_right"),
        ("drosophila_larva","motor"),
        ("drosophila_larva","mushroom_body_right")
    ], 
    "jbpres":[
        ("celegans","witvliet_2020_2"),
        ("celegans","witvliet_2020_3"),
        ("celegans","witvliet_2020_4"),
        ("celegans","witvliet_2020_5"),
        ("celegans","witvliet_2020_6"),
        ("celegans","witvliet_2020_7"),
        ("celegans","witvliet_2020_8"),
        ("celegans","white_1986_whole"),
        ("celegans","cook_2019_hermaphrodite"),
        ("celegans","cook_2019_male")
    ],
    "all":[
        ("drosophila_larva","motor"),
        ("drosophila_larva","antennal_lobe_left"),
        ("drosophila_larva","antennal_lobe_right"),
        ("drosophila_larva","mushroom_body_left"),
        ("drosophila_larva","mushroom_body_right"),
        ("drosophila",      "lateral_horn_right"),
        ("drosophila",      "antennal_lobe_right"),
        ("drosophila",      "mushroom_body_right"),
    ]
}

# "all":[
#         ("celegans","witvliet_2020_1"),
#         ("celegans","witvliet_2020_2"),
#         ("celegans","witvliet_2020_3"),
#         ("celegans","witvliet_2020_4"),
#         ("celegans","witvliet_2020_5"),
#         ("celegans","witvliet_2020_6"),
#         ("celegans","witvliet_2020_7"),
#         ("celegans","white_1986_whole"),
#         ("celegans","cook_2019_hermaphrodite"),
#         ("celegans","cook_2019_male"),
#         ("drosophila_larva","motor"),
#         ("drosophila",      "lateral_horn_right"),
#         ("drosophila_larva","antennal_lobe_right"),
#         ("drosophila",      "antennal_lobe_right"),
#         ("drosophila_larva","mushroom_body_right"),
#         ("drosophila",      "mushroom_body_right"),
#         ("drosophila_larva","winding_2023_whole")
#     ]

groups["witvliet_and_larva"] = [*groups["witvliet"],*groups["larva"]]

groupname = list(groups.keys())[idx]
datasets  = groups[groupname]

plot = Compressibility(groupname,datasets,mode="make",extension=".pdf") 
plot.load_panels()
plot.savefig()