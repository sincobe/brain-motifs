import json
import matplotlib.pyplot as plt
from pathlib import Path
from typing import Tuple
import networkx as nx

from motif_analysis.path.finder import PathFinder

class PlotInterface():
    
    def __init__(self,mode="process_and_produce",nrows=1,ncols=1,extension='.eps',**kwargs):
        self.mode        = mode 
        self.fig,self.ax = plt.subplots(nrows=nrows,ncols=ncols,**kwargs)
        self.panels      = {}
        self.extension   = extension
        self.folderpath  = ""
        self.figurename  = ""
        self.params      = json.load(open('python/plot.json','r'))
        self.finder      = PathFinder()
    
    def get_graph(self, dataset : Tuple[str]):
        return nx.read_edgelist(self.finder.input_network_path(dataset),create_using=nx.DiGraph())

    def set_folderpath(self):
        pass

    def set_folder(self):
        self.set_folderpath()
        if self.folderpath == "" or self.figurename == "":
            self.saveflag = False
            print("motif_analysis.plot.interface.PlotInterface: wrong figurename and/or folderpath.")
        else:
            self.saveflag = True
            p = Path(self.folderpath)
            if not p.exists():
                p.mkdir(parents=True)

    def saveprocessing(self):
        self.set_folder()
        if self.saveflag: 
            json.dump(self.panels,open(self.folderpath + self.figurename + ".json",'w', encoding='utf8'),
                      indent=4, sort_keys=True,separators=(',', ': '), ensure_ascii=False)

    def savefig(self):
        if Path(self.folderpath).exists(): self.fig.savefig(self.folderpath + self.figurename + self.extension)

    def set_panels(self):
        pass

    def load_panels(self):
        self.set_folder()
        filename = self.folderpath + self.figurename + ".json"
        if Path(filename).is_file():
            self.panels = json.load(open(filename,'r'))
            self.set_panels()
        else:
            self.saveflag = False

    def dataset_subpath(self) -> str:
        return self.finder.param["output"]["dataset"]["category"]   [self.dataset[0]] \
            +  self.finder.param["output"]["dataset"]["subcategory"][self.dataset[1]]


    