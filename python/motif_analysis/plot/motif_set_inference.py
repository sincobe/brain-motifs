import numpy as np
import networkx as nx
from mpl_toolkits.axes_grid1 import ImageGrid
from typing import List, Tuple
from motif_analysis.plot.interface import *
from motif_analysis.graphlet.graphlet_distribution import GraphletDistribution
from motif_analysis.stat.planted_motif_model import PMMCollection
from motif_analysis.stat.null_model_codelength.null_model_factory import NullModelFactory

plt.style.use("python/styles/motif_set_inference.mplstyle")

class MotifSetInference(PlotInterface):
    def __init__(self, dataset : Tuple[str], baseModels : List[str], nulls=["simple_erdos-renyi"], method="planted_motif_model", minibatch_size = None, mode="process_and_make", nrows=1, ncols=1,**kwargs):
        
        self.ax_params  = json.load(open("python/ax_params/inference.json",'r'))
        # super().__init__(mode=mode,nrows=nrows,ncols=ncols,**(kwargs | self.ax_params["subplots"]))
        super().__init__(mode=mode,nrows=nrows,ncols=ncols,**kwargs)
        self.dataset    = dataset
        self.method     = method
        self.baseModels = baseModels
        self.nulls      = nulls
        self.figurename = "inference_results"

        if "process" in self.mode:
            self.G = nx.read_edgelist(self.finder.input_network_path(dataset),create_using=nx.DiGraph())
            self.graphlet_distribution = GraphletDistribution(("drosophila_larva","mushroom_body_right"))
            self.collections = {}
            for baseModel in baseModels:
                if "planted_motif_model" in self.method:
                    collection = PMMCollection(dataset,baseModel,minibatch_size=minibatch_size)
                    if collection.optimal_inference() is not None:
                        self.collections[baseModel] = collection
            self.set_optimal_inference()

            if self.is_valid():

                self.process_codelength_evolution()
                self.process_codelength_distribution()
                self.process_graphlet_set()

    def is_valid(self):
        return self.optimal_inference is not None
        
    def set_folderpath(self):
        self.folderpath = "figures_final/motif_set_inference/"+self.dataset_subpath()+self.method+"/"
        
    def set_optimal_inference(self):
        if len(self.collections):
            self.optimal_inference = min([ collection.optimal_inference() for collection in self.collections.values() ], 
                                           key=lambda inference : inference.L)
        else:
            self.optimal_inference = None
            
    def write_optimal_inference(self):
        
        if self.optimal_inference is not None:
            file = {
                "base_model"   : self.optimal_inference.base(),
                "subgraph_set" : self.optimal_inference.subgraph_set,
                "algorithmic_time" : self.optimal_inference.algorithmic_time,
                "graphlets"    : {
                    "total_number" : self.graphlet_distribution.number_of_graphlets(),
                    "properties":{ 
                        graphlet_label : {
                            "gtrie_label"             : self.graphlet_distribution.graphlet_gtrie_label(graphlet_label),
                            "automorphism_group_size" : self.graphlet_distribution.graphlet_automorphism_group_size(graphlet_label) 
                        } for graphlet_label in self.optimal_inference.graphlet_set
                    }
                }
                
            }
            path = Path(self.optimal_inference.filename).parents[1]
            json.dump(file,open(str(path)+"/inference.json", 'w', encoding='utf8'),
                      indent=4, sort_keys=True,separators=(',', ': '), ensure_ascii=False)
            

    def process_codelength_distribution(self):

        panel = {}

        codelengths = {}
        for baseModel,collection in self.collections.items():
            codelengths[baseModel] = np.array([ inference.L for inference in collection.inferences ])

        null_costs = {null : NullModelFactory().make(null,self.G).get() for null in self.nulls}

        panel["codelengths"]    = {baseModel : codelength_arr.tolist() for baseModel,codelength_arr in codelengths.items()}
        panel["null_costs"]     = null_costs 
        panel["L_opt"]          = self.optimal_inference.L
        panel["base_model_opt"] = self.optimal_inference.base()

        self.panels["codelength_histogram"] = panel

    def process_codelength_evolution(self):
        panel = {}
        panel["L_arr"]     = self.optimal_inference.L_arr.tolist()
        panel["null_cost"] = NullModelFactory().make(self.optimal_inference.null(),self.G).get()
        panel["algorithmic_time"] = self.optimal_inference.algorithmic_time
        self.panels["codelength_evolution"] = panel

    def process_graphlet_set(self):
        panel = {}
        self.graphlet_distribution.set_graphlets(list(self.optimal_inference.graphlet_set.keys()))


        for baseModel,collection in self.collections.items():
            self.graphlet_distribution.set_graphlets(list(collection.optimal_inference().graphlet_set.keys()))
            panel[baseModel] = {
                label : {
                    "edgelist" : ''.join([ str(i) + ' ' + str(j) + ',' for i,j in self.graphlet_distribution.graphlets[label].g.edges ])[:-1],
                    "n" : n
                }
                for label,n in collection.optimal_inference().graphlet_set.items()
            }
  
            

        # panel = {
        #     label : {
        #         "edgelist" : ''.join([ str(i) + ' ' + str(j) + ',' for i,j in self.graphlet_distribution.graphlets[label].g.edges ])[:-1],
        #         "n" : n
        #     }
        #     for label,n in self.optimal_inference.graphlet_set.items()
        # }
        self.panels["graphlet_set"] = panel

    def set_codelength_evolution_panel(self):

        self.fig,self.ax = plt.subplots()
        self.figurename = "codelength_evolution"

        ax_params = self.ax_params["codelength_evolution"]
        panel     = self.panels   ["codelength_evolution"]

        baseModel = self.panels["codelength_histogram"]["base_model_opt"]

        self.ax.plot   (panel["L_arr"],ax_params["plot"]["fmt"],   color=self.params["model"][baseModel]["color"])
        self.ax.axhline(panel["null_cost"],**ax_params["axhline"], color=self.params["model"][baseModel]["color"])

        self.ax.set_xlabel(ax_params["xlabel"])
        self.ax.set_ylabel(ax_params["ylabel"])

    def set_codelength_histogram_panel(self):

        self.fig,self.ax = plt.subplots()
        self.figurename = "codcelength_histogram"

        ax_params = self.ax_params["codelength_histogram"]
        panel     = self.panels   ["codelength_histogram"]

        for model,dist in panel["codelengths"].items(): 
            self.ax.hist(dist,**ax_params["hist"],
                            label=self.params["model"][model]["text"],
                            color=self.params["model"][model]["color"])

        for null,cost in panel["null_costs"].items():
            self.ax.axvline(cost,**ax_params["axvline"]["null_costs"],
                               label=self.params["model"][null]["text"],
                               color=self.params["model"][null]["color"]) 
    
        self.ax.axvline(panel["L_opt"],**ax_params["axvline"]["L_opt"])

        self.ax.set_xlabel(ax_params["xlabel"])
        self.ax.set_ylabel(ax_params["ylabel"])

    def set_graphlet_set_panel(self):

        ax_params = self.ax_params["graphlet_set"]
        panel     = self.panels   ["graphlet_set"]

        for baseModel in self.baseModels:
            self.fig,self.ax = plt.subplots()
            self.figurename = "graphlet_set_" + baseModel
            graphlets = panel[baseModel].values()
            ncols_max = ax_params["ncols_max"]

            ncols = np.min([ ncols_max,len(graphlets) ])
            nrows = len(graphlets) // ncols + 1
            
            r = np.array([[0,-1],
                        [1, 0]])

            # grid  = ImageGrid(self.fig, 133, nrows_ncols=(nrows,ncols),share_all=True,axes_pad=0.1)
            grid  = ImageGrid(self.fig, 111, nrows_ncols=(nrows,ncols),share_all=True,axes_pad=0.1)
            graphlets = list(sorted(graphlets,key=lambda g : -g["n"]))
            for ax,graphlet in zip(grid,graphlets):
                edgelist = [ tuple(int(el) for el in pair.split(' ')) for pair in graphlet["edgelist"].split(',') ]
                g = nx.DiGraph()
                g.add_edges_from(edgelist)

                pos = nx.circular_layout(g)
                pos = {idx : np.dot(r,v) for idx,v in pos.items()}
                nx.draw(g,pos,**ax_params["networkx_draw"],ax=ax)
                ax.axis('equal')
                ax.set_title(str(graphlet["n"]),**ax_params["title"])

            for i,ax in enumerate(grid):
                if i >= len(graphlets):
                    ax.remove()
            
            self.ax.axis('off')
            self.savefig() 


    def make(self):
        self.set_codelength_evolution_panel()
        self.savefig()

        self.set_codelength_histogram_panel()
        self.savefig()

        self.set_graphlet_set_panel()     
              

    


        
    
        
        
        



