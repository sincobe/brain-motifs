from motif_analysis.stat.frequency import FrequencyCollection 
from motif_analysis.stat.hierarchy import NullHierarchy
from motif_analysis.plot.interface import PlotInterface
from statsmodels.stats.multitest   import multipletests 


from typing import Tuple,List
import numpy as np
import matplotlib.pyplot as plt
import json
import pandas as pd
import seaborn as sn

plt.style.use("python/styles/descriptive_stat.mplstyle")

class DescriptiveStat(PlotInterface):
    def __init__(self, dataset : Tuple[str], models : List[str], method="bonferroni", alpha=1.e-2, nrows=1,ncols=1, **kwargs) :
        super().__init__(nrows=nrows,ncols=ncols,**kwargs)
        self.alpha      = alpha
        self.method     = method
        self.dataset    = dataset
        self.models     = models
        self.figurename = "descriptive_stat"
        self.ax_params  = json.load(open("python/ax_params/descriptive_stat.json",'r'))

        if "process" in self.mode:
            self.collections = { model : FrequencyCollection(self.dataset,model) for model in self.models }
            self.process_uncorrected_pvalues()


    def dataset_subpath(self) -> str:
        return self.finder.param["output"]["dataset"]["category"]   [self.dataset[0]] \
            +  self.finder.param["output"]["dataset"]["subcategory"][self.dataset[1]]

    def set_folderpath(self):
        self.folderpath = "figures/descriptive_stat/"+self.dataset_subpath()+self.method+"/"

    def process_uncorrected_pvalues(self):
        self.panels["motif_detection"] = { model : self.collections[model].pvalues(type="right-tail") for model in self.models }
        
    def set_motif_detection_panel(self):
        
        self.fig,self.ax = plt.subplots()
        self.figurename = "detection_number"

        panel     = self.panels   ["motif_detection"]
        ax_params = self.ax_params["motif_detection"]
        
        for i,model in enumerate(self.models):
            pvals_dict = panel[model]
            pvals = np.array(list(pvals_dict.values()))
            reject,pvals_corrected,alphacSidak,alphaBonf = multipletests(pvals,self.alpha,self.method)
            self.ax.bar(i,reject.sum(),width=ax_params["width"],alpha=ax_params["opacity"],color=self.params["model"][model]["color"])

        self.ax.set_xticks(np.arange(len(self.models)),[self.params["model"][model]["text"] for model in self.models])
        if "setp" in ax_params:
            labels = self.ax.get_xticklabels()
            plt.setp(labels,**ax_params["setp"])
        self.ax.set_ylabel("#Motifs")

    def set_jaccard_panel(self):

        self.fig,self.ax = plt.subplots()
        self.figurename = "jaccard"

        panel       = self.panels   ["motif_detection"]
        ax_params   = self.ax_params["common_motifs"]
        
        reject_dict = {}
        for i,model in enumerate(self.models):
            pvals_dict = panel[model]
            pvals = np.array(list(pvals_dict.values()))
            reject,pvals_corrected,alphacSidak,alphaBonf = multipletests(pvals,self.alpha,self.method)
            reject_dict[model] = { key : b for key,b in zip(pvals_dict.keys(),reject) }

        X = np.zeros((len(self.models),len(self.models)))
        for i,model_i in enumerate(self.models):
            set_i = { g for g,b in reject_dict[model_i].items() if b }
            set_k = set()
            for j,model_j in enumerate(self.models):

                set_j = { g for g,b in reject_dict[model_j].items() if b }
                set_k = set_k | set_j
                X[i,j] = len(set_i & set_j) / len(set_i | set_j)

            
        index   = [self.params["model"][model]["text"] for model in self.models]
        columns = [self.params["model"][model]["text"] for model in self.models]
        # index[0]    = ""
        # columns[-1] = ""
        df_cm   = pd.DataFrame(X,index=index,columns=columns)

        sn.heatmap(df_cm, annot=True, mask=np.triu(df_cm.corr()), ax=self.ax)

        if "setp" in ax_params:
            labels_a = self.ax.get_xticklabels()
            plt.setp(labels_a,**ax_params["setp"])

    def set_set_difference_panel(self):

        self.fig,self.ax = plt.subplots()
        self.figurename = "set_difference"

        panel       = self.panels   ["motif_detection"]
        ax_params   = self.ax_params["common_motifs"]
        
        reject_dict = {}                
        for i,model in enumerate(self.models):
            pvals_dict = panel[model]
            pvals = np.array(list(pvals_dict.values()))
            reject,pvals_corrected,alphacSidak,alphaBonf = multipletests(pvals,self.alpha,self.method)
            reject_dict[model] = { key : b for key,b in zip(pvals_dict.keys(),reject) }

        Y = np.zeros((len(self.models),len(self.models)))
        for i,model_i in enumerate(self.models):
            set_i = {g for g,b in reject_dict[model_i].items() if b }
            set_k = set()
            for j,model_j in enumerate(self.models):

                set_j = {g for g,b in reject_dict[model_j].items() if b }
                set_k = set_k | set_j

                Y[i,j] = len(set_i - set_j) / len(set_i)
            
        index   = [self.params["model"][model]["text"] for model in self.models]
        columns = [self.params["model"][model]["text"] for model in self.models]
        # index[0]    = ""
        # columns[-1] = ""

        df_cm_2 = pd.DataFrame(Y,index=index,columns=columns)
        sn.heatmap(df_cm_2,annot=True,ax=self.ax)

        if "setp" in ax_params:
            labels_b = self.ax.get_xticklabels()
            plt.setp(labels_b,**ax_params["setp"])


    def set_hierarchy(self, hierarchy : NullHierarchy):
        self.hierarchy = hierarchy

    def make(self):
        self.set_motif_detection_panel()
        self.savefig()

        self.set_jaccard_panel()
        self.savefig()

        self.set_set_difference_panel()
        self.savefig()
        

                    
    