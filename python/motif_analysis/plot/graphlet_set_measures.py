from motif_analysis.plot.interface import *
from motif_analysis.path.finder import PathFinder
from motif_analysis.stat.planted_motif_model import PMMCollection
from motif_analysis.graphlet.graphlet_distribution import GraphletDistribution
from motif_analysis.path.graphlet_census import GraphletMeasureFinder
from typing import List,Tuple,Dict
import numpy as np
import networkx as nx
from scipy.optimize import linear_sum_assignment
import pandas as pd
import seaborn as sn
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
plt.style.use("python/styles/graphlet_set_measures.mplstyle")

class GraphletSetMeasures(PlotInterface):
    def __init__(self, groupname : str, datasets : List[Tuple[str]], baseModels=[],measures=[],method="planted_motif_model",mode="process_and_make",ncols=1,**kwargs):
        super().__init__(mode=mode,nrows=len(measures),ncols=ncols,**kwargs)
        self.datasets   =  datasets
        self.baseModels = baseModels
        self.measures   = measures
        self.method     = method 
        self.groupname  = groupname
        self.figurename = "graphlet_set_measures_all"
        self.ax_params  = json.load(open("python/ax_params/graphlet_set_measures.json",'r'))

        if "process" in self.mode:
            
            self.set_graphlet_distribution()
  
            self.set_graphlet_sets()

            self.set_measures()
            self.set_measures_baseline()
            self.process_graphlet_sets()
            # self.process_graphlet_edit_distance()
            # self.process_graphlet_set_edit_distances()
            
    def set_folderpath(self):
        self.folderpath = "figures/graphlet_set_measures/" + self.method+"/"

    def get_density(self,dataset : Tuple[str]) -> float:
        G = nx.read_edgelist(PathFinder().input_network_path(dataset),create_using=nx.DiGraph())
        return nx.density(G)

    def set_graphlet_distribution(self):
        self.graphlet_distribution = GraphletDistribution(("drosophila_larva","mushroom_body_right"))
        self.graph_densities = { dataset[0]+"_"+dataset[1] : self.get_density(dataset) for dataset in self.datasets }
        self.panels["number_of_graphlets"]   = self.graphlet_distribution.number_of_graphlets()

    def set_graphlet_sets(self):
        self.graphlet_sets = {}
        for dataset in self.datasets:
            L = np.inf
            self.graphlet_sets[dataset] = {}
            for baseModel in self.baseModels:
                collection = PMMCollection(dataset,baseModel)
                configuration = collection.optimal_inference()
                if configuration is not None and configuration.L < L:
                    self.graphlet_sets[dataset] = configuration.graphlet_set 
                    
            # self.graphlet_distribution.set_graphlets(list(self.graphlet_sets[dataset].keys()))
        self.graphlet_distribution.set_all_graphlets()

    def set_measures(self):
        self.panels["graph_densities"]    = self.graph_densities
        self.panels["graphlet_measures"]  = {}
        for label,graphlet in self.graphlet_distribution.graphlets.items():
            for measure in self.measures: graphlet.set_measure(measure)
            self.panels["graphlet_measures"][label] = graphlet.attributes
                

    def process_graphlet_sets(self):
        self.panels["graphlet_sets"]      = {}
        self.panels["graphlet_edgelists"] = { label : ''.join([ str(i) + ' ' + str(j) + ',' for i,j in graphlet.g.edges ])[:-1] for (label,graphlet) in self.graphlet_distribution.graphlets.items() }
        for (dataset,graphlet_set) in self.graphlet_sets.items(): self.panels["graphlet_sets"][dataset[0]+"_"+dataset[1]] = graphlet_set
            # for graphlet_label in graphlet_set.keys():
            #     if graphlet_label not in self.panels["graphlet_measures"]:
            #         self.panels["graphlet_measures"][graphlet_label] = {}
            #         graphlet = self.graphlet_distribution.graphlets[graphlet_label]
            #         g        = graphlet.get_nx_graph()
            #         n,m      = g.number_of_nodes(),g.number_of_edges()
            #         g_rand   = self.rand_graphlet_measures[str(n)+"_"+str(m)]
            #         for measure in self.measures:
            #             self.panels["graphlet_measures"]         [graphlet_label][measure] = graphlet.attributes[measure] 
            #             self.panels["graphlet_measures_baseline"][graphlet_label][measure] = g_rand[measure]

    # def mean_graphlet_set_edit_distance(self, graphlet_set_A : Set[str], graphlet_set_B : Set[str]):
    #     ls_d = []
    #     graphlet_set_ref   = min([graphlet_set_A,graphlet_set_B], key=lambda set : len(set))
    #     graphlet_set_large = max([graphlet_set_A,graphlet_set_B], key=lambda set : len(set))
    #     for graphlet_label_ref in graphlet_set_ref:
    #         d = 1.e32
    #         g_ref = self.graphlet_distribution.graphlets[graphlet_label_ref]
    #         for graphlet_label_large in graphlet_set_large:
    #             g_large = self.graphlet_distribution.graphlets[graphlet_label_large]
    #             d = min([d,nx.graph_edit_distance(g_ref.g,g_large.g)])
    #         ls_d.append(d)
    #     return np.mean(ls_d)

    # def process_graphlet_set_edit_distances(self):
    #     panel = {}
    #     for i,(dataset_A,graphlet_set_A) in enumerate(self.graphlet_sets.items()):
    #         category_A,subcategory_A = dataset_A
    #         for j,(dataset_B,graphlet_set_B) in enumerate(self.graphlet_sets.items()):
    #             if i < j: 
    #                 print(dataset_A,dataset_B)
    #                 category_B,subcategory_B = dataset_B
    #                 key        = category_A+","+subcategory_A+","+category_B+","+subcategory_B
    #                 panel[key] = self.mean_graphlet_set_edit_distance(graphlet_set_A.keys(),graphlet_set_B.keys())

    #     self.panels["graphlet_set_edit_distance"] = panel 

    def set_measures_baseline(self):
        self.panels["graphlet_measures_baseline"]  = {}
        rand_graphlet_measures = {}
        n_rand = 100
        for graphlet_set in self.graphlet_sets.values():
            for label in graphlet_set.keys():
                graphlet0 = self.graphlet_distribution.graphlets[label]
                g0 = graphlet0.get_nx_graph()
                n,m = g0.number_of_nodes(),g0.number_of_edges()
                key = str(n)+"_"+str(m)
                if key not in rand_graphlet_measures:
                    rand_graphlet_measures[key] = {measure : 0. for measure in self.measures}
                    
                    for rep in range(n_rand):
                        g_rand = nx.gnm_random_graph(n,m,directed=True)
                        for graphlet in self.graphlet_distribution.graphlets.values():
                            if nx.is_isomorphic(graphlet.get_nx_graph(),g_rand):
                                for measure in self.measures: rand_graphlet_measures[key][measure] += graphlet.attributes[measure]/n_rand
                                break 
                            
                
                self.panels["graphlet_measures_baseline"][label] = rand_graphlet_measures[key]
            
    def process_graphlet_edit_distance(self):
        graphlet_distances = { label : {} for label in self.panels["graphlet_measures"].keys() }
        for i,label_a in enumerate(graphlet_distances):
            g_a = self.graphlet_distribution.graphlets[label_a]
            print("graphlet edit distances stage",i/len(graphlet_distances))
            for j,label_b in enumerate(graphlet_distances):
                g_b = self.graphlet_distribution.graphlets[label_b]
                if i < j:
                    graphlet_distances[label_a][label_b] = nx.graph_edit_distance(g_a.g,g_b.g)
        self.panels["graphlet_edit_distances"] = graphlet_distances
                
                
    def get_ls_measure(self,dataset,measure):
            ls,ls_rand = [],[]
            graphlet_set = self.panels["graphlet_sets"][dataset[0]+"_"+dataset[1]] 
            for graphlet_label,n_graphlet in graphlet_set.items():
                for rep in range(n_graphlet): 
                    ls     .append(self.panels["graphlet_measures"]         [graphlet_label][measure])
                    ls_rand.append(self.panels["graphlet_measures_baseline"][graphlet_label][measure])
            return ls,ls_rand
                    
    def set_graphlet_set_measures_panel(self):
        
        self.figurename = self.groupname
        

        ax_params = self.ax_params["graphlet_set_measures"]
        xticks    = [ self.params[category][subcategory]["text"] for (category,subcategory) in self.datasets  ] 
        D         = len(self.datasets)
        densities = [self.panels["graph_densities"][category+"_"+subcategory] for (category,subcategory) in self.datasets]
        
        for i,measure in enumerate(self.measures):
            
            lists = [ self.get_ls_measure(dataset,measure) for dataset in self.datasets ]

            means = [ np.mean(ls)                  for ls,ls_rand in lists ]
            stds  = [ np.std (ls)/np.sqrt(len(ls)) for ls,ls_rand in lists ]

            means_rand = [ np.mean(ls_rand)                       for ls,ls_rand in lists ]
            stds_rand  = [ np.std (ls_rand)/np.sqrt(len(ls_rand)) for ls,ls_rand in lists ]
            self.ax[i].errorbar  (np.arange(D),means,yerr=stds,fmt="sr")
            if measure != "density": self.ax[i].errorbar  (np.arange(D),means_rand,yerr=stds_rand,fmt="sb")
            else: self.ax[i].scatter(np.arange(D),densities,color="black")
            self.ax[i].set_xticks(np.arange(D),["" for dataset in self.datasets])
            self.ax[i].set_ylabel(self.params["graphlet_measures"][measure]["text"])

            if measure == "density":
                self.ax[i].set_ylim([-0.1,0.85])
            elif measure == "number_of_simple_cycles":
                self.ax[i].set_ylim([-0.01,27])
            elif measure == "reciprocity":
                self.ax[i].set_ylim([0.25,0.8])
            elif measure == "orbit_polynomial_root":
                self.ax[i].set_ylim([0.2,0.65])
                
        self.ax[i].set_xticks(np.arange(D),xticks)
        
        if "setp" in ax_params:
            labels = self.ax[i].get_xticklabels()
            plt.setp(labels,**ax_params["setp"])
            
    def set_mean_graphlet_measures_panel(self):
        
        plt.close('all')
        self.fig,self.ax = plt.subplots(figsize=(1,4),nrows=len(self.measures))
        self.figurename = self.groupname + "_mean"
        

        ax_params = self.ax_params["graphlet_set_measures"]
        xtick    =  ["Mean"]  
        D         = len(self.datasets)
        densities = [self.panels["graph_densities"][category+"_"+subcategory] for (category,subcategory) in self.datasets]
        
        for i,measure in enumerate(self.measures):
            
            lists = [ self.get_ls_measure(dataset,measure) for dataset in self.datasets ]

            means = [ np.mean(ls)                  for ls,ls_rand in lists ]
            # stds  = [ np.std (ls)/np.sqrt(len(ls)) for ls,ls_rand in lists ]

            means_rand = [ np.mean(ls_rand)                       for ls,ls_rand in lists ]
            # stds_rand  = [ np.std (ls_rand)/np.sqrt(len(ls_rand)) for ls,ls_rand in lists ]
            self.ax[i].scatter  (0,np.mean(means))
            if measure != "density": self.ax[i].scatter  (0,np.mean(means_rand))
            else: self.ax[i].scatter(0,np.mean(densities),color="black")
            
            self.ax[i].set_ylabel(self.params["graphlet_measures"][measure]["text"])
            self.ax[i].set_xticks([0],"")
            if measure == "density":
                self.ax[i].set_ylim([-0.1,0.85])
            elif measure == "number_of_simple_cycles":
                self.ax[i].set_ylim([-0.01,27])
            elif measure == "reciprocity":
                self.ax[i].set_ylim([0.25,0.8])
            elif measure == "orbit_polynomial_root":
                self.ax[i].set_ylim([0.2,0.65])
        
        self.ax[i].set_xticks([0],xtick)
                
        self.savefig()
                
        # self.ax[i].set_xticks(np.arange(D),xticks)
        
        # if "setp" in ax_params:
        #     labels = self.ax[i].get_xticklabels()
        #     plt.setp(labels,**ax_params["setp"])

    def set_cosine_similarity_panel(self, measure : str):
        N_graphlets      = self.panels["number_of_graphlets"]
        ax_params        = self.ax_params["cosine_similarity"]
        self.fig,self.ax = plt.subplots(figsize=ax_params["figsize"])

        ticks = [ self.params[category][subcategory]["text"] for (category,subcategory) in self.datasets  ] 
        D = len(self.datasets)
        X = np.zeros((D,D))
        M = np.zeros((D,N_graphlets))

        labels = self.panels["graphlet_measures"].keys()
        labelToIdx = { label : i for i,label in enumerate(labels) }
        for i,dataset in enumerate(self.datasets):
            for graphlet_label,n_graphlet in self.panels["graphlet_sets"][dataset[0]+"_"+dataset[1]].items():
                
                M[i][labelToIdx[graphlet_label]] = n_graphlet * self.panels["graphlet_measures"][graphlet_label][measure][0]

        for i in range(D):
            for j in range(D):
                X[i,j] = np.dot(M[i].T, M[j]) / ( (M[i]**2).sum()**0.5 * (M[j]**2).sum()**0.5 )

        index   = [self.params[category][subcategory]["text"] for (category,subcategory) in self.datasets]
        columns = [self.params[category][subcategory]["text"] for (category,subcategory) in self.datasets]

        # df_cm   = pd.DataFrame(X,index=index,columns=columns)
        # sn.heatmap(df_cm,  annot=True,ax=self.ax)

        df_cm   = pd.DataFrame(X,index=index,columns=columns)
        sn.heatmap(df_cm,  annot=True,  mask=np.triu(df_cm.corr()), ax=self.ax)
 

        if "setp" in ax_params:
            labels = self.ax.get_xticklabels()
            plt.setp(labels,**ax_params["setp"])

        self.figurename = "cosine_similarity_"+measure+"_"+self.groupname

        self.ax.set_xticks(np.arange(D),ticks)
        self.ax.set_yticks(np.arange(D),ticks)
        if "setp" in ax_params:
            labels = self.ax.get_xticklabels()
            plt.setp(labels,**ax_params["setp"])

    def set_graphlet_set_edit_distances_panel(self):

        ax_params = self.ax_params["graphlet_set_edit_distance"]
        graphlet_edit_distances = self.panels["graphlet_edit_distances"]
        self.fig,self.ax = plt.subplots(figsize=ax_params["figsize"])
        
        # measures_max = { measure : max([ m[measure] for m in self.panels["graphlet_measures"].values()]) for measure in self.measures }

        X = np.zeros((len(self.datasets),len(self.datasets)))
        for i,dataset_A in enumerate(self.datasets):
            for j,dataset_B in enumerate(self.datasets):
                if i > j:
                    graphlet_set_A = self.panels["graphlet_sets"][dataset_A[0]+"_"+dataset_A[1]]
                    graphlet_set_B = self.panels["graphlet_sets"][dataset_B[0]+"_"+dataset_B[1]]
                    labels_A,labels_B = [],[]
                    for (graphlet_label_A,n_graphlet_A),(graphlet_label_B,n_graphlet_B) in zip(graphlet_set_A.items(),graphlet_set_B.items()):
                        # labels_A.append(graphlet_label_A)
                        # labels_B.append(graphlet_label_B)
                        for rep in range(n_graphlet_A): labels_A.append(graphlet_label_A)
                        for rep in range(n_graphlet_B): labels_B.append(graphlet_label_B)

                    C = np.zeros((len(labels_A),len(labels_B)))
                    for a,label_A in enumerate(labels_A):
                        # m_a = np.array([ self.panels["graphlet_measures"][label_A][measure] / measures_max[measure] for measure in self.measures ])
                        for b,label_B in enumerate(labels_B):
                            if label_A != label_B:
                                # m_b = np.array([ self.panels["graphlet_measures"][label_A][measure] / measures_max[measure] for measure in self.measures ])
                                # C[a,b] = 1. - np.dot(m_a.T,m_b)/( (m_a**2).sum()**0.5 * (m_b**2).sum()**0.5 )
                                C[a][b] = graphlet_edit_distances[label_A][label_B] if label_B in graphlet_edit_distances[label_A] else graphlet_edit_distances[label_B][label_A]

                    row_ind, col_ind = linear_sum_assignment(C)
                    X[i][j] = C[row_ind, col_ind].sum() / min([ len(labels_A),len(labels_B) ])
                    
        index   = [self.params[category][subcategory]["text"] for (category,subcategory) in self.datasets]
        columns = [self.params[category][subcategory]["text"] for (category,subcategory) in self.datasets]

        df_cm   = pd.DataFrame(X,index=index,columns=columns)
        sn.heatmap(df_cm,  annot=True, mask=np.triu(df_cm.corr()), ax=self.ax)

        if "setp" in ax_params:
            labels = self.ax.get_xticklabels()
            plt.setp(labels,**ax_params["setp"])

        self.figurename = "edit_distances_"+self.groupname

    def get_graphlet_stat(self,type : str) -> Dict[str,float]:
        graphlet_stat = {}
        for (category,subcategory) in self.datasets:
            name = category+"_"+subcategory
            size = sum(list( self.panels["graphlet_sets"][name].values()))
            for (label,n) in self.panels["graphlet_sets"][name].items():
                if label not in graphlet_stat:
                    graphlet_stat[label] = 0.
                if type == "absolute":
                    graphlet_stat[label] += n  / len(self.datasets)
                elif type == "fraction":
                    graphlet_stat[label] += n  / size / len(self.datasets)
                elif type == "probability":
                    graphlet_stat[label] += 1. / len(self.datasets)
                else:
                    break
        graphlet_stat = dict(sorted(graphlet_stat .items(),key=lambda x : -x[1]))
        return graphlet_stat


    def set_graphlet_stat_panel(self,type,spacing=2,max_iter=10,figsize_g=(5,5),dpi_g=400):
        self.fig,self.ax = plt.subplots(figsize=(5,3),dpi=600)

        graphlet_stat    = self.get_graphlet_stat(type)
        self.figurename  = self.groupname+"_graphlet_stat_"+type
        r = np.array([[0,-1],
                [1, 0]])

        draw_g = { 
            "node_color":"r", 
            "edgecolors":"k",
            "linewidths":2, 
            "node_size":1500,
            "arrowstyle":"<|-",
            "edge_color":"k",
            "arrowsize":50,
            "width":7,
            "connectionstyle":"arc3"
        }
        
        if   type == "fraction":    self.ax.set_ylabel("$c_\\alpha$",fontsize=15)
        elif type == "probability": self.ax.set_ylabel("$p_\\alpha$",fontsize=15)

        xticks = np.arange(0,max_iter*spacing,spacing)
        self.ax.set_xticks(xticks,["" for rep in range(max_iter)])

        tick_labels = self.ax.xaxis.get_ticklabels()
        rep = 0

        for xtick,(label,y) in zip(xticks,graphlet_stat.items()):
            edgelist = [ tuple(int(el) for el in pair.split(' ')) for pair in self.panels["graphlet_edgelists"][label].split(',') ]
            g = nx.DiGraph()
            g.add_edges_from(edgelist)
            pos = nx.circular_layout(g)
            pos = {idx : np.dot(r,v) for idx,v in pos.items()}

            fig_g,ax_g = plt.subplots(figsize=figsize_g,dpi=dpi_g)
            nx.draw(g,pos,**draw_g,ax=ax_g)
            fig_g.savefig('temp_graphlet_image.jpeg')
            plt.close(fig_g)

            im = plt.imread('temp_graphlet_image.jpeg')
            
            ib = OffsetImage(im, zoom=.015)
            ib.image.axes = self.ax
            ab = AnnotationBbox(ib,
                                tick_labels[rep].get_position(),
                                frameon=False,
                                box_alignment=(0.5, 1.2)
                                )
            self.ax.add_artist(ab)

            self.ax.bar(xtick,y,color="coral")
            Path("temp_graphlet_image.jpeg").unlink()
            rep += 1

            if rep == max_iter + 1:
                break
        plt.tight_layout() 

    def set_measure_hist_panel(self,measure : str, figsize=(4,3)):
        self.fig,self.ax = plt.subplots(figsize=figsize)
        self.figurename = "hist_"+measure
        dist = [ graphlet[measure] for graphlet in self.panels["graphlet_measures"].values() ]
        # print("min:",min(dist),"max:",max(dist))
        # self.ax.hist(dist,**self.ax_params["hist"],align='right')
        dist_unique = sorted(set(dist))
        dist_freq   = [ dist.count(value) for value in dist_unique ]
        self.ax.scatter(dist_unique,dist_freq,**self.ax_params["hist"])
        self.ax.set_yscale('log')
        self.ax.set_xlabel("GPR$(\\alpha)$",fontsize=12)
        self.ax.set_ylabel("freq of GPR$(\\alpha)$",fontsize=12)
        self.ax.set_ylim([0.9,len(dist)+1e4])
        self.ax.set_xlim([-0.01,1.05])


    def get_graphlet_measures_file(self):
        return self.panels

    def make(self):
        self.set_graphlet_set_measures_panel()
        self.savefig()



    def make_graphlet_stat(self):
        # self.set_graphlet_stat_panel("absolute")
        # self.savefig()
        
        plt.close('all')

        self.set_graphlet_stat_panel("fraction")
        self.savefig()

        self.set_graphlet_stat_panel("probability")
        self.savefig()
        
        
        # self.set_graphlet_set_edit_distances_panel()
        # self.savefig()
        # for measure in self.measures:
        #     self.set_cosine_similarity_panel(measure)
        #     self.savefig()
