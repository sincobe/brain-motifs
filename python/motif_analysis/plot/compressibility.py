from motif_analysis.plot.interface import *
from motif_analysis.stat.planted_motif_model import PMMCollection
from motif_analysis.stat.null_model_codelength.null_model_factory import NullModelFactory
from typing import List,Tuple,Dict
import numpy as np
import networkx as nx

plt.style.use("python/styles/compressibility.mplstyle")

class Compressibility(PlotInterface):
    def __init__(self, groupname : str, datasets : List[Tuple[str]], baseModels=[], refModels=["simple_erdos-renyi"], method="planted_motif_model",mode="process_and_make",nrows=1,ncols=1,**kwargs):
        super().__init__(mode=mode,nrows=nrows,ncols=ncols,**kwargs)
        self.datasets        = datasets
        self.baseModels      = baseModels
        self.refModels       = refModels
        self.refModel        = refModels[0]
        self.method          = method 
        self.figurename      = "compressibility_all"
        self.groupname       = groupname
        self.ax_params       = json.load(open("python/ax_params/compressibility.json",'r'))

        if "process" in self.mode:
            self.codelengths     = {}
            self.codelengths_ref = {}
                
            for dataset in self.datasets:
                self.codelengths    [dataset[0]+"_"+dataset[1]] = {} 
                self.codelengths_ref[dataset[0]+"_"+dataset[1]] = {}

    def set_folderpath(self):
        self.folderpath = "figures_final/compressibility/" + self.method+"/"
    
    def set_codelengths(self):
        for dataset in self.datasets:
            L_opt     = np.inf
            baseModel_opt = 'None'
            for baseModel in self.baseModels:
                print(dataset,baseModel)
                if self.method=="planted_motif_model":
                    collection = PMMCollection(dataset,baseModel)
                    L = collection.optimal_inference().L if collection.optimal_inference() is not None else np.inf
                    # if L < L_opt:
                    #     L_opt         = L
                    #     baseModel_opt = baseModel
                self.codelengths[dataset[0]+"_"+dataset[1]][baseModel] = L

    def set_codelengths_ref(self):
            
        for dataset in self.datasets:
            G = nx.read_edgelist(self.finder.input_network_path(dataset),create_using=nx.DiGraph())
            self.codelengths_ref[dataset[0]+"_"+dataset[1]] = {ref : NullModelFactory().make(ref,G).get() for ref in self.refModels}
            

    def process_compressibility(self):
        self.set_codelengths()
        self.set_codelengths_ref()

        panel = {}
        panel["null_costs"]  = self.codelengths_ref 
        panel["codelengths"] = self.codelengths
        self.panels["compressibility"] = panel

    def set_null_selection(self,refModel : str):
        self.refModel = refModel

    def get_xticks(self) -> List[str]:
        return [ self.params[dataset[0]][dataset[1]]["text"] for dataset in self.datasets ] 

    # def set_compressibility_panel(self):
    #     panel     = self.panels   ["compressibility"]
    #     ax_params = self.ax_params["compressibility"]
    #     xticks    = self.get_xticks()
    #     self.ax.set_xticks(np.arange(len(self.datasets)),xticks)
        
    #     i = 0
    #     for dataset in self.datasets:
    #         baseModel,L = panel["codelengths"]   [dataset[0]+"_"+dataset[1]]
    #         L_ref       = panel["null_costs"]    [dataset[0]+"_"+dataset[1]] 
    #         null,L0_opt = min(panel["null_costs"][dataset[0]+"_"+dataset[1]].items(), key=lambda item : item[1])
    #     # for dictL,dictL_ref in zip(panel["codelengths"].values(),panel["null_costs"].values()):
    #         # for L,L_ref in zip(dictL.values(),dictL_ref.values()):
    #         G = self.get_graph(dataset)
    #         N,E = G.number_of_nodes(),G.number_of_edges()
    #         # handle_a = plt.scatter([],[],**ax_params["scatter"],     label="Significant motifs")
    #         # handle_b = plt.scatter([],[],**ax_params["scatter_null"],label="No motifs")

    #         if L < L0_opt:
    #             print(dataset,'N:',N,'E:',E,'theta: PMM + ',baseModel)
    #             self.ax.scatter(i,(L_ref[self.refModel]-L     )/E,**ax_params["scatter"])
    #         else:
    #             print(dataset,'N:',N,'E:',E,'theta: Simple graph ',null)
    #             self.ax.scatter(i,(L_ref[self.refModel]-L0_opt)/E,**ax_params["scatter_null"])
    #         i+=1
       
    #     if "setp" in ax_params:
    #         labels = self.ax.get_xticklabels()
    #         plt.setp(labels,**ax_params["setp"])

    #     if "yscale" in ax_params:
    #         self.ax.set_yscale(ax_params["yscale"])

    #     if "ylim" in ax_params:
    #         self.ax.set_ylim(ax_params["ylim"])

    #     self.ax.set_ylabel("Compressibility (bits per edge)")
    #     self.ax.set_ylim(bottom=0.)

        # print(handle_a,handle_b)
        # self.ax.legend(handles=[handle_a,handle_b])
        # else:
        #     pass 


    def compressibility_A(self, width : float):
        panel     = self.panels   ["compressibility"]
        ax_params = self.ax_params["compressibility"]
        
        i = 0
        for dataset in self.datasets:
            G = self.get_graph(dataset)
            N,E = G.number_of_nodes(),G.number_of_edges()
            L_ref = panel["null_costs"]    [dataset[0]+"_"+dataset[1]] 

            nulls = list(panel["null_costs"][dataset[0]+"_"+dataset[1]].keys())
            baseModel,L_opt  = min(panel["codelengths"][dataset[0]+"_"+dataset[1]].items(),key=lambda x : x[1])
            nullModel,L0_opt = min(panel["null_costs" ][dataset[0]+"_"+dataset[1]].items(),key=lambda x : x[1])
            
            if L_opt < L0_opt:
                self.ax.bar(i,(L_ref[self.refModel]-L_opt) /E,width=width,color=self.params["model"][baseModel]["color"],label=self.params["model"]["planted_motif_model/"+baseModel]["text"],**ax_params["bar"])
                self.ax.bar(i,(L_ref[self.refModel]-L0_opt)/E,width=width,color=self.params["model"][nullModel]["color"],label=self.params["model"][nullModel]["text"],**ax_params["bar_null"])

                print(dataset,"absolute compression:",L_ref[self.refModel]-L_opt,", diff compression:", round(L0_opt - L_opt))
            else:
                self.ax.bar(i,(L_ref[self.refModel]-L0_opt)/E,width=width,color=self.params["model"][nullModel]["color"],label=self.params["model"][nullModel]["text"],**ax_params["bar_null"])
                # self.ax.bar(i,(L_ref[self.refModel]-L_opt) /E,color=self.params["model"][baseModel]["color"],label=self.params["model"]["planted_motif_model/"+baseModel]["text"],**ax_params["bar"])
                # print(dataset,"absolute compression:",round(L_ref[self.refModel]-L0_opt))   

            i += 1./len(self.datasets) 


    def compressibility_B(self, width : float):
        panel     = self.panels   ["compressibility"]
        ax_params = self.ax_params["compressibility"]

        i = 0
        for dataset in self.datasets:
            G = self.get_graph(dataset)
            N,E = G.number_of_nodes(),G.number_of_edges()
            L_ref = panel["null_costs"]    [dataset[0]+"_"+dataset[1]] 

            for baseModel,L in panel["codelengths"][dataset[0]+"_"+dataset[1]].items():
                if L != np.inf:
                    self.ax.scatter(i,(L_ref[self.refModel]-L)/E,color=self.params["model"][baseModel]["color"],label=self.params["model"]["planted_motif_model/"+baseModel]["text"],**ax_params["scatter"])
            for null,L0 in panel["null_costs"][dataset[0]+"_"+dataset[1]].items():
                if null != self.refModel:
                    self.ax.scatter(i,(L_ref[self.refModel]-L0)/E,color=self.params["model"][null]["color"],label=self.params["model"][null]["text"],**ax_params["scatter_null"])

            i += 1./len(self.datasets)




    def set_compressibility_panel(self, mode : str):
        ax_params = self.ax_params["compressibility"]
        self.fig,self.ax = plt.subplots()
        self.figurename = self.groupname + mode
        xticks    = self.get_xticks()
        self.ax.set_xticks(np.arange(len(self.datasets))/len(self.datasets),xticks)
    
        width = 0.1 
        # i = 0
        for dataset in self.datasets:
            G = self.get_graph(dataset)
            N,E = G.number_of_nodes(),G.number_of_edges()
            
            
            if mode == 'A': self.compressibility_A(width)
            if mode == 'B': self.compressibility_B(width)


            # nulls = list(panel["null_costs"][dataset[0]+"_"+dataset[1]].keys())
            # baseModel,L_opt  = min(panel["codelengths"][dataset[0]+"_"+dataset[1]].items(),key=lambda x : x[1])
            # nullModel,L0_opt = min(panel["null_costs" ][dataset[0]+"_"+dataset[1]].items(),key=lambda x : x[1])
            
            # if L_opt < L0_opt:
            #     self.ax.bar(i,(L_ref[self.refModel]-L_opt) /E,width=width,color=self.params["model"][baseModel]["color"],label=self.params["model"]["planted_motif_model/"+baseModel]["text"],**ax_params["bar"])
            #     self.ax.bar(i,(L_ref[self.refModel]-L0_opt)/E,width=width,color=self.params["model"][nullModel]["color"],label=self.params["model"][nullModel]["text"],**ax_params["bar_null"])

            #     print(dataset,"absolute compression:",L_ref[self.refModel]-L_opt,", diff compression:", round(L0_opt - L_opt))
            # else:
            #     self.ax.bar(i,(L_ref[self.refModel]-L0_opt)/E,width=width,color=self.params["model"][nullModel]["color"],label=self.params["model"][nullModel]["text"],**ax_params["bar_null"])
            #     # self.ax.bar(i,(L_ref[self.refModel]-L_opt) /E,color=self.params["model"][baseModel]["color"],label=self.params["model"]["planted_motif_model/"+baseModel]["text"],**ax_params["bar"])
            #     print(dataset,"absolute compression:",round(L_ref[self.refModel]-L0_opt))      

            # for baseModel,L in panel["codelengths"][dataset[0]+"_"+dataset[1]].items():
            #     if L != np.inf:
            #         self.ax.bar(i,(L_ref[self.refModel]-L)/E,color=self.params["model"][baseModel]["color"],**ax_params["bar"])
            # for null,L0 in panel["null_costs"][dataset[0]+"_"+dataset[1]].items():
            #     if null != self.refModel:
            #         self.ax.bar(i,(L_ref[self.refModel]-L0)/E,color=self.params["model"][baseModel]["color"],**ax_params["bar_null"])

            # baseModel,L = panel["codelengths"]   [dataset[0]+"_"+dataset[1]]
            # L_ref       = panel["null_costs"]    [dataset[0]+"_"+dataset[1]] 
            # null,L0_opt = min(panel["null_costs"][dataset[0]+"_"+dataset[1]].items(), key=lambda item : item[1])
        # for dictL,dictL_ref in zip(panel["codelengths"].values(),panel["null_costs"].values()):
            # for L,L_ref in zip(dictL.values(),dictL_ref.values()):
            
            # handle_a = plt.scatter([],[],**ax_params["scatter"],     label="Significant motifs")
            # handle_b = plt.scatter([],[],**ax_params["scatter_null"],label="No motifs")

            # if L < L0_opt:
            #     print(dataset,'N:',N,'E:',E,'theta: PMM + ',baseModel)
            #     self.ax.scatter(i,(L_ref[self.refModel]-L     )/E,**ax_params["scatter"])
            # else:
            #     print(dataset,'N:',N,'E:',E,'theta: Simple graph ',null)
            #     self.ax.scatter(i,(L_ref[self.refModel]-L0_opt)/E,**ax_params["scatter_null"])


            # i += 1./len(self.datasets)
       
        if "setp" in ax_params:
            labels = self.ax.get_xticklabels()
            plt.setp(labels,**ax_params["setp"])

        if "yscale" in ax_params:
            self.ax.set_yscale(ax_params["yscale"])

        if "ylim" in ax_params:
            self.ax.set_ylim(ax_params["ylim"])

        self.ax.set_ylabel("Compressibility (bits per edge)")
        self.ax.set_ylim([0.,2.3])
        # self.ax.set_xlim([0,1])

        handles, labels = self.ax.get_legend_handles_labels()
        unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
        self.ax.legend(*zip(*unique))
    # def set_edge_numbers_panel(self):
    #     xticks    = self.get_xticks()
    #     self.ax[1].set_xticks(np.arange(len(self.datasets)),xticks)
    #     ax_params = self.ax_params["edge_numbers"]
    #     if "setp" in ax_params:
    #         labels = self.ax[1].get_xticklabels()
    #         plt.setp(labels,**ax_params["setp"])

    #     edge_numbers = [ self.get_graph(dataset).number_of_edges() for dataset in self.datasets ]
    #     self.ax[1].scatter(np.arange(len(self.datasets)),edge_numbers,**ax_params["scatter"])
    #     self.ax[1].set_ylabel("#Directed connections")
            

    def get_compressibility_file(self):
        return self.panels["compressibility"]

    def set_panels(self):
        # pass
        plt.close('all')
        self.set_compressibility_panel('A')
        self.savefig()
        self.set_compressibility_panel('B')
        self.savefig()
        # self.set_edge_numbers_panel()
        
        