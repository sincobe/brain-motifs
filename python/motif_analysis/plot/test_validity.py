from motif_analysis.stat.frequency import FrequencyCollection 
from motif_analysis.stat.planted_motif_model import PMMCollection
from motif_analysis.stat.null_model_codelength.null_model_factory import NullModelFactory
from motif_analysis.plot.interface import PlotInterface
from statsmodels.stats.multitest   import multipletests 
from scipy.stats import norm

from typing import Tuple,List
import numpy as np
import matplotlib.pyplot as plt
import json
import networkx as nx

plt.style.use("python/styles/test_validity.mplstyle")

class TestValidity(PlotInterface):
    def __init__(self, dataset : Tuple[str], models : List[str], 
                nulls=["simple_erdos-renyi"],
                methodInference="planted_motif_model",
                methodDescription="bonferroni", 
                test_type="right-tail",
                alpha=1.e-2,
                **kwargs) :
        super().__init__(**kwargs)
        self.alpha      = alpha
        self.methodInference   = methodInference
        self.methodDescription = methodDescription
        self.test_type  = test_type
        self.dataset    = dataset
        self.models     = models
        self.nulls      = nulls
        self.figurename = "test_validity"
        self.ax_params  = json.load(open("python/ax_params/test_validity.json",'r'))

        if "process" in self.mode:
            self.collections_description = { model_gen : {model_rand : FrequencyCollection(self.dataset,model_rand,model_gen) for model_rand in self.models} for model_gen in self.models }
            self.collections_inference   = { model_gen : {baseModel  : PMMCollection      (self.dataset,baseModel,model_gen)  for baseModel  in self.models} for model_gen in self.models }

    def set_folderpath(self):
        self.folderpath = "figures/test_validity/"+self.dataset_subpath()

    def process(self):
        
        for model_gen in self.models:
            self.panels[model_gen] = { "planted_motif_model":{},"descriptive_stat":{} }

            for baseModel,null in zip(self.models,self.nulls):
                print("Model gen:",model_gen,", planted motif model with baseModel:",baseModel)
                pc = self.collections_inference[model_gen][baseModel]
                self.panels[model_gen]["planted_motif_model"][baseModel] = []
                i = 0
                while pc.move_to_next_generated_graph():
                    inference = pc.optimal_inference()
                    N_motifs  = len(inference.graphlet_set.keys()) if inference is not None else 0
                    try:
                        G = nx.read_edgelist(pc.finder.random_graph_path(),create_using=nx.DiGraph)
                        l0 = NullModelFactory().make(null,G).get()
                        output = {
                            "L":inference.L if inference is not None else "nan",
                            "L0":l0,
                            "N_motifs":N_motifs
                        }
                        self.panels[model_gen]["planted_motif_model"][baseModel].append(output)
                        print("idx gen graph:",i,"output:",output)
                        i+=1    
                    except:
                        pass
                     

            for model_rand in self.models:
                print("Model gen:",model_gen,", descriptive method with model rand:",model_rand)
                fc = self.collections_description[model_gen][model_rand]
                self.panels[model_gen]["descriptive_stat"][model_rand] = {"Zscores":[]}
                i = 0
                while(fc.move_to_next_generated_graph()):
                    try:
                        # pvals = np.array(list(fc.pvalues().values()))
                        # reject,pvals_corrected,alphacSidak,alphaBonf = multipletests(pvals,self.alpha,self.methodDescription)
                        # self.panels[model_gen]["descriptive_stat"]["N_rejected"][model_rand].append(int(reject.sum()))
                        self.panels[model_gen]["descriptive_stat"][model_rand]["Zscores"].append(list(fc.Zscores().values()))
                        print("idx gen graph:",i)
                        i+=1                        
                    except:
                        pass



    def set_false_positive_number_panel(self,type_="right-tail"):
        self.fig,self.ax = plt.subplots(ncols=len(self.models))
        self.ax[0].set_ylabel("Number of inferred motifs")
        xticks_partial = [self.params["model"][model]["text"] for model in self.models]
        for m,model_gen in enumerate(self.models): 
            pmm = self.panels[model_gen]["planted_motif_model"]
            t   = min([ len(pmm[baseModel]) for baseModel in self.models ])
            arr = []
            x   = np.zeros(t) 
            for r in range(t):
                y  = min([ pmm[baseModel][r] for baseModel in self.models],key=lambda p : p["L"] if p["L"] != "nan" else np.inf)
                y0 = min([ pmm[baseModel][r] for baseModel in self.models],key=lambda p : p["L0"])
                x[r] = y["N_motifs"] if  y["L"] != "nan" and y["L"] < y0["L0"] else 0 
            arr.append(x)


            for j,model_rand in enumerate(self.models):
                Z = self.panels[model_gen]["descriptive_stat"][model_rand]["Zscores"]
                w = np.zeros(len(Z),dtype=int)
                for k,ls_z in enumerate(Z):
                    if   type_ == "two-sided":
                        pvals = np.array([2.*min([norm.cdf(z),norm.cdf(-z)]) for z in ls_z])
                    elif type_ == "left-tail":
                        pvals = np.array([norm.cdf(z)                        for z in ls_z])
                    elif type_ == "right-tail":
                        pvals = np.array([norm.cdf(-z)                       for z in ls_z])
                    else:
                        print("Unvalid type.")
                        pvals = None
            
                    reject,pvals_corrected,alphacSidak,alphaBonf = multipletests(pvals,self.alpha,self.methodDescription)
                    w[k] = int(reject.sum())
                    print(model_gen,model_rand,"idx graph",k,", N_rejected:",w[k])

                arr.append(w)

            self.ax[m].boxplot(arr,positions=np.arange(len(self.models)+1))
            self.ax[m].set_title(self.params["model"][model_gen]["text"])
            self.ax[m].set_xticks(np.arange(len(self.models)+1),["MDL",*xticks_partial])
            if "setp" in self.ax_params:
                labels = self.ax[m].get_xticklabels()
                plt.setp(labels,**self.ax_params["setp"])

        self.figurename = "FP_number_"+type_

    def set_Zscore_distribution_panel(self):
 
        self.fig,self.ax = plt.subplots(nrows=len(self.models))
        xticks = [self.params["model"][model]["text"] for model in self.models]
        for m,model_gen in enumerate(self.models): 
            arr = []
            for j,model_rand in enumerate(self.models):
                Z = self.panels[model_gen]["descriptive_stat"][model_rand]["Zscores"]
                r = []
                for k,ls_z in enumerate(Z):
                    # ls_z = [ z for z in ls_z if z < 1e30 and z > 0]
                    ls_z = [ z for z in ls_z if z < 1e30 ]
                    print(model_gen,model_rand,"idx graph",k)
                    r = r + ls_z
                arr.append(r)
        
            self.ax[m].violinplot(arr,positions=np.arange(len(self.models)))
            # self.ax[m].set_yscale('log')
            self.ax[m].set_ylabel(self.params["model"][model_gen]["text"])
            self.ax[m].set_ylim(top=3,bottom=-3)
            if m != len(self.models)-1:
                self.ax[m].set_xticks(np.arange(len(self.models)),["" for rep in np.arange(len(self.models))])
            else:
                self.ax[m].set_xticks(np.arange(len(self.models)),xticks)
            if "setp" in self.ax_params:
                labels = self.ax[m].get_xticklabels()
                plt.setp(labels,**self.ax_params["setp"])

        self.figurename = "Zscore_dist_zoomed"

    def set_Zscore_categories_panel(self):
        self.fig,self.ax = plt.subplots(nrows=len(self.models))
        xticks = [self.params["model"][model]["text"] for model in self.models]
        width = 1.
        for m,model_gen in enumerate(self.models): 
            pos = 0
            for j,model_rand in enumerate(self.models):
                Z = self.panels[model_gen]["descriptive_stat"][model_rand]["Zscores"]
                N_pos,N_neg,N_inf,N_tot = 0,0,0,0
                for k,ls_z in enumerate(Z):
                    N_inf += int(np.array([ z > 1e30           for z in ls_z]).sum())
                    N_pos += int(np.array([ z > 5 and z < 1e30 for z in ls_z]).sum())
                    N_neg += int(np.array([ z < -5             for z in ls_z]).sum())
                    N_tot += len(ls_z)
                    print(model_gen,model_rand,"idx graph",k)
              
            
                self.ax[m].bar(x=pos-width,height=N_pos/N_tot,color='blue' )
                self.ax[m].bar(x=pos,      height=N_neg/N_tot,color='red'  )
                self.ax[m].bar(x=pos+width,height=N_inf/N_tot,color='black')
                pos += 4*width


            self.ax[m].set_ylabel(self.params["model"][model_gen]["text"])
            self.ax[m].set_ylim(bottom=0,top=1)
            if m != len(self.models)-1:
                self.ax[m].set_xticks(np.arange(0,4*len(self.models),4),["" for rep in np.arange(len(self.models))])
            else:
                self.ax[m].set_xticks(np.arange(0,4*len(self.models),4),xticks)
            if "setp" in self.ax_params:
                labels = self.ax[m].get_xticklabels()
                plt.setp(labels,**self.ax_params["setp"])

        self.figurename = "Zscore_categories_"
    # def set_motif_detection_panel(self):
    #     panel     = self.panels   ["motif_detection"]
    #     ax_params = self.ax_params["motif_detection"]
    #     for i,model in enumerate(self.models):
    #         pvals_dict = panel[model]
    #         pvals = np.array(list(pvals_dict.values()))
    #         reject,pvals_corrected,alphacSidak,alphaBonf = multipletests(pvals,self.alpha,self.method)
    #         self.ax.bar(i,reject.sum(),width=ax_params["width"],alpha=ax_params["opacity"],color=self.params["model"][model]["color"])

    #     self.ax.set_xticks(np.arange(len(self.models)),[self.params["model"][model]["text"] for model in self.models])
    #     if "setp" in ax_params:
    #         labels = self.ax.get_xticklabels()
    #         plt.setp(labels,**ax_params["setp"])
    #     self.ax.set_ylabel("#Motifs")


        
    def make(self):
        # self.set_Zscore_categories_panel()
        # self.savefig()
        
        # self.set_Zscore_distribution_panel()
        # self.savefig()
        for type_ in ["right-tail"]:
            self.set_false_positive_number_panel(type_=type_)
            self.savefig()
        


                    
    