from motif_analysis.plot.interface                 import PlotInterface
from motif_analysis.graphlet.graphlet_distribution import GraphletDistribution
from motif_analysis.graphlet.graphlet              import *
from typing import List
import networkx as nx
from networkx.algorithms import isomorphism
from motif_analysis.path.inference import *
from motif_analysis.stat.planted_motif_model import PerfPMMCollection
import json
from math import ceil


plt.style.use("python/styles/test_performance.mplstyle")

class TestPerformance(PlotInterface):
    def __init__(self, N : int, densities : List[float], graphlets : List[str], baseModels : List[str],  **kwargs):
        super().__init__(nrows=len(graphlets),ncols=len(densities)+1,gridspec_kw={"width_ratios":[0.3,*list(np.ones(len(densities)))]},**kwargs)
        self.N         = N 
        self.densities = densities
        self.baseModels = baseModels
        self.figurename = "test_performance"
        self.figurenameBase = ""
        self.graphlets = graphlets
        self.ax_params = json.load(open("python/ax_params/test_performance.json",'r'))
        

        if "process" in self.mode:
            self.gDist_ref = GraphletDistribution(("drosophila_larva","mushroom_body_right"))
            for density in densities:
                self.panels[density] = {}
                for graphlet in self.graphlets:
                    self.panels[density][graphlet] = {}
                    N_supernodes_ls = get_list_of_supernodes_for_PerfPMM(N,density,graphlet)
                    for N_supernodes in N_supernodes_ls:
                        p = get_folderpath_for_perfPMM(N,density,graphlet) / Path("number_of_supernodes_"+str(N_supernodes))
                        self.panels[density][graphlet][N_supernodes] = []
                        for graphDir in p.iterdir():               
                            # try:       
                            #  
                            graphletItem = (graphlet,self.graphlet_from_name(graphlet))
                            res  = { baseModel : PerfPMMCollection(N,density,graphletItem,graphDir.name,N_supernodes,baseModel).output() for baseModel in self.baseModels }
                            self.add_other_graphlet_label_measure(res,graphlet)
                            self.panels[density][graphlet][N_supernodes].append(res)
                            # except:
                                # print("Missing inference with parameters: N_supernodes:",N_supernodes,", density:",density,", graph name:", graphDir.name)

    def set_folderpath(self):
        self.folderpath = "figures/test_performance/number_of_nodes_"+str(self.N)+"/"
    
    def graphlet_from_name(self,graphletName) -> str:
        gtrie_label = self.params["graphlets"][graphletName]
        g = get_nx_graph_from_gtrie_label(gtrie_label)
        graphlet = None
        for label,graphlet_test in self.gDist_ref.j["graphlets"].items():
            if nx.is_isomorphic(g,get_nx_graph_from_gtrie_label(graphlet_test["gtrie_label"])):
                graphlet = Graphlet(label,graphlet_test) 
                break

        return graphlet
    
    def add_other_graphlet_label_measure(self, res : Dict, graphlet : str):
        
        for baseModel in self.baseModels:
            labels = res[baseModel]["other_graphlet_labels"]
            g_ls   = [ self.gDist_ref.get_graphlet(label) for label in labels ]
            g0     = self.graphlet_from_name(graphlet)
            print(sum([int(isomorphism.DiGraphMatcher(g0.g,g.g).subgraph_is_isomorphic()) for g in g_ls]))
            res[baseModel]["other_graphlet_labels"] = sum([int(isomorphism.DiGraphMatcher(g0.g,g.g).subgraph_is_isomorphic()) for g in g_ls])
    
    def set_figure(self,measure : str, baseModel="erdos_renyi"):
        self.fig,self.ax = plt.subplots(nrows=len(self.graphlets),ncols=len(self.densities)+1,gridspec_kw={"width_ratios":[0.3,*list(np.ones(len(self.densities)))]})
        step = self.ax_params["xticks"]["step"]

        
        for d,density in enumerate(self.densities):
            self.ax[0,d+1].set_title("$\\rho=$"+str(density))

            if measure == "TP_rate": 
                self.ax[0,0].set_ylabel("Discovery rate")
                # self.ax[0,d+1].set_title("Discovery rate")
                self.ax[len(self.graphlets)-1,d+1].set_xlabel("Number of subgraphs placed")

            if measure == "supernode_score": 
                self.ax[0,0].set_ylabel("Number inferred")
                # self.ax[0,d+1].set_title("Number inferred")
                self.ax[len(self.graphlets)-1,d+1].set_xlabel("Number of subgraphs placed")

            for g,graphlet in enumerate(self.graphlets):
                N_supernodes_ls = np.array(sorted(list(self.panels[str(density)][graphlet].keys() - {"0"}),key=lambda x : int(x)))
                # N_supernodes_ls = N_supernodes_ls[:int(ceil(len(N_supernodes_ls)/2)) ]
                # N_supernodes_max = max(N_supernodes_ls,key=lambda n : int(n))
                N_supernodes_max = N_supernodes_ls[-1]
                avg_max = 1.
                for N_supernodes in N_supernodes_ls:
                    n_gen = len(self.panels[str(density)][graphlet][N_supernodes])
                    avg   = sum([float(res[baseModel][measure])/n_gen for res in self.panels[str(density)][graphlet][N_supernodes]])
                    self.ax[g,d+1].scatter(int(N_supernodes),avg,color="k")
                    avg_max = max([avg_max,avg])
                if measure == "TP_rate": 
                    self.ax[g,d+1].set_ylim([-0.1,1.1])

                elif measure == "supernode_score": 
                    # self.ax[g,d+1].set_ylim([-1.,60])
                    self.ax[g,d+1].set_ylim([-1.,21])

                elif measure == "FP_number" and avg_max < 2:
                    self.ax[g,d+1].set_ylim(bottom=-0.1,top=avg_max+0.1)
                    
                    avg_2 = sum([float(res[baseModel]["other_graphlet_labels"])/n_gen for res in self.panels[str(density)][graphlet][N_supernodes]])
                    self.ax[g,d+1].scatter(int(N_supernodes),avg_2,color="blue")
                    
                xticks = np.arange(start=0,stop=int(N_supernodes_max) + 1 + int(N_supernodes_max)%step,step=step)
                self.ax[g,d+1].set_xticks(xticks,xticks)

        for g,graphlet in enumerate(self.graphlets):
            graph = get_nx_graph_from_gtrie_label(self.params["graphlets"][graphlet])
            pos   = nx.nx_pydot.graphviz_layout(graph)
            nx.draw(graph,pos,**self.ax_params["networkx_draw"],ax=self.ax[g,0])
            self.ax[g,0].axis('equal')


        self.figurename = self.figurenameBase+baseModel+"_"+measure 


    def make(self):
        measures = ["other_graphlet_labels","TP_rate","supernode_score"]
        for measure in measures:
            for baseModel in self.baseModels:
                
                self.set_figure(measure,baseModel)
                self.savefig()

                



   