from typing import List,Dict
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from motif_analysis.path.graphlet_census import GraphletMeasureFinder

# sym_file = GraphletMeasureFinder().symmetry_file()

def get_nx_graph_from_gtrie_label(gtrie_label):
    n = int(0.5 * (1 + np.sqrt(1 + 4 * len(gtrie_label))))
    e = []
    l = 0
    for d in range(1,n):
        for i in range(d):
            if gtrie_label[l] == '1':
                e.append((i,d))
            l += 1
            if gtrie_label[l] == '1':
                e.append((d,i))
            l += 1

    return nx.from_edgelist(e,create_using=nx.DiGraph()) 

class Graphlet():
    def __init__(self,_label : str,_j : Dict):
        self.label      = _label 
        self.attributes = _j 
        self.set_nx_graph()

    def set_nx_graph(self):
        gtrie_label = self.attributes["gtrie_label"]
        self.g = get_nx_graph_from_gtrie_label(gtrie_label)

    def get_nx_graph(self) -> nx.DiGraph:
        return self.g

    def set_density(self):
        self.attributes["density"] = nx.density(self.g)

    def set_number_of_simple_cycles(self):
        self.attributes["number_of_simple_cycles"] = len(list(nx.simple_cycles(self.g)))

    def set_reciprocity(self):
        self.attributes["reciprocity"] = nx.reciprocity(self.g)

    def set_measure(self,measure : str):
        if measure not in self.attributes:
            if measure == "density":
                self.set_density()
            elif measure == "number_of_simple_cycles":
                self.set_number_of_simple_cycles()
            elif measure == "reciprocity":
                self.set_reciprocity()
            elif measure == "symmetry_index" or measure == "orbit_polynomial_root":
                self.set_sym_measure(measure)
            else:
                print("Unknown measure")
        else:
            pass

    # def set_sym_measure(self, measure : str):
    #     self.attributes[measure] = sym_file[self.label][measure]
        
