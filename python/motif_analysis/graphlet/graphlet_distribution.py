from typing import Tuple, Dict, List, Set
from motif_analysis.path.graphlet_census import GCFinder
from motif_analysis.graphlet.graphlet import Graphlet 
import matplotlib.pyplot as plt
import json

class GraphletDistribution():
    def __init__(self, dataset : Tuple[str], absolute_path=None):
        self.j         = GCFinder(dataset).census_file() if absolute_path is None else json.load(open(absolute_path,'r'))
        self.graphlets = {}
        self.tot = self.j["total_number_of_subgraphs"]

    def merge_with_map(self, map : Dict):
        for label,g in map.items():
            if label not in self.j["graphlets"]:
                self.j["graphlets"][label] = {
                                                 "gtrie_label"            :g["gtrie_label"],
                                                 "automorphism_group_size":g["automorphism_group_size"],
                                                 "number_of_subgraphs"    :0
                                             }

    def set_graphlets(self, label_ls : List[str]):
        for label in label_ls:
            self.graphlets[label] = Graphlet(label,self.j["graphlets"][label])

    def set_all_graphlets(self):
        for label,graphlet in self.j["graphlets"].items():
            self.graphlets[label] = Graphlet(label,self.j["graphlets"][label])

    def get_graphlet(self, label :str) -> Graphlet:
        return Graphlet(label,self.j["graphlets"][label])

    def graphlet_labels(self) -> Set[str]:
        return self.j["graphlets"].keys()

    def graphlet_frequency(self, label : str):
        return self.j["graphlets"][label]["number_of_subgraphs"] if label in self.j["graphlets"] else 0
    
    def graphlet_concentration(self, label : str):
        return self.graphlet_frequency(label)/self.tot 
    
    def graphlet_gtrie_label(self, label : str) -> str:
        return self.j["graphlets"][label]["gtrie_label"]
    
    def graphlet_automorphism_group_size(self, label :str) -> int:
        return self.j["graphlets"][label]["automorphism_group_size"]
    
    def number_of_graphlets(self):
        return len(self.j["graphlets"])
    



    

                