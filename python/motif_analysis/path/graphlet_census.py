from motif_analysis.path.finder import * 
from pathlib import Path
from typing import Dict

class GCFinder(PathFinder):

    def __init__(self,dataset : Tuple[str]):
        super().__init__()
        self.path =  self.algo_folder("graphlet_census") \
                   + self.output_subpath_data(dataset)  \
                   + "gdist.json"
        
        self.p = Path(self.path)

    def census_file(self) -> Dict:
        return json.load(open(self.path,'r'))
    
class GraphletMeasureFinder(PathFinder):
    def __init__(self,dataset=("drosophila_larva","mushroom_body_right")):
        super().__init__()
        self.path =  self.algo_folder("graphlet_census") \
                   + self.output_subpath_data(dataset)  
        
        self.p = Path(self.path)

    def symmetry_file(self) -> Dict:
        return json.load(open(self.path+"symmetry_measures.json",'r'))
