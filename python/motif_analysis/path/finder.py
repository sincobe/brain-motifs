import json
from typing import Tuple, Dict, List

class PathFinder():
    
    def __init__(self):
        self.param = json.load(open("param/path.json",'r'))
        self.path  = "" 

    def output_root(self) -> str:
        return self.param["base"] + self.param["output"]["base"] 
    
    def input_root(self) -> str:
        return self.param["base"] + self.param["input"]["base"]
    
    def algo_folder(self, algo : str) -> str:
        return self.output_root() + self.param["output"]["algorithm"][algo]
    
    def algo_subfolder(self, algo : str) -> str:
        return self.param["output"]["algorithm"][algo]
    
   

    def input_network_path(self, dataset : Tuple[str]) -> str:
        return self.input_root() \
             + self.param["input"]["dataset"]["base"] \
             + self.param["input"]["dataset"]["category"]   [ dataset[0] ] \
             + self.param["input"]["dataset"]["subcategory"][ dataset[1] ] 
    
    def output_subpath_data(self, dataset : Tuple[str]) -> str:
        return self.param["output"]["dataset"]["category"]   [ dataset[0] ] \
             + self.param["output"]["dataset"]["subcategory"][ dataset[1] ]
    
    def add_subfolder(self, subfolder : str):
        self.path += subfolder + "/"

    def subfolder(self, subfolder : str) -> str:
        return subfolder + "/"
