from motif_analysis.path.randomized_graph import * 

class GCRandFinder(PathFinder):

    def __init__(self, dataset : Tuple[str], model : str):
        super().__init__()
        self.finder_rand_graph = RandomizedGraphFinder(dataset,model)
        self.p_ls = [ Path(self.algo_folder("graphlet_census") + subfolder + "gdist.json") 
                     for subfolder in self.finder_rand_graph.get_subpath_folders() ]

    def census_files(self) -> List[Dict]:
        for p in self.p_ls: 
            yield json.load(open(str(p.absolute()) ,'r'))

    def number_of_random_networks(self) -> int:
        return len(self.p_ls)
    
class GCRandRandFinder(PathFinder):

    def __init__(self, dataset : Tuple[str], model_gen : str, model_rand : str):
        super().__init__()
        self.dataset   = dataset
        self.model_gen = model_gen
        self.finder_rand_graph = RandomizedRandGraphFinder(dataset,model_gen,model_rand)

    def paths(self) -> Path:
        for subfolder in self.finder_rand_graph.subpath_folders_of_graph_gen():
            yield Path(self.algo_folder("graphlet_census") + subfolder + "gdist.json") 

    def census_files(self) -> Dict:
        for p in self.paths(): 
            yield json.load(open(str(p.absolute()) ,'r'))

    def path_to_subg_census_of_generated_graph(self):
        if self.generated_graph() is not None:
            return self.algo_folder("graphlet_census")    + self.algo_subfolder("graph_randomization") + \
                   self.output_subpath_data(self.dataset) + self.model_gen + "/" + self.generated_graph().name + "/gdist.json" 
        else:
            return None

    def number_of_random_networks(self) -> int:
        if self.finder_rand_graph.g is not None:
            return self.finder_rand_graph.number_of_random_networks()
        else:
            return None
        
    def move_to_next_generated_graph(self):
        self.finder_rand_graph.move_to_next_generated_graph()
    
    def generated_graph(self) -> Path:
        return self.finder_rand_graph.g
    
                     
        
        

    