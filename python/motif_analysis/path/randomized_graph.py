from motif_analysis.path.finder import * 
from pathlib import Path
from typing import List

class RandomizedGraphFinder(PathFinder):

    def __init__(self, dataset : Tuple[str], model : str):
        super().__init__()
        self.path = (self.algo_folder("graph_randomization") 
                  +  self.output_subpath_data(dataset)
                  +  self.subfolder(model))        
        self.p = Path(self.path)

    # def census_file(self) -> Dict:
    #     return json.load(open(self.path,'r'))
    
    def list(self) -> str:
        for file in self.p.iterdir():
            yield str(file.absolute())
    
    def get_subpath_folders(self) -> List[str]:
        return [ el.removeprefix(self.output_root()).removesuffix(".txt") + "/" for el in self.list() ]
    
class RandomizedRandGraphFinder(PathFinder):

    def __init__(self, dataset : Tuple[str], model_gen : str, model_rand : str):
        super().__init__()
        self.model_rand = model_rand
        self.path_root  = (self.algo_folder("randomized_random_graphs") 
                        +  self.output_subpath_data(dataset)
                        +  self.subfolder(model_gen))        
        self.p_root     = Path(self.path_root)
        self.generator  = self.p_root.iterdir()

    def list_rand(self) -> str:
        for file in (self.g / self.model_rand).iterdir():
            yield str(file.absolute())
    
    def subpath_folders_of_graph_gen(self) -> str:
        for el in self.list_rand(): 
            yield el.removeprefix(self.output_root()).removesuffix(".txt") + "/" 
    
    def move_to_next_generated_graph(self):
        try:
            self.g =  next(self.generator)
            # print(self.g.name)
        except:
            self.g = None
            # print("END")
        

    def number_of_random_networks(self) -> int:
        return sum([file.suffix == ".txt" for file in (self.g / self.model_rand).iterdir()])
   