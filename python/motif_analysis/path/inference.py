from motif_analysis.path.finder import * 
from pathlib import Path
from typing import List

class PMMFinder(PathFinder):

    def __init__(self,dataset : Tuple[str], baseModel : str, minibatch_size = None , model_gen = None):
        super().__init__()
        self.baseModel = baseModel
        if model_gen is None:
            self.path =  self.algo_folder("graph_encoding") \
                       + self.output_subpath_data(dataset)  
            self.add_subfolder("planted_motif_model") 
            self.add_subfolder(baseModel)
            if minibatch_size is not None: self.add_subfolder("minibatch_size_"+str(minibatch_size))
            self.p = Path(self.path)
        else: 
            self.path =  self.algo_folder("graph_encoding") \
                       + self.algo_subfolder("graph_randomization") \
                       + self.output_subpath_data(dataset)
            self.add_subfolder(model_gen)
            self.p_root = Path(self.path)
            self.generator = self.p_root.iterdir()

    def inference_files(self) -> List[str]:
        ls = []
        for file in self.p.iterdir():
            if file.suffix == '.json':
                ls.append(str(file.absolute()))
        return ls 
    
    def move_to_next_generated_graph(self) -> bool:
        try:
            self.g =  next(self.generator)
            self.p = self.g / Path("planted_motif_model/"+self.baseModel)
            # print(self.g.name)
            return True
        except:
            self.g = None
            # print("END")  
            return False
    
    def random_graph_path(self) -> str:
        return self.output_root()+str(self.g).removeprefix(self.algo_folder("graph_encoding"))+".txt"



def get_folderpath_for_perfPMM(N : int, density : float, graphlet : str) -> Path:
    finder = PathFinder()
    return Path( finder.algo_folder   ("graph_encoding")
            + finder.algo_subfolder("test_non_null")  
            + graphlet
            + "/number_of_nodes_"+ str(N) 
            + "/number_of_edges_"+ str(int(density * N**2)) )
    

def get_list_of_supernodes_for_PerfPMM(N : int, density : float, graphlet : str) -> List[int]:
    p = get_folderpath_for_perfPMM(N,density,graphlet)
    return [ int(f.name.removeprefix("number_of_supernodes_")) for f in p.iterdir() ]

class PerfPMMFinder(PathFinder):
    def __init__(self, N : int, density : float, graphlet : str, N_supernodes : int, graphName : str, baseModel : str):
        super().__init__()
        self.path = (self.algo_folder   ("graph_encoding")
                  +  self.algo_subfolder("test_non_null")  
                  +  graphlet
                  +  "/number_of_nodes_"      + str(N) 
                  +  "/number_of_edges_"      + str(int(density * N**2)) 
                  +  "/number_of_supernodes_" + str(N_supernodes)
                  +  "/"+graphName
                  +  "/planted_motif_model/"+baseModel)
 
        self.p    =  Path(self.path) 

    def inference_files(self) -> List[str]:
        ls = []
        for file in self.p.iterdir():
            if file.suffix == '.json':
                ls.append(str(file.absolute()))
        return ls