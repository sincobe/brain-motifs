from motif_analysis.path.graphlet_census_rand      import GCRandFinder, GCRandRandFinder 
from motif_analysis.graphlet.graphlet_distribution import GraphletDistribution
from typing import Tuple,Dict
import numpy as np
from scipy.stats import norm


class FrequencyCollection():
    def __init__(self, dataset : Tuple[str], model : str, model_gen = None):
        
        self.finder      = GCRandFinder        (dataset,model) if model_gen is None else GCRandRandFinder(dataset,model_gen,model)
        self.gdist_ref   = GraphletDistribution(dataset)      
        self.labels      = self.gdist_ref.graphlet_labels()
        
    def number_of_random_networks(self) -> int:
        return self.finder.number_of_random_networks()

    def frequency_dicts(self) -> Dict[str,int]:
        for census_file in self.finder.census_files():
            if "graphlets" not in census_file: print(census_file)
            self.labels = self.labels | census_file["graphlets"].keys()
            yield { label : g["number_of_subgraphs"] for label,g in census_file["graphlets"].items() }

    def frequency_stat(self) -> Dict[str,Tuple[float]]:
        f = { label : np.zeros(self.number_of_random_networks()) for label in self.labels}
        for i,frequency_dict in enumerate(self.frequency_dicts()):
            for (label,n) in frequency_dict.items():
                if label not in f:
                    f[label] = np.zeros(self.number_of_random_networks())
                f[label][i] += n
        return {label : (np.mean(n),np.std(n,ddof=1)) for label,n in f.items()}.items()
    
    def Zscores(self) -> Dict[str,float]:
        return {label : (self.gdist_ref.graphlet_frequency(label) - stat[0])/(stat[1]+1e-32)
                for label,stat in self.frequency_stat()} 

    def pvalues(self,type="right-tail") -> Dict[str,float]:
        if type == "two-sided":
            return { label : 2.*min([norm.cdf(z),1.-norm.cdf(z)]) for label,z in self.Zscores().items() }
        elif type == "left-tail":
            return { label : norm.cdf(z)                          for label,z in self.Zscores().items() }
        elif type == "right-tail":
            return { label : 1.-norm.cdf(z)                       for label,z in self.Zscores().items() }
        else:
            print("Unvalid pvalues.")
            return None
    
    def move_to_next_generated_graph(self) -> bool:
        self.finder.move_to_next_generated_graph()
        if self.finder.generated_graph() is not None:
            self.gdist_ref = GraphletDistribution(self.finder.dataset,self.finder.path_to_subg_census_of_generated_graph()) 
            self.labels    = self.gdist_ref.graphlet_labels()
            return True
        else:
            return False


