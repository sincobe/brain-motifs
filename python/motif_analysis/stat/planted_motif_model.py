from motif_analysis.inference.planted_motif_model import PlantedMotifModel
from motif_analysis.stat.inference_collection     import *
from motif_analysis.path.inference                import *
from typing import Tuple,Set
from motif_analysis.graphlet.graphlet import Graphlet



class PMMCollection(Collection):
    def __init__(self,dataset : Tuple[str], baseModel : str, minibatch_size = None, model_gen = None):
        self.dataset    = dataset 
        self.baseModel  = baseModel
        self.model_gen = model_gen   
        self.minibatch_size = minibatch_size
        super().__init__() if model_gen is None else super().__init__(skip_inferences=True)

    def set_finder(self):
        self.finder = PMMFinder(self.dataset,self.baseModel,self.minibatch_size,self.model_gen)

    def set_inferences(self):
        self.inferences = []
        for filename in self.finder.inference_files():
            # print(filename)
            try:
                model = PlantedMotifModel(filename) 
                if model.sucess: self.inferences.append(model)  
            except:
                print(filename, "could not be opened")

    def reference_cost(self,model="reference_model") -> float:
        return self.inferences[0].reference_cost(model=model)

    def optimal_inference(self) -> PlantedMotifModel:
        return min(self.inferences, key=lambda x : x.L) if len(self.inferences) else None
    
    def move_to_next_generated_graph(self) -> bool:
        self.finder.move_to_next_generated_graph()
        if self.finder.g is not None: 
            self.set_inferences()
            return True
        else: 
            self.inferences = []
            return False
        
        

class PerfPMMCollection(Collection):
    def __init__(self, N : int, density : float, graphlet : Tuple[str,Graphlet], graphName : str, N_supernodes : int, baseModel="erdos-renyi"):
        self.N = N 
        self.density = density 
        self.graphlet = graphlet 
        self.N_supernodes = N_supernodes
        self.graphName = graphName 
        self.baseModel = baseModel
        super().__init__()
        self.inference = self.optimal_inference()

    def set_finder(self):
        self.finder = PerfPMMFinder(self.N,self.density,self.graphlet[0],self.N_supernodes,self.graphName,self.baseModel)

    def set_inferences(self):
        self.inferences = [ ]
        for filename in self.finder.inference_files():
            model = PlantedMotifModel(filename) 
 
            self.inferences.append(model)  

    def reference_cost(self,model="reference_model") -> float:
        return self.inferences[0].reference_cost(model=model)

    def optimal_inference(self) -> PlantedMotifModel:
        return min(self.inferences, key=lambda x : x.L) if len(self.inferences) else None
    

    def TP_rate(self) -> bool:
        return self.graphlet[1].label in self.inference.graphlet_set if self.inference is not None else False
    
    def FP_number(self) -> int:
        return len(self.inference.graphlet_set.keys() - {self.graphlet[1].label}) if self.inference is not None else 0
    
    def supernode_score(self) -> float:
        return  self.inference.graphlet_set[self.graphlet[1].label] if self.graphlet[1].label in self.inference.graphlet_set else 0
    
    def other_graphlet_labels(self) -> Set[str]:
        return self.inference.graphlet_set.keys() - {self.graphlet[1].label} if self.inference is not None else []
    
    def motif_probability(self) -> bool:
        return bool(len(self.inference.graphlet_set.keys())) if self.inference is not None else False

    def output(self) -> Dict:
        if self.inference is not None:
            return {
                "TP_rate":  self.TP_rate(),
                "FP_number":self.FP_number(),
                "other_graphlet_labels":self.other_graphlet_labels(),
                "N_supernodes" : self.N_supernodes,
                "supernode_score":self.supernode_score(),
                "codelength":self.inference.L,
                "motif_probability":self.motif_probability()
            }
        else: 
            return {
                "TP_rate":        0,
                "FP_number":      0,
                "other_graphlet_labels":self.other_graphlet_labels(),
                "N_supernodes" : self.N_supernodes,
                "supernode_score":0,
                "codelength":self.reference_cost(),
                "motif_probability":0
            }