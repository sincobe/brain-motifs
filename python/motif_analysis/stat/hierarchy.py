class Node():
    def __init__(self,name : str,parent=None):
        self.name     = name
        self.parent   = parent
        self.children = []

    def add_child(self,hierarchy, child : str):
        self.children.append(child)
        hierarchy.models[child] = Node(child,self.name)

    def has_parent(self,parent):
        return self.parent == parent
    
    def has_child(self,child):
        return child in self.children

class NullHierarchy():

    def __init__(self):
        self.models = { "root" : Node("root") }
        
    def extend(self,parent,new_model):
        self.models[parent].add_child( self,new_model )

    def greater_than(self, A : str, B : str) -> bool:
        if A == "root":
            return False
        elif A == B:
            return False 
        elif self.models[B].has_parent(A):
            return True
        else:
            if len(self.models[A].children):
                return sum([self.greater_than(child_A,B) for child_A in self.models[A].children]) 
            else:
                return False

