from motif_analysis.stat.null_model_codelength.null_model import *


class SimpleReciprocalErdosRenyi(NullModel):
    def __init__(self, name : str):
        super().__init__(name)
    

    def set_params(self, G : nx.DiGraph):
        self.N = G.number_of_nodes()
        self.E = G.number_of_edges()
        A      = nx.adjacency_matrix(G)
        self.M = sum( [(A[i,:] * A[:,i]).sum() for i in range(self.N)] ) / 2

    def set_codelength(self):
        self.L = (log_binomial(self.N * (self.N-1) / 2, self.M) + log_binomial(self.N * (self.N-1) / 2 - self.M, self.E - 2*self.M) + self.E - 2*self.M + 
                  self.positive_integer_encoding(self.N+1) +
                  self.positive_integer_encoding(self.E+1))

