from motif_analysis.stat.null_model_codelength.null_model import *

class BianconiReciprocalConfiguration(NullModel):
    def __init__(self,name):
        super().__init__(name)

    def set_params(self, G : nx.DiGraph):
        self.E                     = G.number_of_edges()
        in_degree_seq              = np.array([j for (i,j) in G.in_degree  ])
        out_degree_seq             = np.array([l for (k,l) in G.out_degree ])
        A                          = nx.adjacency_matrix(G)
        if A[A > 1].sum() : print("Bianconi recipr. conf.: reading a multi- instead of simple graph.")
        self.mutual_degree_seq     = np.array([ (A[i,:] * A[:,i]).sum() for i in range(G.number_of_nodes()) ]) 
        self.single_in_degree_seq  = in_degree_seq  - self.mutual_degree_seq 
        self.single_out_degree_seq = out_degree_seq - self.mutual_degree_seq
        self.M                     = 0.5 * self.mutual_degree_seq.sum()

    def set_codelength(self):
        cost_multigraph = (loggamma(self.E - 2*self.M + 1) + loggamma(self.M + 1) + self.M * np.log(2)  - 
                           loggamma(self.single_in_degree_seq  + 1).sum() - 
                           loggamma(self.single_out_degree_seq + 1).sum() -  
                           loggamma(self.mutual_degree_seq     + 1).sum())/np.log(2)
        
        Psi = ((self.mutual_degree_seq**2).sum()**2 / self.mutual_degree_seq.sum()**2 / 2 +  
              (self.single_in_degree_seq**2).sum() * (self.single_out_degree_seq**2).sum() / (self.single_in_degree_seq.sum() * self.single_out_degree_seq.sum()) +  
              (self.single_in_degree_seq * self.single_out_degree_seq).sum()**2 / (self.single_in_degree_seq.sum() * self.single_out_degree_seq.sum()) +  
              (self.mutual_degree_seq * self.single_in_degree_seq).sum() * (self.mutual_degree_seq * self.single_out_degree_seq).sum() / (self.mutual_degree_seq.sum() * self.single_in_degree_seq.sum()))

        self.L = (cost_multigraph  - Psi/2/np.log(2) +  
                  self.integer_sequence_encoding(self.single_in_degree_seq)  + 
                  self.integer_sequence_encoding(self.single_out_degree_seq) + 
                  self.integer_sequence_encoding(self.mutual_degree_seq))
