
from motif_analysis.stat.null_model_codelength.null_model import *
from motif_analysis.stat.null_model_codelength.bianconi_configuration import BianconiConfiguration
from motif_analysis.stat.null_model_codelength.bianconi_reciprocal_configuration import BianconiReciprocalConfiguration
from motif_analysis.stat.null_model_codelength.simple_erdos_renyi import SimpleErdosRenyi
from motif_analysis.stat.null_model_codelength.simple_reciprocal_erdos_renyi import SimpleReciprocalErdosRenyi

class NullModelFactory():
    def make(self, name : str, G : nx.DiGraph) -> NullModel:

        if name == "simple_erdos-renyi":
            model = SimpleErdosRenyi(name)
        elif name == "simple_reciprocal_erdos-renyi":
            model = SimpleReciprocalErdosRenyi(name)
        elif name == "bianconi_configuration":
            model = BianconiConfiguration(name)
        elif name == "bianconi_reciprocal_configuration":
            model = BianconiReciprocalConfiguration(name)

        model.set_params(G)
        model.set_codelength()
        return model
