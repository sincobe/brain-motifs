import numpy as np
from scipy.special import loggamma
import networkx as nx 

def log_binomial(n : float, k : float):
    return (loggamma(n + 1) - loggamma(n - k + 1) - loggamma(k + 1))/np.log(2)

class NullModel():
    def __init__(self,name):
        self.name   = name
        self.L      = 0
        self.params = {}

    def set_codelength(self):
        pass

    def set_params(self, G : nx.DiGraph):
        pass

    def positive_integer_encoding(self, n : int):
        return np.log2(n * (n + 1))

    def integer_sequence_encoding(self, k : np.array, prior = "dirichlet_jeffreys"):
        if prior   == "dirichlet_jeffreys":
            l = 0.5
        elif prior == "dirichlet_uniform":
            l = 1.

        k_min  = np.min(k)
        k_max  = np.max(k)
        Lambda = l * (k_max - k_min + 1)
        dist   = { i : np.where(k == i,1,0).sum() for i in set(k)}
        return (loggamma(np.size(k) + Lambda) - loggamma(Lambda) + len(dist) * loggamma(l) - loggamma(np.array(list(dist.values()))+l).sum())/np.log(2) \
            +   self.positive_integer_encoding(k_max+1) \
            +   self.positive_integer_encoding(k_min+1)



        

    def get(self):
        return self.L
    

    