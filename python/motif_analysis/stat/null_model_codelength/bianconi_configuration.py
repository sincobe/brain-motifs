from motif_analysis.stat.null_model_codelength.null_model import *

class BianconiConfiguration(NullModel):
    def __init__(self,name):
        super().__init__(name)

    def set_params(self, G : nx.DiGraph):
        self.E              = G.number_of_edges()
        self.in_degree_seq  = np.array([j for (i,j) in G.in_degree  ])
        self.out_degree_seq = np.array([l for (k,l) in G.out_degree ])

    def set_codelength(self):
        cost_multigraph = (loggamma(self.E + 1) - loggamma(self.in_degree_seq + 1).sum() - loggamma(self.out_degree_seq + 1).sum())/np.log(2)
        self.L = cost_multigraph  - 0.5 * (self.in_degree_seq**2).sum() * (self.out_degree_seq**2).sum() / self.E**2 / np.log(2) + \
                 self.integer_sequence_encoding(self.in_degree_seq) + \
                 self.integer_sequence_encoding(self.out_degree_seq)
