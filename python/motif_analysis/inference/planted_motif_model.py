from motif_analysis.path.finder import *
from typing import Tuple
import json 
import numpy as np

class PlantedMotifModel(): 
    def __init__(self,filename : str):
        # pf      = OutputPathFinder("graph_encoding",*dataset) 
        # self.j  = pf.get_json("planted_motif_model")
        self.filename = filename
        self.j = json.load(open(filename,'r'))
        self.set_number_of_steps()
        self.set_inference()
        
    
    def base(self):
        return self.j["reference_model"]["model_name"]
    
    def null(self):
        base = self.base()
        if base == "erdos-renyi":
            return "simple_erdos-renyi"
        elif base == "reciprocal_erdos-renyi":
            return "simple_reciprocal_erdos-renyi"
        elif base == "configuration":
            return "bianconi_configuration"
        elif base == "reciprocal_configuration":
            return "bianconi_reciprocal_configuration"
    
    def codelength(self,_j):
        return _j["data"] + _j["model"] + (_j["method"] if "method" in _j else 0.)   

    def reference_cost(self,model="reference_model"):
        return self.codelength(self.j[model]["initial_codelength"])
    
    def cost_at_step(self,step,model="reference_model"):
        return self.codelength(self.j["step_"+str(step)][model]["codelength"])
    
    def set_number_of_steps(self):
        self.T                = len(list(filter(lambda l: "step" in l,list(self.j.keys())))) 
        self.algorithmic_time = self.j["algorithmic_time"]

    def set_inference(self):
        self.L_arr = np.empty(self.T)

        self.L_arr[0] = self.reference_cost()
        for t in range(1,self.T):
            self.L_arr[t] = self.cost_at_step(t)

        self.L     = self.L_arr.min()
        self.t_opt = self.L_arr.argmin()
     
        self.graphlet_set,self.subgraph_set = self.graphlet_label_set_at_step(self.t_opt)
        self.sucess = self.t_opt > 0

    def selected_graphlet_label_at_step(self,step):
        return "".join(map(str,self.j["step_"+str(step)]["reference_model"]["graphlet_label"]))
    
    def selected_subgraph_at_step(self,step):
        return self.j["step_"+str(step)]["reference_model"]["subgraph"]
    

    def graphlet_label_set_at_step(self,step): 
        graphlet_label_set,subgraph_set = {},{}
        for t in range(1,step+1):
            label_t = self.selected_graphlet_label_at_step(t)
            graphlet_label_set[label_t] = graphlet_label_set[label_t] + 1   if label_t in graphlet_label_set else 1
            if label_t not in subgraph_set: subgraph_set[label_t] = []
            subgraph_set[label_t].append(self.selected_subgraph_at_step(t))

        return graphlet_label_set,subgraph_set
    

    
    

        
