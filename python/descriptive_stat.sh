#!/bin/bash
#SBATCH  -o console_plot/log_descriptive_stat_%a.out -e console_plot/log_descriptive_stat_%a.err
#SBATCH --array=5
#SBATCH --mem=30GB
#SBATCH -p dbc_pmo

# python3 python/descriptive_stat_processing.py $SLURM_ARRAY_TASK_ID
python3 python/descriptive_stat_plot.py       $SLURM_ARRAY_TASK_ID

exit 0
