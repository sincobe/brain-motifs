# import motif_analysis.graphlet_distribution as gd

# import motif_analysis.graph_encoding as ge

import motif_analysis.plot_graphlet_distribution as pgd
import motif_analysis.plot_codelength as pc
import motif_analysis.plot_comparative as pcomp
import motif_analysis.rand_planted_motif_model as rpmm

if __name__ == '__main__':

    # dataset   = ("celegans","complete")
    dataset   = ("drosophila_larva","mushroom_body_left")
    method = "planted_motif_model"
    model_ref = "reciprocal_configuration"
    other_models = ["reciprocal_erdos-renyi","configuration","erdos-renyi"] 



    # pgd.GraphletDistributionPlot(*dataset,method,model_ref,other_models).plot_avg_optimal_graphlet_distribution()
    # pgd.GraphletDistributionPlot(*dataset,method,model_ref,other_models).selection_probability_top_graphlets()
    # pgd.GraphletDistributionPlot(*dataset,"planted_motif_model",model_ref,other_models).graphlet_selection_probability(show_graphlets=True)

    # pgd.GraphletDistributionPlot(*dataset,"planted_motif_model",model_ref,other_models).boxplot()



    # pc.CodelengthPlot(*dataset,method,model_ref,other_models).avg()
    # pc.CodelengthPlot(*dataset,"planted_motif_model",model_ref,other_models).hist()

    refs = ["configuration","reciprocal_configuration"] 
    # pcomp.PlotComparative(method,refs).codelength_time_evolution(dataset)
    # pcomp.PlotComparative(method,refs).codelength_hist(dataset)

    # datasets   = [("drosophila_larva","mushroom_body_left"),("celegans","complete")]
    # pcomp.PlotComparative(method,refs).compression(datasets)




    # pgd.GraphletDistributionPlot(*dataset,"planted_motif_model",model_ref,other_models).random(["erdos-renyi"])
    # pgd.GraphletDistributionPlot(*dataset,"planted_motif_model",model_ref,other_models).frequency_vs_codelength(["erdos-renyi"],mode="compression")

    gen = "configuration"
    # gen = "planted_motif_model/erdos-renyi"
    # rpmm.RandPlantedMotifModel(*dataset,gen,model_ref)

    pcomp.PlotComparative(method,refs).test_compression_evolution(dataset,gen,baseline="configuration_uncorrelated")



    
    

    