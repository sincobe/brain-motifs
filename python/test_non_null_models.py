from motif_analysis import plot_test_non_null as ptnn
import json 
import sys

j       = json.load(open('plot.json','r'))

method       = "planted_motif_model"
refs         = ["reciprocal_configuration","configuration","erdos-renyi","reciprocal_erdos-renyi"]
# spe          = "highest_automorphism_group_size/"
spe          = ""
gens         = ["planted_motif_model/erdos-renyi/highest_automorphism_group_size"]

# plot = pcomp.PlotComparative(method,refs)
# for gen in gens:
#     print("Test for randomized model:",gen)
#     plot.test_compression_evolution(dataset,gen)


from motif_analysis.generated_planted_motif_model import NonNullGeneratedPlantedMotifTest
number_of_nodes = 1000
number_of_edges_ls = [4995,7492,9990,24975,49950]
number_of_samples = 5

# p = pfnn.NonNullGenerationPathFinder(number_of_nodes=number_of_nodes,number_of_edges=number_of_edges,number_of_supernodes=number_of_supernodes,subpath="highest_automorphism_group_size/")
# print(p.get_graph_name())
# test = NonNullGeneratedPlantedMotifTest(number_of_nodes=number_of_nodes,number_of_edges=number_of_edges,number_of_supernodes=number_of_supernodes)

plot  = ptnn.PlotTestNonNull(method,refs)
modes = ["stat","raw"]
nulls = ["erdos-renyi","configuration_uncorrelated","multi_erdos-renyi","multi_configuration","multi_reciprocal_erdos-renyi","multi_reciprocal_configuration"]
# options_encoding = ["compression","absolute"]
options_encoding = []
options_covering = []
# options_covering = ["none","motif_set_size","number_of_cliques","stratified_motif_set_size","number_of_cliques_vs_non_cliques"]

for number_of_edges in number_of_edges_ls:
    for mode in modes: 
        plot.comparing_base_encoding_efficiencies(nodeNumber=number_of_nodes,edgeNumber=number_of_edges,sampleSize=number_of_samples,mode=mode,specialsubpath="highest_automorphism_group_size/")
        plot.comparing_base_encoding_efficiencies(nodeNumber=number_of_nodes,edgeNumber=number_of_edges,sampleSize=number_of_samples,mode=mode,specialsubpath="")
        plot.comparing_search_method_efficiencies(nodeNumber=number_of_nodes,edgeNumber=number_of_edges,sampleSize=number_of_samples,mode=mode)
        for option in options_covering:
            plot.covering_performance(nodeNumber=number_of_nodes,edgeNumber=number_of_edges,sampleSize=number_of_samples,mode=mode,option=option,specialsubpath=spe)
        for option in options_encoding:
            plot.encoding_efficiency(nodeNumber=number_of_nodes,edgeNumber=number_of_edges,sampleSize=number_of_samples,nulls=nulls,mode=mode,option=option,specialsubpath=spe)

    
# edgeNumbers = [2446,3669,4893,12232,24465,36697,48930]
# plot.test_non_null_generation_flattened_graph_densities(number_of_nodes,edgeNumbers,number_of_samples)