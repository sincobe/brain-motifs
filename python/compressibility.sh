#!/bin/bash
#SBATCH  -o console_plot/log_compressibility_%a.out -e console_plot/log_compressibility_%a.err
#SBATCH --array=0
#SBATCH --mem=30GB
#SBATCH -p dbc_pmo

python3 python/compressibility_processing.py 
# python3 python/compressibility_plot.py       $SLURM_ARRAY_TASK_ID

exit 0
