from motif_analysis.plot.motif_set_inference import MotifSetInference
import sys 
import json



idx      = int(sys.argv[1])
datasets = json.load(open("param/log/datasets_A2.json"))
dataset  = (datasets[idx]["category"],datasets[idx]["subcategory"])

baseModels = [ "erdos-renyi", 
               "reciprocal_erdos-renyi",
               "configuration",
               "reciprocal_configuration" ]

nulls = ["simple_erdos-renyi","simple_reciprocal_erdos-renyi","bianconi_configuration","bianconi_reciprocal_configuration"]

plot = MotifSetInference(dataset,baseModels,nulls=nulls,mode="process")

if plot.is_valid():
    plot.saveprocessing() 
    plot.write_optimal_inference() 
    
    