from motif_analysis.plot.descriptive_stat import DescriptiveStat
import numpy as np
import json
import sys

idx      = int(sys.argv[1])
datasets = json.load(open("param/log/datasets_B.json",'r'))
dataset  = (datasets[idx]["category"],datasets[idx]["subcategory"])
models_a = ["erdos-renyi","configuration","reciprocal_erdos-renyi","reciprocal_configuration"]
models_b = [ "planted_motif_model/"+model for model in models_a ]
models   = [*models_a,*models_b] 

ds = DescriptiveStat(dataset,models)
ds.saveprocessing()
