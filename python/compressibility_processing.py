from motif_analysis.plot.compressibility import Compressibility
import sys 
import json 

datasets  = json.load(open("param/log/datasets_all.json",'r'))

datasets  = [ (dataset["category"],dataset["subcategory"]) for dataset in datasets ]

baseModels = [ "erdos-renyi", 
               "reciprocal_erdos-renyi",
               "configuration",
               "reciprocal_configuration" ]

refModels  = ["simple_erdos-renyi",
              "simple_reciprocal_erdos-renyi",
              "bianconi_configuration",
              "bianconi_reciprocal_configuration"]
groupname = "all"
plot = Compressibility(groupname,datasets,baseModels=baseModels,refModels=refModels)
plot.process_compressibility()
plot.saveprocessing()