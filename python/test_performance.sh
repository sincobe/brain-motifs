#!/bin/bash
#SBATCH  -o console_plot/log_%a.out -e console_plot/log_%a.err
#SBATCH --array=0
#SBATCH --mem=50GB
#SBATCH -q fast
##SBATCH -p dedicated
#SBATCH -p dbc_pmo

# python3 python/test_performance_processing.py 
python3 python/test_performance_plot.py 

exit 0
