from motif_analysis.plot.test_performance import TestPerformance
import numpy as np
import json
import sys

models   = ["erdos-renyi"]

N = 100
densities = [ 0.01,0.025,0.05,0.1,0.075,0.1]
graphlets = ["path","star","hourglass","perceptron","clique"]
tp = TestPerformance(N,densities,graphlets,models)
tp.saveprocessing()
