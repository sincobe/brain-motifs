#!/bin/bash
#SBATCH  -o console_plot/log_gset_%a.out -e console_plot/log_gset_%a.err
#SBATCH --array=0
#SBATCH --mem=30GB
#SBATCH -p dbc_pmo

python3 python/graphlet_set_measures_processing.py $SLURM_ARRAY_TASK_ID
# python3 python/graphlet_set_measures_plot.py       $SLURM_ARRAY_TASK_ID

exit 0
