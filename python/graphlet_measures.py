from motif_analysis import plot_graphlet_measure as pgm
import json 

j      = json.load(open('../param/datasets.json','r'))
refs   = ["configuration","reciprocal_configuration","erdos-renyi","reciprocal_erdos-renyi"]
method = "planted_motif_model"
# datasets = [
#     ("celegans","witvliet_2020_1"),
#     ("celegans","witvliet_2020_2"),
#     ("celegans","witvliet_2020_3"),
#     ("celegans","witvliet_2020_4"),
#     ("celegans","witvliet_2020_5"),
#     ("celegans","witvliet_2020_6"),
#     ("celegans","witvliet_2020_7"),
#     ("celegans","witvliet_2020_8")
# ]

datasets = [
    ("celegans","witvliet_2020_1"),
    ("celegans","witvliet_2020_2"),
    ("celegans","witvliet_2020_3"),
    ("celegans","witvliet_2020_4"),
    ("celegans","witvliet_2020_5"),
    ("celegans","witvliet_2020_6"),
    ("celegans","witvliet_2020_7"),
    ("drosophila_larva","antennal_lobe_right"),
    ("drosophila_larva","mushroom_body_right")
]
for ref in refs:
    plot = pgm.GraphletMeasuresPlot(method=method,referenceModelName=ref,datasets=datasets)
    # plot.compressions_vs_symmetry_measures()
    # plot.distribution_symmetry_measure(subtitle="drosophila_larva")
    for measure in [ "density","average_clustering","transitivity","automorphism_group_size", "orbit_polynomial_root","symmetry_index" ]:
        plot.comparison_of_measures(measure=measure,subtitle="drosophila_larva",datasets=datasets)
