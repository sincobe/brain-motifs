from motif_analysis import plot_comparative as pcomp
import json 
import sys

j       = json.load(open('plot.json','r'))
idx     = int(sys.argv[1])
dataset = (j["dataset"][idx]["category"],j["dataset"][idx]["subcategory"])

method = "planted_motif_model"
refs   = ["erdos-renyi","reciprocal_erdos-renyi","configuration","reciprocal_configuration"]
plot = pcomp.PlotComparative(method,refs)

plot.codelength_time_evolution(dataset)
plot.codelength_hist(dataset)
