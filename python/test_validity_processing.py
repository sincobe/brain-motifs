from motif_analysis.plot.test_validity import TestValidity
import numpy as np
import json
import sys

idx      = int(sys.argv[1])
datasets = json.load(open("param/log/datasets_B.json",'r'))
dataset  = (datasets[idx]["category"],datasets[idx]["subcategory"])
models   = ["erdos-renyi",       "configuration",         "reciprocal_erdos-renyi",       "reciprocal_configuration"]
nulls    = ["simple_erdos-renyi","bianconi_configuration","simple_reciprocal_erdos-renyi","bianconi_reciprocal_configuration"]
ts = TestValidity(dataset,models,nulls=nulls)
ts.process()
ts.saveprocessing()
