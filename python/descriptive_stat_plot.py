from motif_analysis.plot.descriptive_stat import DescriptiveStat
from motif_analysis.stat.hierarchy        import NullHierarchy
import numpy as np
import json
import sys

idx      = int(sys.argv[1])
datasets = json.load(open("param/log/datasets_B.json",'r'))
dataset  = (datasets[idx]["category"],datasets[idx]["subcategory"])
# models_a = ["erdos-renyi","reciprocal_erdos-renyi","configuration","reciprocal_configuration"]
# models_b = [ "planted_motif_model/"+model for model in models_a ]
# models   = [*models_a,*models_b] 
models = ["erdos-renyi","reciprocal_erdos-renyi","configuration","reciprocal_configuration"]

# hierarchy = NullHierarchy()
# hierarchy.extend("root","erdos-renyi")
# hierarchy.extend("erdos-renyi","reciprocal_erdos-renyi")
# hierarchy.extend("erdos-renyi","configuration")
# hierarchy.extend("configuration","reciprocal_configuration")
# hierarchy.extend("reciprocal_erdos-renyi","reciprocal_configuration")

ds = DescriptiveStat(dataset,models,alpha=1.e-2,mode="make",extension=".pdf")
# ds.set_hierarchy(hierarchy)
ds.load_panels()
ds.make()
