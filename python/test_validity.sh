#!/bin/bash
#SBATCH  -o console_plot/log_%a.out -e console_plot/log_%a.err
#SBATCH --array=0-5
#SBATCH --mem=50GB
#SBATCH -q fast
#SBATCH -p dedicated


python3 python/test_validity_plot.py $SLURM_ARRAY_TASK_ID

exit 0
