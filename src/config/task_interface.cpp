//
//  task_interface.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/18/22.
//

#include "task_interface.hpp"

std::string ITask::get_input() const{
    int idx_dataset = job_id % static_cast<std::size_t>(cr.j["number_of_datasets"]);
    std::string category    = pf.datasets[idx_dataset]["category"];
    std::string subcategory = pf.datasets[idx_dataset]["subcategory"];
    return pf.get_input(category,subcategory);
}

std::string ITask::get_output() const{
    int idx_dataset = job_id % static_cast<std::size_t>(cr.j["number_of_datasets"]);
    nlohmann::json label = pf.datasets[idx_dataset];
    // if (job_id >= 0){
    //     label = pf.datasets[dataset_idx()];
    // }
    // else{
    //     label = pf.datasets[dataset_idx()];
    // }
    
    std::string method      = cr.j["task"];
    std::string category    = label["category"];
    std::string subcategory = label["subcategory"];
    return pf.get_output(method,category,subcategory);
}


