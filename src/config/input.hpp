//
//  read_input.hpp
//  brain-motifs
//
//  Created by Benichou Alexis on 8/21/21.
//

#ifndef read_input_hpp
#define read_input_hpp

#include <string>
#include <vector>
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <array>
#include <map>
#include <cmath>
#include <json.hpp>
#include <unordered_map>
#include <filesystem>
#include <set>

enum FileSuffix {CSV,JSON,EdgeList};

struct FileReader{
 
    FileReader() = default;
    virtual ~FileReader() = default;
    
    virtual void read(const std::string &filename) = 0;
    virtual std::vector<int> get_adjacency_matrix() const = 0;
    
};

struct CSVReader : FileReader{
    

    std::vector<std::string> labels;
    std::vector<std::string> str;

    CSVReader() = default;

    char lineSeparator;
    
    
    void set_line_separator(std::string &&s);
    
    void read(const std::string &filename) override;
    std::vector<int> get_adjacency_matrix() const override;
    static std::unordered_map<std::string,int> read_community_labels(const std::string &filename);
};

struct JSONReader : FileReader{
    nlohmann::json j;
    
    JSONReader() = default;
    
    void read(const std::string &filename) override;
    std::vector<int> get_adjacency_matrix() const override;
};

struct EdgeListReader : FileReader{
    std::vector<std::pair<int,int>> edgeList;
    std::set<int> labels;

    EdgeListReader() = default;
    ~EdgeListReader() = default;

    void read(const std::string &filename) override;
    std::vector<int> get_adjacency_matrix() const override;
};

struct InputFactory{
    std::map<FileSuffix,std::shared_ptr<FileReader>> map_file;
    
    FileSuffix suffix;    
    InputFactory();
    void set_suffix(const std::string &filename);
    std::vector<int> read(const std::string &filename);
};


#endif /* read_input_hpp */
