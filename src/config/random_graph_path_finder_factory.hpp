#ifndef random_graph_path_finder_factory_hpp
#define random_graph_path_finder_factory_hpp

#include "random_graph_path_finder.hpp"

struct RandomGraphPathFinderFactory{

    static std::unique_ptr<RandomGraphPathFinder> make(const RandPFType &type,const ConfigReader &cr, const PathFinder &pf, const int &job_id){

        switch (type)
        {
        case RandPFType::NullGraphRandomization:
            return std::make_unique<NullRandomizationPathFinder>(cr,pf,job_id);
            break;
        case RandPFType::RandomizedRandomGraphs:
            return std::make_unique<RandomizedRandomGraphsPathFinder>(cr,pf,job_id);
            break;
        case RandPFType::NonNullGraphGeneration:
            return std::make_unique<NonNullGenerationPathFinder>(cr,pf,job_id);
            break;

        default:
        return nullptr;
            break;
        }

    }

};

#endif