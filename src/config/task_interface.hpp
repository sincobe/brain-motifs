//
//  task_interface.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/18/22.
//

#ifndef task_interface_hpp
#define task_interface_hpp

#include "input.hpp"
#include "config_reader.hpp"
#include <boost/mpi.hpp>
#include "random_graph_path_finder_factory.hpp"



namespace mpi = boost::mpi;

/**
 * Task interface that avoids writing multiple main files
 */



struct ITask{
    
    mpi::communicator world; /**< Has information about current process*/
    ConfigReader cr; /**<  Reads algorithm parameters in a JSON file */
    PathFinder   pf; /**<  Path dictionnary in JSON file*/
    std::unique_ptr<RandomGraphPathFinder> rg_pf;
    std::size_t N_networks;
    int job_id; /**< If job array on cluster, the id will correspond  to the data index */

    ITask() : cr{""},job_id{}{} 
    ITask(std::string &input, int id = -1) : cr{input},job_id{id}{ 
        if (cr.j.find("artificial_data") != cr.j.end()){
            rg_pf = RandomGraphPathFinderFactory::make(read_randgraph_kind(),cr,pf,job_id);
            N_networks = rg_pf->number_of_random_networks();
        }
        else if (cr.j["task"] == "test_non_null"){
            N_networks = 0;
        }
        else{
            N_networks = static_cast<std::size_t>(cr.j["number_of_datasets"]);
        }
     }
    virtual ~ITask() = default;
    
    virtual void set_parameters() = 0;
    virtual void make() = 0;
    

    std::string get_input()  const; /**< Get path of input adjacency matrix*/
    std::string get_output() const; /**< Get path of output folder*/

    virtual std::filesystem::path get_output_path(){
        return std::filesystem::current_path();
    }

    RandPFType read_randgraph_kind() const{
        std::string kind{cr.j["artificial_data"]["kind"]};
        if (kind == "graph_randomization"){
            return RandPFType::NullGraphRandomization;
        }
        else if(kind == "test_validity"){
            return RandPFType::RandomizedRandomGraphs; 
        }
        else if (kind == "test_non_null"){
            return RandPFType::NonNullGraphGeneration;
        }
        else{
            std::cout << "ITask: mispelled artificial data kind in configuration JSON file.\n";
            return RandPFType::None;   
        }
    }

    std::string get_datetime_string() const{
        std::stringstream ss{};
        auto now     = std::chrono::system_clock::now();
        auto time_t  = std::chrono::system_clock::to_time_t(now);
        auto delta_t = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        ss << std::put_time(std::localtime(&time_t),"_%m:%d:%X");
        ss << '.' << std::setfill('0') << std::setw(3) << delta_t.count();
        return ss.str();
    }

    std::vector<int> get_adjacency_matrix() const{
        InputFactory input{};
        if (cr.j.find("artificial_data") == cr.j.end()){
            return std::move(input.read(get_input()));
        }
        else{
            return std::move(input.read(rg_pf->get()));
        }
    }

    std::string output_folder_of_rand_graph(const std::string &taskname) const{
        std::string task_rootname  {pf.get_output_root(taskname)};
        std::string output_rootname{pf.get_output_root()};

        std::string subpathname = rg_pf->get();

        subpathname.erase(subpathname.find_last_of("."));
        subpathname.erase(subpathname.find(output_rootname),output_rootname.size());

        return task_rootname + subpathname + "/";
    }

    std::string output_folder_of_real_graph(const std::string &taskname) const{
        int idx_dataset         = job_id % static_cast<int>(cr.j["number_of_datasets"]);
        std::string category    = static_cast<std::string>(pf.datasets[idx_dataset]["category"]);
        std::string subcategory = static_cast<std::string>(pf.datasets[idx_dataset]["subcategory"]);
        return pf.get_output(taskname,category,subcategory);
    }

    std::string output_folder(const std::string &taskname){
        std::string output_folder;
        if (cr.j.find("artificial_data") == cr.j.end()){
            output_folder = output_folder_of_real_graph(taskname);
        }
        else{
            output_folder = output_folder_of_rand_graph(taskname);
        }
        std::filesystem::create_directories(output_folder);
        return output_folder;
    }

    // virtual std::size_t number_of_samples() const {
    //     return cr.j["randomized_graph"]["number_of_samples"];
    // }

    // inline bool input_is_random_graph() const{
    //     if (cr.j["task"] != "graph_randomization"){
    //         return static_cast<bool>(cr.j["randomized_graph"]["value"]);
    //     }

    //     else{
    //         return false;
    //     }   
    // }

    // virtual inline int dataset_idx() const{
    //     if (input_is_random_graph()){
    //         return job_id / number_of_samples() % number_of_datasets();
    //     }
    //     else{
    //         return job_id % number_of_datasets() ;
    //     }
    // }

    // inline int sample_idx() const{
    //     return job_id % number_of_samples();
    // }

    // std::string get_input_null_model() const {
    //     std::string model_str = static_cast<std::string>(cr.j["randomized_graph"]["model"]) + "/" ;

    //     if (model_str == "planted_motif_model/"){
    //         model_str += static_cast<std::string>(cr.j["randomized_graph"]["base"]) + "/" ;
    //     }
    //     return model_str;
    // }
    
//    inline nlohmann::json get_dataset_output_root_json() const{
//         return  pf.datasets[job_id % static_cast<std::size_t>(cr.j["number_of_datasets"])];
//     }

    // std::string get_input_random_graph_root_path() const{
    //     auto label = get_dataset_output_root_json();
    //     std::string category    = label["category"];
    //     std::string subcategory = label["subcategory"];
    //     return pf.get_output("graph_randomization",category,subcategory);
    // }

    // std::string get_input_random_graph_path() const{
        
    //     std::string base      = get_input_random_graph_root_path();
    //     std::string model_str = get_input_null_model();
    //     auto p = std::make_unique<std::filesystem::path>(base + model_str);
    //     if (model_str.find("planted_motif_model") != std::string::npos){
    //         auto dir_intermediate = std::filesystem::directory_iterator(*p.get());
    //         while (!std::filesystem::is_directory(dir_intermediate->symlink_status())){
    //             ++dir_intermediate;
    //         }
    //         for (int step = 0 ; step < cr.j["randomized_graph"]["inference_idx"] ; step++){
    //             ++dir_intermediate;
    //             while (!std::filesystem::is_directory(dir_intermediate->symlink_status())){
    //                 ++dir_intermediate;
    //             }
    //         }
    //         model_str += static_cast<std::string>(dir_intermediate->path().filename()) + "/";
    //         p = std::make_unique<std::filesystem::path>(base + model_str);
    //     }
    //     auto dir =  std::filesystem::directory_iterator(*p.get());

    //     while (static_cast<std::string>(dir->path().extension()) != ".txt"){
    //         ++dir;
    //     }

    //     int ref{};
    //     while (ref != sample_idx()){
    //         ++ref;
    //         ++dir;
        
    //         while (static_cast<std::string>(dir->path().extension()) != ".txt"){
    //             ++dir;
    //         }
    //     }
    //     // std::cout <<  base + model_str + static_cast<std::string>(dir->path().filename()) << std::endl;
    //     return base + model_str + static_cast<std::string>(dir->path().filename());
    // }

    // std::vector<int> adjacency_matrix() const{
    //     InputFactory input{};
    //     if (input_is_random_graph()){
    //         return std::move(input.read(get_input_random_graph_path()));
    //     }
    //     else{
    //         return std::move(input.read(get_input()));
    //     }
    // }


    // std::string get_output_graphlet_census(){
    //     auto label = get_dataset_output_root_json();
    //     std::string category    = label["category"];
    //     std::string subcategory = label["subcategory"]; 
    //     std::string directory_path_name;
    //     if (input_is_random_graph()){
    //         std::filesystem::path p{get_input_random_graph_path()};
    //         directory_path_name =   pf.get_output("graphlet_census",category,subcategory)  + 
    //                                 "randomized/" +  
    //                                 get_input_null_model() + 
    //                                 (get_input_null_model().find("planted_motif_model") != std::string::npos ? 
    //                                 static_cast<std::string>(p.parent_path().filename()) + "/" : "") +
    //                                 static_cast<std::string>(p.stem()) + "/" ;
    //     }
    //     else{
    //         directory_path_name = pf.get_output("graphlet_census",category,subcategory);
    //     }
    //     if (cr.j["task"] == "graphlet_census"){
    //         std::filesystem::create_directories(directory_path_name + "store/");
    //     }
        
    //     return directory_path_name;
    // }


    // std::string get_output_graph_encoding(){
    //     auto label = get_dataset_output_root_json();
    //     std::string category    = label["category"];
    //     std::string subcategory = label["subcategory"]; 
    //     std::string encoding_path_name = static_cast<std::string>(cr.j["model"]["encoding"]) + "/";
    //     if (cr.j["model"]["encoding"] == "planted_motif_model"){
    //         encoding_path_name += static_cast<std::string>(cr.j["model"]["base"]) + "/";
    //     }
        
    //     if (cr.j.find("graphlet_criterium") != cr.j.end()){
    //         std::string criterium = static_cast<std::string>(cr.j       ["graphlet_criterium"]);
    //         encoding_path_name   += pf.get_criterium(criterium);
    //     }

    //     std::string directory_path_name;
    //     if (input_is_random_graph()){
    //         std::filesystem::path p{get_input_random_graph_path()};
    //         directory_path_name =   pf.get_output("graph_encoding",category,subcategory) +
    //                                 encoding_path_name +  
    //                                 "randomized/" +  
    //                                 get_input_null_model() + 
    //                                 (get_input_null_model().find("planted_motif_model") != std::string::npos ? 
    //                                 static_cast<std::string>(p.parent_path().filename()) + "/" : "") +
    //                                 static_cast<std::string>(p.stem()) + "/" ;
    //     }
    //     else{
    //         directory_path_name = pf.get_output("graph_encoding",category,subcategory) + encoding_path_name;
    //     }

    

    //     std::filesystem::create_directories(directory_path_name);
    //     return directory_path_name;
    // }
};



#endif /* task_interface_hpp */
