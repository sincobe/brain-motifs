//
//  config_reader.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/17/22.
//

#ifndef config_reader_hpp
#define config_reader_hpp

#include <json.hpp>
#include <iostream>
#include <fstream>
#include <string>



enum Task{
    graphlet_census,
    graphlet_measures,
    graph_encoding,
    graph_randomization,
    test_non_null
};

/**
 * Gathers algorithm parameters from JSON file
 */
struct ConfigReader{
    
    nlohmann::json j;
    Task t;
    
    ConfigReader(const std::string &input){
        set_task(input);
        set_json_task();
    }
    
    inline void set_task(const std::string &input){
        if (input == "graphlet_census"){
            t = Task::graphlet_census;
        }
        else if (input == "graph_encoding"){
            t = Task::graph_encoding;
        }
        else if (input == "graph_randomization"){
            t = Task::graph_randomization;
        }
        else if (input == "test_non_null"){
            t = Task::test_non_null;
        }
        else if (input == "graphlet_measures"){
            t = Task::graphlet_measures; 
        }
        else{
            std::cout << "Mispelled task name" << std::endl;
        }
    }
    
    void set_json_task();
};

/**
 * Finds input adjacency matrix and output path from JSON file
 */
struct PathFinder{
    nlohmann::json datasets;
    nlohmann::json path_rep;
    PathFinder(){
        std::string filename_p{"param/path.json"};
        std::string filename_d{"param/datasets.json"};
        std::ifstream file_p{filename_p};
        std::ifstream file_d{filename_d};
        if (!file_p.good()){
            std::cout << "Param pathname is wrong: " << filename_p << std::endl;
        }
        if (!file_d.good()){
            std::cout << "Dataset pathname is wrong: " << filename_d << std::endl;
        }
        file_p >> path_rep;
        file_d >> datasets;
        file_p.close();
        file_d.close();
    }
    

    std::string get_input(const std::string &name, const std::string &element) const;
    
    std::string get_output_root(const std::string &taskName) const{
        std::string baseIO,baseOutput,method; 
        baseIO      = path_rep["base"];
        baseOutput  = path_rep["output"]["base"];
        method      = path_rep["output"]["algorithm"][taskName];
        return baseIO + baseOutput + method;

    }

    std::string get_output_root() const{
        std::string baseIO,baseOutput; 
        baseIO      = path_rep["base"];
        baseOutput  = path_rep["output"]["base"];
        return baseIO + baseOutput;
    }

    std::string get_output(const std::string &algorithm,
                           const std::string &name,
                           const std::string &element) const;

    inline std::string get_criterium(const std::string &criterium) const{
        return static_cast<std::string>(path_rep["output"]["graphlet_criterium"][criterium]);
    }



};

#endif /* config_reader_hpp */
