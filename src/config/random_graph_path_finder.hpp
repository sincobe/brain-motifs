#ifndef random_graph_path_finder_hpp
#define random_graph_path_finder_hpp

#include <iostream>
#include <cmath>
#include <string>
#include <filesystem>
#include "config_reader.hpp"
#include "../data_structure/graphlet/topological.hpp"

enum class RandPFType {
    NullGraphRandomization,
    RandomizedRandomGraphs,
    NonNullGraphGeneration,
    None
};


struct RandomGraphPathFinder{

    const std::string key;
    std::size_t N_networks;
    std::filesystem::path p;
    RandomGraphPathFinder() : key{"artificial_data"},N_networks{},p{}{}
    virtual ~RandomGraphPathFinder() = default;
    
    inline std::filesystem::path get() const{return p;}
    inline std::size_t number_of_random_networks() const{return N_networks;}

    virtual void set(const ConfigReader &cr, const PathFinder &pf, const int &job_id) = 0;
};

struct NullRandomizationPathFinder : RandomGraphPathFinder{

    NullRandomizationPathFinder(const ConfigReader &cr, const PathFinder &pf, const int &job_id) : RandomGraphPathFinder{}{set(cr,pf,job_id);}
    void set(const ConfigReader &cr, const PathFinder &pf, const int &job_id)override{

        std::size_t number_of_randomizations = static_cast<std::size_t>(cr.j[key]["number_of_randomizations"]);
        std::size_t number_of_datasets       = static_cast<std::size_t>(cr.j[key]["number_of_datasets"]);
        std::vector<std::string> models      = cr.j[key]["models"];
        
        int idx_randomization = job_id % number_of_randomizations;
        int idx_model         = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/number_of_randomizations)) % models.size();
        int idx_dataset       = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/number_of_randomizations/models.size())) % number_of_datasets;

        std::string category    = static_cast<std::string>(pf.datasets[idx_dataset]["category"]);
        std::string subcategory = static_cast<std::string>(pf.datasets[idx_dataset]["subcategory"]);

        std::filesystem::path p_root{pf.get_output("graph_randomization",category,subcategory) + models[idx_model] + "/"};
        int idx{};
        for (auto &&p_curr : std::filesystem::directory_iterator(p_root)){
            if (p_curr.path().extension() == ".txt" &&  idx == idx_randomization){
                p = p_curr;
                break;
            }
            else if (p_curr.path().extension() == ".txt") {idx++;}   
        }
        N_networks = number_of_randomizations * number_of_datasets * models.size();
    }
};

struct RandomizedRandomGraphsPathFinder : RandomGraphPathFinder{

    RandomizedRandomGraphsPathFinder(const ConfigReader &cr, const PathFinder &pf, const int &job_id) : RandomGraphPathFinder{}{set(cr,pf,job_id);}
    
    void set(const ConfigReader &cr, const PathFinder &pf, const int &job_id)override{

        std::size_t N_gen                    = static_cast<std::size_t>(cr.j[key]["number_of_networks_per_model_gen"]);
        std::size_t N_rand                   = static_cast<std::size_t>(cr.j[key]["number_of_randomizations_per_model_rand"]);
        std::size_t number_of_datasets       = static_cast<std::size_t>(cr.j[key]["number_of_datasets"]);
        std::vector<std::string> models_gen  = cr.j[key]["models_gen" ];
        std::vector<std::string> models_rand = cr.j[key]["models_rand"];
        
        int idx_randomization_rand = job_id % N_rand;
        int idx_randomization_gen  = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/N_rand)) % N_gen;
        int idx_model_rand         = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/N_gen/N_rand)) % models_rand.size();
        int idx_model_gen          = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/N_gen/N_rand/models_rand.size())) % models_gen.size();
        int idx_dataset            = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/N_rand/N_gen/models_gen.size()/models_rand.size())) % number_of_datasets;

        std::string category    = static_cast<std::string>(pf.datasets[idx_dataset]["category"]);
        std::string subcategory = static_cast<std::string>(pf.datasets[idx_dataset]["subcategory"]);


        std::filesystem::path p_root{ pf.get_output("randomized_random_graphs",category,subcategory) + models_gen[idx_model_gen] + "/" };
        // std::filesystem::directory_iterator dir_it{p_root};
        // for (int idx{} ; idx < idx_randomization_gen + 1; idx++){
        //     ++dir_it;
        // }

        int idx{};
        for (auto &&p_curr : std::filesystem::directory_iterator(p_root)){
            if (std::filesystem::is_directory(p_root) && idx == idx_randomization_gen){
                p_root = p_curr.path();
                break;
            }
            else if (std::filesystem::is_directory(p_root)) {idx++;}                
        }

        p_root = std::filesystem::path( static_cast<std::string>(std::filesystem::absolute(p_root)) + "/" + models_rand[idx_model_rand] + "/" );
        // dir_it = std::filesystem::directory_iterator(p_root);
        // for (int idx{} ; idx <= idx_randomization_rand + 1; idx++){
        //     ++dir_it;
        // }

        idx = 0;
        for (auto &&p_curr : std::filesystem::directory_iterator(p_root)){
            if (p_curr.path().extension() == ".txt" &&  idx == idx_randomization_rand){
                p = p_curr.path();
                break;
            }
            else if (p_curr.path().extension() == ".txt") {idx++;}   
        }

        // p          = dir_it->path();
        N_networks = N_gen * N_rand * number_of_datasets * models_gen.size() * models_rand.size() ;
    }
};

struct NonNullGenerationPathFinder : RandomGraphPathFinder{

    NonNullGenerationPathFinder(const ConfigReader &cr, const PathFinder &pf, const int &job_id) : RandomGraphPathFinder{}{set(cr,pf,job_id);}

    void set(const ConfigReader &cr, const PathFinder &pf, const int &job_id) override{

        std::size_t number_of_generations    = cr.j[key]["number_of_generations"];
        std::vector<double>      densities   = static_cast<std::vector<double>>(cr.j[key]["densities"]); 
        auto graphlets                       =                                  cr.j[key]["graphlets"];

        int idx_density    = job_id % densities.size();
        int idx_graphlet   = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/densities.size())) % graphlets.size();

        std::size_t N              = cr.j[key]["graph_size"];
        double density             = densities[idx_density];
        std::string gtrie_label    = graphlets[idx_graphlet]["gtrie_label"];
        std::string graphlet_alias = graphlets[idx_graphlet]["alias"];

        std::size_t E = std::floor(density * N * N);
        std::size_t max_supernode_number = std::floor(static_cast<double>(N) / TopologicalGraphlet::size_from_gtrie_label(gtrie_label));  

        std::size_t N_supernodes = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/densities.size()/graphlets.size())) % (max_supernode_number + 1);    
        int idx_gen              = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/densities.size()/graphlets.size()   / (max_supernode_number + 1))) % number_of_generations ;                                                            

        std::string directory_rootpathname = pf.get_output_root("test_non_null") + graphlet_alias + "/number_of_nodes_" + std::to_string(N) + "/number_of_edges_" + std::to_string(E) + "/number_of_supernodes_" + std::to_string(N_supernodes) + "/";
        std::filesystem::path p_root{directory_rootpathname};
        std::filesystem::directory_iterator dir_it{p_root};

        int idx{};
        for (auto &&p_curr : std::filesystem::directory_iterator(p_root)){
            if (p_curr.path().extension() == ".txt" &&  idx == idx_gen){
                p = p_curr.path();
                break;
            }
            else if (p_curr.path().extension() == ".txt") {idx++;}   
        }

        // p = dir_it->path();
        N_networks =  number_of_generations * densities.size() * graphlets.size() * (max_supernode_number + 1);
    }
};

#endif