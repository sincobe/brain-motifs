////
////  read_input.cpp
////  brain-motifs
////
////  Created by Benichou Alexis on 8/21/21.
////
//
#include "input.hpp"

using namespace nlohmann;
//

void CSVReader::read(const std::string &filename){
    
    
    std::ifstream file_test{filename};
    
    if(!file_test.good()){
        std::cout << "CSVReader: File not found." <<std::endl;
    }
    else{
        std::stringstream ss_test{};
        ss_test << file_test.rdbuf();
        set_line_separator(ss_test.str());
        file_test.close();
        
        std::ifstream file{filename};
        std::string line{},lab{},element{};
        std::getline(file,line,lineSeparator);
        std::stringstream ss{};

        ss << line;
        while (std::getline(ss,element,',')){
            if (element.size()){
                labels.push_back(element);
            }
        }
        
        str.resize(labels.size() * labels.size());
        int i{};
        while (std::getline(file,line,lineSeparator)){
            ss.clear();
            ss << line;
            bool first_element = true;
            while (std::getline(ss, element, ',')) {
                if (first_element){
                    first_element = false;
                }
                else{
                    if (element.size()){
                        str[i] = element;
                        i++;
                    }
                    

                }
                
            }
        }
        file.close();
    }
    file_test.close();
    
}

void EdgeListReader::read(const std::string &filename){
    std::ifstream file{filename};
    if(!file.good()){
        std::cout << "EdgeListReader: File not found." <<std::endl;
       
    }
    else{
        std::string line{},element{};
        
        while (std::getline(file,line,'\n')){
            std::stringstream ss{line};

            std::string from = line.substr(0,line.find(' '));
            std::string to   = line.substr(line.find(' ') + 1,std::string::npos);

            labels.insert(std::stoi(from));
            labels.insert(std::stoi(to));

            edgeList.emplace_back(std::make_pair(std::stoi(from),std::stoi(to)));
        }
    }
    file.close();
}

std::vector<int> EdgeListReader::get_adjacency_matrix() const{
    
    std::size_t N = *std::max_element(labels.begin(),labels.end()) + 1;
    std::vector<int> adjMat(N * N);
    int i,j;
    for (auto &&item : edgeList){
        i = item.first;
        j = item.second;
        adjMat[i * N + j]++;
    }

    return adjMat;

}

std::unordered_map<std::string,int> CSVReader::read_community_labels(const std::string &filename){
    
    std::unordered_map<std::string,int> nodeToBlock{};
    std::ifstream file_test{filename};
    
    if(!file_test.good()){
        std::cout << "File not found." <<std::endl;
    }
    std::stringstream ss_test{};
    ss_test << file_test.rdbuf();

    file_test.close();
    
    std::ifstream file{filename};
    std::string line{},lab{},element{};
    std::stringstream ss{};

    while (std::getline(file,line,'\n')){
        ss.clear();
        ss << line;
    
        std::vector<std::string> pair;
        while (std::getline(ss, element, ',')) {
            pair.push_back(static_cast<std::string>(element));
        }
        nodeToBlock[pair[0]] = std::stoi(pair[1]);
    }
    return nodeToBlock;
}

void CSVReader::set_line_separator(std::string &&s){


    if (s.find('\n') != s.npos){
        lineSeparator = '\n';
    }
    else if (s.find('\r') != s.npos){
        lineSeparator = '\r';
    }
    else{
        std::cout << "Invalid file: No matching separator" << std::endl;
    }
}

std::vector<int> CSVReader::get_adjacency_matrix() const{
    std::vector<int>mat(labels.size() * labels.size());
    auto it_mat{mat.begin()};
    for (auto &s : str){
//        std::cout << s << std::endl;
        *it_mat = std::stoi(s);
        ++it_mat;
    }
    return mat;
}

void JSONReader::read(const std::string &filename){
    std::ifstream file{filename};
    j = nlohmann::json::parse(file);
//        std::cout << j <<std::endl;
    file.close();
}

std::vector<int> JSONReader::get_adjacency_matrix() const{
    std::vector<int> mat{};
    return mat;
}

InputFactory::InputFactory(){
    
    map_file[FileSuffix::CSV]  = std::make_shared<CSVReader>();
    map_file[FileSuffix::JSON] = std::make_shared<JSONReader>();
    map_file[FileSuffix::EdgeList] = std::make_shared<EdgeListReader>();

}

void InputFactory::set_suffix(const std::string &filename){
    std::filesystem::path p{filename};
    auto ext = p.extension();
    if (p.extension() == ".csv"){
        suffix = FileSuffix::CSV;
    }
    else if (p.extension() == ".json") {
        suffix = FileSuffix::JSON;
    }

    else if (filename.find("edge_list") != std::string::npos){
        suffix = FileSuffix::EdgeList;
    }

    else{
        std::cout << "Invalid input file: No matching extension" << std::endl;
    }
}

std::vector<int> InputFactory::read(const std::string &filename){

        set_suffix(filename);

        // suffix = FileSuffix::CSV;
        map_file[suffix]        -> read(filename);
        return map_file[suffix] -> get_adjacency_matrix();
    }

