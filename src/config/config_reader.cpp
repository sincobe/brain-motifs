//
//  config_reader.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/17/22.
//

#include "config_reader.hpp"

void ConfigReader::set_json_task(){
    std::string filename;
    switch (t){
        case Task::graphlet_census:
            filename = "param/graphlet_census.json";
            break;
        case Task::graphlet_measures:
            filename = "param/graphlet_measures.json";
            break;
        case Task::graph_encoding:
            filename = "param/graph_encoding.json";
            break;
        case Task::graph_randomization: 
            filename = "param/graph_randomization.json";
            break;
        case Task::test_non_null: 
            filename = "param/test_non_null.json";
            break;
        default:
            std::cout << "Wrong task name." << std::endl;
            break;
    }
   
    std::ifstream file{filename};

    if (!file.good()){
        std::cout << "Path name for task config file is wrong: " << filename << std::endl;
    }
    file >> j;
    file.close();
}

std::string PathFinder::get_input(const std::string &name, const std::string &element) const{
    
    std::string baseIO,baseInput,baseData;
    
    baseIO    = path_rep["base"];
    baseInput = path_rep["input"]["base"];
    baseData  = path_rep["input"]["dataset"]["base"];
    
    std::string base{baseIO + baseInput + baseData},category,subcategory;
    
    category    = path_rep["input"]["dataset"]["category"][name];
    subcategory = path_rep["input"]["dataset"]["subcategory"][element];

    
    return base + category + subcategory;
}

std::string PathFinder::get_output(const std::string &algorithm,
                                   const std::string &name,
                                   const std::string &element) const{
    
    std::string baseIO;
    std::string baseOutput;
    
    baseIO      = path_rep["base"];
    baseOutput  = path_rep["output"]["base"];
    std::string base{ baseIO + baseOutput },category,subcategory,method;

    category    = path_rep["output"]["dataset"]["category"][name];
    subcategory = path_rep["output"]["dataset"]["subcategory"][element];
    method      = path_rep["output"]["algorithm"][algorithm];
    
    
    return base + method + category + subcategory ;
}

