#ifndef topological_information_hpp
#define topological_information_hpp

#include "network_measure.hpp"
#include <nauty_label.hpp>

template<class T> 
struct TopologicalInformation : T{
    
    std::shared_ptr<NautyLabel> nl;

    TopologicalInformation(const T& measure) : T{measure},nl{std::make_shared<NautyLabel>(*measure.G.get())}{
        nl->set_canonical_labelisation();
        set_topological_information();
        set_automorphism_size();
        set_symmetry_index();
    }

    std::map<int,double> orbital_distribution(){
        std::map<int,double> orb_dist;
        std::vector<int> orbits(nl->orbits,nl->orbits+nl->n);
        for (auto &&o : orbits){
            orb_dist[o]++;
        }
        return orb_dist;
    }

    void set_automorphism_size(){
        const NetworkMeasureType type{NetworkMeasureType::AutomorphismGroupSize};
        this->m[type] = nl->stats.grpsize1 * std::pow(10, nl->stats.grpsize2);
    }

    void set_topological_information(){
        const NetworkMeasureType type{NetworkMeasureType::TopologicalInformation};
        std::map<int,double> orb_dist{orbital_distribution()};
        this->m[type] = 0.;
        for (auto &&[l,n] : orb_dist){
            this->m[type] -= n / nl->n * std::log2(n / nl->n);
        }
    }

    void set_symmetry_index(){
        const NetworkMeasureType type{NetworkMeasureType::SymmetryIndex};
        this->m[type] = std::log2(nl->n) - this->m[NetworkMeasureType::TopologicalInformation] + 
                        std::log2(this->m[NetworkMeasureType::AutomorphismGroupSize]);
    }
};

#endif 