#ifndef network_measure_hpp
#define network_measure_hpp

#include <graph.hpp>
#include <json.hpp>

enum class NetworkMeasureType{
    None,
    AutomorphismGroupSize, 
    TopologicalInformation,
    SymmetryIndex,
    OrbitPolynomialRoot
};

template<class T>
struct NetworkMeasure{

    std::shared_ptr<Graph<T>> G;
    std::map<NetworkMeasureType,double> m;

    template<class U>
    NetworkMeasure(const std::vector<U> &adjMat, EdgeType dir = EdgeType::directed) : 
    G{std::make_shared<Graph<T>>(dir,adjMat)},m{}{} 

    NetworkMeasure(const NetworkMeasure &measure) : G{measure.G},m{measure.m}{}

    nlohmann::json get_output() const{
        nlohmann::json j;
        for (auto &&[measure_type,value] : m){
            std::string name{network_measure_type_name(measure_type)};
            j[name] = value;
        }
        return j;
    }

    std::string network_measure_type_name(const NetworkMeasureType &type) const {
        switch (type)
        {   
        case NetworkMeasureType::AutomorphismGroupSize:
            return "automorphism_group_size";
            break;
        case NetworkMeasureType::TopologicalInformation:
            return "topological_information";
            break;
        case NetworkMeasureType::SymmetryIndex:
            return "symmetry_index";
            break;
        case NetworkMeasureType::OrbitPolynomialRoot:
            return "orbit_polynomial_root";
            break;
        default:
            return "unvalid";
            break;
        }
    }

    static NetworkMeasureType type(const std::string &name){
        if (name == "automorphism_group_size"){
            return NetworkMeasureType::AutomorphismGroupSize;
        }
        else if (name == "topological_information"){
            return NetworkMeasureType::TopologicalInformation;
        }
        else if (name == "symmetry_index"){
            return NetworkMeasureType::SymmetryIndex;
        }
        else if (name == "orbital_polynomial_root"){
            return NetworkMeasureType::OrbitPolynomialRoot;
        }
        else{
            std::cout << "Unvalid network measure type." << std::endl;
            return NetworkMeasureType::None;
        }
    }
};

#endif