#ifndef orbit_polynomial_hpp
#define orbit_polynomial_hpp 

#include "topological_information.hpp"
#include <boost/math/tools/roots.hpp>
using namespace boost::math::tools;

template<class T>
struct OrbitPolynomialRoot : TopologicalInformation<T>{

    OrbitPolynomialRoot(const T &measure) : TopologicalInformation<T>{measure}{
        set();
    }


    void set(){


        std::map<int,double> orb_dist = this->orbital_distribution();
        std::map<double,double> orb_size_dist;
        for (auto &&[l,n] : orb_dist){
            orb_size_dist[n]++;
        }

        auto Polynomial = [&orb_size_dist](const double &z){
            
            double polynomial{1.};
            for (auto &&[i,n] : orb_size_dist){
                polynomial -= n * std::pow(z,i);
            }
            return polynomial;
        };
    
        int digits = std::numeric_limits<double>::digits;
        eps_tolerance<double> tol(digits-1.);
        std::uintmax_t maxit = 20;

        std::pair<double,double> r = bisect(Polynomial,0.,1.,tol,maxit);
        this->m[NetworkMeasureType::OrbitPolynomialRoot] = (r.first + r.second)/2;

    }

};

#endif