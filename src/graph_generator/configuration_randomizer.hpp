#ifndef configuration_randomizer_hpp
#define configuration_randomizer_hpp

#include "graph_randomizer.hpp"

struct ConfigurationRandomizer : GraphRandomizer{
    template<typename T>
    ConfigurationRandomizer(const Graph<T> &_G) : GraphRandomizer{_G}{G->remove_multiedges();}   
    ConfigurationRandomizer(std::shared_ptr<Graph<int>> &_G) : GraphRandomizer{_G}{G->remove_multiedges();}   

    void swap() override{
        int idxA,idxB;
        std::vector<std::pair<int,int>>::iterator edgeA,edgeB;

        while (1){
            idxA = random_idx(SequenceType::Edge);
            idxB = random_idx(SequenceType::Edge);
            edgeA  = sequence_map[SequenceType::Edge].begin() + idxA;
            edgeB  = sequence_map[SequenceType::Edge].begin() + idxB;


            bool stop_sampling  =   idxA != idxB && 
                                    edgeA -> first != edgeB -> second &&
                                    edgeB -> first != edgeA -> second &&
                                    !G->at(edgeA -> first,edgeB -> second) && 
                                    !G->at(edgeB -> first,edgeA -> second);
                                    // G->at(edgeA -> first, edgeA -> second) == G->at(edgeB -> first, edgeB->second); // uncomment to preserve parallel edges

            

            if (stop_sampling){
                break;
            }
        }
        
        G->edge_swap(*edgeA,*edgeB);

        std::swap(edgeA->second,edgeB->second);    
    }

    virtual bool sanity_check(const Graph<bool> &G0) override{
        return G0.get_degree_seq(Neighborhood::in)  == G->get_degree_seq(Neighborhood::in) 
            && G0.get_degree_seq(Neighborhood::out) == G->get_degree_seq(Neighborhood::out);
    }

};

#endif