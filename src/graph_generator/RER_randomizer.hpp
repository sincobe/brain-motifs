#ifndef RER_randomizer_hpp
#define RER_randomizer_hpp

#include "ER_randomizer.hpp"

struct RERRandomizer : ERRandomizer{
    std::unique_ptr<std::uniform_real_distribution<double>> p;
    template<typename T>
    RERRandomizer(const Graph<T> &_G) : ERRandomizer{_G},p{std::make_unique<std::uniform_real_distribution<double>>(0.,1)}{}
    RERRandomizer(std::shared_ptr<Graph<int>> &_G) : ERRandomizer{_G},p{std::make_unique<std::uniform_real_distribution<double>>(0.,1)}{}


    void set_seq() override{
        int i,j;
        for (i = 0 ; i < G->N ; i++){
            for (j = i+1 ; j < G->N ; j++){

                if (G->at(i,j) && G->at(j,i)){
                    sequence_map[SequenceType::MutualEdge].emplace_back(std::make_pair(i,j));
                }

                else if (G->at(i,j) && !G->at(j,i)){
                    sequence_map[SequenceType::SingleEdge].emplace_back(std::make_pair(i,j));
                }

                else if (!G->at(i,j) && G->at(j,i)){
                    sequence_map[SequenceType::SingleEdge].emplace_back(std::make_pair(j,i));
                }

                else {
                    sequence_map[SequenceType::NonEdge]   .emplace_back(std::make_pair(i,j));
                }
            }
        }



        randIdx_map[SequenceType::MutualEdge] =  std::make_shared<std::uniform_int_distribution<int>>
                                                    (0,sequence_map[SequenceType::MutualEdge].size()-1);

        randIdx_map[SequenceType::SingleEdge]  =  std::make_shared<std::uniform_int_distribution<int>>
                                                (0,sequence_map[SequenceType::SingleEdge].size()-1);

        randIdx_map[SequenceType::NonEdge] =  std::make_shared<std::uniform_int_distribution<int>>
                                                    (0,sequence_map[SequenceType::NonEdge].size()-1);
    }

    double coin(){
        return std::move((*p.get())(*eng_ptr.get()));
    }

    void swap() override{
        int idxEdge,idxNonEdge;
        std::vector<std::pair<int,int>>::iterator edge,non_edge;
        
        idxNonEdge = random_idx  (SequenceType::NonEdge);
        non_edge   = sequence_map[SequenceType::NonEdge].begin() + idxNonEdge;

        SequenceType type;
        if (coin() < .5){
            type = SequenceType::SingleEdge;
            if (coin() < .5){
                std::swap(non_edge->first,non_edge->second);
            }
        }
        else{
            type = SequenceType::MutualEdge;
        }

        idxEdge = random_idx  (type);
        edge    = sequence_map[type].begin() + idxEdge;
        
        G -> add_edge   (non_edge->first,non_edge->second,G->at(edge->first,edge->second));
        G -> remove_edge(edge    ->first,edge    ->second);
        

        if (type == SequenceType::MutualEdge){
            G -> add_edge   (non_edge->second,non_edge->first,G->at(edge->second,edge->first));
            G -> remove_edge(edge    ->second,edge    ->first);
            
        }
        std::swap(*edge,*non_edge);
    }

    virtual bool sanity_check(const Graph<bool> &G0) override{
        return G0.number_of_mutual_edges() == G->number_of_mutual_edges() 
            && G0.number_of_single_edges() == G->number_of_single_edges() ;
    }
};

#endif