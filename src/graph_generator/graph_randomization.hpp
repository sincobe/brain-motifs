#ifndef graph_randomization_hpp
#define graph_randomization_hpp

#include <task_interface.hpp>
#include <erdos-renyi.hpp>
#include <configuration.hpp>
#include <planted_motif_model.hpp>
#include "../graph_operator/planted_motif_expander.hpp"
#include "../graph_operator/random_supernode_expander.hpp"
#include "graph_randomizer_factory.hpp"



struct GraphRandomization : ITask{
    NullModel model;
    std::size_t number_of_repetitions;
    std::shared_ptr<GraphRandomizer> randomizer;
    std::string output_folder_path;

    GraphRandomization(std::string &input, int job_id = -1) : ITask{input,job_id}{
        set_parameters();
    }

    std::string get_model_name( )const{
        std::vector<std::string> allModels  = cr.j["models"];
        int idx = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/N_networks)) % allModels.size();
        return allModels[idx];
    }

    NullModel get_model_type(const std::string &model_name) const{
        if (model_name == "erdos-renyi"){
            return NullModel::ErdosRenyi;
        }
        else if (model_name == "reciprocal_erdos-renyi"){
            return NullModel::ReciprocalErdosRenyi;
        }
        else if (model_name == "configuration"){
            return NullModel::Configuration;
        }
        else if (model_name == "reciprocal_configuration"){
            return NullModel::ReciprocalConfiguration;
        }
        else if (model_name.find("planted_motif_model") != std::string::npos){
            return NullModel::PlantedMotifModel;
        }
        else{
            std::cout << "Unvalid graph model name" << std::endl;
            return NullModel::ErdosRenyi;
        }
    }

    NullModel get_base_model_subtype(const std::string &model_name) const{
        if (model_name.find("erdos-renyi") != std::string::npos){
            return NullModel::ErdosRenyi;
        }
        else if (model_name.find("reciprocal_erdos-renyi") != std::string::npos){
            return NullModel::ReciprocalErdosRenyi;
        }
        else if (model_name.find("configuration") != std::string::npos){
            return NullModel::Configuration;
        }
        else if (model_name.find("reciprocal_configuration") != std::string::npos){
            return NullModel::ReciprocalConfiguration;
        }
        else{
            std::cout << "Unvalid base graph submodel name" << std::endl;
            return NullModel::ErdosRenyi;
        }
    }

    void set_parameters() override{
        std::string model_str = get_model_name();
        model = get_model_type(model_str);
        number_of_repetitions = cr.j["number_of_repetitions"];
        output_folder_path    = get_output_path();
    }

    // std::string get_algorithm_path(const std::string &algo){
    //     nlohmann::json label = get_dataset_output_root_json();
    //     std::string category    = label["category"];
    //     std::string subcategory = label["subcategory"];
    //     return pf.get_output(algo,category,subcategory);
    // }

    // std::string get_planted_motif_model_inference_folder(){
    //     std::string base = get_algorithm_path("graph_encoding");
    //     std::string inputFolderPath = base + static_cast<std::string>(cr.j["model"]["name"]) + "/" + static_cast<std::string>(cr.j["model"]["base"]) + "/";
    //     return inputFolderPath;
    // }

    // std::string get_graphlet_folder_path(){
    //     return get_algorithm_path("graphlet_census");
    // }

    // std::string get_graph_randomization_path(){
    //     return get_algorithm_path("graph_randomization");
    // }

    // std::string get_planted_motif_model_randomization_folder(){
    //     return get_algorithm_path("graph_randomization") + static_cast<std::string>(cr.j["model"]["name"]) + "/" + static_cast<std::string>(cr.j["model"]["base"]) + "/";
    // }



    // std::string path(){
    //     std::string model    = static_cast<std::string>(cr.j["model"]["name"]);
    //     std::string pathname = get_algorithm_path("graph_randomization") + model + "/";

    //     if (model == "planted_motif_model"){
    //         pathname += static_cast<std::string>(cr.j["model"]["base"]) + "/";
    //     }

    //     if (cr.j.find("graphlet_criterium") != cr.j.end()){
    //         pathname += static_cast<std::string>(cr.j["graphlet_criterium"]) + "/";
    //     }
    //     return pathname;
    // }


    // std::size_t number_of_samples() const override{
    //     return cr.j["model"]["sampling_size"];
    // }

    // inline int dataset_idx() const override{
    //     if (model == NullModel::PlantedMotifModel){
    //         return job_id / number_of_samples() % number_of_datasets();
    //     }
    //     else{
    //         return job_id;
    //     }
        
    // }



    // std::string get_input_planted_motif_inference(){
    //     auto label = get_dataset_output_root_json();
    //     std::string category    = label["category"];
    //     std::string subcategory = label["subcategory"];
    //     std::string base = pf.get_output("graph_encoding",category,subcategory);
    //     std::string PMM_path = "planted_motif_model/" + static_cast<std::string>(cr.j["model"]["base"]) + "/";

    //     if (cr.j.find("graphlet_criterium") != cr.j.end()){
    //         PMM_path += static_cast<std::string>(cr.j["graphlet_criterium"]) + "/";           
    //     }

    //     std::filesystem::path p{base + PMM_path};
    //     auto dir =  std::filesystem::directory_iterator(p);
    //     while (static_cast<std::string>(dir->path().extension()) != ".json"){
    //         ++dir;
    //     }

    //     int ref{};
    //     while (ref != sample_idx()){
    //         ++ref;
    //         ++dir;
        
    //         while (static_cast<std::string>(dir->path().extension()) != ".json"){
    //             ++dir;
    //         }
    //     }
        
    //     return base + PMM_path + static_cast<std::string>(dir->path().filename());

    // }

    template <class T>
    void randomize_and_expand_PMM(T &code){
    
        // auto model_base = get_model_type(cr.j["model"]["base"]);
        std::string model_name = get_model_name();
        auto model_base = get_base_model_subtype(model_name);
        // randomizer      = GraphRandomizerFactory::make(code->G,model_base);
        randomizer      = GraphRandomizerFactory::make(code->G,model_base);
        randomizer->generate();
        auto expander   = RandSupernodeExpander(*code.get());
        expander.expand();
    }

    // template<typename T,class U>
    // std::unique_ptr<PlantedMotifModel<U>> get_planted_motif_representation(const Graph<T> &G, const nlohmann::json &j, const std::string &path){
    //     auto model_base = get_model_type(cr.j["model"]["base"]);
    //     switch(model_base){
    //         case NullModel::ErdosRenyi:    
    //             return std::make_unique<PlantedMotifModel<MultiER>>(G,j,path);
    //             break;
    //         case NullModel::ReciprocalErdosRenyi:    
    //             return std::make_unique<PlantedMotifModel<ReciprocalMultiER>>(G,j,path);
    //             break;
    //         case NullModel::Configuration:    
    //             return std::make_unique<PlantedMotifModel<ConfigurationModel>>(G,j,path);
    //             break;
    //         case NullModel::ReciprocalConfiguration:    
    //             return std::make_unique<PlantedMotifModel<ReciprocalConfigurationModel>>(G,j,path);
    //             break;
    //         default:
    //             return nullptr;
    //             break;
    //     }
    // }



    template<typename T>
    void generate_from_PMM(const Graph<T> &G, nlohmann::json &j, std::string path, TimeStepType t_type = TimeStepType::Optimal){
        std::string model_name = get_model_name();
        // model_name = model_name.substr(model_name.find_last_of("/")+1);
        auto model_base = get_base_model_subtype(model_name);
        switch(model_base){
            case NullModel::ErdosRenyi:{
                auto code = std::make_shared<PlantedMotifModel<MultiER>>(G,j,path,t_type);
                randomize_and_expand_PMM(code);
                break;
            }
            case NullModel::ReciprocalErdosRenyi:{  
                auto code = std::make_shared<PlantedMotifModel<ReciprocalMultiER>>(G,j,path,t_type);
                randomize_and_expand_PMM(code);
                break;
            }
            case NullModel::Configuration:{    
                auto code = std::make_shared<PlantedMotifModel<ConfigurationModel>>(G,j,path,t_type);
                randomize_and_expand_PMM(code);
                break;
            }
            case NullModel::ReciprocalConfiguration:{  
                auto code =std::make_shared<PlantedMotifModel<ReciprocalConfigurationModel>>(G,j,path,t_type);
                randomize_and_expand_PMM(code);
                break;
            }
            default:
                break;
        }
    }

    
    
    nlohmann::json get_inference(){
        std::string inference_folder{ output_folder("graph_encoding") +  get_model_name() };
        // std::cout << inference_folder << std::endl;     

        nlohmann::json inference_best;
        std::string stem;
        double L_best{1.e32};

        for (auto &&dir_entry : std::filesystem::directory_iterator{inference_folder}){
            if (dir_entry.is_regular_file()){
                if (dir_entry.path().extension() == ".json"){
                    std::ifstream file(dir_entry.path());
                    nlohmann::json inference_test;
                    file >> inference_test;
                    double L_test = PlantedGraphletReader<double>::minimum_codelength(inference_test);
                    if (L_test != -1 && L_best > L_test){
                        L_best         = L_test;
                        inference_best = inference_test;
                        stem           = static_cast<std::string>(dir_entry.path().stem());
                    }
                }
            }
        }
        

        // std::size_t number_of_inferences  = static_cast<std::size_t>(cr.j["number_of_inferences"]);
        
        // std::size_t idx_inference  = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/number_of_datasets)) % number_of_inferences ;

        // int idx;
        // for (idx = 0 ; idx < idx_inference ; idx++){
        //     ++dir_it;
        // }
        return inference_best;
    }

    std::filesystem::path get_output_path(){
        std::string pathname(output_folder("graph_randomization") + get_model_name());

        // std::string encoding_pathname(output_folder("graph_encoding"));
        return std::filesystem::path(pathname);
    }

    // std::string get_inference_path() const{
    //     return output_folder_of_real_graph("graph_encoding");
    // }

    // TimeStepType get_time_step_type(){
    //     if (cr.j.find("time_step_type") != cr.j.end()){
    //         if (cr.j["time_step_type"] == "optimal"){
    //             return TimeStepType::Optimal;
    //         }
    //         else{
    //             return TimeStepType::Max;
    //         }
    //     }
    //     else{
    //         return TimeStepType::Max;
    //     }
    // }

    void make() override{

        std::vector<int> adjMat = get_adjacency_matrix();
        
        for (int rep{} ; rep < number_of_repetitions ; rep++){

            Graph<bool> G = {EdgeType::directed, adjMat};

            if (model == NullModel::PlantedMotifModel){
                auto inference = get_inference();
                if (inference.size()){generate_from_PMM(G,inference,output_folder("graphlet_census"));}
            }
            else{
                randomizer = GraphRandomizerFactory::make(G,model);
                randomizer->generate();
                if (!randomizer->sanity_check(G)){ std::cout << "Sanity check failed" << std::endl; }
            }

            if (randomizer != nullptr){
                std::filesystem::create_directories(output_folder_path);
                std::string filename = output_folder_path + "/edge_list_" + get_datetime_string()+".txt";
                std::cout << filename << std::endl;
                randomizer->G->dump_edge_list(filename);
            }
            
        }
    }
};


#endif