#ifndef test_non_null_hpp
#define test_non_null_hpp

#include <task_interface.hpp>
#include <planted_motif_model.hpp>
#include "rand_multiER_pmm_generator.hpp"

struct TestNonNull : ITask{
    
    std::size_t number_of_generations;
    std::size_t N;
    double      density;
    std::string gtrie_label;
    std::string graphlet_alias;
    std::unique_ptr<RandPMMGenerator> generator;

    TestNonNull(std::string &input, int job_id = -1) : ITask{input,job_id}{
        set_parameters();
    }   

    void set_parameters() override{
        number_of_generations                = cr.j["number_of_generations"];
        std::vector<std::size_t> graph_sizes = static_cast<std::vector<size_t>>(cr.j["graph_sizes"]);
        std::vector<double>      densities   = static_cast<std::vector<double>>(cr.j["densities"]); 
        auto graphlets                       =                                  cr.j["graphlets"];

        int idx_graph_size  = job_id % graph_sizes.size();
        int idx_density     = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/graph_sizes.size()))                  % densities.size();
        int idx_gtrie_label = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/graph_sizes.size()/densities.size())) % graphlets.size();

        N              = graph_sizes[idx_graph_size];
        density        = densities  [idx_density];
        gtrie_label    = graphlets  [idx_gtrie_label]["gtrie_label"];
        graphlet_alias = graphlets  [idx_gtrie_label]["alias"];

        // N = static_cast<std::size_t>(cr.j["number_of_nodes"]);
        // E = static_cast<std::size_t>(cr.j["number_of_edges"]);
        // for (auto &&item : cr.j["supernode_distribution"].items()){
        //     supernodeDistribution[item.key()] = static_cast<std::size_t>(item.value());
        // }
    }

    // std::size_t number_of_supernodes() const{
    //     return std::accumulate(supernodeDistribution.begin(),supernodeDistribution.end(),0,
    //         [](std::size_t a, std::pair<std::string,std::size_t> b){
    //             return a + b.second;
    //         });
    // }

    std::string test_non_null_graph_path(const std::size_t &N, const std::size_t &E, const std::size_t &supernodeNumber) const {
        std::string output_rootpathname = pf.get_output_root(cr.j["task"]) + graphlet_alias + "/";

        std::string nodeNumber_pathname      = "number_of_nodes_"      + std::to_string(N) + "/";
        std::string edgeNumber_pathname      = "number_of_edges_"      + std::to_string(E) + "/";
        std::string supernodeNumber_pathname = "number_of_supernodes_" + std::to_string(supernodeNumber) + "/";

        std::string directory_pathname{output_rootpathname + nodeNumber_pathname + edgeNumber_pathname + supernodeNumber_pathname};
        std::filesystem::create_directories(directory_pathname);
        return directory_pathname + "edge_list" + get_datetime_string() + ".txt";
    }

    void make() override{

        std::map<std::string,std::size_t> supernodeDistribution;

        EdgeType dir{EdgeType::directed};
        std::vector<std::shared_ptr<IGraphlet>> graphlets = {std::make_shared<TopologicalGraphlet>(dir,gtrie_label)};
        
        std::size_t E = std::floor(density * N * N);
        std::size_t max_supernode_number = std::floor(std::min(static_cast<double>(E) /  (*graphlets.begin())->number_of_edges(), 
                                                               static_cast<double>(N) / ((*graphlets.begin())->size)));

        supernodeDistribution[gtrie_label] = 0;
        auto it_supernodeDistribution = supernodeDistribution.begin();
        for (std::size_t n{} ; n <= max_supernode_number ; n++){
            it_supernodeDistribution->second = n;
            for (int gen{} ; gen < number_of_generations ; gen++){
                std::string output_path = test_non_null_graph_path(N,E,it_supernodeDistribution->second);
                std::cout << output_path << std::endl;
                generator = std::make_unique<RandMultiERPMMGenerator>(N,E,supernodeDistribution);
                generator->generate_random_graph();
                generator->dump_random_graph(output_path);
            }
        } 
    } 
};

#endif