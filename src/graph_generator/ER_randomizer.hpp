#ifndef ER_randomizer_hpp
#define ER_randomizer_hpp

#include "graph_randomizer.hpp"

struct ERRandomizer : GraphRandomizer{

    template<typename T>
    ERRandomizer(const Graph<T> &_G) : GraphRandomizer{_G}{}  

    ERRandomizer(std::shared_ptr<Graph<int>> &_G) : GraphRandomizer{_G}{}   


    virtual void set_seq() override{
        int i,j;
        for (i = 0 ; i < G->N ; i++){
            for (j = i+1 ; j < G->N ; j++){
                if (!G->at(i,j)){
                    sequence_map[SequenceType::NonEdge].emplace_back(std::make_pair(i,j));
                }

                if (!G->at(j,i) && G->dir == EdgeType::directed){
                    sequence_map[SequenceType::NonEdge].emplace_back(std::make_pair(j,i));
                }
            }
        }

        if (sequence_map[SequenceType::NonEdge].size() > 1){
            randIdx_map[SequenceType::NonEdge] =   std::make_shared<std::uniform_int_distribution<int>>
                                                   (0,sequence_map[SequenceType::NonEdge].size()-1);
        }
    }

    virtual void swap() override{
        int idxEdge    = random_idx(SequenceType::Edge);
        int idxNonEdge = random_idx(SequenceType::NonEdge);
        auto edge      = sequence_map[SequenceType::Edge]   .begin() + idxEdge;
        auto non_edge  = sequence_map[SequenceType::NonEdge].begin() + idxNonEdge;
        
        G->add_edge   (non_edge->first,non_edge->second,G->at(edge -> first, edge -> second));
        G->remove_edge(edge    ->first,edge    ->second);

        std::swap(*edge,*non_edge);    
    }

    virtual bool sanity_check(const Graph<bool> &G0) override{
        return G0.number_of_edges() == G->number_of_edges();
    }


};

#endif