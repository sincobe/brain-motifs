#ifndef graph_randomizer_hpp
#define graph_randomizer_hpp

#include "../data_structure/graph.hpp"
#include <random>
#include <map>

enum class SequenceType{
    Edge,
    NonEdge,
    MutualEdge,
    SingleEdge
};

struct GraphRandomizer{
    std::shared_ptr<std::mt19937> eng_ptr;
    
    std::shared_ptr<Graph<int>> G;
    NullModel model;
    std::size_t N_operations;
    int current_swap;
    std::map<SequenceType,std::vector<std::pair<int,int>>> sequence_map;
    std::map<SequenceType,std::shared_ptr<std::uniform_int_distribution<int>>> randIdx_map;

    template<typename T>
    GraphRandomizer(const Graph<T> &_G) : G{std::make_shared<Graph<int>>(_G)}{initial_set();}
    GraphRandomizer(std::shared_ptr<Graph<int>> &_G) : G{_G}{initial_set();}

    void generate(){
        set_seq();
        for (int rep = 0 ; rep < N_operations ; rep++){
            swap();
            current_swap++;

        }
        // std::cout << G->number_of_edges() << " " << G->number_of_active_nodes() << std::endl;
    }

    virtual void set_seq() {}
    virtual void swap() = 0;
    virtual bool sanity_check(const Graph<bool> &_G) = 0;
    int random_idx(const SequenceType &type) const{
        return (*(randIdx_map.find(type)->second.get()))(*eng_ptr.get());
    }
    inline void set_operation_number(const std::size_t &n){
        N_operations = n;
    }

    void initial_set(){
        int i,j;
        for (i = 0 ; i < G->sizeMat ; i++){
            for (j = i+1 ; j < G->sizeMat ; j++){
                if(G->at(i,j)){
                    sequence_map[SequenceType::Edge].emplace_back(std::make_pair(i,j));
                }
                if (G->dir == EdgeType::directed){
                    if (G->at(j,i)){
                        sequence_map[SequenceType::Edge].emplace_back(std::make_pair(j,i));
                    }
                }
            }
        }

        if (sequence_map[SequenceType::Edge].size() > 1){
            randIdx_map[ SequenceType::Edge] =   std::make_shared<std::uniform_int_distribution<int>>
                                                (0,sequence_map[SequenceType::Edge].size()-1);
        }
        
        current_swap = 0;
        N_operations =  100 * G->number_of_edges();  //arbitrary
        std::random_device seeder;
        const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        eng_ptr = std::make_shared<std::mt19937>(static_cast<std::mt19937::result_type>(seed));
    }
};

#endif