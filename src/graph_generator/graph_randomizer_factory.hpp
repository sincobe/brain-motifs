#ifndef graph_randomizer_factory_hpp
#define graph_randomizer_factory_hpp

#include "ER_randomizer.hpp"
#include "RER_randomizer.hpp"
#include "configuration_randomizer.hpp" 
#include "reciprocal_configuration_randomizer.hpp"

struct GraphRandomizerFactory{
    template<class T>
    static std::shared_ptr<GraphRandomizer> make(T &G, const NullModel &model){
        switch (model)
        {
        case NullModel::ErdosRenyi:
            return std::make_shared<ERRandomizer>(G);
            break;
        case NullModel::ReciprocalErdosRenyi: 
            return std::make_shared<RERRandomizer>(G);
            break;
        case NullModel::Configuration:  
            return std::make_shared<ConfigurationRandomizer>(G); 
            break;
        case NullModel::ReciprocalConfiguration:
            return std::make_shared<ReciprocalConfigurationRandomizer>(G);  
            break;

        default:{
            std::cout << "GraphRandomizerFactory: Unvalid model \n";
            return nullptr;
            break;
        }
            
        }
    }

};

#endif