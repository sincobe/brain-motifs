#ifndef rand_multiER_pmm_generator_hpp
#define rand_multiER_pmm_generator_hpp 

#include "rand_planted_motif_model.hpp"

// struct RandMultiERPMMGenerator : RandPMMGenerator{

//     std::size_t E;
//     RandMultiERPMMGenerator(const std::size_t &edgeNumber, const std::size_t &nodeNumber, const std::map<std::string,std::size_t> &supernodeDistribution, EdgeType dir = EdgeType::directed) : 
//     RandPMMGenerator(nodeNumber,supernodeDistribution,dir),E{edgeNumber}{}

//     void generate_reduced_graph() override{

//         std::uniform_int_distribution<int> randIdx(0,H->number_of_active_nodes()-1);
//         std::size_t current_E{};

//         int i,j;
//         while (current_E != E){
//             i = randIdx(*(eng_ptr.get()));
//             j = randIdx(*(eng_ptr.get()));
//             if (i!=j){
//                 H->add_edge(i,j,1);
//                 current_E++;
//             }
//         }
//     }
// };

struct RandMultiERPMMGenerator : RandPMMGenerator{

    std::size_t E;
    std::size_t hiddenE;
    RandMultiERPMMGenerator(const std::size_t &nodeNumber, const std::size_t &edgeNumber, const std::map<std::string,std::size_t> &supernodeDistribution, EdgeType dir = EdgeType::directed) : 
    RandPMMGenerator(nodeNumber,supernodeDistribution,dir),E{edgeNumber},hiddenE{}{
        for (auto &&[g,n_g] : graphlet_map){
            hiddenE += n_g * g->number_of_edges();
        }

        if (hiddenE > E){
            std::cout << "RandMultiERPMMGenerator: The desired number of edges of the expanded graph is less than the number of contracted (hidden) edges. Start over with a good combination." << std::endl;
        }
    }

    void generate_reduced_graph() override{

        std::uniform_int_distribution<int> randIdx(0,H->number_of_active_nodes()-1);
        std::size_t current_E{hiddenE};

        int i,j;
        while (current_E != E){
            i = randIdx(*(eng_ptr.get()));
            j = randIdx(*(eng_ptr.get()));
            if (i!=j){
                H->add_edge(i,j,1);
                current_E++;
            }
        }
    }
};

#endif