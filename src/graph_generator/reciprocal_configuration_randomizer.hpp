#ifndef reciprocal_configuration_randomizer_hpp
#define reciprocal_configuration_randomizer_hpp

#include "configuration_randomizer.hpp"

struct ReciprocalConfigurationRandomizer : ConfigurationRandomizer{
    std::unique_ptr<std::uniform_real_distribution<double>> p;

    template<typename T>
    ReciprocalConfigurationRandomizer(const Graph<T> &_G) : ConfigurationRandomizer{_G},p{std::make_unique<std::uniform_real_distribution<double>>(0.,1.)}{G->remove_multiedges();} 

    ReciprocalConfigurationRandomizer(std::shared_ptr<Graph<int>> &_G) : ConfigurationRandomizer{_G},p{std::make_unique<std::uniform_real_distribution<double>>(0.,1.)}{G->remove_multiedges();}  


    void set_seq() override{
        int i,j;
        for (i = 0 ; i < G->N ; i++){
            for (j = i+1 ; j < G->N ; j++){
                int a_ij = G->at(i,j);
                int a_ji = G->at(j,i);
                if (a_ij && a_ji){
                    sequence_map[SequenceType::MutualEdge].emplace_back(std::make_pair(i,j));
                    if (a_ij > a_ji){
                        sequence_map[SequenceType::SingleEdge].emplace_back(std::make_pair(i,j));
                    }
                    else if (a_ij != a_ji){
                        sequence_map[SequenceType::SingleEdge].emplace_back(std::make_pair(j,i));
                    }
                }

                else if (a_ij && !a_ji){
                    sequence_map[SequenceType::SingleEdge].emplace_back(std::make_pair(i,j));
                }

                else if (!a_ij && a_ji){
                    sequence_map[SequenceType::SingleEdge].emplace_back(std::make_pair(j,i));
                }


            }
        }



        randIdx_map[SequenceType::MutualEdge] = std::make_shared<std::uniform_int_distribution<int>>
                                                (0,sequence_map[SequenceType::MutualEdge].size()-1);

        randIdx_map[SequenceType::SingleEdge] = std::make_shared<std::uniform_int_distribution<int>>
                                                (0,sequence_map[SequenceType::SingleEdge].size()-1);

    }

    double coin(){
        return std::move((*p.get())(*eng_ptr.get()));
    }

    void swap() override{
        int idxEdgeA,idxEdgeB,a_ij,a_ji,a_kl,a_lk,value{1};
        std::vector<std::pair<int,int>>::iterator edgeA,edgeB;
        


        SequenceType type;
        if (coin() < .5){
            type = SequenceType::SingleEdge;
        }
        else{
            type = SequenceType::MutualEdge;
        }

        while (1){
            idxEdgeA = random_idx  (type);
            idxEdgeB = random_idx  (type);
            edgeA    = sequence_map[type].begin() + idxEdgeA;
            edgeB    = sequence_map[type].begin() + idxEdgeB;

            a_ij = G->at(edgeA->first,edgeA->second);
            a_ji = G->at(edgeA->second,edgeA->first);
            a_kl = G->at(edgeB->first,edgeB->second);
            a_lk = G->at(edgeB->second,edgeB->first);
            bool stop_sampling  =   idxEdgeA != idxEdgeB && 
                                    edgeA -> first != edgeB -> second &&
                                    edgeB -> first != edgeA -> second &&
                                    !G->at(edgeA -> first,edgeB -> second) && 
                                    !G->at(edgeB -> first,edgeA -> second) &&
                                    !G->at(edgeA -> second,edgeB -> first) && 
                                    !G->at(edgeB -> second,edgeA -> first) ;
                                    
                                    
            // switch(type){
            //     case SequenceType::SingleEdge:{
            //         stop_sampling = stop_sampling && (a_ij - a_ji == a_kl - a_lk);
            //         break;
            //     }
            //     case SequenceType::MutualEdge:{
            //         stop_sampling = stop_sampling && (std::min(a_ij,a_ji) == std::min(a_lk,a_kl));
            //         break;
            //     }
            //     default:
            //     break;
            // }



            if (stop_sampling){
                break;
            }
        }

        // switch(type){
        //     case SequenceType::SingleEdge:{
        //         value = a_ij - a_ji;
        //         break;
        //     }
        //     case SequenceType::MutualEdge:{
        //         value = std::min(a_ij,a_ji);
        //         break;
        //     }
        //     default:
        //     break;
        // }


        G->edge_swap(*edgeA,*edgeB,value);
        

        if (type == SequenceType::MutualEdge){
            std::swap(edgeA->first,edgeA->second);
            std::swap(edgeB->first,edgeB->second);
            G->edge_swap(*edgeA,*edgeB,value);

        }
        std::swap(edgeA->second,edgeB->second);

    }

    virtual bool sanity_check(const Graph<bool> &G0) override{
        return G0.get_degree_seq(Neighborhood::single_in)  == G->get_degree_seq(Neighborhood::single_in) 
            && G0.get_degree_seq(Neighborhood::single_out) == G->get_degree_seq(Neighborhood::single_out)
            && G0.get_degree_seq(Neighborhood::mutual)     == G->get_degree_seq(Neighborhood::mutual);
    }

};

#endif