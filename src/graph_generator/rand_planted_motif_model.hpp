#ifndef rand_planted_motif_model_hpp
#define rand_planted_motif_model_hpp

#include "../encoding/planted_motif_model.hpp"

// struct RandPMMGenerator{
//     std::shared_ptr<std::mt19937> eng_ptr;
//     std::shared_ptr<Graph<int>> H;
//     std::map<std::shared_ptr<IGraphlet>,std::size_t> graphlet_map;
//     std::unique_ptr<RandSupernodeExpander> expander;

//     RandPMMGenerator(const std::size_t &reducedGraphSize, const std::map<std::string,std::size_t> &supernodeDistribution, EdgeType dir = EdgeType::directed){
        
//         //Check the validity of the supernode distribution given the input graph size
//         std::size_t supernodeNumber{};
//         std::size_t expandedGraphSize{reducedGraphSize};
//         for (auto &&item : supernodeDistribution){
//             supernodeNumber   += item.second;
//             expandedGraphSize += (TopologicalGraphlet::size_from_gtrie_label(item.first) - 1) * item.second;
//         }

//         if (supernodeNumber > reducedGraphSize){
//             std::cout << "The wanted number of supernodes exceeds the input graph size. Start again with a valid combination." << std::endl;
//         }
//         H = std::make_shared<Graph<int>>(expandedGraphSize,dir);

//         int start_id{},excess_id{static_cast<int>(reducedGraphSize)},i{};
//         std::vector<int> subg{};
//         for (auto &&item : supernodeDistribution){  
//             auto g = std::make_shared<TopologicalGraphlet>(dir,item.first);
//             graphlet_map[ g ] = item.second;

//             while (i != start_id + item.second){

//                 subg.resize(g->size); 
//                 subg[0] = i;
//                 auto it = subg.begin() + 1;
//                 while(it != subg.end()){
//                     *it = excess_id;
//                     H->nodes[*it]->switch_activity();
//                     ++excess_id;
//                     ++it;
//                 }

//                 switch(H->dir){
//                     case EdgeType::directed:
//                         H->nodes[i] = std::make_shared<Supernode<DiNode>>(item.first,subg,H->nodes[i]);
//                         break;
//                     case EdgeType::undirected:
//                         H->nodes[i] = std::make_shared<Supernode<Node>>  (item.first,subg,H->nodes[i]);
//                         break;
//                     default:
//                         break;
//                 }
                
//                 ++i;
//                 subg.clear();
//             }  
//             start_id = i;
            
//         }

//         const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
//         eng_ptr = std::make_shared<std::mt19937>(static_cast<std::mt19937::result_type>(seed));

//         expander = std::make_unique<RandSupernodeExpander>(H,graphlet_map);
//     }

//     virtual void generate_reduced_graph() = 0;
//     void generate_random_graph(){
//         generate_reduced_graph();
//         expander->expand();
//     }
//     void dump_random_graph(const std::string &output_pathname){
//         H->dump_edge_list(output_pathname);
//     }
// };

struct RandPMMGenerator{
    std::shared_ptr<std::mt19937> eng_ptr;
    std::shared_ptr<Graph<int>> H;
    std::map<std::shared_ptr<IGraphlet>,std::size_t> graphlet_map;
    std::unique_ptr<RandSupernodeExpander> expander;

    RandPMMGenerator(const std::size_t &expandedGraphSize, const std::map<std::string,std::size_t> &supernodeDistribution, EdgeType dir = EdgeType::directed){
        
        //Check the validity of the supernode distribution given the input graph size
        std::size_t supernodeNumber{};
        std::size_t reducedGraphSize{expandedGraphSize};
        for (auto &&item : supernodeDistribution){
            supernodeNumber   += item.second;
            reducedGraphSize  -= (TopologicalGraphlet::size_from_gtrie_label(item.first) - 1) * item.second;
        }

        if (supernodeNumber > reducedGraphSize){
            std::cout << "The wanted number of supernodes exceeds the input graph size. Start again with a valid combination." << std::endl;
        }
        H = std::make_shared<Graph<int>>(expandedGraphSize,dir);

        int start_id{},excess_id{static_cast<int>(reducedGraphSize)},i{};
        std::vector<int> subg{};
        for (auto &&item : supernodeDistribution){  
            auto g = std::make_shared<TopologicalGraphlet>(dir,item.first);
            graphlet_map[ g ] = item.second;

            while (i != start_id + item.second){

                subg.resize(g->size); 
                subg[0] = i;
                auto it = subg.begin() + 1;
                while(it != subg.end()){
                    *it = excess_id;
                    H->nodes[*it]->switch_activity();
                    ++excess_id;
                    ++it;
                }

                switch(H->dir){
                    case EdgeType::directed:
                        H->nodes[i] = std::make_shared<Supernode<DiNode>>(item.first,subg,H->nodes[i]);
                        break;
                    case EdgeType::undirected:
                        H->nodes[i] = std::make_shared<Supernode<Node>>  (item.first,subg,H->nodes[i]);
                        break;
                    default:
                        break;
                }
                
                ++i;
                subg.clear();
            }  
            start_id = i;
            
        }

        const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        eng_ptr = std::make_shared<std::mt19937>(static_cast<std::mt19937::result_type>(seed));

        expander = std::make_unique<RandSupernodeExpander>(H,graphlet_map);
    }

    virtual void generate_reduced_graph() = 0;
    void generate_random_graph(){
        generate_reduced_graph();
        expander->expand();
    }
    void dump_random_graph(const std::string &output_pathname){
        H->dump_edge_list(output_pathname);
    }
};


#endif