#ifndef subgraph_contractor_hpp
#define subgraph_contractor_hpp


#include "../data_structure/graph.hpp"
#include "../data_structure/supernode.hpp"
#include <algorithm>

struct SubgraphContractor{

    std::shared_ptr<INode> supernode;
    std::vector<int> S;
    template<typename T>
    SubgraphContractor(std::shared_ptr<Graph<T>> &G, const std::string &label, const std::vector<int> &subg) : 
    supernode{G->nodes[*subg.begin()]},S{subg}{
        
        remove_subg_edges(G,subg);
        for (auto &&i : subg){
            collapse_node(G,i);
        }

        switch(G->dir){
            case EdgeType::directed:
                G->nodes[supernode->id] = std::make_shared<Supernode<DiNode>>(label,subg,supernode);
                break;
            case EdgeType::undirected:
                G->nodes[supernode->id] = std::make_shared<Supernode<Node>>  (label,subg,supernode);
                break;
            default:
                break;
        }
    }

    template<typename T>
    void collapse_node(std::shared_ptr<Graph<T>> &G,int nodeId){

        if (nodeId != supernode->id){
            auto node = G->nodes[nodeId];
            supernode -> merge_neighborhood(G->nodes[nodeId]);
            for (auto &&n : node->get_neighborhood()){

                
                G->nodes[n]->replace_in_neighborhood(node,supernode);

                G->at(n,supernode->id) += G->at(n,nodeId);  
                G->at(supernode->id,n) += G->at(nodeId,n);    

                G->at(n,nodeId)        = 0;
                G->at(nodeId,n)        = 0;                
            }

            
            node      -> switch_activity();
        }
    }

    template<typename T>
    void remove_subg_edges(std::shared_ptr<Graph<T>> &G, const std::vector<int> &subg){
        for (auto &&i : subg){
            for (auto &&j : subg){
                G -> remove_edge(i,j);
            }
        }
    }


};

#endif