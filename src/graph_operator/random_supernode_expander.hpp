#ifndef random_supernode_expander_hpp
#define random_supernode_expander_hpp

#include <random>
#include "supernode_expander.hpp"
#include "../encoding/planted_motif_model.hpp"


struct RandSupernodeExpander{

    std::unique_ptr<std::mt19937> eng_ptr;
    std::shared_ptr<Graph<int>> G;
    std::map<std::string,std::shared_ptr<IGraphlet>> graphlet_map;
    template<class T>
    RandSupernodeExpander(const PlantedMotifModel<T>&code) : G{code.G}{
        for (auto &&item : code.get_graphlet_map()){
            auto g = item.second->g;
            graphlet_map[item.first] = g;
        }

        // std::random_device seeder;
        const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        eng_ptr = std::make_unique<std::mt19937>(static_cast<std::mt19937::result_type>(seed));
    }

    template<class T>
    RandSupernodeExpander(std::shared_ptr<Graph<int>> &_G, std::map<std::shared_ptr<IGraphlet>,T> &map) : G{_G}{
        for (auto &&item : map){
            auto g = item.first;
            graphlet_map[g->gtrie_label] = g;
        }

        // std::random_device seeder;
        const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        eng_ptr = std::make_unique<std::mt19937>(static_cast<std::mt19937::result_type>(seed));
    }


    void expand(){
        for (auto &node : G->nodes){
            if (node -> state == NodeState::supernode){
                rewire_regular_neighborhood(node->id);
                insert_subg(node->id);
            }
        }

        for (auto &nodeA : G->nodes){
            if (nodeA -> state == NodeState::supernode){
                for (auto &nodeB : G->nodes){
                    if (nodeB -> state == NodeState::supernode){
                        
                        rewire_supernode_pair(nodeA->id,nodeB->id);

                    }
                }
                nodeA -> switch_activity();
            }
            else if (nodeA -> state == NodeState::inactive){
                nodeA ->switch_activity();
            }
        }
    }

    void insert_subg(const int &supernodeId){

        auto subg =              G->nodes[supernodeId]->get_subg();
        auto g    = graphlet_map[G->nodes[supernodeId]->get_label()];
       
        std::shuffle(subg.begin(),subg.end(),*eng_ptr.get());

        int i,j;
        for (i = 0 ; i < subg.size() ; i++){
            for (j = 0 ; j < subg.size() ; j++){
                if (g->at(i,j)){
                    G->add_edge(subg[i],subg[j],1);
                }
            }
        }
    }

    void rewire_regular_neighborhood(const int &supernodeId){

        auto supernode = G->nodes[supernodeId];
        auto subg = supernode->get_subg();
        int l;
        switch (G->dir)
        {
        case EdgeType::undirected:{
            auto neighborhood = supernode->get_neighborhood();
            std::unique(neighborhood.begin(),neighborhood.end());
            for (auto &&j : neighborhood){
                auto neighbor = G->nodes[j];
                if (neighbor->state == NodeState::active){
                    auto a_ij = G->at(supernodeId,j);
                    G->remove_edge(supernodeId,j);
                    std::shuffle(subg.begin(),subg.end(),*eng_ptr.get());
                    for (l = 0 ; l < std::min({a_ij,static_cast<int>(subg.size())}) ; l++){
                        G->add_edge(subg[l],j,1);
                    }
                }
            }
            break;
        }

        case EdgeType::directed:{
            auto neighborhood_out = supernode->get_neighborhood(Neighborhood::out);
            std::sort(neighborhood_out.begin(),neighborhood_out.end());
            auto last_out = std::unique(neighborhood_out.begin(),neighborhood_out.end());
            neighborhood_out.erase(last_out,neighborhood_out.end());
            for (auto &&j : neighborhood_out){
                auto neighbor_out = G->nodes[j];
                if (neighbor_out->state == NodeState::active){
                    int a_ij = G->at(supernodeId,j);
                    G->remove_edge(supernodeId,j);
                    std::shuffle(subg.begin(),subg.end(),*eng_ptr.get());
                    for (l = 0 ; l < std::min({a_ij,static_cast<int>(subg.size())}) ; l++){
                        G->add_edge(subg[l],j,1);
                    }
                }
            }

            auto neighborhood_in = supernode->get_neighborhood(Neighborhood::in);
            std::sort(neighborhood_in.begin(),neighborhood_in.end());
            auto last_in = std::unique(neighborhood_in.begin(),neighborhood_in.end());
            neighborhood_in.erase(last_in,neighborhood_in.end());
            for (auto &&j : neighborhood_in){
                auto neighbor_in = G->nodes[j];
                if (neighbor_in->state == NodeState::active){
                    int a_ji = G->at(j,supernodeId);
                    G->remove_edge(j,supernodeId);
                    std::shuffle(subg.begin(),subg.end(),*eng_ptr.get());
                    for (l = 0 ; l < std::min({a_ji,static_cast<int>(subg.size())}) ; l++){
                        G->add_edge(j,subg[l],1);
                    }
                }
            }
            break;
        }
        default:
            break;
        }
        


    }

    void rewire_supernode_pair(const int &supernodeIdA, const int &supernodeIdB){
        auto a_ij = G->at(supernodeIdA,supernodeIdB);
        auto a_ji = G->at(supernodeIdB,supernodeIdA);

        if ((a_ij || a_ji)){
            auto supernodeA = G->nodes[supernodeIdA];
            auto supernodeB = G->nodes[supernodeIdB];
            auto subgA = supernodeA -> get_subg();
            auto subgB = supernodeB -> get_subg();
            std::vector<std::pair<int,int>> node_pairs;

            
            G->remove_edge(supernodeIdA,supernodeIdB);
            G->remove_edge(supernodeIdB,supernodeIdA);
            for (auto &&i : subgA){
                for (auto &&j : subgB){
                    node_pairs.emplace_back(std::make_pair(i,j));
                }
            }

            std::shuffle(node_pairs.begin(),node_pairs.end(),*eng_ptr.get());

            int l;
            for (l = 0 ; l < std::min({a_ij,static_cast<int>(subgA.size()) * static_cast<int>(subgB.size())}) ; l++){
                G->add_edge(node_pairs[l].first,node_pairs[l].second,1);
            }

            if (G->dir == EdgeType::directed){            
                std::shuffle(node_pairs.begin(),node_pairs.end(),*eng_ptr.get());
                for (l = 0 ; l < std::min({a_ji,static_cast<int>(subgA.size()) * static_cast<int>(subgB.size())}) ; l++){
                    G->add_edge(node_pairs[l].second,node_pairs[l].first,1);
                }
            }
        }
    }
};

#endif