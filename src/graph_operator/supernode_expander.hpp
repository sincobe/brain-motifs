#ifndef supernode_expander_hpp 
#define supernode_expander_hpp

#include "graph_contractor.hpp"

struct SupernodeExpander{

    template<typename T, typename U>
    SupernodeExpander(const int &supernodeId, std::shared_ptr<Graph<T>> &H, const Graph<U> &G0){
        auto supernode = H->nodes[supernodeId];
        if (supernode->state != NodeState::supernode){
            std::cout << "SupernodeExpander: This node does not correspond to a supernode and thus cannot be expanded \n";
        }
        else{

            auto subg = supernode->get_subg();
            remove_supernode_edges(supernodeId,H);

            for (auto &&i : subg){
                if (i != supernodeId){
                    H->nodes[i]->switch_activity();
                }
            }
            restore_subg_edges(subg,H,G0);

        }
    }

    template<typename T>
    void remove_supernode_edges(const int &supernodeId, std::shared_ptr<Graph<T>> &H){
        for (auto &&j : H->nodes[supernodeId]->get_neighborhood()){
            H -> remove_edge(supernodeId,j);
            H -> remove_edge(j,supernodeId);
        }
        switch(H->dir){
            case EdgeType::directed:
                H->nodes[supernodeId] = std::make_shared<DiNode>(supernodeId);
                break;
            case EdgeType::undirected:
                H->nodes[supernodeId] = std::make_shared<Node>  (supernodeId);
                break;
            default:
                break;
        }
    }
    
    template<typename T, typename U>
    void restore_subg_edges(const std::vector<int> &subg, std::shared_ptr<Graph<T>> &H, const Graph<U> &G0){
        for (auto &&i : subg){
            switch (G0.dir)
            {
                case EdgeType::undirected:{
                    for (auto &&j : G0.nodes[i]->get_neighborhood()){
                        H->add_edge(i,j,static_cast<T>(G0.at(i,j)));
                    }
                    break;
                }

                case EdgeType::directed:{
                    for (auto &&j : G0.nodes[i]->get_neighborhood(Neighborhood::out)){
                        if (!H->at(i,j)){                        
                            H->add_edge(i,j,static_cast<T>(G0.at(i,j)));
                        }
                    }

                    for (auto &&j : G0.nodes[i]->get_neighborhood(Neighborhood::in)){
                        if (!H->at(j,i)){
                            H->add_edge(j,i,static_cast<T>(G0.at(j,i)));
                        }
                    }
                    break;
                }

                default:
                    break;
            }

        }
    }
};



#endif