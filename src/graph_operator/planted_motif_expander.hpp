#ifndef planted_motif_expander_hpp
#define planted_motif_expander_hpp

#include "supernode_expander.hpp"


struct PlantedMotifExpander{

    template<typename U>
    static void expand(Code<int> &code,const Graph<U> &G0){
        for (auto &node : code.G->nodes){
            if (node->state == NodeState::supernode){
                SupernodeExpander(node->id,code.G,G0);
            }
        }
    }

};

#endif