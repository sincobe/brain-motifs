#ifndef planted_graphlet_compresser_hpp
#define planted_graphlet_compresser_hpp 



// #include "integer_sequence.hpp"
#include "graph_compression.hpp"
namespace mpi = boost::mpi;

enum class GraphletCriterium{ None, LowDens, HighAut };


template<class T>
struct PlantedGraphletCompresser : GraphCompresser<T>{

    MinibatchStrategy strat;
    std::size_t B;
    std::vector<bool> overlap;


    
    int n_contractions;
    std::size_t N_graphlets;

    mpi::communicator world;


    PlantedGraphletCompresser( std::shared_ptr<Graph<int>> &_G) :
    GraphCompresser<T>(_G),n_contractions{}{
        SurveyorFactory::make(this->surveyor,GraphOperationType::SubgContraction,_G);
        overlap.resize(this->G->sizeMat);
    }


    PlantedGraphletCompresser(  std::shared_ptr<Graph<int>> &_G, 
                                const std::string &folderPath,
                                const nlohmann::json &j, 
                                std::vector<NullModel> _models = {}) : 
    GraphCompresser<T>(_G,_models), n_contractions{}{

        B = j["minibatch_strategy"]["size"];
        bool activate_chunk_reading;
        if (j["minibatch_strategy"].find("chunk_reading") != j["minibatch_strategy"].end()){
            activate_chunk_reading = static_cast<bool>(j["minibatch_strategy"]["chunk_reading"]);
        }
        else{
            activate_chunk_reading = false;
        }
         
        switch (static_cast<int>(j["minibatch_strategy"]["id"]))
        {
        case 0:
            strat = MinibatchStrategy::uniform;
            break;
        case 1: 
            strat = MinibatchStrategy::equilibrated;
            break;
        default:
            break;
        }
        
        SurveyorFactory::make(this->surveyor,GraphOperationType::SubgContraction,_G);

        overlap.resize(this->G->sizeMat);

        std::ifstream gdist_stream{folderPath+"gdist.json"};
        nlohmann::json gdist;
        gdist_stream >> gdist;
        gdist_stream.close();
    
        N_graphlets = gdist["graphlets"].size();

        auto criterium = get_graphlet_criterium(j);
        int idx_g{};

        switch (criterium){
            case GraphletCriterium::None:{
                for (auto &&[label,g] : gdist["graphlets"].items()){
                    add_graphlet(label,g,gdist,folderPath,idx_g,activate_chunk_reading);
                    ++idx_g;
                }
            break;
            }
            default:{
                auto label = find_graphlet_label(criterium,gdist["graphlets"]);
                add_graphlet(label,gdist["graphlets"][label],gdist,folderPath,idx_g,activate_chunk_reading);
            break;
            }
        }
        this->surveyor->L.additional = PositiveIntegerCodelength(this->number_of_graphlets()).get() + 1.;
    }

    inline std::size_t number_of_graphlets() const override{
        return N_graphlets;
    }
    
    GraphletCriterium get_graphlet_criterium(const nlohmann::json &j){
        if (j.find("graphlet_criterium") != j.end()){
            if (j["graphlet_criterium"] == "lowest_density"){
                return GraphletCriterium::LowDens;
            }
            else if (j["graphlet_criterium"] == "highest_automorphism_group_size"){
                return GraphletCriterium::HighAut;
            }
            else{
                std::cout << "Invalid graphlet criterium." << std::endl;
                return GraphletCriterium::None;
            }
        }
        else{
            return GraphletCriterium::None;
        }
    }

 
    std::string find_graphlet_label(const GraphletCriterium &criterium, const nlohmann::json &graphlets){

        auto graphlet_density = [](const nlohmann::json &g){
            auto gtrie_label = g["gtrie_label"];
            return static_cast<double>(std::count(gtrie_label.begin(),gtrie_label.end(),'1')) / std::pow(static_cast<double>(g["number_of_nodes"]),2);
        };

        switch (criterium)
        {
        case GraphletCriterium::HighAut:{

            std::map<std::string, int> autSize_map; 
            for (auto &[label,g] : graphlets.items()){
                autSize_map[label] = static_cast<int>(g["automorphism_group_size"]);
            }

            return std::max_element(autSize_map.begin(),autSize_map.end(),
            [](const std::pair<std::string,int> &autSize_a, const std::pair<std::string,int> &autSize_b){
                return autSize_a.second < autSize_b.second;
            })->first;
            break;
        }
        case GraphletCriterium::LowDens:{

            std::map<std::string, double> density_map; 
            for (auto &[label,g] : graphlets.items()){
                density_map[label] = graphlet_density(g);
            }

            return std::min_element(density_map.begin(),density_map.end(),
            [&graphlet_density](const std::pair<std::string,double> &density_a,  const std::pair<std::string,double> &density_b){
                return density_a < density_b;
            }) -> first; 
            break;
        }
        
        default:
            return "";
            break;
      
        }

    }


    void add_graphlet(const std::string &label, const nlohmann::json &g, const nlohmann::json &gdist, const std::string &folderPath, const int &idx_g, bool activate_chunk_reading=false){
        
        // std::cout << "Adding graphlet of label: " << label << std::endl;
        std::string filename = folderPath+"store/"+label+".bin";
        std::string key      = g["gtrie_label"];
        std::shared_ptr<GraphletComponent> gc;

        if (idx_g % world.size() == world.rank()){
            switch(strat){
                case MinibatchStrategy::uniform: 
                    gc = std::make_shared<GraphletComponent>(B,filename,g,activate_chunk_reading);
                    break; 
                case MinibatchStrategy::equilibrated:
                    gc = std::make_shared<GraphletComponent>(B,gdist["total_number_of_subgraphs"],filename,g,activate_chunk_reading);
                    break;
                default: 
                    break;
            }
        }
        else{
            gc = std::make_shared<GraphletComponent>(g);
        }
        
        this->graphlet_map[key] = gc;
        this->surveyor -> add_graphlet_param(gc->g);
    }

    bool check_subgraph_validity(const std::vector<int> &subg) const override{
        bool valid_subg{true};
        // std::cout << " subg has size: " << subg.size() << std::endl;
        for (auto &&i : subg){
            // std::cout << i << " ";
            if (this->overlap[i]){
                valid_subg = false;
                break;
            }
        }
        // std::cout << std::endl;
        return valid_subg;
    }

    void update_constraint(const std::vector<int> &subg) override{
        for (auto &&i : subg){
            overlap[i] = true;
        }
    }

    void update_tracker(std::shared_ptr<GraphletComponent> &gc) override{
        nlohmann::json j;
        auto g    = gc->get_graphlet();
        auto subg = gc->get_subg();
        j["graphlet_label"] = g->label;
        j["subgraph"] = subg;
        j["neighborhood"] = this->surveyor->G->get_subg_neighborhood(subg);
        j["codelength"] = this->surveyor->L.get_json("method");
        this->tracker->add_json(j,gc);

    }

    Codelength subg_score(const std::string &key, const std::vector<int> &subg) const override{
        if (subg.size()){
            return this->surveyor->get_codelength_difference(key,subg);
        }
        else{
            return Codelength(-std::numeric_limits<double>::infinity(),-std::numeric_limits<double>::infinity());
        }
    }

    // static void update_parameters(std::shared_ptr<CompressionMeasure<MultiER>> &surveyor, std::shared_ptr<GraphletComponent> &gc){
    //     ParamUpdater::MultiER::update(surveyor,gc);
    // }

    // static void update_parameters(std::shared_ptr<CompressionMeasure<ConfigurationModel>> &surveyor, std::shared_ptr<GraphletComponent> &gc){
    //     ParamUpdater::ConfigurationModel::update(surveyor,gc);
    // }

    // static void update_parameters(std::shared_ptr<CompressionMeasure<ReciprocalConfigurationModel>> &surveyor, std::shared_ptr<GraphletComponent> &gc){
    //     ParamUpdater::ReciprocalConfigurationModel::update(surveyor,gc);
    // }

    // static void update_parameters(std::shared_ptr<CompressionMeasure<ReciprocalMultiER>> &surveyor, std::shared_ptr<GraphletComponent> &gc){
    //     ParamUpdater::ReciprocalMultiER::update(surveyor,gc);
    // }

    std::shared_ptr<GraphletComponent> select_among_processes(std::shared_ptr<GraphletComponent> &gc){

        
        CandidatePair cp{gc->g->gtrie_label,gc->candidate};

        CandidatePair new_cp;
        all_reduce(world,cp,new_cp, mpi::maximum<CandidatePair>());

        auto gc_selected = this->graphlet_map[new_cp.gtrie_label];
        gc_selected->candidate = new_cp.candidate;
        return gc_selected;
    }

    void compress() override{
        auto t0 = std::chrono::high_resolution_clock::now();
        if (this->tracker != nullptr && !world.rank()){ this->tracker->set_reference_model(this->surveyor);}

        while (1){
            auto gc_selected = this->subg_selection();

            if (world.size() > 1){
                auto gc_selected_global = select_among_processes(gc_selected); 
                gc_selected = gc_selected_global;
            }
            
            
            if (!gc_selected->valid_subg()){
                break;
            }
            else{
                
                // for (auto &&i : gc_selected->candidate.first){
                //     std::cout << i << " ";
                // }
                // std::cout << std::endl;
                // if (this->tracker != nullptr){ this->tracker->update_measures(gc_selected); }

                n_contractions++;
                
                update_constraint(gc_selected->get_subg());
                SubgraphContractor(this->G,gc_selected->g->gtrie_label,gc_selected->get_subg());
                this->surveyor->update_param(gc_selected);
                if (this->tracker != nullptr && !world.rank()){ this->update_tracker(gc_selected); }

                // auto code = MultiER(this->G);
                // auto code = ReciprocalConfigurationModel(this->G);

                // std::cout << code.paramEdge  [TypeParameter::EdgeNumber][0] << " " << this->surveyor->paramEdge  [TypeParameter::EdgeNumber][0] << std::endl;

                // std::cout <<"Ref Mutual Edges: " << *(code.paramEdge  [TypeParameter::MutualEdgeNumber].begin()) << std::endl;
                // std::cout <<"Ref Single Edges: " << *(code.paramEdge  [TypeParameter::SingleEdgeNumber].begin()) << std::endl;
                // std::cout <<"Ref Edges: " << 2 * *(code.paramEdge  [TypeParameter::MutualEdgeNumber].begin())  + 
                //                               *(code.paramEdge  [TypeParameter::SingleEdgeNumber].begin()) << std::endl;

                // std::cout << "e: "<< gc_selected->g->number_of_edges() << std::endl;
                // std::cout << "current codelength: " << this->surveyor->get()  << " " << code.get() << std::endl;   
                // std::cout << "current codelength: " << this->surveyor->get()  <<  std::endl; 

                if (!world.rank()){
                    std::cout << "contraction: " << n_contractions << ", current codelength: " << this->surveyor->L.get() << std::endl;
                }
                                  
            }

        }
        auto tf = std::chrono::high_resolution_clock::now();
        if (this->tracker != nullptr && !world.rank()){ 
            auto timer = std::chrono::duration_cast<std::chrono::milliseconds>(tf - t0).count();
            this->tracker -> j["algorithmic_time"] = timer;
        }
    }


    void reset(){
        SurveyorFactory::make(this->surveyor,GraphOperationType::SubgContraction,this->G);
        if (this->models.size()){
            this->tracker = std::make_unique<CompressionTracker>(this->G,GraphOperationType::SubgContraction, this->models);
        }
        overlap.clear();
        overlap.resize(this->G->sizeMat);
        n_contractions = 0;

        for (auto &[label,gc] : this->graphlet_map){        
            gc->reset();
            this->surveyor -> add_graphlet_param(gc->g);
        }  
        this->surveyor->L.additional = PositiveIntegerCodelength(this->number_of_graphlets()).get() + 1.;
    }
};

#endif 