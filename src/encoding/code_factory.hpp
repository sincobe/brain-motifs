#ifndef code_factory_hpp
#define code_factory_hpp 

#include "erdos-renyi.hpp"
#include "configuration.hpp"

struct CodeFactory{

    template<typename T>
    static std::unique_ptr<Code<T>> make(const Graph<T> &G, const NullModel &model){

        switch (model)
        {

        case NullModel::ErdosRenyi:
            return std::make_unique<MultiER>(G);
            break;
        
        case NullModel::ReciprocalErdosRenyi:
            return std::make_unique<ReciprocalMultiER>(G);
            break;
        
        case NullModel::Configuration:  
            return std::make_unique<ConfigurationModel>(G);
            break;
        
        case NullModel::ReciprocalConfiguration:  
            return std::make_unique<ReciprocalConfigurationModel>(G); 
            break;

        default:
            return nullptr;
            break;
        }

    }

};


#endif