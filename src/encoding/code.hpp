//
//  codelength.hpp
//  brain-motifs
//
//  Created by Benichou Alexis on 8/21/21.
//
#ifndef code_hpp
#define code_hpp

#include <graph.hpp>
#include <cmath>
#include <algorithm>
#include <map>
#include <vector>
#include "integer_sequence.hpp"
#include "graphlet_component.hpp"
#include <json.hpp>






enum class TypeParameter{
    GraphSize,
    Degree,
    InDegree,
    OutDegree,
    EdgeNumber,
    SingleEdgeNumber,
    MutualEdgeNumber,
    DegreeSequence,
    AutomorphismGroupSize,
    GraphletNumber
};

// template<typename T>
// struct CodeParameter{
//     TypeParameter type;
//     T value;

//     template<typename U>
//     CodeParameter(U parameter) : value{parameter}{}

//     T get() const{
//         return value;
//     }

// };

enum class CodeTask { ComputeCodelength, Inference, Reset };



template<typename T>
struct Code{

    PriorType prior;
    Codelength L;
    std::shared_ptr<Graph<T>> G;

    template<typename U>
    Code(const Graph<U> &_G) : G{std::make_shared<Graph<T>>(_G)}{} 


    Code(std::shared_ptr<Graph<T>> &_G) : G{_G}{} 

    virtual ~Code() = default;

    virtual void set_model_codelength() = 0;
    virtual void set_data_codelength()  = 0;


    inline void set_codelength() {
        set_model_codelength();
        set_data_codelength();
    }

    virtual std::string get_name()       const = 0;
    virtual std::string get_prior_name() const {return "";}
    virtual NullModel get_model_type()   const = 0;

    double get() const{
        return L.get();
    }



    double test_model_encoding(PriorType prior, std::vector<std::size_t> &vec) const{
        switch(prior){
            case PriorType::DirichletUniform:{
                return DirichletProcessCodelength(vec,prior).get();
                break;
            }

            case PriorType::DirichletJeffreys:{
                return DirichletProcessCodelength(vec,prior).get();
                break;
            }

            case PriorType::Uniform:{
                return UniformSeqCodelength(vec).get();
                break;
            }

            default: 
                return 0.;
                break;
        }
    }

    virtual void adapt(std::shared_ptr<GraphletComponent> &gc){};
    virtual nlohmann::json get_json_output() const{return "{}";}
    void dump_edge_list(const std::string &filename){
        G->dump_edge_list(filename);
    }


    void reset_graph(const std::vector<T> &adjMat){
        G = std::make_shared<Graph<T>>(G->dir,adjMat);
    }

    virtual void task(const CodeTask &task){
        switch(task){
            case CodeTask::ComputeCodelength:{
                set_data_codelength() ;
                set_model_codelength();
                break;
            }
            default: 
                break;
        }
    }
};


#endif /* read_input_hpp */