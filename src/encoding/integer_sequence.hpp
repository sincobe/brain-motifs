#ifndef integer_sequence_hpp
#define integer_sequence_hpp 

#include <vector>

enum class PriorType{
    Uniform,
    DirichletUniform,
    DirichletJeffreys
};


struct PositiveIntegerCodelength{

    double cost;
    PositiveIntegerCodelength(const std::size_t &val) : cost{1.}{
        if (val){
            cost = std::log2(val * (val + 1));
        }
    }

    double get() const {
        return cost;
    }
};

struct IntegerSequenceEncoding{

    double cost;
    std::size_t max;
    std::size_t min;
    std::size_t size;

    std::map<std::size_t,std::size_t> dist;

    IntegerSequenceEncoding(const std::vector<std::size_t> &seq) : 
    size{seq.size()},max{},min{},cost{}{
        if (size){
            max = *std::max_element(seq.begin(),seq.end());
            min = *std::min_element(seq.begin(),seq.end());
            for (auto n{min} ; n <= max ; n++){
                dist[n] = std::count(seq.begin(),seq.end(),n);
            }
            cost = PositiveIntegerCodelength(max + 1).get() +  PositiveIntegerCodelength(min + 1).get();
        }

        
    }



    virtual double get_swap_cost  (const std::vector<std::size_t> &oldSubset, const std::vector<std::size_t> &newSubset) = 0;
    virtual void update_after_swap(const std::vector<std::size_t> &oldSubset, const std::vector<std::size_t> &newSubset) = 0;



    // virtual double get_swap_cost(const std::vector<std::size_t> &subset, const std::size_t &new_el) const     = 0;

    // // is the below funciton useful as a virtual member? May be better to use a single function in the base class (here) to update min/max/freq
    // virtual void update_min_max_after_swap(const std::size_t &new_el) = 0;

    // virtual void update_after_swap(const std::vector<std::size_t> &subset, const std::size_t &new_el)         = 0;


    // virtual double get_exchange_cost(const std::size_t &old_el, const std::size_t &new_el) const     = 0;

    // // is the below funciton useful as a virtual member? May be better to use a single function in the base class (here) to update min/max/freq
    // virtual void update_min_max_after_exchange(const std::size_t &old_el, const std::size_t &new_el) = 0;

    // virtual void update_after_exchange(const std::size_t &old_el, const std::size_t &new_el)         = 0;

    double get() const{
        return cost;
    }

};

struct DirichletProcessCodelength : IntegerSequenceEncoding{


    double Lambda; 
    // std::map<std::size_t,double> lambda_vec;
    double lambda; 

    // std::map<std::size_t,std::pair<double,std::size_t>> freq; //first: Dirichlet param, second: stored value
    

    PriorType prior; 

    DirichletProcessCodelength(const std::vector<std::size_t> &seq, const PriorType &_prior) : 
                                IntegerSequenceEncoding{seq},prior{_prior}{
        
        // Set the prior type that determine the definition and distribution of parameters.         

        // auto seq_copy = seq;
        // std::sort(seq_copy.begin(),seq_copy.end(),std::greater<std::size_t>());
        // std::vector<std::size_t> unique_values;
        // std::unique_copy(seq_copy.begin(),seq_copy.end(),std::back_inserter(unique_values));

        // Basic approach: uniformly encode values between a minimum and maximum. This part needs to be adapted for more generic settings. 
        

        switch(prior){
            case PriorType::DirichletUniform:
                lambda = 1.;
                break; 
            
            case PriorType::DirichletJeffreys:
                lambda = .5;
                break;
            
            default:
                lambda = 0.;
                break;
        }


        for (std::size_t x{min} ; x <= max ; x++){
            // double param;
            // switch(prior){
            //     case PriorType::DirichletUniform:
            //         param = 1.;
            //         break; 
                
            //     case PriorType::DirichletJeffreys:
            //         param = .5;
            //         break;
                
            //     default:
            //         param = 0.;
            //         break;
            // }

            auto n = std::count(seq.begin(),seq.end(),x);
            // freq[x] = std::pair<double,std::size_t>(param,n);
            cost += (std::lgamma(lambda) - std::lgamma(n + lambda))/std::log(2);
            // lambda_vec[x] = param;
            
        }
        Lambda = lambda * (max - min + 1.); 
        cost += (std::lgamma(seq.size() + Lambda) - std::lgamma(Lambda))/std::log(2);


       

    }
    double get_swap_cost(const std::vector<std::size_t> &oldSubset, const std::vector<std::size_t> &newSubset) override{

        std::size_t min_oldSubset = *std::min_element(oldSubset.begin(),oldSubset.end());
        std::size_t min_newSubset = *std::min_element(newSubset.begin(),newSubset.end());
        std::size_t max_oldSubset = *std::max_element(oldSubset.begin(),oldSubset.end());
        std::size_t max_newSubset = *std::max_element(newSubset.begin(),newSubset.end());

        std::size_t new_min{min}, new_max{max};

        double score{};
        double DeltaLambda{};

        if (min_newSubset < min){
            new_min = min_newSubset;
            DeltaLambda += lambda * (min - min_newSubset);
            // score -= (min - min_newSubset) * std::lgamma(lambda);
        }

        else{

            auto freq_min_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),min);
            auto freq_min_newSubset = std::count(newSubset.begin(),newSubset.end(),min);

            if (!freq_min_newSubset && freq_min_oldSubset == dist[min]){

                std::size_t next_min;
                auto it = ++dist.begin();
                while (it != dist.end()){
                    if (it->second){
                        next_min = it->first;
                        break;
                    }
                    ++it;
                }

                new_min  = std::min(next_min,min_newSubset);
                DeltaLambda -= lambda * (new_min - min);
                // score += (new_min - min) * std::lgamma(lambda); 
            }
        }

        if (max_newSubset > max){
            new_max = max_newSubset;
            DeltaLambda += lambda * (max_newSubset - max);
            // score -= (max_newSubset - max) * std::lgamma(lambda);
        }

        else{

            auto freq_max_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),max);
            auto freq_max_newSubset = std::count(newSubset.begin(),newSubset.end(),max);

            if (!freq_max_newSubset && freq_max_oldSubset == dist[max]){

                std::size_t next_max;
                auto it = ++dist.rbegin();
                while (it != dist.rend()){
                    if (it->second){
                        next_max = it->first;
                        break;
                    }
                    ++it;
                }

                new_max  = std::max(next_max,max_newSubset);
                DeltaLambda -= lambda * (max - new_max);
                // score += (max - new_max) * std::lgamma(lambda);
            }

        }

        if (max != new_max || min != new_min){
            score += (PositiveIntegerCodelength(max + 1).get() 
            + PositiveIntegerCodelength(min + 1)       .get() 
            - PositiveIntegerCodelength(new_max + 1)   .get()
            - PositiveIntegerCodelength(new_min + 1)   .get()) * std::log(2);
        }


        std::map<std::size_t,long> delta_dist;

        for (auto &old_el : oldSubset){
            delta_dist[old_el]--;
        }

        for (auto &new_el : newSubset){
            delta_dist[new_el]++;
        }

        for (auto &[el,dn]: delta_dist){
            if ((el <= new_max &&  el > max) || (el >= new_min && el < min)){
                score += std::lgamma(dn + lambda) - std::lgamma(lambda);
            }
            else{
                auto n = dist[el];
                score -= std::lgamma(n + lambda) - std::lgamma(n + dn + lambda);
            }
        }

        long DeltaSize = static_cast<long>(newSubset.size()) - static_cast<long>(oldSubset.size());
        score   +=  std::lgamma(size + Lambda) - std::lgamma(size +  DeltaSize + Lambda + DeltaLambda) 
                -  (std::lgamma(Lambda) - std::lgamma(Lambda + DeltaLambda));

        return score/std::log(2);
    }

    void update_after_swap(const std::vector<std::size_t> &oldSubset, const std::vector<std::size_t> &newSubset) override{

        std::size_t min_oldSubset = *std::min_element(oldSubset.begin(),oldSubset.end());
        std::size_t min_newSubset = *std::min_element(newSubset.begin(),newSubset.end());
        std::size_t max_oldSubset = *std::max_element(oldSubset.begin(),oldSubset.end());
        std::size_t max_newSubset = *std::max_element(newSubset.begin(),newSubset.end());

        std::size_t new_min{min}, new_max{max};

        double score{};
        double DeltaLambda{};

        if (min_newSubset < min){
            new_min = min_newSubset;
            DeltaLambda += lambda * (min - min_newSubset);
            // score   -= (min - min_newSubset) * std::lgamma(lambda);
        }

        else{

            auto freq_min_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),min);
            auto freq_min_newSubset = std::count(newSubset.begin(),newSubset.end(),min);

            if (!freq_min_newSubset && freq_min_oldSubset == dist[min]){

                std::size_t next_min;
                auto it = ++dist.begin();
                while (it != dist.end()){
                    if (it->second){
                        next_min = it->first;
                        break;
                    }
                    ++it;
                }

                new_min  = std::min(next_min,min_newSubset);
                DeltaLambda -= lambda * (new_min - min);
                // score += (new_min - min) * std::lgamma(lambda); 
            }
        }

        if (max_newSubset > max){
            new_max = max_newSubset;
            DeltaLambda += lambda * (max_newSubset - max);
            // score -= (max_newSubset - max) * std::lgamma(lambda);
        }

        else{

            auto freq_max_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),max);
            auto freq_max_newSubset = std::count(newSubset.begin(),newSubset.end(),max);

            if (!freq_max_newSubset && freq_max_oldSubset == dist[max]){

                std::size_t next_max;
                auto it = ++dist.rbegin();
                while (it != dist.rend()){
                    if (it->second){
                        next_max = it->first;
                        break;
                    }
                    ++it;
                }


                new_max  = std::max(next_max,max_newSubset);
                DeltaLambda -= lambda * (max - new_max);
                // score += (max - new_max) * std::lgamma(lambda);
            }

        }

        if (max != new_max || min != new_min){
            score += (PositiveIntegerCodelength(max + 1).get() 
            + PositiveIntegerCodelength(min + 1)       .get() 
            - PositiveIntegerCodelength(new_max + 1)   .get()
            - PositiveIntegerCodelength(new_min + 1)   .get()) * std::log(2);
        }
        

        std::map<std::size_t,long> delta_dist;

        for (auto &old_el : oldSubset){
            delta_dist[old_el]--;
        }   

        for (auto &new_el : newSubset){
            delta_dist[new_el]++;
        }

        for (auto &[el,dn]: delta_dist){
            if ((el <= new_max &&  el > max) || (el >= new_min && el < min)){
                score += std::lgamma(dn + lambda) - std::lgamma(lambda);
            }
            else{
                auto n = dist[el];
                score -= std::lgamma(n + lambda) - std::lgamma(n + dn + lambda);
            }
        }

        long DeltaSize = static_cast<long>(newSubset.size()) - static_cast<long>(oldSubset.size());
        score   +=  std::lgamma(size + Lambda) - std::lgamma(size +  DeltaSize + Lambda + DeltaLambda) 
                -  (std::lgamma(Lambda) - std::lgamma(Lambda + DeltaLambda));


        for (auto &&item : delta_dist){
            if (item.second < 0){
                auto it = dist.find(item.first);
                if (static_cast<std::size_t>(-item.second) == it->second){
                    dist.erase(item.first);
                }
                else{
                    it->second += item.second; 
                }
            }
            else{
                dist[item.first] += item.second;
            }   
        }

        size   += DeltaSize;
        Lambda += DeltaLambda;
        cost   -= score/std::log(2);
        max = new_max;
        min = new_min;
        // std::cout << "Greedy,     min: " << min << ", max: " << max << ", size: " << size << ", Lambda: " << Lambda << ", cost: " << cost << std::endl;
    }


    // std::pair<double,std::size_t> get_freq_el(const std::size_t &el) const{
    //     std::pair<double,std::size_t> item;

    //     auto it = this->freq.find(el);
    //     if (it != this->freq.end()){
    //         item = it->second;
    //     }
    //     else{
    //         switch (prior)
    //         {
    //         case PriorType::DirichletUniform:
    //             item = std::pair<double,std::size_t>(1.,0);
    //             break;

    //         case PriorType::DirichletJeffreys:
    //             item = std::pair<double,std::size_t>(.5,0);
    //             break;
            
    //         default:
    //             item = std::pair<double,std::size_t>(0.,0);
    //             break;
    //         }
    //     }

    //     return std::move(item);
    // }

    // std::map<std::size_t,long> get_DeltaFreq_swap(const std::vector<std::size_t> &subset, const std::size_t &new_el) const{
    //     std::map<std::size_t,long> DeltaFreq;
    //     for (auto &&el : subset){
    //         DeltaFreq[el]++;
    //     }
    //     DeltaFreq[new_el]--;
    //     return std::move(DeltaFreq);
    // }

    // double get_DeltaLambda_swap(std::map<std::size_t,long> &DeltaFreq,const std::size_t &new_el) const{
    //     double DeltaLambda{};

    //     // following ONLY TRUE FOR UNIFORM PRIORS (INCLUDING JEFFREYS)
    //     if (new_el > max){
    //         DeltaLambda -= (new_el - max) * freq.find(max)->second.first;
    //     }
    //     else if (new_el < min){
    //         DeltaLambda -= (min - new_el) * freq.find(min)->second.first;
    //     }


    //     if (new_el < max && new_el >= min && DeltaFreq.find(max) != DeltaFreq.end()){
    //         if (DeltaFreq[max] == freq.find(max)->second.second){
    //             std::size_t i{max - 1};
    //             while (i > new_el){

    //                 if (get_freq_el(i).second){
    //                     break;
    //                 }
    //                 else{
    //                     switch (this->prior)
    //                     {
    //                     case PriorType::DirichletUniform:
    //                         DeltaLambda += 1.;
    //                         break;

    //                     case PriorType::DirichletJeffreys:
    //                         DeltaLambda += 0.5;
    //                         break;
                        
    //                     default:
    //                         break;
    //                     }
    //                 }
    //                 --i;
    //             }
    //         }
    //     }


    //     else if (new_el > min && new_el <= max && DeltaFreq.find(min) != DeltaFreq.end()){
    //         if (DeltaFreq[min] == freq.find(min)->second.second){
    //             std::size_t i{min + 1};
    //             while (i < new_el){

    //                 if (get_freq_el(i).second){
    //                     break;
    //                 }
    //                 else{
    //                     switch (this->prior)
    //                     {
    //                     case PriorType::DirichletUniform:
    //                         DeltaLambda += 1.;
    //                         break;

    //                     case PriorType::DirichletJeffreys:
    //                         DeltaLambda += 0.5;
    //                         break;
                        
    //                     default:
    //                         break;
    //                     }
    //                 }
    //                 ++i;
    //             }
    //         }
    //     }

    //     return std::move(DeltaLambda);
    // }

    

    // double get_swap_cost(const std::vector<std::size_t> &subset, const std::size_t &new_el) const override{
    //     double swap_cost = std::lgamma(size + Lambda);

    //     std::map<std::size_t,long> DeltaFreq = get_DeltaFreq_swap(subset,new_el); // first: value, second: frequency of value in subset
    //     double DeltaLambda = get_DeltaLambda_swap(DeltaFreq,new_el);

    //     swap_cost -= std::lgamma(size - subset.size() + 1 + Lambda - DeltaLambda);
    //     for (auto &&df : DeltaFreq){
    //         auto item  = get_freq_el(df.first);
    //         swap_cost -= std::lgamma(item.first + item.second) - std::lgamma(item.second - df.second + item.first);
    //     }

    //     swap_cost /= std::log(2);

    //     return swap_cost;
    // }


    // void update_min_max_after_swap(const std::size_t &new_el) override{

    //     if (new_el > max || new_el < min){
    //         auto get_intial_pair = [this](){
    //             switch (this->prior)
    //             {
    //             case PriorType::DirichletUniform:
    //                 return std::pair<double,std::size_t>(1.,0);
    //                 break;

    //             case PriorType::DirichletJeffreys:
    //                 return std::pair<double,std::size_t>(.5,0);
    //                 break;
                
    //             default:
    //                 return std::pair<double,std::size_t>(0.,0);
    //                 break;
    //             }
    //         };

    //         if (new_el > max){
    //             for (auto el{max + 1} ; el <= new_el ; el++){
    //                 freq[el] = get_intial_pair();
    //             }
    //             max = new_el;
    //         }

    //         else if (new_el < min){
    //             for (auto el{new_el} ; el < min ; el++){
    //                 freq[el] = get_intial_pair();
    //             }
    //             min = new_el;
    //         }
    //     }
    //     else if(new_el <= max && new_el > min && freq.find(min)->second.second == 0){
    //         std::size_t i{min + 1};
    //         while (i < new_el){

    //             if (get_freq_el(i).second){
    //                 min = i;
    //                 break;
    //             }
    //             else{
    //                 freq.erase(i);
    //             }
    //             ++i;
    //         }
    //     }
    //     else if(new_el < max && new_el >= min && freq.find(max)->second.second == 0){
    //         std::size_t i{max - 1};
    //         while (i < new_el){

    //             if (get_freq_el(i).second){
    //                 max = i;
    //                 break;
    //             }
    //             else{
    //                 freq.erase(i);
    //             }
    //             --i;
    //         }
    //     }
    // }

    // void update_after_swap(const std::vector<std::size_t> &subset, const std::size_t &new_el) override{
    //     std::map<std::size_t,long> DeltaFreq = get_DeltaFreq_swap(subset,new_el); // first: value, second: frequency of value in subset
    //     Lambda -= get_DeltaLambda_swap(DeltaFreq,new_el);
    //     size   -= subset.size() - 1;

    //     for (auto &&el : subset){
    //         auto it = freq.find(el);
    //         it->second.second--;
    //     } 
    //     auto it = freq.find(new_el);
    //     it->second.second++;

        
    //      update_min_max_after_swap(new_el);
    // }


    // double get_exchange_cost(const std::size_t &old_el, const std::size_t &new_el) const override{
    //     if (new_el >= min && new_el <= max){
            
    //         //TRUE FOR UNIFORM DIST

    //         return 0;
            
    //     }
    //     else{
    //         std::cout << "New element is outside [min,max]: Either some prior pb or soemthing needs to be change in the encoding variation." << std::endl;
    //         return 0.;
    //     }
    // }

    // is the below funciton useful as a virtual member? May be better to use a single function in the base class (here) to update min/max/freq
    // void update_min_max_after_exchange(const std::size_t &old_el, const std::size_t &new_el) override{

    // }

    // void update_after_exchange(const std::size_t &old_el, const std::size_t &new_el) override{

    // }

};


struct UniformSeqCodelength : IntegerSequenceEncoding{

    // TODO: Distinguish active vs inactive nodes

    UniformSeqCodelength(const std::vector<std::size_t> &seq) : IntegerSequenceEncoding{seq}{

        if (seq.size() > 1){
            cost += seq.size() * std::log2(max - min + 1);
        }
        else{
            cost = PositiveIntegerCodelength(*seq.cbegin()).get();
        }
    }

    double get_swap_cost(const std::vector<std::size_t> &oldSubset, const std::vector<std::size_t> &newSubset) override{
        std::size_t min_oldSubset = *std::min_element(oldSubset.begin(),oldSubset.end());
        std::size_t min_newSubset = *std::min_element(newSubset.begin(),newSubset.end());
        std::size_t max_oldSubset = *std::max_element(oldSubset.begin(),oldSubset.end());
        std::size_t max_newSubset = *std::max_element(newSubset.begin(),newSubset.end());

        std::size_t new_min{min}, new_max{max};

        double score{};

        if (min_newSubset < min){
            new_min = min_newSubset;
        }

        else {

            auto freq_min_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),min);
            auto freq_min_newSubset = std::count(newSubset.begin(),newSubset.end(),min);

            if (!freq_min_newSubset && freq_min_oldSubset == dist[min]){

                auto next_min = (++dist.begin())->first;

                new_min  = std::min(next_min,min_newSubset);
            }
        }

        if (max_newSubset > max){
            new_max = max_newSubset;
        }

        else{

            auto freq_max_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),max);
            auto freq_max_newSubset = std::count(newSubset.begin(),newSubset.end(),max);

            if (!freq_max_newSubset && freq_max_oldSubset == dist[max]){

                auto next_max = (++dist.rbegin())->first;
                new_max  = std::max(next_max,max_newSubset);
            }
        }

        long DeltaSize = static_cast<long>(newSubset.size()) - static_cast<long>(oldSubset.size());

 

        score += cost - (size + DeltaSize) * std::log2(new_max - new_min + 1.)
        - PositiveIntegerCodelength(new_max + 1).get() 
        - PositiveIntegerCodelength(new_min + 1).get();

        return score;
    }

    void update_after_swap(const std::vector<std::size_t> &oldSubset, const std::vector<std::size_t> &newSubset) override{
        std::size_t min_oldSubset = *std::min_element(oldSubset.begin(),oldSubset.end());
        std::size_t min_newSubset = *std::min_element(newSubset.begin(),newSubset.end());
        std::size_t max_oldSubset = *std::max_element(oldSubset.begin(),oldSubset.end());
        std::size_t max_newSubset = *std::max_element(newSubset.begin(),newSubset.end());

        std::size_t new_min{min}, new_max{max};

        double score{};

        if (min_newSubset < min){
            new_min = min_newSubset;
        }

        else{

            auto freq_min_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),min);
            auto freq_min_newSubset = std::count(newSubset.begin(),newSubset.end(),min);

            if (!freq_min_newSubset && freq_min_oldSubset == dist[min]){

                auto next_min = (++dist.begin())->first;

                new_min  = std::min(next_min,min_newSubset);
            }
        }

        if (max_newSubset > max){
            new_max = max_newSubset;
        }

        else{

            auto freq_max_oldSubset = std::count(oldSubset.begin(),oldSubset.end(),max);
            auto freq_max_newSubset = std::count(newSubset.begin(),newSubset.end(),max);

            if (!freq_max_newSubset && freq_max_oldSubset == dist[max]){

                auto next_max = (++dist.rbegin())->first;
                new_max  = std::max(next_max,max_newSubset);
            }

        }

        

        long DeltaSize = static_cast<long>(newSubset.size()) - static_cast<long>(oldSubset.size());

        // std::cout << "delta size: " << << std::endl;
        score += cost - (size + DeltaSize) * std::log2(new_max - new_min + 1.)
        - PositiveIntegerCodelength(new_max + 1).get() 
        - PositiveIntegerCodelength(new_min + 1).get();

        std::map<std::size_t,long> delta_dist;

        for (auto &old_el : oldSubset){
            delta_dist[old_el]--;
        }   

        for (auto &new_el : newSubset){
            delta_dist[new_el]++;
        }

        for (auto &&item : delta_dist){
            if (item.second < 0){
                auto it = dist.find(item.first);
                if (static_cast<std::size_t>(-item.second) == it->second){
                    dist.erase(item.first);
                }
                else{
                    it->second += item.second; 
                }
            }
            else{
                dist[item.first] += item.second;
            }   
        }

        size += DeltaSize;
        cost -= score;
        max = new_max;
        min = new_min;
        // std::cout << "min: " << min << ", max: " << max << ", size: " << size << ", cost = " << cost << std::endl;
    }

    // double get_swap_cost(const std::vector<std::size_t> &subset, const std::size_t &new_el) const override{
    //     if (new_el >= min && new_el <= max){
    //         return -(subset.size() - 1) * std::log2(max - min + 1);
    //     }

    //     else if (new_el > max){
    //         return -((size - subset.size() + 1) * std::log2(new_el - min + 1) + 
    //         PositiveIntegerCodelength(new_el).get() + PositiveIntegerCodelength(min).get() - cost);
    //     }

    //     else if (new_el < min){
    //         return -((size - subset.size() + 1) * std::log2(max - new_el + 1) + 
    //         PositiveIntegerCodelength(new_el).get() + PositiveIntegerCodelength(max).get() - cost);
    //     }
    //     else{
    //         return 0;
    //     }
    // }

    // void update_min_max_after_swap(const std::size_t &new_el) override{

    //     // NOT SURE HOW TO DO THIS PROPERLY WHEN NEW <= MAX AND MIN IS CHANGED, MAYBE FREQ MUST BE IN BASE CLASS
    // }

    // void update_after_swap(const std::vector<std::size_t> &subset, const std::size_t &new_el) override{
    //     size -= subset.size() - 1;
        
    //     if (new_el >= max && new_el <= min){
    //         cost -= (subset.size() - 1) * std::log2(max - min + 1);
    //     }

    //     else if (new_el > max){
    //         cost -= (size - subset.size() + 1) * std::log2(new_el - min + 1) + 
    //         PositiveIntegerCodelength(new_el).get() + PositiveIntegerCodelength(min).get() - cost;
    //         max = new_el;
    //     }

    //     else if (new_el < min){
    //         cost -= (size - subset.size() + 1) * std::log2(max - new_el + 1) + 
    //         PositiveIntegerCodelength(new_el).get() + PositiveIntegerCodelength(max).get() - cost;
    //         min = new_el;

    //     }

    // }

    // double get_exchange_cost(const std::size_t &old_el, const std::size_t &new_el) const   override{
    //     if (new_el >= min && new_el <= max){
    //         return 0.;
    //     }
    //     else{
    //         std::cout << "New element is outside [min,max]: Either some prior pb or soemthing needs to be change in the encoding variation." << std::endl;
    //         return 0.;
    //     }

    //     /********

    //      * NEW EL IS ONLY EXPECTED TO BE IN THE RANGE MAX - MIN 

    //     *********/
    // }

    // // is the below funciton useful as a virtual member? May be better to use a single function in the base class (here) to update min/max/freq
    // void update_min_max_after_exchange(const std::size_t &old_el, const std::size_t &new_el) override{

    // }

    // void update_after_exchange(const std::size_t &old_el, const std::size_t &new_el) override{
        
    // }


};


struct IntSeqEncodingFactory{

    static std::shared_ptr<IntegerSequenceEncoding> make(const PriorType &prior,const std::vector<std::size_t> &seq){
        switch(prior){
            case PriorType::Uniform:
                return std::make_shared<UniformSeqCodelength>(seq);
                break; 
            case PriorType::DirichletUniform:
                return std::make_shared<DirichletProcessCodelength>(seq,prior);
                break;
            case PriorType::DirichletJeffreys:
                return std::make_shared<DirichletProcessCodelength>(seq,prior);
                break;
            default:
                return nullptr;
                break;
        }
    }
};

#endif