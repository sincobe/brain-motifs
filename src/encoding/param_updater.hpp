// #ifndef param_updater
// #define param_updater


// #include "graph_compression.hpp"
// #include "diff_codelength/erdos-renyi.hpp"
// #include "diff_codelength/configuration.hpp"

// struct ParamUpdater{
//     struct MultiER{
//         template<class T>
//         static void update(std::shared_ptr<T> &code, std::shared_ptr<GraphletComponent> &gc){
//             auto subg = gc->get_subg();

//             code->paramNode[0]                                      -= subg.size() - 1;
//             code->methodParam[CompressionTypeParameter::NodeNumber] -= subg.size() - 1;

//             code->paramEdge[TypeParameter::EdgeNumber][0] -= gc->g->number_of_edges();

//             code->methodParam[CompressionTypeParameter::SubgraphSequenceSize]++;
//             code->graphletParam[gc->g->gtrie_label][TypeParameter::GraphletNumber]++;
//             code->L -= gc->candidate.second;  
//         }
//     };

//     struct ConfigurationModel{
//         template<class T>
//         static void update(std::shared_ptr<T> &code, std::shared_ptr<GraphletComponent> &gc){
//             auto subg = gc->get_subg();
        
//             code->methodParam[CompressionTypeParameter::NodeNumber] -= subg.size() - 1;

//             switch(code->G->dir){
//                 case EdgeType::undirected:{
//                     code->paramStub[TypeParameter::Degree][0] -= 2 * gc->g->number_of_edges();

//                     std::vector<std::size_t> subset,new_subset;
//                     auto it_k = code->paramDegree[TypeParameter::Degree][0].begin();

//                     for (auto &&i : subg){
//                         subset.push_back(*(it_k + i));
//                         *(it_k + i) = 0;
//                     }

//                     auto k_subg = code->G->get_node_degree(subg[0],Neighborhood::undirected);
//                     *(it_k + subg[0]) = k_subg;
//                     code->seq_code[TypeParameter::Degree][0]->update_after_swap(subset, new_subset);

//                     break;
//                 }
//                 case EdgeType::directed:{
//                     code->paramStub  [TypeParameter::InDegree] [0] -= gc->g->number_of_edges();
//                     code->paramStub  [TypeParameter::OutDegree][0] -= gc->g->number_of_edges();

                    
//                     auto it_k_in  = code->paramDegree[TypeParameter::InDegree] [0].begin();
//                     auto it_k_out = code->paramDegree[TypeParameter::OutDegree][0].begin();
//                     std::vector<std::size_t> subset_in,subset_out,new_subset_in,new_subset_out;

//                     for (auto &&i : subg){
//                         subset_in .push_back(*(it_k_in  + i));
//                         subset_out.push_back(*(it_k_out + i));
//                         *(it_k_in  + i) = 0;
//                         *(it_k_out + i) = 0;
//                     }

 
//                     auto k_subg_in  = code->G->get_node_degree(subg[0],Neighborhood::in);
//                     auto k_subg_out = code->G->get_node_degree(subg[0],Neighborhood::out);
//                     *(it_k_in   + subg[0]) = k_subg_in;
//                     *(it_k_out  + subg[0]) = k_subg_out;
//                     new_subset_in .push_back(k_subg_in);
//                     new_subset_out.push_back(k_subg_out);

//                     code->seq_code[TypeParameter::InDegree] [0]->update_after_swap(subset_in, new_subset_in);
//                     code->seq_code[TypeParameter::OutDegree][0]->update_after_swap(subset_out,new_subset_out);

//                     break;
//                 }
//                 default:
//                     break;
//             }
//             code->methodParam [CompressionTypeParameter::SubgraphSequenceSize]++;
//             code->graphletParam[gc->g->gtrie_label][TypeParameter::GraphletNumber]++;
//             code->L -= gc->candidate.second; 
            
//         }
//     };

//     struct ReciprocalMultiER{
//         template<class T>
//         static void update(std::shared_ptr<T> &code, std::shared_ptr<GraphletComponent> &gc){
//             auto subg = gc->get_subg();

//             code->paramNode[0]                                      -= subg.size() - 1;
//             code->methodParam[CompressionTypeParameter::NodeNumber] -= subg.size() - 1;

//             code->paramEdge  [TypeParameter::MutualEdgeNumber][0] = code->G->number_of_mutual_edges();
//             code->paramEdge  [TypeParameter::SingleEdgeNumber][0] = code->G->number_of_single_edges();

//             code->methodParam [CompressionTypeParameter::SubgraphSequenceSize]++;
//             code->graphletParam[gc->g->gtrie_label][TypeParameter::GraphletNumber]++;

//             code->L -= gc->candidate.second;
//         }
//     };

//     struct ReciprocalConfigurationModel{
//         template<class T>
//         static void update(std::shared_ptr<T> &code, std::shared_ptr<GraphletComponent> &gc){
//             auto subg = gc->get_subg();
        
//             code->methodParam[CompressionTypeParameter::NodeNumber] -= subg.size() - 1;

//             code->paramStub  [TypeParameter::Degree]   [0] = 2*code->G->number_of_mutual_edges();
//             code->paramStub  [TypeParameter::InDegree] [0] =   code->G->number_of_single_edges();
//             code->paramStub  [TypeParameter::OutDegree][0] =   code->G->number_of_single_edges();

//             auto it_k     = code->paramDegree[TypeParameter::Degree]   [0].begin();
//             auto it_k_in  = code->paramDegree[TypeParameter::InDegree] [0].begin();
//             auto it_k_out = code->paramDegree[TypeParameter::OutDegree][0].begin();
//             std::vector<std::size_t> subset,subset_in,subset_out,new_subset,new_subset_in,new_subset_out;
//             for (auto &&i : subg){
//                 subset    .push_back(*(it_k     + i));
//                 subset_in .push_back(*(it_k_in  + i));
//                 subset_out.push_back(*(it_k_out + i));
//                 *(it_k     + i) = 0;
//                 *(it_k_in  + i) = 0;
//                 *(it_k_out + i) = 0;
//             }

//             auto k_subg     = code->G->get_node_degree(subg[0],Neighborhood::mutual);
//             auto k_subg_in  = code->G->get_node_degree(subg[0],Neighborhood::single_in);
//             auto k_subg_out = code->G->get_node_degree(subg[0],Neighborhood::single_out);
//             *(it_k      + subg[0]) = k_subg;
//             *(it_k_in   + subg[0]) = k_subg_in;
//             *(it_k_out  + subg[0]) = k_subg_out;
//             new_subset    .push_back(k_subg);
//             new_subset_in .push_back(k_subg_in);
//             new_subset_out.push_back(k_subg_out);
            
//             // code->paramDegree[TypeParameter::Degree][0][subg[0]] = 
//             // code->G->get_subg_degree(subg,Neighborhood::mutual);

//             // code->paramDegree[TypeParameter::InDegree][0][subg[0]]= 
//             // code->G->get_subg_degree(subg,Neighborhood::single_in);

//             // code->paramDegree[TypeParameter::OutDegree][0][subg[0]] = 
//             // code->G->get_subg_degree(subg,Neighborhood::single_out);


//             for (auto &j : code->G->get_subg_neighborhood(subg)){
                
//                 subset.push_back(*(it_k + j));
//                 *(it_k + j)   = code->G->get_node_degree(j,Neighborhood::mutual); 
//                 new_subset.push_back(*(it_k + j));

                
//                 subset_in.push_back(*(it_k_in + j));
//                 *(it_k_in + j) = code->G->get_node_degree(j,Neighborhood::single_in); 
//                 new_subset_in.push_back(*(it_k_in + j));

//                 subset_out.push_back(*(it_k_out + j));
//                 *(it_k_out + j) = code->G->get_node_degree(j,Neighborhood::single_out);
//                 new_subset_out.push_back(*(it_k_out + j));    
//             }

//             code->methodParam [CompressionTypeParameter::SubgraphSequenceSize]++;
//             code->graphletParam[gc->g->gtrie_label][TypeParameter::GraphletNumber]++;
//             code->L -= gc->candidate.second;

//             code->seq_code[TypeParameter::InDegree] [0]->update_after_swap(subset_in, new_subset_in);
//             code->seq_code[TypeParameter::OutDegree][0]->update_after_swap(subset_out,new_subset_out);
//             code->seq_code[TypeParameter::Degree]   [0]->update_after_swap(subset, new_subset);
//         }
//     };


// };




// #endif