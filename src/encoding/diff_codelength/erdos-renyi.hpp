#ifndef diff_codelength_er_hpp 
#define diff_codelength_er_hpp 

#include "diff_codelength_interface.hpp"
#include "../graphlet_component.hpp"



template<class T>
struct ICompressionMeasureER : CompressionMeasure<T>{


    
    std::map<std::string,std::map<TypeParameter,std::size_t>> graphletParamEdge;


    ICompressionMeasureER(GraphOperationType type,std::shared_ptr<Graph<int>> &G):
    CompressionMeasure<T>{type,G}{}

    

    void set_model_codelength_difference(const std::string &key, const std::vector<int> &subg) override{
        this->dL.model = diff_node_number_encoding(key,subg);
        for (auto &param_edge_set : this->paramEdge){
            TypeParameter edgeType = param_edge_set.first;
            this->dL.model += diff_edge_number_encoding(key,subg,edgeType);
        }
    }

    virtual double diff_edge_number_encoding(const std::string &key, const std::vector<int> &subg, TypeParameter &edgeType) = 0;
    virtual double diff_node_number_encoding(const std::string &key, const std::vector<int> &subg) = 0;

    

    void set_data_codelength_difference(const std::string &key, const std::vector<int> &subg) override{
        
        this->dL.data  = 0.;
       
        unsigned long Q = this->paramNode.size() ;
        int p,q;
        for (auto &param_edge_set : this->paramEdge){
            TypeParameter edgeType = param_edge_set.first;
            for (p = 0 ; p < Q ; p++){
                for (q = p ; q < Q ; q++){
                    this->dL.data += diff_log_w(key,subg,p,q,edgeType) + diff_log_M(key,subg,p,q,edgeType);
                }
            }
        }
    }

    virtual double diff_log_w(const std::string &key, const std::vector<int> &subg, int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) = 0;
    virtual double diff_log_M(const std::string &key, const std::vector<int> &subg, int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) = 0;

};


struct CompressionMeasureMultiER : ICompressionMeasureER<MultiER>{
 
    CompressionMeasureMultiER(GraphOperationType type,std::shared_ptr<Graph<int>> &G):
    ICompressionMeasureER<MultiER>{type,G}{}

    virtual void set_preliminaries(const std::string &key, const std::vector<int> &subg) override{}

    double diff_node_number_encoding(const std::string &key, const std::vector<int> &subg) override{
        return PositiveIntegerCodelength(this->paramNode[0] + 1.).get() - PositiveIntegerCodelength(this->paramNode[0] - subg.size() + 2.).get();
    }

    double diff_edge_number_encoding(const std::string &key, const std::vector<int> &subg, TypeParameter &edgeType) override{
        unsigned long e = graphletParamEdge[key][edgeType];
        return PositiveIntegerCodelength(this->paramEdge[edgeType][0] + 1.).get() - PositiveIntegerCodelength(this->paramEdge[edgeType][0] - e + 1.).get();
    }

    double diff_log_w(const std::string &key, const std::vector<int> &subg, int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{

        long N = *paramNode.begin();
        long E = *(paramEdge.begin()->second.begin());
        long e = graphletParamEdge[key][TypeParameter::EdgeNumber]; 

        switch (G->dir){
            case EdgeType::directed:  
                return 2 * E * std::log2(N) - 2 * (E - e) * std::log2(N - subg.size() + 1) 
                       - (std::lgamma(E + 1) - std::lgamma(E - e + 1))/std::log(2);
                break; 
            case EdgeType::undirected:  
                return E * (std::log2(N * (N + 1)) - 1) - (E - e) * (std::log2((N - subg.size() + 1)*(N - subg.size() + 2)) - 1)
                       - (std::lgamma(E + 1) - std::lgamma(E - e + 1))/std::log(2);
                break; 
            default:  
                return 0.;
                break;
        }
    }

    double diff_log_M(const std::string &key, const std::vector<int> &subg, int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{

        std::set<int> subg_neighbors{this->G->get_subg_neighborhood(subg)};

        double delta_A{};

        for (auto &&i : subg_neighbors){

            delta_A +=  std::lgamma(std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(b,i);
                                    }) + 1.)
                    -   std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + std::lgamma(this->G->at(b,i) + 1.);
                                    });

            if (this->G->dir == EdgeType::directed){
                
                delta_A +=  std::lgamma(std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i](double a, int b){
                                            return std::move(a) + this->G->at(i,b);
                                        }) + 1.)
                        -   std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i](double a, int b){
                                            return std::move(a) + std::lgamma(this->G->at(i,b) + 1.);
                                        });
                    
            }
        }


        return -delta_A/std::log(2);

    }

    void add_graphlet_param(std::shared_ptr<IGraphlet> &g) override{
        graphletParamEdge[g->gtrie_label][TypeParameter::EdgeNumber]            = g->number_of_edges();
        graphletParam    [g->gtrie_label][TypeParameter::AutomorphismGroupSize] = g->autSize;
        graphletParam    [g->gtrie_label][TypeParameter::GraphletNumber]        = 0;
    }

    void update_param(std::shared_ptr<GraphletComponent> &gc) override{
        auto subg = gc->get_subg();

        paramNode[0]                                      -= subg.size() - 1;

        paramEdge[TypeParameter::EdgeNumber][0] -= gc->g->number_of_edges();

        common_updating(gc);
    }

};

#endif