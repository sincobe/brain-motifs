#ifndef compression_measure_factory_hpp
#define compression_measure_factory_hpp

#include "diff_codelength_interface.hpp"

struct CompressionMeasureFactory
{   

    static std::shared_ptr<Code<int>> make(NullModel model, GraphOperationType operation, std::shared_ptr<Graph<int>> &G){
        switch (model)
        {
        case NullModel::ErdosRenyi:
            return std::make_shared<CompressionMeasureMultiER>(operation,G);
            break;
        
        case NullModel::ReciprocalErdosRenyi:
            return std::make_shared<CompressionMeasureReciprocalMultiER>(operation,G);
            break;
        
        case NullModel::Configuration:  
            return std::make_shared<CompressionMeasureConfiguration>(operation,G);
            break;
        
        case NullModel::ReciprocalConfiguration:  
            return std::make_shared<CompressionMeasureReciprocalConfiguration>(operation,G); 
            break;

        default:
            return nullptr;
            break;
        }
    }
};


#endif