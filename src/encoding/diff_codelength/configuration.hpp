#ifndef diff_codelength_configuration_hpp 
#define diff_codelength_configuration_hpp 

#include "diff_codelength_interface.hpp"



template<class T>
struct ICompressionMeasureConfiguration : CompressionMeasure<T>{
    
    std::map<std::string,std::map<TypeParameter,std::vector<std::size_t>>> graphletParamStub;

    ICompressionMeasureConfiguration(GraphOperationType type,std::shared_ptr<Graph<int>> &G):
    CompressionMeasure<T>{type,G}{}

    void set_preliminaries(const std::string &key, const std::vector<int> &subg) override{}

    void set_model_codelength_difference(const std::string &key, const std::vector<int> &subg) override{

        // this->dL.model = 0;

        // int p,q;
        // for (auto &&[type,deg_seq_vec] : this->paramDegree){
        //     std::vector<size_t> new_subset;
        //     int Q = deg_seq_vec.size();
        //     int idx;
        //     for (p = 0 ; p < Q ; p++){
        //         for (q = p + 1 ; q < Q ; q++){
        //             idx = p * Q + q; 
        //             std::vector<std::size_t> subset;
        //             for (auto &i : subg){
        //                 subset.push_back(deg_seq_vec[idx][i]);           
        //             }
        //             // k_subg = get_supernode_value(type,subg,p,q);
        //             newsubset = get_new_subsequence(type,subg,p,q);
        //             this->dL.model += seq_encoding[type][idx]->get_swap_cost(subset,new_subset);
        //         }
        //     }
        // }
    }

    // virtual std::size_t get_new_subsequence(TypeParameter type,  const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id) const = 0;
    // virtual std::size_t get_supernode_value(TypeParameter type,  const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id) const = 0;
    virtual double diff_log_Perm               (const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id) = 0;
    virtual double diff_log_M                  (const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) = 0;
    virtual double diff_log_uniform_degree_dist(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) = 0;

    void set_data_codelength_difference(const std::string &key, const std::vector<int> &subg) override{

    
        this->dL.data  = 0;
        this->dL.model = 0; 
    
       
        unsigned long Q = this->paramDegree.begin()->second.size() ;
        int p,q;

        for (p = 0 ; p < Q ; p++){
            for (q = p ; q < Q ; q++){
                this->dL.data -= diff_log_Perm(key,subg,p,q);
                for (auto &param_degree_set : this->paramDegree){
                    TypeParameter degreeType = param_degree_set.first;
                    this->dL.data += diff_log_M(key,subg,p,q,degreeType) + diff_log_uniform_degree_dist(key,subg,p,q,degreeType);
                }
            }
        }   
    }
};



struct CompressionMeasureConfiguration : ICompressionMeasureConfiguration<ConfigurationModel>{
 
    CompressionMeasureConfiguration(GraphOperationType type,std::shared_ptr<Graph<int>> &G):
    ICompressionMeasureConfiguration<ConfigurationModel>{type,G}{}

    // std::size_t get_supernode_value(TypeParameter degreeType,  const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id){
    //     switch(degreeType){
    //         case TypeParameter::Degree:
    //             return this->G->get_subg_degree(subg,Neighborhood::undirected);
    //             break;
    //         case TypeParameter::OutDegree: 
    //             return this->G->get_subg_degree(subg,Neighborhood::out);
    //             break;
    //         case TypeParameter::InDegree: 
    //             return this->G->get_subg_degree(subg,Neighborhood::in);
    //             break;
    //         default: 
    //             return 0;
    //             break;
    //     }
    // }

    // std::vector<std::size_t> get_new_subsequence(TypeParameter degreeType,  const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id){
    //     switch(degreeType){
    //         case TypeParameter::Degree:
    //             return {this->G->get_subg_degree(subg,Neighborhood::undirected)};
    //             break;
    //         case TypeParameter::OutDegree: 
    //             return {this->G->get_subg_degree(subg,Neighborhood::out)};
    //             break;
    //         case TypeParameter::InDegree: 
    //             return {this->G->get_subg_degree(subg,Neighborhood::in)};
    //             break;
    //         default: 
    //             return 0;
    //             break;
    //     }
    // }

    double diff_log_uniform_degree_dist(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) override{
        
        int idx       = paramDegree_a_id * paramDegree.size() + paramDegree_b_id;
        std::size_t K = paramStub  [degreeType][idx];
        auto it_k     = paramDegree[degreeType][idx].begin();
        auto k        = paramDegree[degreeType][idx];

        std::size_t deltaK = graphletParamStub[key][degreeType][idx];
        std::size_t k_subg{};

        switch(degreeType){
            case TypeParameter::Degree:
                k_subg = this->G->get_subg_degree(subg,Neighborhood::undirected);
                break;
            case TypeParameter::OutDegree: 
                k_subg = this->G->get_subg_degree(subg,Neighborhood::out);
                break;
            case TypeParameter::InDegree: 
                k_subg = this->G->get_subg_degree(subg,Neighborhood::in);
                break;
            default: 
                break;
        }

        std::vector<std::size_t> subset,new_subset;
        new_subset.push_back(k_subg);
        for (auto &&i : subg){
            subset.push_back(*(it_k + i));
        }
        this->dL.model += this->seq_code[degreeType][idx]->get_swap_cost(subset,new_subset);

        return (std::lgamma(K + 1.) - std::lgamma(K - deltaK + 1.) +
                std::lgamma(k_subg + 1.)  -
                std::accumulate(subg.begin(),subg.end(),0.,
                [&it_k](double a, int b){
                    return std::move(a) + std::lgamma(*(it_k + b) + 1.);
                }))/std::log(2);        
    }

    double diff_log_Perm(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id) override{
        
        switch (this->G->dir){

            case EdgeType::directed:{
                std::size_t e = *graphletParamStub[key][TypeParameter::InDegree].begin(); 
                std::size_t E = *paramStub[TypeParameter::InDegree].begin();
                return (std::lgamma(E + 1) - std::lgamma(E - e + 1))/std::log(2);
                // return 0.;
                break;
            }

            case EdgeType::undirected:{
                std::size_t e = *graphletParamStub[key][TypeParameter::Degree].begin(); 
                std::size_t E = *paramStub[TypeParameter::Degree].begin() / 2;
                return (std::lgamma(E + 1) - std::lgamma(E - e + 1))/std::log(2) + e;
                break;
            }
            
            default:
                return 0.;
                break;
        }
    }

    double diff_log_M(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) override{

        std::set<int> subg_neighbors{this->G->get_subg_neighborhood(subg)};

        double delta_A{};

        for (auto &&i : subg_neighbors){
            switch(degreeType){
                case TypeParameter::Degree:{

                    delta_A +=  std::lgamma(std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(b,i);
                                    }) + 1)
                    -   std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + std::lgamma(this->G->at(b,i) + 1);
                                    });
                    break;
                }
                case TypeParameter::OutDegree:{

                    delta_A +=  std::lgamma(std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(b,i);
                                    }) + 1)
                    -   std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + std::lgamma(this->G->at(b,i) + 1);
                                    });
                    break;
                }
                case TypeParameter::InDegree:{

                    delta_A +=  std::lgamma(std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i](double a, int b){
                                            return std::move(a) + this->G->at(i,b);
                                        }) + 1.)
                        -   std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i](double a, int b){
                                            return std::move(a) + std::lgamma(this->G->at(i,b) + 1);
                                        });
                    break;
                }
                default: 
                    break;
            }            
        }

        return -delta_A/std::log(2);
        
   

    }

    void add_graphlet_param(std::shared_ptr<IGraphlet> &g) override{

        switch (G->dir){

            case EdgeType::undirected:
                graphletParamStub[g->gtrie_label][TypeParameter::Degree]   .emplace_back(2 * g->number_of_edges());
                break;

            case EdgeType::directed:{
                graphletParamStub[g->gtrie_label][TypeParameter::InDegree] .emplace_back(g->number_of_edges());
                graphletParamStub[g->gtrie_label][TypeParameter::OutDegree].emplace_back(g->number_of_edges());
                break;
            }

            default:  
                std::cout << "should not happen" << std::endl;
                break;
        }
        
        graphletParam    [g->gtrie_label][TypeParameter::AutomorphismGroupSize] = g->autSize;
        graphletParam    [g->gtrie_label][TypeParameter::GraphletNumber]        = 0;
    }

    void update_param(std::shared_ptr<GraphletComponent> &gc) override{
        auto subg = gc->get_subg();
        

        switch(this->G->dir){
            case EdgeType::undirected:{
                this->paramStub[TypeParameter::Degree][0] -= 2 * gc->g->number_of_edges();

                std::vector<std::size_t> subset,new_subset;
                auto it_k = this->paramDegree[TypeParameter::Degree][0].begin();

                for (auto &&i : subg){
                    subset.push_back(*(it_k + i));
                    *(it_k + i) = 0;
                }

                auto k_subg = this->G->get_node_degree(subg[0],Neighborhood::undirected);
                *(it_k + subg[0]) = k_subg;
                this->seq_code[TypeParameter::Degree][0]->update_after_swap(subset, new_subset);

                break;
            }
            case EdgeType::directed:{
                this->paramStub  [TypeParameter::InDegree] [0] -= gc->g->number_of_edges();
                this->paramStub  [TypeParameter::OutDegree][0] -= gc->g->number_of_edges();

                
                auto it_k_in  = this->paramDegree[TypeParameter::InDegree] [0].begin();
                auto it_k_out = this->paramDegree[TypeParameter::OutDegree][0].begin();
                std::vector<std::size_t> subset_in,subset_out,new_subset_in,new_subset_out;

                for (auto &&i : subg){
                    subset_in .push_back(*(it_k_in  + i));
                    subset_out.push_back(*(it_k_out + i));
                    *(it_k_in  + i) = 0;
                    *(it_k_out + i) = 0;
                }


                auto k_subg_in  = this->G->get_node_degree(subg[0],Neighborhood::in);
                auto k_subg_out = this->G->get_node_degree(subg[0],Neighborhood::out);
                *(it_k_in   + subg[0]) = k_subg_in;
                *(it_k_out  + subg[0]) = k_subg_out;
                new_subset_in .push_back(k_subg_in);
                new_subset_out.push_back(k_subg_out);

                this->seq_code[TypeParameter::InDegree] [0]->update_after_swap(subset_in, new_subset_in);
                this->seq_code[TypeParameter::OutDegree][0]->update_after_swap(subset_out,new_subset_out);

                break;
            }
            default:
                break;
        }
        common_updating(gc); 
    }   


};

#endif