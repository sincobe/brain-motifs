#ifndef diff_codelength_rer_hpp 
#define diff_codelength_rer_hpp 

#include "erdos-renyi.hpp"

struct CompressionMeasureReciprocalMultiER : ICompressionMeasureER<ReciprocalMultiER>{

    std::map<TypeParameter,long> deltaE_map;

    CompressionMeasureReciprocalMultiER(GraphOperationType type,std::shared_ptr<Graph<int>> &G):
    ICompressionMeasureER<ReciprocalMultiER>{type,G}{}

    void set_preliminaries(const std::string &key, const std::vector<int> &subg) override{

        for (auto &&[type,unused] : this->paramEdge){
            set_deltaE(key,subg,type);
        }
       
    }

    void set_deltaE(const std::string &key, const std::vector<int> &subg, TypeParameter edgeType){

        long N = *paramNode.begin();
        long E = *paramEdge[edgeType].begin();
        long e = graphletParamEdge[key][edgeType]; 

        long deltaE;
        deltaE = -e;

        for (auto &neighbor : this->G->get_subg_neighborhood(subg)){
            switch (edgeType){
                case TypeParameter::MutualEdgeNumber:{
                    deltaE += std::min(std::accumulate(subg.begin(),subg.end(),0,[&neighbor,this](long a, int subg_node){
                                        return std::move(a) + this->G->at(subg_node,neighbor);
                                    }),
                                    std::accumulate(subg.begin(),subg.end(),0,[&neighbor,this](long a, int subg_node){
                                        return std::move(a) + this->G->at(neighbor,subg_node);
                                    }))
                            -       std::accumulate(subg.begin(),subg.end(),0,[&neighbor,this](long a, int subg_node){
                                        return std::move(a) + std::min(this->G->at(subg_node,neighbor),this->G->at(neighbor,subg_node));
                                    });
                    break;
                }
                case TypeParameter::SingleEdgeNumber:{
                    deltaE += std::abs(std::accumulate(subg.begin(),subg.end(),0,[&neighbor,this](long a, int subg_node){
                                        return std::move(a) + this->G->at(subg_node,neighbor);
                                    })  
                            -       std::accumulate(subg.begin(),subg.end(),0,[&neighbor,this](long a, int subg_node){
                                        return std::move(a) + this->G->at(neighbor,subg_node);
                                    }))
                            -       std::accumulate(subg.begin(),subg.end(),0,[&neighbor,this](long a, int subg_node){
                                        return std::move(a) + std::abs(this->G->at(subg_node,neighbor) - this->G->at(neighbor,subg_node));
                                    });
                    break;
                }
                default:{  
                    std::cout << "Should Not Happen" << std::endl;
                    break;
                }
            }
        }

        deltaE_map[edgeType] = deltaE;
    }

    double diff_node_number_encoding(const std::string &key, const std::vector<int> &subg) override{
        return PositiveIntegerCodelength(this->paramNode[0] + 1.).get() - PositiveIntegerCodelength(this->paramNode[0] - subg.size() + 2.).get();
    }

    double diff_edge_number_encoding(const std::string &key, const std::vector<int> &subg, TypeParameter &edgeType) override{
        long deltaE = deltaE_map[edgeType];
        return PositiveIntegerCodelength(this->paramEdge[edgeType][0] + 1.).get() - PositiveIntegerCodelength(this->paramEdge[edgeType][0] + deltaE + 1.).get();
    }

    double diff_log_w(const std::string &key, const std::vector<int> &subg, int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{

        long N = *paramNode.begin();
        long E = *paramEdge[edgeType].begin();
        long deltaE = deltaE_map[edgeType];
      
        switch (edgeType){
            case TypeParameter::SingleEdgeNumber:{
                return 2 * E * std::log2(N) - 2 * (E + deltaE) * std::log2(N - subg.size() + 1) - (std::lgamma(E + 1) - std::lgamma(E + deltaE + 1))/std::log(2);
                break;
            }
            case TypeParameter::MutualEdgeNumber:{
                return  E * (std::log2(N * (N + 1.)) - 1.) - (E + deltaE) * (std::log2((N - subg.size() + 2.) * (N - subg.size() + 1.)) - 1.) 
                        -  (std::lgamma(E + 1.) - std::lgamma(E + deltaE + 1.))/std::log(2);
                break;
            }
            default:{
                return 0.;
                std::cout << "Should Not Happen" << std::endl;
                break;
            }
        }

    }

    double diff_log_M(const std::string &key, const std::vector<int> &subg, int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{

        std::set<int> subg_neighbors{this->G->get_subg_neighborhood(subg)};

        double delta_A{};

        for (auto &&i : subg_neighbors){

            switch (edgeType)   
            {
            case TypeParameter::SingleEdgeNumber:{
                delta_A +=  std::lgamma(std::abs(
                            std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(b,i);
                                    }) 
                        -   std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(i,b);
                                    }))+ 1)
                        -   std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + std::lgamma(std::abs(this->G->at(b,i) - this->G->at(i,b))  + 1);
                                    });
                break;
            }

            case TypeParameter::MutualEdgeNumber:{
                delta_A +=  std::lgamma(std::min(
                            std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(b,i);
                                    }), 
                            std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + this->G->at(i,b);
                                    }))+ 1)
                        -   std::accumulate(subg.begin(),subg.end(),0.,
                                    [this,&i](double a, int b){
                                        return std::move(a) + std::lgamma(std::min(this->G->at(b,i),this->G->at(i,b))  + 1);
                                    });
                break;
            }
            
            default:
                break;
            }
        }


        return -delta_A/std::log(2);
        // return 0.;

    }

    void add_graphlet_param(std::shared_ptr<IGraphlet> &g) override{
        graphletParamEdge[g->gtrie_label][TypeParameter::SingleEdgeNumber]      = g->number_of_single_edges();
        graphletParamEdge[g->gtrie_label][TypeParameter::MutualEdgeNumber]      = g->number_of_mutual_edges();
        graphletParam    [g->gtrie_label][TypeParameter::AutomorphismGroupSize] = g->autSize;
        graphletParam    [g->gtrie_label][TypeParameter::GraphletNumber]        = 0;
    }

    void update_param(std::shared_ptr<GraphletComponent> &gc) override{
        auto subg = gc->get_subg();

        this->paramNode[0]                                      -= subg.size() - 1;

        this->paramEdge  [TypeParameter::MutualEdgeNumber][0] = this->G->number_of_mutual_edges();
        this->paramEdge  [TypeParameter::SingleEdgeNumber][0] = this->G->number_of_single_edges();

        common_updating(gc);
    }

};

#endif