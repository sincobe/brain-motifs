#ifndef reciprocal_configuration_comoression_measure_hpp
#define reciprocal_configuration_comoression_measure_hpp

#include "configuration.hpp"

struct CompressionMeasureReciprocalConfiguration : ICompressionMeasureConfiguration<ReciprocalConfigurationModel>{
 
    CompressionMeasureReciprocalConfiguration(GraphOperationType type, std::shared_ptr<Graph<int>> &G):
    ICompressionMeasureConfiguration<ReciprocalConfigurationModel>{type,G}{}

    double diff_log_uniform_degree_dist(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) override{
        
        int idx       = paramDegree_a_id * paramDegree.size() + paramDegree_b_id;
        std::size_t K = paramStub  [degreeType][idx];
        auto it_k     = paramDegree[degreeType][idx].begin();


        std::size_t deltaK =  graphletParamStub[key][degreeType][idx] + std::accumulate(subg.begin(),subg.end(),0,
                                                                        [&it_k](std::size_t a, int b){
                                                                            return std::move(a) + *(it_k + b);
                                                                        });

        std::set<int> subg_neighbors = this->G->get_subg_neighborhood(subg);
        std::vector<long> delta_k_vec(this->G->N);

        for (auto &&i : subg){
            delta_k_vec[i] -= *(it_k + i);
        }

        for (auto &&j : subg_neighbors){

            long oldValue{},newValue{};
            switch(degreeType){
                case TypeParameter::Degree:{
                    newValue = std::min(std::accumulate(subg.begin(),subg.end(),0,
                                            [&j,this](long a, int subg_node){
                                                return std::move(a) + this->G->at(j,subg_node);
                                        }),
                                        std::accumulate(subg.begin(),subg.end(),0,
                                            [&j,this](long a, int subg_node){
                                                return std::move(a) + this->G->at(subg_node,j);
                                        }));
                    oldValue = std::accumulate(subg.begin(),subg.end(),0,[&j,this](long a, int subg_node){
                                                    return std::move(a) + std::min(this->G->at(subg_node,j),this->G->at(j,subg_node));
                                                    });
                    break;
                }
                case TypeParameter::OutDegree:{
                    newValue = std::max(std::accumulate(subg.begin(),subg.end(),0,
                        [&j,this](long a, int subg_node){
                            return std::move(a) + this->G->at(subg_node,j);
                    }) - 
                    std::accumulate(subg.begin(),subg.end(),0,
                        [&j,this](long a, int subg_node){
                            return std::move(a) + this->G->at(j,subg_node);
                    }),0);
                    oldValue = std::accumulate(subg.begin(),subg.end(),0,[&j,this](long a, int subg_node){
                                                    return std::move(a) + std::max(this->G->at(subg_node,j)-this->G->at(j,subg_node),0);
                                                    });
                    break;
                }
                case TypeParameter::InDegree:{
                    newValue = std::max(std::accumulate(subg.begin(),subg.end(),0,
                        [&j,this](long a, int subg_node){
                            return std::move(a) + this->G->at(j,subg_node);
                    }) - 
                    std::accumulate(subg.begin(),subg.end(),0,
                        [&j,this](long a, int subg_node){
                            return std::move(a) + this->G->at(subg_node,j);
                    }),0);
                    oldValue = std::accumulate(subg.begin(),subg.end(),0,[&j,this](long a, int subg_node){
                                                    return std::move(a) + std::max(this->G->at(j,subg_node)-this->G->at(subg_node,j),0);
                                                    });
                    break;
                }
                default:  
                    break;
            }
     
            delta_k_vec[j] = newValue - oldValue;
        }

        std::size_t k_subg;
        switch(degreeType){
            case TypeParameter::Degree:{
                k_subg = this->G->get_subg_degree(subg,Neighborhood::mutual);
                break;
            }
            case TypeParameter::OutDegree:{
                k_subg  = this->G->get_subg_degree(subg,Neighborhood::single_out);
                break;
            }
            case TypeParameter::InDegree:{
                k_subg  = this->G->get_subg_degree(subg,Neighborhood::single_in);
                break;
            }
            default:  
                break;
        }

        std::vector<std::size_t> subset,new_subset;
        new_subset.push_back(k_subg);
        for (auto &&i : subg){
            subset.push_back(*(it_k + i));
        }
        for (auto &&j : subg_neighbors){
            subset.push_back(*(it_k + j));
            new_subset.push_back(*(it_k + j) + *(delta_k_vec.begin() + j));
        }

        this->dL.model += this->seq_code[degreeType][idx]->get_swap_cost(subset,new_subset);


        return      (std::lgamma(K + 1.) - std::lgamma(K + k_subg + std::accumulate(delta_k_vec.begin(),delta_k_vec.end(),0) + 1.) 
                -   std::accumulate(subg_neighbors.begin(),subg_neighbors.end(),0.,
                    [&it_k](double a, int b){ 
                        return std::move(a) + std::lgamma(*(it_k + b) + 1);
                     }) 
                +   std::accumulate(subg_neighbors.begin(),subg_neighbors.end(),0.,
                    [&it_k, &delta_k_vec](double a, int b){ 
                        return std::move(a) + std::lgamma(*(it_k + b) + delta_k_vec[b] + 1);
                    })
                -   std::accumulate(subg.begin(),subg.end(),0.,
                    [&it_k](double a, int b){
                        return std::move(a) + std::lgamma(*(it_k + b) + 1.);
                    })
                +   std::lgamma(k_subg + 1))/std::log(2);


        
    }

    double diff_log_Perm(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id) override{
    
        std::size_t single_e = *graphletParamStub[key][TypeParameter::InDegree].begin(); 
        std::size_t mutual_e = *graphletParamStub[key][TypeParameter::Degree].begin()/2; 
        std::size_t singleE  = *paramStub[TypeParameter::InDegree].begin();
        std::size_t mutualE  = *paramStub[TypeParameter::Degree].begin()/2;

        std::size_t k_subg_mutual = this->G->get_subg_degree(subg,Neighborhood::mutual);

        auto it_k     = paramDegree[TypeParameter::Degree].begin()->begin();

        long deltaMutualE{},deltaSingleE{};
        deltaMutualE += mutual_e + k_subg_mutual - std::accumulate(subg.begin(),subg.end(),0,
                                                                  [&it_k](long a, int b){
                                                                    return std::move(a) + *(it_k + b);
                                                                  });
        deltaSingleE -= single_e + 2 * mutual_e + 2 * deltaMutualE; 

        return (std::lgamma(singleE + 1) - std::lgamma(singleE + deltaSingleE + 1) + 
                std::lgamma(mutualE + 1) - std::lgamma(mutualE + deltaMutualE + 1))/std::log(2) - deltaMutualE;

    }

    double diff_log_M(const std::string &key, const std::vector<int> &subg, int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) override{

        double delta_A{};
        if (degreeType == TypeParameter::Degree || degreeType == TypeParameter::InDegree){
            std::set<int> subg_neighbors{this->G->get_subg_neighborhood(subg)};

            for (auto &&i : subg_neighbors){
                switch(degreeType){
                    case TypeParameter::Degree:{
                        delta_A +=  std::lgamma(std::min(std::accumulate(subg.begin(),subg.end(),0.,
                                                [this,&i](double a, int b){
                                                    return std::move(a) + this->G->at(b,i);
                                                }),
                                                std::accumulate(subg.begin(),subg.end(),0.,
                                                [this,&i](double a, int b){
                                                    return std::move(a) + this->G->at(i,b);
                                                })) + 1.) -

                                                std::accumulate(subg.begin(),subg.end(),0.,
                                                [this,&i](double a, int b){
                                                    return std::move(a) + std::lgamma(std::min(this->G->at(b,i),this->G->at(i,b)) + 1.);
                                                });
                        break;
                    }

                    case TypeParameter::InDegree:{
                        delta_A +=  std::lgamma(std::abs(std::accumulate(subg.begin(),subg.end(),0.,
                                                [this,&i](double a, int b){
                                                    return std::move(a) + this->G->at(i,b);
                                                }) - 
                                                std::accumulate(subg.begin(),subg.end(),0.,
                                                [this,&i](double a, int b){
                                                    return std::move(a) + this->G->at(b,i);
                                                })) + 1.) -

                                                std::accumulate(subg.begin(),subg.end(),0.,
                                                [this,&i](double a, int b){
                                                    return std::move(a) + std::lgamma(std::abs(static_cast<double>(this->G->at(i,b)) - this->G->at(b,i)) + 1.);
                                                });
                        break;
                    }

                    default:
                        break;
                }
            }
        }
       
        return -delta_A/std::log(2);
    }

    void add_graphlet_param(std::shared_ptr<IGraphlet> &g) override{

        graphletParamStub[g->gtrie_label][TypeParameter::Degree]   .emplace_back(2 * g->number_of_mutual_edges());
        graphletParamStub[g->gtrie_label][TypeParameter::InDegree] .emplace_back(g->number_of_single_edges());
        graphletParamStub[g->gtrie_label][TypeParameter::OutDegree].emplace_back(g->number_of_single_edges());

        graphletParam    [g->gtrie_label][TypeParameter::AutomorphismGroupSize] = g->autSize;
        graphletParam    [g->gtrie_label][TypeParameter::GraphletNumber]        = 0;
    }

    void update_param(std::shared_ptr<GraphletComponent> &gc) override{
        auto subg = gc->get_subg();


        this->paramStub  [TypeParameter::Degree]   [0] = 2*this->G->number_of_mutual_edges();
        this->paramStub  [TypeParameter::InDegree] [0] =   this->G->number_of_single_edges();
        this->paramStub  [TypeParameter::OutDegree][0] =   this->G->number_of_single_edges();

        auto it_k     = this->paramDegree[TypeParameter::Degree]   [0].begin();
        auto it_k_in  = this->paramDegree[TypeParameter::InDegree] [0].begin();
        auto it_k_out = this->paramDegree[TypeParameter::OutDegree][0].begin();
        std::vector<std::size_t> subset,subset_in,subset_out,new_subset,new_subset_in,new_subset_out;
        for (auto &&i : subg){
            subset    .push_back(*(it_k     + i));
            subset_in .push_back(*(it_k_in  + i));
            subset_out.push_back(*(it_k_out + i));
            *(it_k     + i) = 0;
            *(it_k_in  + i) = 0;
            *(it_k_out + i) = 0;
        }

        auto k_subg     = this->G->get_node_degree(subg[0],Neighborhood::mutual);
        auto k_subg_in  = this->G->get_node_degree(subg[0],Neighborhood::single_in);
        auto k_subg_out = this->G->get_node_degree(subg[0],Neighborhood::single_out);
        *(it_k      + subg[0]) = k_subg;
        *(it_k_in   + subg[0]) = k_subg_in;
        *(it_k_out  + subg[0]) = k_subg_out;
        new_subset    .push_back(k_subg);
        new_subset_in .push_back(k_subg_in);
        new_subset_out.push_back(k_subg_out);
        

        for (auto &j : this->G->get_subg_neighborhood(subg)){
            
            subset.push_back(*(it_k + j));
            *(it_k + j)   = this->G->get_node_degree(j,Neighborhood::mutual); 
            new_subset.push_back(*(it_k + j));

            
            subset_in.push_back(*(it_k_in + j));
            *(it_k_in + j) = this->G->get_node_degree(j,Neighborhood::single_in); 
            new_subset_in.push_back(*(it_k_in + j));

            subset_out.push_back(*(it_k_out + j));
            *(it_k_out + j) = this->G->get_node_degree(j,Neighborhood::single_out);
            new_subset_out.push_back(*(it_k_out + j));    
        }



        this->seq_code[TypeParameter::InDegree] [0]->update_after_swap(subset_in, new_subset_in);
        this->seq_code[TypeParameter::OutDegree][0]->update_after_swap(subset_out,new_subset_out);
        this->seq_code[TypeParameter::Degree]   [0]->update_after_swap(subset, new_subset);
        
        common_updating(gc);
    }



};

#endif