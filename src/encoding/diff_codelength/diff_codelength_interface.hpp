#ifndef diff_codelength_hpp 
#define diff_codelength_hpp 

#include "../code.hpp"
#include "../basic_func.hpp"
#include "graphlet_component.hpp"

enum class GraphOperationType{

    SubgContraction

};


enum class CompressionTypeParameter{
    NodeNumber,
    GraphletNumber,
    MaxGraphletNumber,
    GraphletSubsetSize,
    SubgraphSequenceSize,
    Rewiring
};


template<class T>
struct CompressionMeasure : T{
    GraphOperationType graph_op;
    Codelength dL;
    std::map<CompressionTypeParameter,double> methodParam;
    std::map<std::string,std::map<TypeParameter,std::size_t>> graphletParam;

    // template<typename ... Args>
    // CompressionMeasure(GraphOperationType type, Args ...args):graph_op{type},
    // T(std::forward<Args>(args)...),dL{}{}

    void adapt(std::shared_ptr<GraphletComponent> &gc) override{
        if (graphletParam.find(gc->g->gtrie_label) == graphletParam.end()){
            add_graphlet_param(gc->g);
        }

        auto key  = gc->g->gtrie_label;
        auto subg = gc->get_subg();
        auto gc_temp = std::make_shared<GraphletComponent>(gc);
        gc_temp->candidate.second = get_codelength_difference (key,subg);
        SubgraphContractor(this->G,gc->g->gtrie_label,gc->get_subg());
        update_param(gc_temp);
    }

    virtual void update_param(std::shared_ptr<GraphletComponent> &gc) = 0;

    void common_updating(std::shared_ptr<GraphletComponent> &gc){
        methodParam[CompressionTypeParameter::SubgraphSequenceSize]++;
        methodParam[CompressionTypeParameter::NodeNumber] -= gc->g->size - 1;
        auto it_graphletNumber = graphletParam.find(gc->g->gtrie_label)->second.find(TypeParameter::GraphletNumber);
        it_graphletNumber->second++;
        if (it_graphletNumber->second == 1) { 
            methodParam[CompressionTypeParameter::GraphletSubsetSize]++; 
        }
        else if (it_graphletNumber->second > methodParam[CompressionTypeParameter::MaxGraphletNumber]){ 
            methodParam[CompressionTypeParameter::MaxGraphletNumber] = it_graphletNumber->second; 
        }
        this->L -= gc->candidate.second;
    }

    template<typename U>
    CompressionMeasure(GraphOperationType type, std::shared_ptr<Graph<U>> &G) :
    T{G},graph_op{type},dL{}{
        methodParam[CompressionTypeParameter::NodeNumber]           = G->N;
        methodParam[CompressionTypeParameter::SubgraphSequenceSize] = 0;
        methodParam[CompressionTypeParameter::MaxGraphletNumber]    = 0; 
        methodParam[CompressionTypeParameter::GraphletSubsetSize]   = 0;

    }

    virtual void set_preliminaries(const std::string &key,const std::vector<int> &subg) = 0;
    virtual void set_model_codelength_difference(const std::string &key,const std::vector<int> &subg) = 0;
    virtual void set_data_codelength_difference (const std::string &key,const std::vector<int> &subg) = 0;


    double get_method_codelength_difference(const std::string &key, const std::vector<int> &subg, CompressionTypeParameter type) const{
        switch(type){

            case CompressionTypeParameter::NodeNumber:{
                std::size_t N_H = methodParam.find(CompressionTypeParameter::NodeNumber)->second;
                return (std::lgamma(N_H - subg.size() + 2) - std::lgamma(N_H + 1))/std::log(2);
                break;
            }

            case CompressionTypeParameter::SubgraphSequenceSize:{
                std::size_t autSize      = graphletParam.find(key)->second.find(TypeParameter::AutomorphismGroupSize) ->second;
                std::size_t n_graphlet   = graphletParam.find(key)->second.find(TypeParameter::GraphletNumber)        ->second;
                std::size_t N_supernodes = methodParam  .find(CompressionTypeParameter::SubgraphSequenceSize)         ->second;
                std::size_t N_H          = methodParam  .find(CompressionTypeParameter::NodeNumber)                   ->second;
                std::size_t n_max        = methodParam  .find(CompressionTypeParameter::MaxGraphletNumber)            ->second;
                std::size_t graphletSetSize    = graphletParam.size();
                std::size_t graphletSubsetSize = methodParam.find(CompressionTypeParameter::GraphletSubsetSize)       ->second;

                std::size_t n_max_new = std::max(n_graphlet+1,n_max);
                return    std::log2(static_cast<double>(n_graphlet+1)/(N_supernodes+1)) 
                        + (lgbinom(N_H,N_supernodes) - lgbinom(N_H - subg.size() + 1,N_supernodes+1))/std::log(2)                 
                        - (n_max >= n_max_new ? 0. : (graphletSubsetSize * std::log2(n_max_new)    - 
                          (n_max == 0  ? 0. :  graphletSubsetSize * std::log2(n_max))       +
                          PositiveIntegerCodelength(n_max_new).get()   - 
                          PositiveIntegerCodelength(n_max)    .get())) - 
                          (n_graphlet == 0 ? std::log2(n_max_new * graphletSetSize) : 0.)           
                        - std::lgamma(subg.size()+1)/std::log(2) + std::log2(autSize);                      
                break;
            }

            case CompressionTypeParameter::Rewiring:{
                std::set<int> subg_neighbors{this->G->get_subg_neighborhood(subg)};
                double delta_A{};

                for (auto &&i : subg_neighbors){

                    switch (this->G->nodes[i]->state){

                        case NodeState::active: {
                            delta_A +=  lgbinom(subg.size(),
                                        std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i,&subg](int a, int b){
                                            return std::move(a) + this->G->at(b,i);
                                        }));
                            break;
                        }

                        case NodeState::supernode: {
                            delta_A +=  lgbinom(subg.size() * this->G->get_node_property(i,NodeProperty::size), 
                                        std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i,&subg](int a, int b){
                                            return std::move(a) + this->G->at(b,i);
                                        })) 
                                    -   std::accumulate(subg.begin(),subg.end(),0.,
                                        [this,&i,&subg](double a, int b){
                                            return std::move(a) + 
                                            lgbinom(this->G->get_node_property(i,NodeProperty::size),this->G->at(b,i));
                                        });
                            break;
                        }

                        case NodeState::inactive:{
                            break;
                        }
                        
                    }

                    if (this->G->dir == EdgeType::directed){

                        switch (this->G->nodes[i]->state){
                            case NodeState::active: {
                                delta_A +=  lgbinom(subg.size(),
                                            std::accumulate(subg.begin(),subg.end(),0.,
                                            [this,&i,&subg](int a, int b){
                                                return std::move(a) + this->G->at(i,b);
                                            }));
                                break;
                            }
                            case NodeState::supernode: {
                                delta_A +=  lgbinom(subg.size() * this->G->get_node_property(i,NodeProperty::size), 
                                            std::accumulate(subg.begin(),subg.end(),0.,
                                            [this,&i,&subg](int a, int b){
                                                return std::move(a) + this->G->at(i,b);
                                            })) 
                                        -   std::accumulate(subg.begin(),subg.end(),0.,
                                            [this,&i,&subg](double a, int b){
                                                return std::move(a) + 
                                                lgbinom(this->G->get_node_property(i,NodeProperty::size),this->G->at(i,b));
                                            });
                                break;
                            }

                            case NodeState::inactive:{
                                break;
                            }
                            
                        }
                    }
                }
                return -delta_A/std::log(2);
                break;
            }

            default: 
                return 0.;
                break;
        }
    }

    virtual void add_graphlet_param(std::shared_ptr<IGraphlet> &g) = 0;
    inline Codelength get_codelength_difference(const std::string &key, const std::vector<int> &subg){
        set_preliminaries(key,subg);
        set_model_codelength_difference(key,subg);
        set_data_codelength_difference (key,subg);

        double methodCost{};

        auto params = {
            CompressionTypeParameter::SubgraphSequenceSize,
            CompressionTypeParameter::NodeNumber,
            CompressionTypeParameter::Rewiring
        };

        for (auto &&param : params){
            methodCost += get_method_codelength_difference(key,subg,param);
        }

        dL.additional = methodCost;
        return dL;
    }


};




#endif