#ifndef graphlet_component_hpp
#define graphlet_component_hpp

#include <subgraph_minibatch.hpp>
#include <graphlet.hpp>
#include "codelength.hpp"

#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>

struct GraphletComponent{

    std::shared_ptr<IGraphlet> g;
    SubgraphMinibatch minibatch;
    std::vector<Codelength> scores;
    std::pair< std::vector<int>, Codelength > candidate;

    GraphletComponent(const std::size_t &_B, const std::string &filename, const nlohmann::json &j, bool activate_chunk_reading = false) : 
    minibatch{SubgraphMinibatch(_B,filename,j,activate_chunk_reading)},candidate{}{
        std::string label = j["gtrie_label"];
        int size          = j["number_of_nodes"];
        g = std::make_shared<TopologicalGraphlet>(size,EdgeType::directed,label);
        scores.resize(_B);
    }

    GraphletComponent(const std::size_t &_B, const std::size_t &totalSubgNumber, const std::string &filename, const nlohmann::json &j, bool activate_chunk_reading = false) : 
    minibatch{SubgraphMinibatch(_B,totalSubgNumber,filename,j,activate_chunk_reading)},candidate{}{
        std::string label = j["gtrie_label"];
        int size          = j["number_of_nodes"];
        g = std::make_shared<TopologicalGraphlet>(size,EdgeType::directed,label);
        scores.resize(minibatch.B);
    }

    GraphletComponent(const nlohmann::json &j) : 
    minibatch{},candidate{}{
        std::string label = j["gtrie_label"];
        int size          = j["number_of_nodes"];
        g = std::make_shared<TopologicalGraphlet>(size,EdgeType::directed,label);
    }

    GraphletComponent(std::shared_ptr<GraphletComponent> &gc) : minibatch{}{
        g = gc->g;
        candidate = gc->candidate;
        scores.resize(1);
    }

    inline void set_default_candidate(){
        candidate = std::pair<std::vector<int>,Codelength>(std::vector<int>(),Codelength(-1e32,-1e32)); 
    }

    template<class Compressor, class Constraint, class Scoring>
    inline void update(const Compressor &obj, const Constraint &constraint_rule, const Scoring &scoring_func){
        if (minibatch.subgs.size()){
            minibatch.update(constraint_rule,obj);
            if (minibatch.subgs.size()){
                if (scores.size() != minibatch.subgs.size()){
                    scores.resize(minibatch.subgs.size());
                }


                std::transform(minibatch.subgs.cbegin(),minibatch.subgs.cend(),scores.begin(),
                [&obj,&scoring_func,this](const std::vector<int> &subg){
                    return scoring_func(obj,this->g->gtrie_label,subg);
                });

                auto it_score = std::max_element(scores.begin(),scores.end(), [] (const Codelength &a, const Codelength &b){ return a.get() < b.get(); });
                auto it_subg  = minibatch.subgs.begin() + std::distance(scores.begin(),it_score);

                candidate = std::pair<std::vector<int>,Codelength>(*it_subg,*it_score); 
            }
            else{
                set_default_candidate();
            }
        }
        else{
            set_default_candidate();
        }

        
       

       
    }

    void reset(){
        minibatch.reset();
    }

    inline bool valid_subg() const{
        return static_cast<bool>(candidate.first.size());
    }

    inline std::vector<int>& get_subg(){
        return candidate.first;
    }
    inline Codelength& get_cost(){
        return candidate.second;
    } 
    
    inline std::shared_ptr<IGraphlet>& get_graphlet(){
        return g;
    }

};

struct CandidatePair{

    std::string gtrie_label;
    std::pair<std::vector<int>,Codelength> candidate;

    CandidatePair() = default;

    CandidatePair(const std::string &name, const std::pair<std::vector<int>,Codelength> &_candidate)
    : gtrie_label{name},candidate{_candidate}{}

    friend bool operator<(const CandidatePair &a, const CandidatePair &b){
        return a.candidate.second < b.candidate.second; 
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const int version){
        ar & gtrie_label;
        ar & candidate;
    }    

};

#endif