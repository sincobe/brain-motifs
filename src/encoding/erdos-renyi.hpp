#ifndef erdos_renyi_hpp
#define erdos_renyi_hpp

#include "code.hpp"


struct ICodeER : Code<int>{
    
   std::vector<std::size_t> paramNode;
   std::map<TypeParameter,std::vector<std::size_t>> paramEdge;

    template<typename T>
    ICodeER(const Graph<T> &_G) : Code<int>{_G}{};

    template<typename T>
    ICodeER(std::shared_ptr<Graph<T>> &_G) : Code<int>{_G}{};

    virtual ~ICodeER() = default;

    virtual void set_params() = 0;
    virtual double log_w(int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) = 0 ;
    virtual double log_M(int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) = 0 ;
    
    void set_model_codelength() override{

        L.model = 0.;
        
        for (auto &&n : paramNode){
            L.model += PositiveIntegerCodelength(n + 1.).get();
        }
        unsigned long Q = paramNode.size() ;
        int p,q;
        for (auto &param_edge_set : paramEdge){
            TypeParameter edgeType = param_edge_set.first;
            for (p = 0 ; p < Q ; p++){
                for (q = p ; q < Q ; q++){
                    L.model += PositiveIntegerCodelength(param_edge_set.second[p * Q + q] + 1.).get();
                }
            }
        }
        
    }

    void set_data_codelength()  override{

        L.data = 0.;
        unsigned long Q = paramNode.size() ;
        int p,q;
        for (auto &param_edge_set : paramEdge){
            TypeParameter edgeType = param_edge_set.first;
            for (p = 0 ; p < Q ; p++){
                for (q = p ; q < Q ; q++){
                    L.data += log_w(p,q,edgeType) + log_M(p,q,edgeType);
                }
            }
        }
    }

    virtual std::string get_name() const override{
        return "erdos-renyi"; 
    }

    virtual std::string get_prior_name() const override{
        return "integer_encoding"; 
    }

    virtual NullModel get_model_type() const override = 0 ;
};

struct MultiER : ICodeER{

    template<typename T>
    MultiER(const Graph<T> &_G) : ICodeER{_G}{
        set_params();
        set_codelength();
    }

 
    MultiER(std::shared_ptr<Graph<int>> &_G) : ICodeER{_G}{
        set_params();
        set_codelength();
    }

    void set_params() override{
        paramNode.emplace_back(std::accumulate(G->nodes.begin(),G->nodes.end(),0,[](std::size_t n, std::shared_ptr<INode> &node){
            return std::move(n) + (node->state != NodeState::inactive ? 1 : 0);
        }));
        paramEdge.emplace(TypeParameter::EdgeNumber,
                          std::vector<std::size_t>{G->number_of_edges()});
    }

    double log_w(int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{
        unsigned long N = *paramNode.begin();
        unsigned long E = *(paramEdge.begin()->second.begin());

        switch (G->dir){
            case EdgeType::directed:
                return 2 * E * std::log2(N) - std::lgamma(E + 1)/std::log(2);
                break;
            
            case EdgeType::undirected:
                return E * (std::log2(N * (N + 1)) - 1) - std::lgamma(E + 1)/std::log(2);
                break;

            default:
                return 0.; 
                break;
        }
    }

    double log_M(int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{

        double parallelEdgeCost = std::accumulate(this->G->adjMat.begin(),this->G->adjMat.end(),0.,
        [](double a, int b){
            return std::move(a) + std::lgamma(b + 1);
        });

        if (G->dir == EdgeType::undirected){
            parallelEdgeCost *= 0.5;
        }
        parallelEdgeCost /= std::log(2);

        return parallelEdgeCost;


    }

    std::string get_name() const override{
        return "erdos-renyi";
    }

    std::string get_prior_name() const override{
        return "integer_encoding"; 
    }

    NullModel get_model_type() const override {
        return NullModel::ErdosRenyi;
    }
};


struct ReciprocalMultiER : ICodeER{

    template<typename T>
    ReciprocalMultiER(const Graph<T> &_G) : ICodeER{_G}{
        set_params();
        set_codelength();
    }

    template<typename T>
    ReciprocalMultiER(std::shared_ptr<Graph<T>> &_G) : ICodeER{_G}{
        set_params();
        set_codelength();
    }

    void set_params() override{
        paramNode.emplace_back(std::accumulate(G->nodes.begin(),G->nodes.end(),0,[](std::size_t n, std::shared_ptr<INode> &node){
            return std::move(n) + (node->state != NodeState::inactive ? 1 : 0);
        }));

        paramEdge.emplace(TypeParameter::SingleEdgeNumber,
                          std::vector<unsigned long>{G->number_of_single_edges()});
        paramEdge.emplace(TypeParameter::MutualEdgeNumber,
                          std::vector<unsigned long>{G->number_of_mutual_edges()});
    }

    double log_w(int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{
        unsigned long N = *paramNode.begin();
        unsigned long E = *(paramEdge[edgeType].begin());

        switch (edgeType){
            case TypeParameter::SingleEdgeNumber:
                return 2 * E * std::log2(N) - std::lgamma(E + 1)/std::log(2);
                break;

            case TypeParameter::MutualEdgeNumber:
                return E * (std::log2(N * (N + 1)) - 1) - std::lgamma(E + 1)/std::log(2);
                break;
            
            default:
                return 0.;
                break;
        }

    }

    double log_M(int &paramNode_a_id, int &paramNode_b_id, TypeParameter &edgeType) override{

        double parallelEdgeCost{};

        int i,j;
        for (i = 0 ; i < G->sizeMat ; i++){
            for (j = i+1 ; j < G->sizeMat ; j++){

                switch (edgeType)
                {
                    
                case TypeParameter::SingleEdgeNumber:
                    parallelEdgeCost += std::lgamma(std::abs(G->at(i,j)-G->at(j,i)) + 1); 
                    break;

                case TypeParameter::MutualEdgeNumber:
                    parallelEdgeCost += std::lgamma(std::min(G->at(i,j),G->at(j,i)) + 1);
                    break;
                
                default:
                    break;
                }
                
            }
        }
        parallelEdgeCost /= std::log(2);

        return parallelEdgeCost;
    }

    std::string get_name() const override{
        return "reciprocal_erdos-renyi";
    }

    std::string get_prior_name() const override{
        return "integer_encoding"; 
    }

    NullModel get_model_type() const override {
        return NullModel::ReciprocalErdosRenyi;
    }
};

#endif