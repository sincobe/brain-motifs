#ifndef compression_tracker_hpp
#define compression_tracker_hpp

#include <json.hpp>
#include "diff_codelength/compression_measure_factory.hpp"

struct CompressionTracker{

    nlohmann::json j;
    int stepIdx;
    std::vector<std::shared_ptr<Code<int>>> measures;

    template<typename T>
    CompressionTracker(std::shared_ptr<Graph<T>> &G, GraphOperationType op,  std::vector<NullModel> &models) : stepIdx{}{
        for (auto &&model : models){
            auto G_copy = std::make_shared<Graph<int>>(G->dir,G->adjMat);
            measures.emplace_back(CompressionMeasureFactory::make(model,op,G_copy));
            j["step_"+std::to_string(stepIdx)][(*measures.rbegin())->get_name()]["initial_codelength"] = (*measures.rbegin())->L.get_json();
            j["step_"+std::to_string(stepIdx)][(*measures.rbegin())->get_name()]["prior_name"]         = (*measures.rbegin())->get_prior_name();
        }
    }

    template<class T>
    void set_reference_model(const std::shared_ptr<T> &code){
        j["reference_model"]["model_name"]         = code->get_name();
        j["reference_model"]["prior_name"]         = code->get_prior_name();
        j["reference_model"]["initial_codelength"] = code->L.get_json();
        
    }


    void add_json(nlohmann::json &_j,std::shared_ptr<GraphletComponent> &gc){
        stepIdx++;
        for (auto &measure : measures){
            measure->adapt(gc);
            j["step_"+std::to_string(stepIdx)][measure->get_name()]["codelength"] = measure->L.get_json();
        }
        j["step_"+std::to_string(stepIdx)]["reference_model"] = _j;
    }

};

#endif