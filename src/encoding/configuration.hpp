#ifndef configuration_hpp
#define configuration_hpp

#include "code.hpp"



struct ICodeConfiguration : Code<int>{

    std::map<TypeParameter, std::vector<std::vector<std::size_t>>> paramDegree;
    std::map<TypeParameter, std::vector<std::size_t>> paramStub;
    std::map<TypeParameter, std::vector<std::shared_ptr<IntegerSequenceEncoding>>> seq_code;

    template<typename T>
    ICodeConfiguration(const Graph<T> &_G) : Code{_G}{}

    template<typename T>
    ICodeConfiguration(std::shared_ptr<Graph<T>> &_G) : Code{_G}{}


    virtual void set_params() = 0;
    virtual double log_Perm(int &paramDegree_a_id, int &paramDegree_b_id) = 0 ;
    virtual double log_M   (int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) = 0 ;



    double log_uniform_degree_dist(int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType){
        int idx = paramDegree_a_id * paramDegree.size() + paramDegree_b_id;
        std::vector<std::size_t> k = paramDegree[degreeType][idx];
        std::size_t K              = paramStub  [degreeType][idx];


        auto lgamma_sum = [](double a, int b){
            return std::move(a) + std::lgamma(b + 1);
        };

        return (std::lgamma(K + 1) - 
                std::accumulate(k.begin(),k.end(),0.,lgamma_sum))/std::log(2);

    }

    void set_data_codelength() override{
        L.data = 0;

        unsigned long Q = paramDegree.begin()->second.size();
        int p,q;
        for (p = 0 ; p < Q ; p++){
            for (q = p ; q < Q ; q++){
                L.data -= log_Perm(p,q);
                for (auto &param_degree_set : paramDegree){
                    TypeParameter degreeType = param_degree_set.first;
                    L.data += log_M(p,q,degreeType) + log_uniform_degree_dist(p,q,degreeType);
                }
            }
        }
    }

    void set_model_codelength() override{

        L.model = std::log2(3); // number of prior types

        std::vector<PriorType> model_encodings = {  PriorType::Uniform, 
                                                    PriorType::DirichletUniform, 
                                                    PriorType::DirichletJeffreys    };

        std::map<PriorType,std::map<TypeParameter,std::vector<std::shared_ptr<IntegerSequenceEncoding>>>> code_prototypes;
        std::map<PriorType, double> model_costs; 

        for (auto &&prior : model_encodings){
            for (auto &&[type,degree_seq_vec] : paramDegree){
                for (auto &&degree_seq : degree_seq_vec){

                    std::vector<std::size_t> filtered_degree_seq;
                    for (int i = 0 ; i < G->nodes.size() ; i++){
                        if (G->nodes[i]->state != NodeState::inactive){
                            filtered_degree_seq.push_back(degree_seq[i]);
                        } 
                    }
                    code_prototypes[prior][type].emplace_back(IntSeqEncodingFactory::make(prior,filtered_degree_seq));
                    model_costs    [prior] += test_model_encoding(prior,filtered_degree_seq);
                }
            }
            // switch(prior){
            //     case PriorType::DirichletUniform:
            //     std::cout << "Prior: dirichlet uniform ";
            //     break;
            //     case PriorType::DirichletJeffreys:
            //     std::cout << "Prior: dirichlet jeffreys ";
            //     break;
            //     case PriorType::Uniform:
            //     std::cout << "Prior: uniform ";
            //     break;
            //     default:
            //     break;
            // }
            // std::cout << model_costs[prior] << std::endl;
            
        }

        auto it = std::min_element(model_costs.begin(),model_costs.end(),[](const std::pair<PriorType,double> &a, const std::pair<PriorType,double> &b){
            return a.second < b.second;
        });

        prior = it->first;
        seq_code = code_prototypes[prior];
        L.model += it->second; 

        prior = PriorType::DirichletJeffreys;
        seq_code = code_prototypes[prior];
        L.model = model_costs[prior];

    }

    std::string get_name() const override{
        return "configuration"; 
    }

    std::string get_prior_name() const override{
        switch(prior){
            case PriorType::Uniform:  
                return "uniform";
                break;

            case PriorType::DirichletUniform:  
                return "dirichlet_uniform";
                break; 

            case PriorType::DirichletJeffreys:   
                return "dirichlet_jeffreys";
                break;

            default: 
                return "uniform";
                break;
        }
    }

    NullModel get_model_type() const override = 0;

};

struct ConfigurationModel : ICodeConfiguration{

    template<typename T>
    ConfigurationModel(const Graph<T> &_G) : ICodeConfiguration{_G}{
        set_params();
        set_codelength();
    }

    template<typename T>
    ConfigurationModel(std::shared_ptr<Graph<T>> &_G) : ICodeConfiguration{_G}{
        set_params();
        set_codelength();
    }

    void set_params() override{

        switch (G->dir){
            case EdgeType::directed:{
                paramDegree[TypeParameter::InDegree] .emplace_back(G->get_degree_seq(Neighborhood::in));
                paramDegree[TypeParameter::OutDegree].emplace_back(G->get_degree_seq(Neighborhood::out));

                paramStub.emplace(TypeParameter::InDegree,
                std::vector<unsigned long>{G->number_of_edges()});
                paramStub.emplace(TypeParameter::OutDegree,
                std::vector<unsigned long>{G->number_of_edges()});
                break;
            }

            case EdgeType::undirected:{
                paramDegree[TypeParameter::Degree].emplace_back(G->get_degree_seq(Neighborhood::undirected));
                paramStub.emplace(TypeParameter::Degree,
                std::vector<unsigned long>{2 * G->number_of_edges()});
                break;
            }
        
        default:
            break;
        }
        

    }

    double log_M(int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) override{

        auto lgamma_sum = [](double a, int b){
            return std::move(a) + std::lgamma(b + 1);
        };
        double parallelEdgeCost = std::accumulate(G->adjMat.begin(),G->adjMat.end(),0.,lgamma_sum)/std::log(2);
        switch (degreeType){
            case TypeParameter::Degree:  
                return 0.5*parallelEdgeCost;
                break;
            case TypeParameter::InDegree:  
                return parallelEdgeCost;
                break;
            default:  
                return 0.;
                break;
        }
        

        

    }

    double log_Perm(int &paramDegree_a_id, int &paramDegree_b_id) override{

        
        switch (G->dir)
        {
        case EdgeType::directed:{
            unsigned long E = *paramStub[TypeParameter::InDegree].begin();
            return std::lgamma(E + 1)/std::log(2); 
            break;
        }

        case EdgeType::undirected:{
            unsigned long E = *paramStub[TypeParameter::Degree].begin() / 2;
            return std::lgamma(E + 1)/std::log(2) + E;
            break;
        }
        
        default:
            return 0.;
            break;
        }
    }

    std::string get_name() const override{
        return "configuration";
    }



    NullModel get_model_type() const override{
        return NullModel::Configuration;
    }

};

struct ReciprocalConfigurationModel : ICodeConfiguration{

    template<typename T>
    ReciprocalConfigurationModel(const Graph<T> _G) : ICodeConfiguration{_G}{
        set_params();
        set_codelength();
    }

    template<typename T>
    ReciprocalConfigurationModel(std::shared_ptr<Graph<T>> &_G) : ICodeConfiguration{_G}{
        set_params();
        set_codelength();
    }

    void set_params() override{

        paramDegree[TypeParameter::InDegree] .emplace_back(G->get_degree_seq(Neighborhood::single_in));
        paramDegree[TypeParameter::OutDegree].emplace_back(G->get_degree_seq(Neighborhood::single_out));

        paramStub.emplace(TypeParameter::InDegree,
        std::vector<unsigned long>{G->number_of_single_edges()});
        paramStub.emplace(TypeParameter::OutDegree,
        std::vector<unsigned long>{G->number_of_single_edges()});

        paramDegree[TypeParameter::Degree].emplace_back(G->get_degree_seq(Neighborhood::mutual));
        paramStub.emplace(TypeParameter::Degree,
        std::vector<unsigned long>{2*G->number_of_mutual_edges()});
    }

    double log_M(int &paramDegree_a_id, int &paramDegree_b_id, TypeParameter &degreeType) override{

        double parallelEdgeCost{};

        int i,j;
        int mat_ij,mat_ji; 
        for (i = 0 ; i < G->N ; i++){
            for (j = i+1 ; j < G->N ; j++){
                mat_ij = G->at(i,j);
                mat_ji = G->at(j,i);

                switch(degreeType){

                    case TypeParameter::InDegree:
                        parallelEdgeCost += std::lgamma(std::abs(mat_ji - mat_ij) + 1);
                        break;

                    case TypeParameter::Degree:
                        parallelEdgeCost += std::lgamma(std::min(mat_ij,mat_ji) + 1);
                        break;

                    default:
                        break;
                }
            }
        }
        parallelEdgeCost /= std::log(2);

        return parallelEdgeCost;
    }

    double log_Perm(int &paramDegree_a_id, int &paramDegree_b_id) override{

        unsigned long singleE = *paramStub[TypeParameter::InDegree].begin();
        unsigned long mutualE = *paramStub[TypeParameter::Degree]  .begin() / 2;

        return (std::lgamma(singleE + 1) + 
                std::lgamma(mutualE + 1))/std::log(2) + mutualE; 
    }

    std::string get_name() const override{
        return "reciprocal_configuration";
    }

    NullModel get_model_type() const override{
        return NullModel::ReciprocalConfiguration;
    }
};

#endif