#ifndef codelength_hpp
#define codelength_hpp 

#include <json.hpp>

struct Codelength{

    double model;
    double data;    
    double additional;

    Codelength () = default;
    Codelength(const double &data_, const double &model_, double _additional = 0.) : data{data_},model{model_},additional{_additional}{}
    ~Codelength() = default;

    Codelength& operator=(const Codelength& L){
        data  = L.data;
        model = L.model;
        additional = L.additional;
        return *this;
    }

    Codelength& operator+=(const Codelength& L){
        data  += L.data;
        model += L.model;
        additional += L.additional;
        return *this;
    }


    Codelength& operator-=(const Codelength& L){
        data  -= L.data;
        model -= L.model;
        additional -= L.additional;
        return *this;
    }

    friend bool operator<(const Codelength &L_a, const Codelength &L_b){
        return L_a.get() < L_b.get();
    }

    double get() const{
        return data + model + additional;
        
    }

    nlohmann::json get_json(std::string s = "") const{
        nlohmann::json j;
        j["data"]  = data;
        j["model"] = model;
        if (s != ""){
            j[s] = additional;
        }
        return j;
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive &ar, const int version){
        ar & model;
        ar & data;
        ar & additional;
    }

};

struct CodelengthOperator : Codelength{

    Codelength add(const Codelength &a, const Codelength &b){
        data = a.data   + b.data;
        model = b.model + b.model;
        additional = a.additional + b.additional;
        return *this;
    }

    Codelength substract(const Codelength &a, const Codelength &b){
        data = a.data   - b.data;
        model = b.model - b.model;
        additional = a.additional - b.additional;
        return *this;
    }

};

#endif