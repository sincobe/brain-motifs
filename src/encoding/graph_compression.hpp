#ifndef graph_compression_hpp
#define graph_compression_hpp

#include "../graph_operator/graph_contractor.hpp"
#include "../graph_operator/supernode_expander.hpp"
#include "diff_codelength/erdos-renyi.hpp"
#include "diff_codelength/configuration.hpp"
#include "diff_codelength/reciprocal_erdos_renyi.hpp"
#include "diff_codelength/reciprocal_configuration.hpp"
#include "compression_tracker.hpp"

#include <json.hpp>
#include <iostream>
#include <limits>
#include <chrono>
#include <tuple>

struct SurveyorFactory{

    static void make(std::shared_ptr<CompressionMeasure<MultiER>> &surveyor,
                    GraphOperationType op, 
                    std::shared_ptr<Graph<int>> &G){
        surveyor = std::make_shared<CompressionMeasureMultiER>(GraphOperationType::SubgContraction,G);
    }
    static void make(std::shared_ptr<CompressionMeasure<ConfigurationModel>> &surveyor,
                    GraphOperationType op, 
                    std::shared_ptr<Graph<int>> &G){
        surveyor = std::make_shared<CompressionMeasureConfiguration>(GraphOperationType::SubgContraction,G);
    }
    
    static void make(std::shared_ptr<CompressionMeasure<ReciprocalMultiER>> &surveyor,
                    GraphOperationType op, 
                    std::shared_ptr<Graph<int>> &G){
        surveyor = std::make_shared<CompressionMeasureReciprocalMultiER>(GraphOperationType::SubgContraction,G);
    }
    
    static void make(std::shared_ptr<CompressionMeasure<ReciprocalConfigurationModel>> &surveyor,
                    GraphOperationType op, 
                    std::shared_ptr<Graph<int>> &G){
        surveyor = std::make_shared<CompressionMeasureReciprocalConfiguration>(GraphOperationType::SubgContraction,G);
    }
};


template<class T>
struct GraphCompresser{

    typedef std::pair<std::string,std::shared_ptr<GraphletComponent>> graphlet_pair;
    typedef std::map <std::string,std::shared_ptr<GraphletComponent>> GMap;

    std::shared_ptr<CompressionMeasure<T>> surveyor; 
    GMap graphlet_map;


    std::shared_ptr<Graph<int>> G;
    std::unique_ptr<CompressionTracker> tracker;
    std::vector<NullModel> models;


    GraphCompresser(std::shared_ptr<Graph<int>> &_G, std::vector<NullModel> _models = {}) : 
    G{_G},models{_models},tracker{},surveyor{},graphlet_map{}{
        if (_models.size()){
            tracker = std::make_unique<CompressionTracker>(_G,GraphOperationType::SubgContraction, models);
        }
        
    }
    virtual bool check_subgraph_validity(const std::vector<int> &subg) const = 0;
    virtual Codelength subg_score(const std::string &label, const std::vector<int> &subg) const = 0;
    virtual void update_constraint(const std::vector<int> &subg) = 0;
    virtual void update_tracker(std::shared_ptr<GraphletComponent> &gc) = 0;
    virtual void compress() = 0;


    virtual inline std::size_t number_of_graphlets() const{
        return graphlet_map.size();
    }

    inline nlohmann::json get_json_output() const{
        return tracker->j;
    }

    std::shared_ptr<GraphletComponent>& subg_selection(){

        int idx{};
        auto constraint_rule = std::mem_fn(&GraphCompresser::check_subgraph_validity);
        auto scoring_func    = std::mem_fn(&GraphCompresser::subg_score);

        // auto t1 = std::chrono::high_resolution_clock::now();

        for (auto &[label, g] : graphlet_map){
            g -> update(*this, constraint_rule, scoring_func);            
        }
        auto it_selected = std::max_element(graphlet_map.begin(),graphlet_map.end(),
                                            [](const graphlet_pair &gp_A, const graphlet_pair &gp_B){
                                                return gp_A.second->candidate.second.get() < gp_B.second->candidate.second.get();
                                            });

        // auto t2 = std::chrono::high_resolution_clock::now();
        // auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
        // std::cout << ms.count() << std::endl;

        return it_selected->second;
    }


    Codelength get_codelength(){
        return surveyor->L;
    }

    double get_cost(){
        return surveyor->get();
    }

    double get_method_cost(){
        return surveyor->L.additional;
    }

    std::map<CompressionTypeParameter,double> get_surveyor_method_param() const{
        return surveyor->methodParam;
    }

    std::map<std::string,std::map<TypeParameter,std::size_t>> get_surveyor_graphlet_param() const{
        return surveyor->graphletParam;
    }
    
    std::shared_ptr<Graph<int>> get_transformed_graph() {
        return std::make_shared<Graph<int>>(*(surveyor->G.get()));
    }

    

};

#endif