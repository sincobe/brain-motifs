#ifndef planted_motif_model_hpp
#define planted_motif_model_hpp

#include "code_factory.hpp"
#include "../graph_operator/graph_contractor.hpp"
#include "planted_graphlet_compresser.hpp"
#include "planted_graphlet_compresser_reader.hpp"
#include "basic_func.hpp"


struct ReconstructionCost{
    double node_insertion_cost;
    double supernode_dist_cost;
    double rewiring_cost;

    ReconstructionCost() = default;

    void reset(){
        node_insertion_cost = 0.;
        supernode_dist_cost = 0.;
        rewiring_cost       = 0.;
    }

    double get() const {
        return node_insertion_cost + 
               supernode_dist_cost + 
               rewiring_cost;
    }

    void set_insertion_cost(const std::size_t &N, const std::size_t &N_H){
        node_insertion_cost = (std::lgamma(N + 1) - std::lgamma(N_H + 1))/std::log(2);
    }

    template<class T>
    void set_supernode_dist_cost(const std::size_t &N_H, const std::size_t &N_supernodes, 
                                 const std::map<std::string, std::size_t> &supernode_dist,
                                 const T &graphlet_map){
        
        std::size_t n_max   = std::max(std::max_element(supernode_dist.begin(),supernode_dist.end(),
                              [](const std::pair<std::string,std::size_t> &a, const std::pair<std::string,std::size_t> &b){
                                    return a.second < b.second;
                              })->second,static_cast<std::size_t>(1));
        std::size_t graphletSetSize = graphlet_map.size();
      
        supernode_dist_cost = PositiveIntegerCodelength(graphletSetSize).get() + PositiveIntegerCodelength(n_max).get() 
                            + supernode_dist.size() * std::log2(n_max * graphletSetSize);
        supernode_dist_cost +=  (lgamma(N_H + 1) - lgamma(N_H - N_supernodes + 1)  
                            +   std::accumulate(supernode_dist.begin(),supernode_dist.end(),0.,
                                [&graphlet_map](double a, const std::pair<std::string, std::size_t> &b){

                                std::size_t autSize = graphlet_map.find(b.first)->second->g->autSize;
                                std::size_t size    = graphlet_map.find(b.first)->second->g->size;
                                return std::move(a) - std::lgamma(b.second + 1) + b.second * (std::lgamma(size + 1) - std::log(autSize));
                                }))/std::log(2);
    }

    template<typename T>
    void set_rewiring_cost(std::shared_ptr<Graph<T>> &G){

        for (auto &&n : G->nodes){
            if (n->state == NodeState::supernode){
                std::set<int> unique_neighbors ;
                unique_neighbors.insert(n->neighborhood_begin(),n->neighborhood_end());

                for (auto &&neighbor_id : unique_neighbors){
                    if (G->nodes[neighbor_id] -> state == NodeState::supernode){
                            rewiring_cost += 0.5*lgbinom(n->get(NodeProperty::size) * G->get_node_property(neighbor_id,NodeProperty::size),G->at(n->id,neighbor_id));
                            if (G->dir == EdgeType::directed){
                                rewiring_cost += 0.5*lgbinom(n->get(NodeProperty::size) * G->get_node_property(neighbor_id,NodeProperty::size),G->at(neighbor_id,n->id));
                            }
                        }
                    else {                     
                        rewiring_cost += lgbinom(n->get(NodeProperty::size), G->at(n->id,neighbor_id));
                        if (G->dir == EdgeType::directed){
                            rewiring_cost += lgbinom(n->get(NodeProperty::size), G->at(neighbor_id,n->id));
                        }
                    }
                }
            }
        }
        rewiring_cost /= std::log(2);
    }
};

template<class T>
struct PlantedMotifModel : Code<int>{

    ReconstructionCost rc;
    std::shared_ptr<PlantedGraphletCompresser<T>> compresser; 
    std::vector<int> adjMatIni;
    // std::map<std::string, std::size_t> supernode_dist;

    template<typename U>
    PlantedMotifModel(  const Graph<U> & _G,
                        const std::string &folderPath, 
                        const nlohmann::json &j,
                        std::vector<NullModel> models = {}) : 
    Code<int>{_G},rc{}{
        compresser = std::make_shared<PlantedGraphletCompresser<T>>(this->G,folderPath,j,models);
        adjMatIni = G->adjMat;
    }   

    template<typename U>
    PlantedMotifModel(const Graph<U> &_G,const nlohmann::json &j, const std::string &graphletFolderPath, TimeStepType t_type = TimeStepType::Optimal) : Code(_G){
        compresser = std::make_shared<PlantedGraphletReader<T>>(G,j,graphletFolderPath,t_type);
    }



    void set_data_codelength() override{
        this->L.data       = compresser->surveyor->L.data;
    }

    void set_model_codelength() override{
        this->L.model      = compresser->surveyor->L.model;
        this->L.additional = compresser->surveyor->L.additional;
    }

    // void set_supernode_dist() {
    //     for (auto &node : G->nodes){
    //         if (node->state == NodeState::supernode){
    //             supernode_dist[node->get_label()]++;
    //         }
    //     }
    // }

    void inference(){
        
        compresser->compress();
        sanity_check_cost();

    }

    void sanity_check_cost(){
        auto H    = compresser->get_transformed_graph();
        auto code = CodeFactory::make(*H.get(),get_model_type());
        set_reconstruction_cost();
        auto d_check = std::abs(rc.get() + code->get() - compresser->get_cost());
        if (d_check >= 1e-2){
            std::cout << "Planted motif model: sanity check failed, with distance: " << d_check << std::endl;
        }
    }

    void set_reconstruction_cost(){

        std::size_t N{G->N};
        
        auto methodParam   = compresser->get_surveyor_method_param();
        auto graphletParam = compresser->get_surveyor_graphlet_param();
        auto H             = compresser->get_transformed_graph();
        std::size_t N_H          = methodParam[CompressionTypeParameter::NodeNumber];
        std::size_t N_supernodes = methodParam[CompressionTypeParameter::SubgraphSequenceSize];
        std::map<std::string,std::size_t> supernodeDist;
        for (auto &&[label,m] : graphletParam){
            std::size_t n = m[TypeParameter::GraphletNumber];
            if (n > 0){ supernodeDist[label] = n;}
        }
        rc.reset();
        rc.set_insertion_cost(N,N_H);
        rc.set_supernode_dist_cost(N_H,N_supernodes,supernodeDist,get_graphlet_map());
        rc.set_rewiring_cost(H);
    }
   
    void reset(){

        this->reset_graph(adjMatIni);
        compresser->G = std::make_shared<Graph<int>>(G->dir,adjMatIni); 
        compresser->reset();
    }

    std::string get_name() const override{
        return "planted_motif_model";
    }

    std::string get_prior_name() const override{
        return compresser->surveyor->get_prior_name();
    }

    NullModel get_model_type() const override{
        return compresser->surveyor->get_model_type();
    }

    nlohmann::json get_json_output() const override{
        return compresser->get_json_output();
    }


    std::map<std::string,std::shared_ptr<GraphletComponent>>& get_graphlet_map() const {
        return compresser->graphlet_map;
    }

    void task(const CodeTask &task) override{
         switch(task){
            case CodeTask::Inference:{
                inference();
                break;
            }
            case CodeTask::Reset:{
                reset();
                break;
            }
            default: 
                break;
        }
    }
};


#endif