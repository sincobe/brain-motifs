#ifndef basic_func_hpp
#define basic_func_hpp

#include <iostream>
#include <math.h>

template<typename T, typename U>
static double lgbinom(const T &a, const U &b){

    if (a >= b && b >= 0){
        return std::lgamma(a + 1) - std::lgamma(a - b + 1) - std::lgamma(b + 1);
    }
    else{
        std::cout << "Wrong parameters, a: " << a << " " << ", b: " << b << std::endl;
        return 0.;
    }
}

#endif