#ifndef planted_graphlet_compresser_reader_hpp
#define planted_graphlet_compresser_reader_hpp

#include "planted_graphlet_compresser.hpp"


enum class TimeStepType{
    Optimal,
    Max
};


template<class T>
struct PlantedGraphletReader : PlantedGraphletCompresser<T>{
    int timestep;

    template<typename U>
    PlantedGraphletReader(std::shared_ptr<Graph<U>> &_G, const nlohmann::json &j, const std::string &graphletFolderPath, TimeStepType t_type = TimeStepType::Optimal) : 
    PlantedGraphletCompresser<T>(_G){

        switch (t_type)
        {
        case TimeStepType::Max:
            timestep = total_number_of_steps(j);
            break;
        
        default:
            timestep = optimal_timestep(j);
            break;
        }
        
        if (timestep){
            std::ifstream gdist_stream{graphletFolderPath+"gdist.json"};
            nlohmann::json gdist;
            gdist_stream >> gdist;
            gdist_stream.close();

            this->surveyor->L.additional = PositiveIntegerCodelength(gdist.size()).get();
            this->surveyor->L = Codelength( j["reference_model"]["initial_codelength"]["data"],
                                            j["reference_model"]["initial_codelength"]["model"]);

            for (int t = 1 ; t <= timestep ; t++){
                auto j_t = j["step_"+std::to_string(t)];
                std::vector<std::size_t> label_raw = j_t["reference_model"]["graphlet_label"];
                std::string label{};
                for (auto &&l : label_raw){
                    label += std::to_string(l);
                }

                auto g = gdist["graphlets"][label];
                std::string key = g["gtrie_label"];
                if (this->graphlet_map.find(key) == this->graphlet_map.end()){
                    auto gc_new = std::make_shared<GraphletComponent>(g);
                    this->surveyor -> add_graphlet_param(gc_new->g);
                    this->graphlet_map[key] = gc_new;
                }

                this->n_contractions++;
                
                auto gc = this->graphlet_map[key];
                gc->candidate.first  = static_cast<std::vector<int>>(j_t["reference_model"]["subgraph"]);
                gc->candidate.second = CodelengthOperator().substract(this->surveyor-> L, Codelength( j_t["reference_model"]["codelength"]["data"],
                                                                                                      j_t["reference_model"]["codelength"]["model"],
                                                                                                      j_t["reference_model"]["codelength"]["method"]));

                this->update_constraint(gc->get_subg());
                SubgraphContractor(this->G,gc->g->gtrie_label,gc->get_subg());
                this->surveyor->update_param(gc);
            }

        }
        else{
            if (!this->world.rank()){
                std::cout << "Planted Graphlet Reader: Previous inference failed to compress initial graph." << std::endl;
            }
            
        }       





    }
    


    static int optimal_timestep(const nlohmann::json &j){
        int t{1},t_ref{};
        
        double cost_ref =   static_cast<double>(j["reference_model"]["initial_codelength"]["data"]) + 
                            static_cast<double>(j["reference_model"]["initial_codelength"]["model"]);

        while (1){
            if (j.find("step_"+std::to_string(t)) != j.end()){
                auto j_t = j["step_"+std::to_string(t)];

                double cost_test =  static_cast<double>(j_t["reference_model"]["codelength"]["data"])  +
                                    static_cast<double>(j_t["reference_model"]["codelength"]["model"]) + 
                                    static_cast<double>(j_t["reference_model"]["codelength"]["method"]);

                if (cost_test < cost_ref){
                    cost_ref = cost_test;
                    t_ref    = t;
                }
                ++t;
            }
            else{
                break;
            }
        }                  
        return t_ref;
    }   

    static double minimum_codelength(const nlohmann::json &j){
        int t    = optimal_timestep(j);
        auto j_t = j["step_"+std::to_string(t)];
    
        
        return t == 0 ? -1 : static_cast<double>(j_t["reference_model"]["codelength"]["data"])  +
                             static_cast<double>(j_t["reference_model"]["codelength"]["model"]) + 
                             static_cast<double>(j_t["reference_model"]["codelength"]["method"]);
               
    }

    int total_number_of_steps(const nlohmann::json &j){

        int t{1};
        while (1){
            if (j.find("step_"+std::to_string(t)) != j.end()){
                ++t;
            }
            else{
                break;
            }
        }                  
        return t-1;
    }

}; 

#endif