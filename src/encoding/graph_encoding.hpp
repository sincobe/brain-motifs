
#ifndef graph_encoding_hpp
#define graph_encoding_hpp

#include <task_interface.hpp>
#include "erdos-renyi.hpp"
#include "configuration.hpp"
#include "planted_motif_model.hpp"

enum class EncodingType {
    plain, 
    planted_motif,
    subgraph_cover
};

struct GraphEncoding : ITask{

    std::string              base_model;
    std::vector<std::string> tracking_models;
    std::string method; 
    std::size_t number_of_repetitions;
    std::string subfolder;
    GraphEncoding(std::string &input, int job_id = -1) : ITask{input,job_id}{
        set_parameters();
    }

    NullModel get_model_type(const std::string &model_name) const{
        if (model_name.find("erdos-renyi") != std::string::npos && model_name.find("reciprocal") == std::string::npos){
            return NullModel::ErdosRenyi;
        }
        else if (model_name.find("reciprocal_erdos-renyi") != std::string::npos){
            return NullModel::ReciprocalErdosRenyi;
        }
        else if (model_name.find("configuration") != std::string::npos && model_name.find("reciprocal") == std::string::npos){
            return NullModel::Configuration;
        }
        else if (model_name.find("reciprocal_configuration") != std::string::npos){
            return NullModel::ReciprocalConfiguration;
        }
        else{
            std::cout << "Unvalid graph model name" << std::endl;
            return NullModel::ErdosRenyi;
        }
    }

    std::vector<NullModel> get_model_type_vector(const std::vector<std::string> &v_str) const{
        std::vector<NullModel> v;
        for (auto &&el : v_str){
            v.emplace_back(get_model_type(el));
        }
        return v;
    }

    EncodingType get_encoding_type(const std::string &encoding_type){
        
        if (encoding_type.find("regular") != std::string::npos){
            return EncodingType::plain;
        }
        else if (encoding_type.find("planted_motif_model") != std::string::npos){
            return EncodingType::planted_motif;
        }
        else if (encoding_type.find("subgraph_cover") != std::string::npos){
            return EncodingType::subgraph_cover;
        }
        else{
            std::cout << "Unvalid encoding type" << std::endl;
            return EncodingType::plain;
        }
    }

    void set_models() {
        std::vector<std::string> allModels  = cr.j["models"]; 
        int idx = static_cast<std::size_t>(std::floor(static_cast<double>(job_id)/N_networks)) % allModels.size();
        base_model = allModels[idx];
        allModels.erase(allModels.begin()+idx);
        for (auto &&m : allModels){
            tracking_models.emplace_back(m);
        }
    }

    void set_method(){
        method = cr.j["method"];
        if (cr.j.find("graphlet_criterium") != cr.j.end()){
            method += "/" + static_cast<std::string>(cr.j["graphlet_criterium"]);
        }
    }

    void set_parameters() override{

        set_method();
        set_models();
        number_of_repetitions = cr.j["number_of_repetitions"];
        // std::string encoding_type = cr.j["model"]["encoding"];
        // std::vector<std::string> model_names = cr.j["model"]["other_models"];
        
 
        // reference_model = get_model_type(encoding_type);
        // for (auto &&model_name : model_names){
        //     models.emplace_back(get_model_type(model_name));
        // }

        
    }

    // std::string get_algorithm_path(const std::string &algo_str) const{
    //     nlohmann::json label;
    //     if (job_id >= 0){
    //         label = pf.datasets[job_id];
    //     }
    //     else{
    //         label = pf.datasets;
    //     }
        
    //     std::string category    = label["category"];
    //     std::string subcategory = label["subcategory"];
    //     return pf.get_output(algo_str,category,subcategory);
    // }

    template<typename T>
    std::unique_ptr<Code<int>> build_code(const Graph<T> &G, const std::string &path, const nlohmann::json &j){
        switch (get_model_type(base_model)){
            case NullModel::ErdosRenyi:
                return std::make_unique<PlantedMotifModel<MultiER>>(G,path,j,get_model_type_vector(tracking_models));
                break;
            case NullModel::ReciprocalErdosRenyi:
                return std::make_unique<PlantedMotifModel<ReciprocalMultiER>>(G,path,j,get_model_type_vector(tracking_models));
                break;
            case NullModel::Configuration:
                return std::make_unique<PlantedMotifModel<ConfigurationModel>>(G,path,j,get_model_type_vector(tracking_models));
                break;
            case NullModel::ReciprocalConfiguration:
                return std::make_unique<PlantedMotifModel<ReciprocalConfigurationModel>>(G,path,j,get_model_type_vector(tracking_models));
                break;
            default:
                return nullptr;
                break;
        }
    }

    std::filesystem::path get_output_path() override{
        // std::string encoding_name = cr.j["model"]["encoding"];
        // if (cr.j.find("graphlet_criterium") != cr.j.end()){
        //     encoding_name += "/" + static_cast<std::string>(cr.j["graphlet_criterium"]);
        // }

        int minibatch_size = cr.j["minibatch_strategy"]["size"];
        std::string pathname{output_folder("graph_encoding") 
                            + method + "/" 
                            + base_model + "/"
                            + "minibatch_size_" + std::to_string(minibatch_size) + "/"};
        std::filesystem::create_directories(pathname);
        return std::filesystem::path(pathname);
    }

    void build_output(nlohmann::json &j){

        // std::string encoding_name = cr.j["model"]["encoding"];
        // std::string model_name    = j["reference_model"]["model_name"];
        j["minibatch_strategy"] = cr.j["minibatch_strategy"];

        std::string output_filename = static_cast<std::string>(get_output_path()) + get_datetime_string() + ".json";
        std::cout << output_filename << std::endl;
        std::ofstream o{output_filename,std::ios::binary}; 
        o << std::setw(4) << j;
        o.close();
    }

    void make() override{
        mpi::communicator world;

        std::vector<int> adjMat = get_adjacency_matrix();
        Graph<bool> G = {EdgeType::directed, adjMat};

        
        if (!world.rank()){
            std::cout << output_folder("graphlet_census") << std::endl;
        }

        auto code = build_code(G,output_folder("graphlet_census"),cr.j);

        if (!world.rank()){
            std::cout << "Code object has been created." << std::endl;
        }

        for (int rep{} ; rep < number_of_repetitions ; rep++){
            code->task(CodeTask::Inference);
            if (!world.rank()){
                auto j = code->get_json_output();
                build_output(j);
            }
            code->task(CodeTask::Reset);
        }
        
    }
};

#endif