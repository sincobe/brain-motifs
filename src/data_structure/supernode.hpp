#ifndef supernode_hpp
#define supernode_hpp

#include "node.hpp"

template<class T>
struct Supernode : T{
    static_assert(std::is_base_of<Node,T>::value,
    "Template argument must be base of Node");

    std::vector<int> subg;
    std::string      label;
    
    Supernode(const std::string &_label, const std::vector<int> &_subg, const T &node) : 
    T(node),
    subg{_subg},
    label{_label}{
        this->state = NodeState::supernode;
    }

    std::size_t size() const{
        return subg.size();
    }

    std::size_t get(NodeProperty property) const override{
        switch(property){
            case NodeProperty::size: 
                return size();
                break; 
            default: 
                return 0;
                break; 
        }
    }

    std::string get_label() const override{
        return label;
    }

    std::vector<int> get_subg() const override{
        return subg;
    }  
};

#endif