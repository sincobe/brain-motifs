//
//  node.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 3/24/22.
//

#include "node.hpp"

void Node::add_neighbor(std::shared_ptr<INode> &n){
    this->update_neighborhood(operationType::addition, {n->id});
    n   ->update_neighborhood(operationType::addition, {id});
}

void Node::remove_neighbor(std::shared_ptr<INode> &n){

    this->update_neighborhood(operationType::substraction, {n->id});
    n   ->update_neighborhood(operationType::substraction, {id});

}

void Node::remove_stub(std::shared_ptr<INode> &n){
    this->update_neighborhood(operationType::single_substraction, {n->id});
    n   ->update_neighborhood(operationType::single_substraction, {id});
}

void Node::update_neighborhood(operationType op, std::vector<int> &&ids){

    switch (op)
    {
    case operationType::addition:
        neighbors.push_back(*ids.begin());
        break;
    
    case operationType::substraction:{
        neighbors.erase(std::remove(neighborhood_begin(),neighborhood_end(),*ids.begin()),neighbors.end());
        break;
    }

    case operationType::single_substraction:{
        neighbors.erase(std::find(neighborhood_begin(),neighborhood_end(),*ids.begin()));
        break;
    }

    default:
        break;
    }
    
}



void DiNode::add_neighbor(std::shared_ptr<INode> &n){
    this->update_neighborhood(operationType::addition, {0,n->id});
    n   ->update_neighborhood(operationType::addition, {1,id});
}

void DiNode::remove_neighbor(std::shared_ptr<INode> &n){
    this->update_neighborhood(operationType::substraction, {0,n->id});
    n   ->update_neighborhood(operationType::substraction, {1,id});
}

void DiNode::remove_stub(std::shared_ptr<INode> &n){
    this->update_neighborhood(operationType::single_substraction, {0,n->id});
    n   ->update_neighborhood(operationType::single_substraction, {1,id});
}



void DiNode::update_neighborhood(operationType op, std::vector<int> &&ids){

    int extremity{ids[0]},id{ids[1]};
    
    switch (op){

        case operationType::addition:{

            if (std::find(neighbors.begin(),neighbors.end(),id) == neighbors.end()){
                neighbors.push_back(id);
            }
            
            switch (extremity){
                case 0:
                    out.push_back(id);
                    break;
                case 1:
                    in.push_back(id);
                    break;
            }
            break;

        }

        case operationType::substraction:{

            neighbors.erase(std::remove(neighbors.begin(),neighbors.end(),id),neighbors.end());
            
            switch (extremity){
                case 0:{
                    out.erase(std::remove(out.begin(),out.end(),id),out.end());

                    if (std::find(out.begin(),out.end(),id) != out.end()){
                        std::cout << "id should not be in out-neighbors" << std::endl;
                    }
                    break;
                }
                case 1:{
                    in.erase(std::remove(in.begin(),in.end(),id),in.end());
                    if (std::find(in.begin(),in.end(),id) != in.end()){
                        std::cout << "id should not be in in-neighbors" << std::endl;
                    }
                    break;
                }
            }
            break;
        }

        case operationType::single_substraction:{
            
            if (std::count(neighbors.begin(),neighbors.end(),id) > 1){
                neighbors.erase(std::find(neighbors.begin(),neighbors.end(),id));
            }

            
            switch (extremity){
                case 0:{
                    out.erase(std::find(out.begin(),out.end(),id));
                    break;
                }
                case 1:{
                    in.erase(std::find(in.begin(),in.end(),id));
                    break;
                }
            }
            break;
        }
        
        default:
            break;
    }

    
}


