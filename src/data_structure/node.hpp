//
//  node.hpp
//  brain-motifs
//
//  Created by Benichou Alexis on 9/5/21.
//

#ifndef node_hpp
#define node_hpp

#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include <algorithm>
#include <set>

enum class NodeState{
    active,
    inactive, 
    supernode
};

enum class Neighborhood{
    undirected,
    in,
    out,
    single_in,
    single_out,
    mutual
};

enum class NodeProperty{
    size,
    label
};

/**
 * Node interface
 */
struct INode{
    const int id; /**< Node label */
    NodeState state; 

    void switch_activity(){
        switch (state){
            case NodeState::active:{
                state = NodeState::inactive;
                clear_neighborhood();
                break;
            }
            case NodeState::inactive:{
                state = NodeState::active;
                break;
            }
            case NodeState::supernode:{
                state = NodeState::active;
                break;
            }
            default:
                break;
        }
    }

    enum class operationType{
        addition,
        substraction,
        single_substraction
    };

    INode(int i) : id{i}, state{NodeState::active}{};
    virtual ~INode() = default;
    
    virtual std::vector<int> get_neighborhood(Neighborhood type = Neighborhood::undirected) const = 0;
    virtual std::vector<int>::iterator neighborhood_begin(Neighborhood type = Neighborhood::undirected) = 0 ;
    virtual std::vector<int>::iterator neighborhood_end  (Neighborhood type = Neighborhood::undirected) = 0 ;
    virtual void update_neighborhood(operationType op,std::vector<int> &&ids) = 0;
    virtual void add_neighbor(std::shared_ptr<INode> &n)     = 0;
    virtual void remove_neighbor(std::shared_ptr<INode> &n)  = 0;
    virtual void remove_stub(std::shared_ptr<INode> &n)      = 0;
    virtual unsigned long get_degree(Neighborhood type = Neighborhood::undirected)const = 0;
    virtual void merge_neighborhood(std::shared_ptr<INode> &node) = 0;
    virtual void replace_in_neighborhood(std::shared_ptr<INode> &oldNode, std::shared_ptr<INode> &newNode) = 0;
    virtual void clear_neighborhood() = 0;


    virtual std::size_t get(NodeProperty property) const{ return 0;}
    virtual std::vector<int> get_subg() const{return {id};}
    virtual std::string get_label() const{ return "";}

    static std::string get_neighborhood_type_string(const Neighborhood  &type){
        switch (type)
        {
        case Neighborhood::undirected:
            return "undirected";
            break;
        case Neighborhood::in:
            return "in";
            break;
        case Neighborhood::out:
            return "out";
            break;
        case Neighborhood::single_in:
            return "single_in";
            break;
        case Neighborhood::single_out:
            return "single_out";
            break;
        case Neighborhood::mutual:
            return "mutual";
            break;
        default:
            return "null"; 
            break;
        }
    }

};

struct Node : INode{
    std::vector<int> neighbors; /**<  Labels of adjacent nodes */
    Node(int i) : INode{i}{};
    Node(std::shared_ptr<INode> &node) : INode{node->id},neighbors{node->get_neighborhood()}{}
    
    inline virtual std::vector<int> get_neighborhood(Neighborhood type = Neighborhood::undirected) const override{
        if (type == Neighborhood::undirected){
            return neighbors;
        }
        else{
            return std::vector<int>();
        }
    }

    inline virtual std::vector<int>::iterator neighborhood_begin(Neighborhood type = Neighborhood::undirected) override{
        return neighbors.begin();
    }
    
    inline virtual std::vector<int>::iterator neighborhood_end(Neighborhood type = Neighborhood::undirected) override{
        return neighbors.end();
    }

    virtual void update_neighborhood(operationType op, std::vector<int> &&ids) override;
    virtual void add_neighbor(std::shared_ptr<INode> &n)     override;
    virtual void remove_neighbor(std::shared_ptr<INode> &n)  override;
    virtual void remove_stub(std::shared_ptr<INode> &n)      override;

    inline bool has_neighbor(std::shared_ptr<Node> &n){
        return std::find(neighbors.begin(),neighbors.end(),n->id) != neighbors.end();
    }
    
    inline virtual unsigned long get_degree(Neighborhood type = Neighborhood::undirected) const override{
        return neighbors.size();
    }

    virtual inline void clear_neighborhood() override{
        neighbors.clear();
    }

    virtual inline void merge_neighborhood(std::shared_ptr<INode> &node) override{
        std::move(node->neighborhood_begin(),node->neighborhood_end(),std::back_inserter(neighbors));
    }

    virtual void replace_in_neighborhood(std::shared_ptr<INode> &oldNode, std::shared_ptr<INode> &newNode) override{
        std::replace(neighbors.begin(),
                     neighbors.end(), 
                     oldNode->id,
                     newNode->id);
    }


};

struct DiNode : Node{
    std::vector<int> in; /**< Labels of nodes that points towards this*/
    std::vector<int> out; /**< Labels of nodes that this towards*/
    DiNode(int i) : Node{i}{};
    DiNode(const DiNode &node) : Node{node},in{node.in},out{node.out}{}

    DiNode(std::shared_ptr<INode> &node) : 
    Node{node},in{node->get_neighborhood(Neighborhood::in)},out{node->get_neighborhood(Neighborhood::out)}{}
    
    inline virtual std::vector<int> get_neighborhood(Neighborhood type = Neighborhood::undirected) const override{
        switch (type)
        {
        case Neighborhood::in:
            return in;
            break;

        case Neighborhood::out:
            return out;
            break;

        case Neighborhood::undirected:
            return neighbors;
            break;
        
        default:
            return std::vector<int>();
            break;
        }
    }

    inline virtual std::vector<int>::iterator neighborhood_begin(Neighborhood type = Neighborhood::undirected) override{
        switch (type) {
            case Neighborhood::in:
                return in.begin();
                break;
            case Neighborhood::out:
                return out.begin();
                break;
                
            default:
                return neighbors.begin();
                break;
        }
    }
    
    virtual std::vector<int>::iterator neighborhood_end(Neighborhood type = Neighborhood::undirected) override{
        switch (type) {
            case Neighborhood::in:
                return in.end();
                break;
            case Neighborhood::out:
                return out.end();
                break;
                
            default:
                return neighbors.end();
                break;
        }
    }

    virtual void update_neighborhood(operationType op, std::vector<int> &&ids) override;
    virtual void add_neighbor(std::shared_ptr<INode> &n)                       override;
    virtual void remove_neighbor(std::shared_ptr<INode> &n)                    override;
    virtual void remove_stub(std::shared_ptr<INode> &n)                        override;
    
    inline virtual unsigned long get_degree(Neighborhood type = Neighborhood::undirected) const override{
        switch (type) {

            case Neighborhood::in:
                return in.size();
                break;

            case Neighborhood::out:
                return out.size();
                break;

            default:
                return neighbors.size();
                break;
        }
    }

    inline void clear_neighborhood() override{
        neighbors.clear();
        in.clear();
        out.clear();
    }

    virtual void merge_neighborhood(std::shared_ptr<INode> &node) override{
        
        std::move(node->neighborhood_begin(),node->neighborhood_end(),std::back_inserter(neighbors));

        std::sort(neighbors.begin(),neighbors.end());
        auto last = std::unique(neighbors.begin(),neighbors.end());
        neighbors.erase(last,neighbors.end());

        std::move(node->neighborhood_begin(Neighborhood::in), node->neighborhood_end(Neighborhood::in), std::back_inserter(in));
        std::move(node->neighborhood_begin(Neighborhood::out),node->neighborhood_end(Neighborhood::out),std::back_inserter(out));
    }

    virtual void replace_in_neighborhood(std::shared_ptr<INode> &oldNode, std::shared_ptr<INode> &newNode) override{

        std::replace(neighbors.begin(),
                     neighbors.end(), 
                     oldNode->id,
                     newNode->id);

        if (std::find(neighbors.begin(),neighbors.end(),newNode->id) != neighbors.end()){
            std::sort(neighbors.begin(),neighbors.end());
            auto last = std::unique(neighbors.begin(),neighbors.end());
            neighbors.erase(last,neighbors.end());
        }

        std::replace(out.begin(),
                     out.end(), 
                     oldNode->id,
                     newNode->id);

        std::replace(in.begin(),
                     in.end(), 
                     oldNode->id,
                     newNode->id);
    }
};

/**
 * Add an integer attribute, e.g. a block label, to a node
 */
template<typename T>
struct coloredNode : T{
    const int color;
    coloredNode(T &n, int c) : T{n}, color{c}{}
};

/**
 * Add an ensemble of attributes, e.g. spatial coordinates, to a node
 */
template<typename T, typename U>
struct embeddedNode : T{
    std::vector<U> attributes;
    embeddedNode(T &n, const std::vector<U> &_attributes) : T{n}, attributes{_attributes}{}
};



#endif /* node_hpp */
