#ifndef nauty_label_hpp
#define nauty_label_hpp

#include <nauty.h>
#include "graph.hpp"

/**
 * Canonical labeling that allows fast isomorphism tests
 */
struct NautyLabel{
    const int n;
    const int m;
    int *lab;
    int *ptn;
    int *orbits;
    DEFAULTOPTIONS_GRAPH(options);
    statsblk stats;
    graph *g;
    graph *cg;

    NautyLabel(const int &n, const EdgeType &dir) : n{n},m{SETWORDSNEEDED(n)}{
        initialize(n,dir);
    }
    

    template<typename T>
    NautyLabel(const Graph<T> &G) : n{static_cast<int>(G.N)},m{SETWORDSNEEDED(static_cast<int>(G.N))}{
        initialize(n,G.dir);
        set_nauty_graph(G.adjMat);
    }


    ~NautyLabel(){
        delete [] lab;
        delete [] ptn;
        delete [] orbits;
        delete [] g;
        delete [] cg;
    }
    
    void initialize(const int &n, const EdgeType &dir){
        lab = new int [n * m];
        ptn = new int [n * m];
        orbits = new int [n * m];
        options.getcanon = true;
        
        if (dir == EdgeType::directed){
            options.digraph = true;
        }
        
        g  = new graph [n * m];
        cg = new graph [n * m];
    }

    template<typename T>
    void set_nauty_graph(const std::vector<T> &adjMat){
        EMPTYGRAPH(g,n,m);
        int i,j;
        for (i = 0 ; i < n ; i++){
            for (j = i+1 ; j < n ; j++){
                
                if (!options.digraph){
                    if (adjMat[i * n + j]){
                        ADDONEEDGE(g,i,j,m);
                    }
                    
                }
                else{
                    
                    if (adjMat[i * n + j]){
                        ADDONEARC(g,i,j,m);
                    }
                    if (adjMat[j * n + i]){
                        ADDONEARC(g,j,i,m);
                    }
                    
                }
            }
        }
    }

    void set_canonical_labelisation(){
        densenauty(g,lab,ptn,orbits,&options,&stats,m,n,cg);
    }
};


#endif 