//
//  topological.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 3/30/22.
//

#ifndef topological_hpp
#define topological_hpp


#include <stdio.h>
#include <vector>
#include <string>
#include <numeric>
#include "igraphlet.hpp"

struct TopologicalGraphlet : IGraphlet{
    
    std::vector<bool> adjMat;

    void read_gtrie_label(const std::string &str) override;
    void set_canonical_label() override;
    
    TopologicalGraphlet(int n, EdgeType dir, std::string &str) : IGraphlet{n,dir}{
        adjMat.resize(n*n);
        read_gtrie_label(str);
        set_canonical_label();
    }

    TopologicalGraphlet(EdgeType dir, const std::string &str) : IGraphlet{size_from_gtrie_label(str),dir}{
        int n = size_from_gtrie_label(str);
        adjMat.resize(n*n);
        read_gtrie_label(str);
        set_canonical_label();
    }

    static int size_from_gtrie_label(const std::string &str){
        return static_cast<int>(0.5 * (1 +  std::pow(1. + 4 * str.size(),0.5)));
    }



    unsigned long number_of_edges() override{
        unsigned long e = std::accumulate(adjMat.begin(),adjMat.end(),0,
                                         [](unsigned long a, bool b){
                                             return b ? std::move(a) + 1 : std::move(a);
                                         });

        switch (dir){
            case EdgeType::directed:
                return e;
                break;
            case EdgeType::undirected: 
                return e/2;
                break;
            default: 
                return 0;
                break;
        }
    }

    bool at(const int &i, const int &j) override{
        return adjMat[i * size + j];
    }

    std::size_t number_of_single_edges() override{
        std::size_t e{};
        int i,j;
        for (i = 0 ; i < size ; i++){
            for (j = i + 1 ; j < size ; j++){
                e += at(i,j) && !at(j,i) ? 1 : 0;
                e += at(j,i) && !at(i,j) ? 1 : 0;
            }
        }
        return e; 
    }

    std::size_t number_of_mutual_edges() override{
        std::size_t e{};
        int i,j;
        for (i = 0 ; i < size ; i++){
            for (j = i + 1 ; j < size ; j++){
                e += at(i,j) && at(j,i) ? 1 : 0;
            }
        }
        return e; 
    }
};

#endif /* topological_hpp */
