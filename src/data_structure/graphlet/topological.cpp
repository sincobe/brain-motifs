//
//  topological.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 3/30/22.
//


#include "topological.hpp"

void TopologicalGraphlet::set_canonical_label(){
    
    NautyLabel nauty(this->size, this->dir);
    nauty.set_nauty_graph(adjMat);
    graph * cg = new graph[nauty.n * nauty.m];
    densenauty(nauty.g, nauty.lab, nauty.ptn, nauty.orbits, &nauty.options, &nauty.stats, nauty.m, size, cg);
    
    this->label.resize(nauty.n * nauty.m);
    for (int l = 0 ; l < nauty.n * nauty.m ; l++){
        this->label[l] = cg[l];
    }
    this->autSize = nauty.stats.grpsize1 * std::pow(10, nauty.stats.grpsize2);
    delete [] cg;
}

void TopologicalGraphlet::read_gtrie_label(const std::string &label){

    gtrie_label = label;
    
    int d,i;
    auto it_label{label.begin()};

    for (d = 1 ; d < size ; d++){
        for (i = 0 ; i < d ; i++){
            if (*it_label == '1'){
                adjMat[i*size+d] = 1;
            }
            ++it_label;
            if (*it_label == '1'){
                adjMat[d*size+i] = 1;
            }
            ++it_label;
        }
    }
}


