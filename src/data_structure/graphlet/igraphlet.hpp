//
//  igraphlet.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 3/30/22.
//

#ifndef igraphlet_hpp
#define igraphlet_hpp


#include <stdio.h>
#include "../nauty_label.hpp"
#include "../graph.hpp"

enum class GraphletType{ Topological };

/**
 * Graphlet interface
 */
struct IGraphlet{
    
    const int size;
    int autSize;  /**< Size if automorphism group */
    EdgeType dir; /**< Directionnality of edges */

    std::vector<std::size_t> label; /**< Canonical nauty label */
    std::string gtrie_label; /**< Gtrie label that encodes the graphlet structure  */

    
    IGraphlet(int n, EdgeType dir) : size{n},dir{dir}{


    }
    virtual ~IGraphlet() = default;
    virtual void read_gtrie_label(const std::string &label) = 0;
    virtual void set_canonical_label()                = 0;
    
    
    friend bool operator< (const IGraphlet &g1, const IGraphlet &g2){
        return g1.label < g2.label;
    }
    
    inline std::string get_str() const{
        std::string str{};
        for (auto &l : label){
            str += std::to_string(l);
        }
        return str;
    }

   virtual std::size_t number_of_edges() = 0;
   virtual std::size_t number_of_single_edges() = 0;
   virtual std::size_t number_of_mutual_edges() = 0;

   virtual bool at(const int &i, const int &j) = 0;
};

// /**
//  * Canonical labeling that allows fast isomorphism tests
//  */
// struct NautyLabel{
//     const int n;
//     const int m;
//     int *lab;
//     int *ptn;
//     int *orbits;
//     DEFAULTOPTIONS_GRAPH(options);
//     statsblk stats;
//     graph *g;

//     NautyLabel(const int &n, EdgeType &dir);
//     ~NautyLabel(){
//         delete [] lab;
//         delete [] ptn;
//         delete [] orbits;
//         delete [] g;
//     }
    
//     template<typename T>
//     void set_nauty_graph(const std::vector<T> &adjMat);
// };



#endif /* igraphlet_hpp */
