//
//  graphlet_distribution_reader.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/15/22.
//

#include "graphlet_store_reader.hpp"
#include <iostream>
GraphletStoreReader::GraphletStoreReader(const std::string &filename, const int &n, const std::size_t &f, bool activate_chunk_reading) :
input{filename, std::ifstream::binary},current_idx{-1},frequency{f},size{n}{
    if (!input.good()){
        std::cout << "GraphletStore: file not found " << std::endl;
        std::cout << filename << std::endl;
    }
    
    if (activate_chunk_reading){
        chunk_reading.resize( frequency / chunk_size + 1 );
        for (auto &chunk : chunk_reading){
            chunk = chunk_size;
        }
        *chunk_reading.rbegin() = frequency % chunk_size;
        it_chunk = chunk_reading.begin();
    }
    initialize();
    // std::random_device seeder;

}

void GraphletStoreReader::initialize(){
    // current_idx = 0;
    subg.resize(size);
    // file_idx.resize(std::min({f,static_cast<std::size_t>(std::floor(0.1 * static_cast<double>(file_idx.max_size())))}));
    
    if (chunk_reading.size()){
        file_idx.resize(*it_chunk);
    }
    else{
        file_idx.resize(frequency);
    }
    
    initialize_file_idx(0);    
}

void GraphletStoreReader::initialize_file_idx(const int &start_pos){
    input.seekg(start_pos,std::ios::beg);
    char c{};
    int i{},pos{};
    while (i != file_idx.size()){
        // file_idx[i] = input.tellg();
        file_idx[i] = pos;
        while (1){
            while (1){
                input.get(c);
                ++pos;
                if (c == ',' || c == '\n'){
                    break;
                }
            }

            if (c == '\n'){
                break;
            }
        }
        ++i;
    }

    if (chunk_reading.size()){
        idx_chunk = pos;
    }

    const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 eng(static_cast<std::mt19937::result_type>(seed));
    std::shuffle(file_idx.begin(),file_idx.end(),eng);
}

void GraphletStoreReader::update(){
    
    if (file_idx.size()){
        ++current_idx;
        if (current_idx == file_idx.size()){
            current_idx = 0;
        }
        input.seekg(file_idx[current_idx],std::ios::beg);
        auto it_subg{subg.begin()};
        char c{};
        while (1){
            std::string temp{};
            while (1){
                input.get(c);
                if (c != ',' && c != '\n'){
                    temp += c;
                }
                else{
                    break;
                }
            }
        
            *it_subg = std::stoi(temp);
            ++it_subg;
            if (c == '\n'){
                break;
            }
        }
    }
    else if(chunk_reading.size() && it_chunk != chunk_reading.end()){

        int start_pos = idx_chunk;
        file_idx.clear();
        file_idx.resize(*it_chunk);
        initialize_file_idx(start_pos);

        ++it_chunk;
    }
    else{
        subg.clear();
    }
}

