#ifndef subgraph_minibatch_hpp
#define subgraph_minibatch_hpp

#include "graphlet_store_reader.hpp"
#include <memory>
#include <map>
#include <string>
#include <json.hpp>
#include <functional>
#include <iterator>

enum class MinibatchStrategy{
    uniform, 
    equilibrated
};

struct SubgraphMinibatch{

    MinibatchStrategy strat;
    const std::size_t B;
    std::unique_ptr<GraphletStoreReader> reader;
    std::vector<std::vector<int>> subgs;

    SubgraphMinibatch() : strat{MinibatchStrategy::uniform},B{},reader{},subgs{}{}

    SubgraphMinibatch(const std::size_t &_B, const std::string &filename, const nlohmann::json &j, bool activate_chunk_reading = false) : B{_B}{

        std::size_t freq = j["number_of_subgraphs"];
        reader = std::make_unique<GraphletStoreReader>(filename,j["number_of_nodes"],freq,activate_chunk_reading);  
        strat = MinibatchStrategy::uniform;
        subgs.resize(B);    
    }

    SubgraphMinibatch(const std::size_t &_B, const std::size_t &totalSubgNumber, const std::string &filename, const nlohmann::json &j, bool activate_chunk_reading = false) : 
    B{static_cast<std::size_t>(
    std::round(static_cast<double>(j["number_of_subgraphs"])/totalSubgNumber * _B))}{

        reader = std::make_unique<GraphletStoreReader>(filename,j["number_of_nodes"],j["number_of_subgraphs"]);  
        strat = MinibatchStrategy::equilibrated;
        subgs.resize(B);    
    }

    template <class T, class U>
    void update(const T &rule, const U &obj){

        // if (subgs.size() == B){
        if (subgs.size()){
            auto it_subgs = subgs.begin();
            while (reader->file_idx.size() && it_subgs != subgs.end()){
                reader->update();
                if (rule(obj, reader->get())){
                    *it_subgs = reader->get();
                    ++it_subgs;
                }
                else{
                    reader->remove_from_list();
                }
            }

            if (it_subgs != subgs.end()){
                auto d = std::distance(subgs.begin(),it_subgs);
                if (d){
                    subgs.resize(d);
                }
                else{
                    subgs.clear();
                }
                // subgs.clear();
            }
            // while(it_subgs != subgs.cend()){
            //    it_subgs->clear();
            //    ++it_subgs;
            // }
        }
        else{
            subgs.clear();
        }
    }

    void reset(){
        if (reader != nullptr){
            subgs.resize(B);
            reader->reset();
        }
        
    }
};

#endif 