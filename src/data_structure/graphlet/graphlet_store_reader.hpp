//
//  graphlet_distribution_reader.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/15/22.
//

#ifndef graphlet_store_reader_hpp
#define graphlet_store_reader_hpp

#include <stdio.h>
#include <fstream>
#include <vector>
#include <random>
#include <chrono>
#include <algorithm>
#include <utility>

/**
 * Iterate over stored subgraphs
 */

struct GraphletStoreReader{
  
    const std::size_t chunk_size = 1e6;
    std::ifstream input;
    std::vector<int> subg;     /**< Current subgraph*/
    std::vector<int> file_idx; /**< Used to random access a graphlet occurrence*/
    std::vector<int> chunk_reading;
    std::vector<int>::iterator it_chunk;
    int idx_chunk;
    int current_idx;
    std::size_t frequency;
    int size;

    ~GraphletStoreReader(){ input.close(); }
    GraphletStoreReader(const std::string &filename, const int &n, const std::size_t &f, bool activate_chunk_reading = false);
    
    void initialize();
    void initialize_file_idx(const int &start_pos);

    void update();

    void remove_from_list(){
        
        if (file_idx.size() > 1){
            auto it_to_remove = file_idx.begin() + current_idx;
            auto it_last      = file_idx.rbegin();
            std::swap(*it_to_remove,*it_last);
            file_idx.pop_back();
            if (current_idx == file_idx.size()){
                current_idx = 0 ;
            }
        }
        else{
            file_idx.clear();
            subg.clear();
        }
        
    }

    inline std::vector<int>& get(){
        return subg;
    }

    void reset(){
        initialize();
    }
};

#endif /* graphlet_store_reader_hpp */
