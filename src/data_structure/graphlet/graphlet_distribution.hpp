//
//  graphlet_distribution.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/4/22.
//

#ifndef graphlet_distribution_hpp
#define graphlet_distribution_hpp

#include <stdio.h>
#include <fstream>
#include <chrono>
#include <json.hpp>
#include <boost/mpi.hpp>
namespace mpi = boost::mpi;

#include "topological.hpp"


/**
 * Gathers the subgraph frequency and potentially writes all occurrences in a storing folder
 */


struct GraphletStore{
    
    unsigned long int frequency; /**< Number of occurrences */
    std::shared_ptr<IGraphlet> graphlet;

    std::ofstream o;
     
    /**
     *@param path If specified, subgraph will be dumped at the end of this path
     */

    GraphletStore(GraphletType type, std::shared_ptr<IGraphlet> &g, std::string path = "");
    
    /**
     * Increases frequency and may collect a set of node labels
     * @param subg Set of node labels
     * @param depth Graphlet size
     */
    void add(const std::vector<int> &subg, int &depth);
   
//    void add(std::vector<int> &subg, int &depth,);
    
    /**
     * Serialize graphlet in JSON
     * @param j Target JSON object
     */
    void dump(nlohmann::json &j);
};

/**
 * Data structure that support a graphlet-census
 */

struct GraphletDistribution {
    
    typedef std::map<std::vector<unsigned long>,std::shared_ptr<GraphletStore>>  GMap;
  
    static unsigned long int totalFrequency;
    GraphletType type;      /**< Determines the nature of the isomorphism tests */
    GMap graphlets;
    std::string path;       /**< Output path where to dump the graphlet distribution */
                            /** Indicates whether subgraphs are dumped in the storing folder */
    nlohmann::json &j;
    const int min_subg_size;
    
    GraphletDistribution(nlohmann::json &_j,
                         int _min_subg_size = 3,
                         GraphletType _type = GraphletType::Topological) :
    type{_type},j{_j},path{},min_subg_size{_min_subg_size}{}
    
    
    
    inline GMap::iterator find(std::vector<unsigned long> &label){
        return graphlets.find(label);
    }
    
    inline GMap::iterator end(){
        return graphlets.end();
    }
    
    inline unsigned long size() const{
        return graphlets.size();
    }
    
    inline void set_type(GraphletType type){
        this->type = type;
    }
    
    std::shared_ptr<GraphletStore> add(std::shared_ptr<IGraphlet>& g);

    inline void set_path(std::string &str){
        path = str;
    }
    
    inline void close_files(){
        for (auto &item : graphlets){
            item.second->o.close();
        }
        std::cout << "number of enumerated subgraphs: " << GraphletDistribution::totalFrequency << std::endl;
    }
};



#endif /* graphlet_distribution_hpp */
