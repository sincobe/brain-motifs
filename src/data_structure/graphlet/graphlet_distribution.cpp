//
//  graphlet_distribution.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/19/22.
//

#include "graphlet_distribution.hpp"


unsigned long int GraphletDistribution::totalFrequency = 0;


GraphletStore::GraphletStore(GraphletType type, std::shared_ptr<IGraphlet> &g, std::string path) :
graphlet{g},frequency{}{
    
    if (path.size()){
        mpi::communicator world;
        std::string filename{path + "store/" + g->get_str()};
        if (world.size() > 1){
            filename += "_"+std::to_string(world.rank());
        }
        filename += ".bin";
        o.open(filename, std::ios::out | std::ios::binary);
    }
}


void GraphletStore::add(const std::vector<int> &subg, int &depth){
    
    frequency++;
    GraphletDistribution::totalFrequency++;
    if (o.is_open()){
        auto it_subg = subg.begin();
        for (int step{} ; step < graphlet -> size ; step++){
            o << std::to_string(*it_subg);
            if (step != graphlet -> size - 1) {
                o << ',';
            }
            ++it_subg;
        }
        o << '\n';
    }
}

void GraphletStore::dump(nlohmann::json &j){
    if (frequency > 0){
        std::string key{graphlet->get_str()};
        j["graphlets"][key]["gtrie_label"]             = graphlet->gtrie_label;
        j["graphlets"][key]["number_of_nodes"]         = graphlet->size;
        j["graphlets"][key]["automorphism_group_size"] = graphlet->autSize;
        j["graphlets"][key]["number_of_subgraphs"]     = frequency;
    }
}



// void GraphletDistribution::make_strategy(std::shared_ptr<GraphletStore> &gs){
//     switch (static_cast<int>(j["id"])){
//         case 1:{
//             gs->strat = std::make_unique<UniformSubgSampling>();
//             gs->strat->set(static_cast<int>(j["step"]));
//         }
//         default:
//         ;
//     }
// }


std::shared_ptr<GraphletStore> GraphletDistribution::add(std::shared_ptr<IGraphlet> &g){
    std::shared_ptr<GraphletStore> gs;
    if (static_cast<bool>(j["storing"]) && g->size >= min_subg_size){
        gs = std::make_shared<GraphletStore>(type,g,path);
    }
    else{
        gs = std::make_shared<GraphletStore>(type,g);
    }
    graphlets.emplace(g->label,gs);
    return gs;

   
}



