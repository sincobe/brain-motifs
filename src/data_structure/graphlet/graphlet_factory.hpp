//
//  graphlet_factory.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/14/22.
//

#include <stdio.h>
#include "topological.hpp"


struct GraphletFactory{
    
  
    static std::shared_ptr<IGraphlet> make(GraphletType gt, int n, EdgeType dir, std::string &str){
        switch (gt){
            
            case GraphletType::Topological:
                return std::make_shared<TopologicalGraphlet>(n,dir,str);
                break;
            default: 
                return nullptr;
                break;
        }
    }
    
};
