//
//  graph.hpp
//  brain-motifs
//
//  Created by Benichou Alexis on 8/23/21.
//

#ifndef graph_hpp
#define graph_hpp

#include <stdio.h>
#include <cmath>
#include "node.hpp"
#include <set>
#include <numeric>
#include <fstream>
/**
 * Distinguishes directed and undirected graphs
 */
enum class EdgeType{directed,undirected};

enum class NullModel{
    ErdosRenyi,
    ReciprocalErdosRenyi,
    Configuration,
    ReciprocalConfiguration,
    PlantedMotifModel
};

/**
 * Data structure that encodes a network
 */
template<typename T>
struct Graph{
    
    const EdgeType dir;
    const unsigned long sizeMat;
    unsigned long N;                /**< number of "active" nodes, smaller or equal to sizeMat */

    std::vector<T> adjMat;          /**< adjacency matrix of dimensions sizeMat x sizeMat */
    
    std::vector<std::shared_ptr<INode>> nodes;

    /**
     * @param mat Structure of the graph contained in the adjacency matrix
     */
 
    template<typename U>
    Graph(EdgeType _dir,const std::vector<U> &mat) :
    dir{_dir},
    sizeMat{static_cast<unsigned long>(std::sqrt(mat.size()))}{
        
        adjMat.resize(mat.size());
        
        N = sizeMat;
        
        for (int n = 0 ; n < N ; n++){
            add_node(n);
        }
        
        int i,j;
        for (i = 0 ; i < N ; i++){
            switch(dir){
                case EdgeType::directed:
                    j = 0;
                    break;
                case EdgeType::undirected:
                    j = i + 1;
                    break;
            }
            
            while (j < N){
                auto mat_ij = mat[i * N + j]; 
                if (mat_ij && i!=j){
                    set_edge(i,j,mat_ij);
                }

             
                j++;
            }
        }
    }

    
    template<typename U>
    Graph(const Graph<U> &G) : dir{G.dir},N{G.N},sizeMat{G.sizeMat}{
        adjMat.clear();
        adjMat.resize(G.adjMat.size());
        for (int n = 0 ; n < N ; n++){
            add_node(n);
        }

        int i,j;
        for (i = 0 ; i < N ; i++){
            switch(dir){
                case EdgeType::directed:
                    j = 0;
                    break;
                case EdgeType::undirected:
                    j = i + 1;
                    break;
            }
            
            while (j < N){
                auto mat_ij = G.at(i,j); 
                if (mat_ij && i!=j){
                    set_edge(i,j,mat_ij);
                }

             
                j++;
            }
        }
    }

    Graph(const std::size_t &graphSize, EdgeType _dir) : dir{_dir},N{graphSize},sizeMat{graphSize}{
        adjMat.resize(N*N);
        for (int n = 0 ; n < N ; n++){
            add_node(n);
        }
    }

    /**
     *@param i Source node
     *@param j Target node
     *@return Edge value at position (i,j)
     */
    inline T& at(const int &i, const int &j) {
        return adjMat[i * sizeMat + j];
    }

    inline T at(const int &i, const int &j) const{
        return adjMat[i * sizeMat + j];
    }

    


    unsigned long number_of_edges() const{
        unsigned long E{};
        int i,j; 

        for (i = 0 ; i < sizeMat ; i++){
            switch(dir){
                case EdgeType::directed:
                    j = 0;
                    break;
                case EdgeType::undirected:
                    j = i + 1;
                    break;
            }

            while (j < sizeMat){
                E += at(i,j);
                ++j;
            }
            
        }
        return E;
    }

    void add_node(int &n){
    
        switch(dir){
            case EdgeType::undirected:
                nodes.emplace_back(std::make_shared<Node>(n));
                break;
            case EdgeType::directed:
                nodes.emplace_back(std::make_shared<DiNode>(n));
                break;
        }
    }
    
    template<typename U>
    void set_edge(const int &i, const int &j, const U &value){

        adjMat.at(i*sizeMat + j) = static_cast<T>(value);
        if (dir == EdgeType::undirected){
            adjMat.at(j*sizeMat + i) = static_cast<T>(value);
        }
        
        for (int rep = 0 ; rep < static_cast<T>(value) ; rep++){
            nodes[i]->add_neighbor(nodes[j]);
        }
    }

    void add_edge(const int &i, const int &j, const T &value){

        for (int rep = 0 ; rep < value ; rep++){
            nodes[i]->add_neighbor(nodes[j]);
        }

        at(i,j) += value;

        if (dir == EdgeType::undirected){
            at(j,i) += value;
        }
        
        
        
    }

    void  remove_edge(const int &i, const int &j, const T &value){

        if (at(i,j)){
            for (int rep = 0 ; rep < value ; rep++){
                nodes[i]->remove_stub(nodes[j]);
            }

            at(i,j) -= value;

            if (dir == EdgeType::undirected){
                at(j,i) -= value;
            }
            
        }        

    }

    void remove_edge(const int &i, const int &j){

        if (at(i,j)){
            at(i,j) = 0;

            if (dir == EdgeType::undirected){
                at(j,i) = 0;
            }
            
            nodes[i]->remove_neighbor(nodes[j]);
        }
        
    }

    void remove_multiedges(){
        int i,j;
        for (i = 0 ; i < N ; i++){
            switch(dir){
                case EdgeType::directed:
                    j = 0;
                    break;
                case EdgeType::undirected:
                    j = i + 1;
                    break;
            }
            
            while (j < N){
                auto mat_ij = at(i,j); 
                if (mat_ij  > 1){
                    remove_edge(i,j,mat_ij - 1);
                }
                j++;
            }
        }
    }

    void edge_swap(const std::pair<int,int> &e_ij, const std::pair<int,int> &e_kl, T value = 0){
        int i{e_ij.first},j{e_ij.second},k{e_kl.first},l{e_kl.second};
        auto a_ij = at(i,j);

        if (value){
            a_ij = value;
        }
            // at(i,j)--;
            // at(k,l)--;
            // at(i,l)++;
            // at(k,j)++;

            // if (dir == EdgeType::undirected){
            //     at(j,i)--;
            //     at(l,k)--;
            //     at(l,i)++;
            //     at(j,k)++;
            // }

            // nodes[i]->remove_stub(nodes[j]);
            // nodes[k]->remove_stub(nodes[l]);
            // nodes[i]->add_neighbor(nodes[l]);
            // nodes[k]->add_neighbor(nodes[j]);

            add_edge(i,l,a_ij);
            add_edge(k,j,a_ij);
            remove_edge(i,j,a_ij);
            remove_edge(k,l,a_ij);





        

    }

    
    std::size_t number_of_single_edges() const{
        std::size_t E{};
        int i,j; 
        for (i = 0 ; i < sizeMat ; i++){
            for (j = i + 1 ; j < sizeMat ; j++){
                E += std::abs( at(i,j) - at(j,i) );
            }
        }
        return E;
    }

    std::size_t number_of_mutual_edges() const{
        std::size_t E{};
        int i,j; 
        for (i = 0 ; i < sizeMat ; i++){
            for (j = i + 1 ; j < sizeMat ; j++){
                E += static_cast<unsigned long>(std::min(at(i,j),at(j,i)));
            }
        }
        return E;
    }



    std::size_t get_single_degree(const int &i,Neighborhood neighborType) const{
        std::size_t deg{};
        T mat_ij,mat_ji;
        for (int j = 0 ; j < sizeMat ; j++){
            mat_ij = at(i,j);
            mat_ji = at(j,i);
            switch(neighborType){
                case Neighborhood::single_in:
                    deg += mat_ji > mat_ij ? static_cast<unsigned long>(mat_ji - mat_ij) : 0; 
                    break;
                case Neighborhood::single_out: 
                    deg += mat_ij > mat_ji ? static_cast<unsigned long>(mat_ij - mat_ji) : 0; 
                    break;
                default:
                    break;
            }
        }
        return deg;
    }

    std::size_t get_mutual_degree(const int &i) const{
        std::size_t deg{};
        for (int j = 0 ; j < N ; j++){
            deg += std::min(at(i,j),at(j,i));
        }
        return deg;
    }

    std::size_t get_node_degree(const int &idx, Neighborhood neighborType) const{
        switch (neighborType)
        {
            case Neighborhood::single_in:
                return get_single_degree(idx,neighborType);
                break;

            case Neighborhood::single_out:
                return get_single_degree(idx,neighborType);
                break;

            case Neighborhood::mutual:
                return get_mutual_degree(idx);
                break;
        
            default:
                return nodes[idx]->get_degree(neighborType);
                break;
        }
    }

    std::vector<std::size_t> get_degree_seq(Neighborhood neighborType) const{
        std::vector<std::size_t> k(sizeMat);
        for (int i = 0 ; i < sizeMat ; i++){
            k[i] = get_node_degree(i,neighborType);
        }
        return k;
    }


    std::set<int> get_subg_neighborhood(const std::vector<int> &subg) const{
       
        std::set<int> subg_neighbors;
        for (auto &&i : subg){
            subg_neighbors.insert(nodes[i]->neighborhood_begin(),nodes[i]->neighborhood_end());
        }

        for (auto &&i : subg){
            subg_neighbors.erase(i);
        }

        return subg_neighbors;
    } 

    long get_subg_degree(const std::vector<int> &subg, const Neighborhood &type) const{
        std::set<int> subg_neighbors{get_subg_neighborhood(subg)};

        
        auto sumRule = [this,&subg,&subg_neighbors](const std::function<T(const std::vector<int>&,int&)> &addRule){
            return std::accumulate  (subg_neighbors.begin(),subg_neighbors.end(),0,
                                    [this,&subg,&addRule](long a, int neighbor){
                                        return std::move(a) + addRule(subg,neighbor);
                                    });
        };

        switch(type){

            case Neighborhood::undirected:{
                return sumRule([this](const std::vector<int> &subg, int neighbor){
                    return std::accumulate(subg.begin(),subg.end(),0,
                    [this, &neighbor](std::size_t a, int nodeId){
                        return std::move(a) + at(nodeId,neighbor);
                    });
                });
                break;
            }

            case Neighborhood::out:{
                return sumRule([this](const std::vector<int> &subg, int neighbor){
                    return std::accumulate(subg.begin(),subg.end(),0,
                    [this, &neighbor](std::size_t a, int nodeId){
                        return std::move(a) + at(nodeId,neighbor);
                    });
                });
                break;
            }

            case Neighborhood::in:{
                return sumRule([this](const std::vector<int> &subg, int neighbor){
                    return std::accumulate(subg.begin(),subg.end(),0,
                    [this, &neighbor](std::size_t a, int nodeId){
                        return std::move(a) + at(neighbor,nodeId);
                    });
                });
                break;
            }

            case Neighborhood::single_out:{
                return sumRule([this](const std::vector<int> &subg, int neighbor){
                        return std::max( 
                            std::accumulate(subg.begin(),subg.end(),0,
                            [this, &neighbor](long a, int nodeId){
                                return std::move(a) + at(nodeId,neighbor);
                            }) - 
                            std::accumulate(subg.begin(),subg.end(),0,
                            [this, &neighbor](long a, int nodeId){
                                return std::move(a) + at(neighbor,nodeId);
                            }),0);
                    });
                break;
            }

            case Neighborhood::single_in:{
                return sumRule([this](const std::vector<int> &subg, int neighbor){
                        return std::max( 
                            std::accumulate(subg.begin(),subg.end(),0,
                            [this, &neighbor](long a, int nodeId){
                                return std::move(a) + at(neighbor,nodeId);
                            }) - 
                            std::accumulate(subg.begin(),subg.end(),0,
                            [this, &neighbor](long a, int nodeId){
                                return std::move(a) + at(nodeId,neighbor);
                            }),0);
                });
                break;
            }

            case Neighborhood::mutual:{
                return sumRule([this](const std::vector<int> &subg, int &neighbor){
                        return std::min( 
                            std::accumulate(subg.begin(),subg.end(),0,
                            [this, &neighbor](long a, int nodeId){
                                return std::move(a) + at(nodeId,neighbor);
                            }) ,
                            std::accumulate(subg.begin(),subg.end(),0,
                            [this, &neighbor](long a, int nodeId){
                                return std::move(a) + at(neighbor,nodeId);
                            }));
                });
                break;
            }

            default:
                return 0.;
                break;
        }

    }

    std::unique_ptr<Graph<T>> get_graph_obj_from_subg(const std::vector<int> &subg) const{
        
        std::size_t n{subg.size()};
        std::vector<T> adjMat_subg(n*n);
        auto g = std::make_unique<Graph<T>>(dir,adjMat_subg);
        int k{},l{};
        for (auto &&i : subg){
            for (auto &&j : subg){
                T a_ij = at(i,j);
                switch (dir)
                {
                case EdgeType::directed:{
                    
                    if (a_ij){
                        g->set_edge(k,l,a_ij);
                    }
                    
                    break;
                }
                case EdgeType::undirected:{
                    if (i < j && a_ij){
                        g->set_edge(k,l,a_ij);
                    }
                    
                    break;
                }
                default:
                    break;
                }
                ++l;
            }
            l = 0;
            ++k;
            
        }
        return g;
    }

    inline std::size_t get_node_property(const int &i,NodeProperty prop){
        return nodes[i]->get(prop);
    }

    std::size_t number_of_active_nodes(){
        return std::accumulate(nodes.begin(),nodes.end(),0,
        [](std::size_t a, std::shared_ptr<INode> &b){
            return std::move(a) + ((b->state != NodeState::inactive) ? 1 : 0);
        });
    }

    std::size_t number_of_nodes_with_state(const NodeState &state){
        return std::accumulate(nodes.begin(),nodes.end(),0,
        [&state](std::size_t a, std::shared_ptr<INode> &b){
            return std::move(a) + ((b->state == state) ? 1 : 0);
        });
    }


    void dump_edge_list(const std::string &filename) const{
        std::ofstream o{filename,std::ios::binary};
        int i,j,rep;
        for (i = 0 ; i < sizeMat ; i++){
            for (j = i + 1 ; j < sizeMat ; j++){
                for (rep = 0 ; rep < at(i,j) ; rep++){
                    o << i << " " << j << '\n';
                }
                if (dir == EdgeType::directed){
                    for(rep = 0 ; rep < at(j,i) ; rep++){
                        o << j << " " << i << '\n';
                    }
                }
            }
        }

        o.close();
    }


};


#endif /* graph_hpp */
