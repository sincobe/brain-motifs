//
//  graphlet_census_parametrization.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/17/22.
//

#ifndef graphlet_census_parametrization_hpp
#define graphlet_census_parametrization_hpp

#include "gtrie.hpp"
#include <task_interface.hpp>




struct GraphletCensus : ITask{
    
    int max_subg_size;
    int min_subg_size;
    GraphletType gt;
    nlohmann::json sampling_strategy;
    
    GraphletCensus(std::string &input, int job_id = -1) : ITask{input,job_id},min_subg_size{2}{
        set_parameters();
    }
    
    std::filesystem::path get_output_path() override;
    void set_parameters() override;
    void make() override;
    
    bool overwrite(){
        std::string path_str = static_cast<std::string>(get_output_path()) + "/gdist.json";
        std::filesystem::path p(path_str);
        if (std::filesystem::exists(p) && cr.j.find("overwrite") != cr.j.end()){
            bool b = static_cast<bool>(cr.j["overwrite"]);
            if (b){
                std::filesystem::remove_all(get_output_path());
                return true;
            }
            else{
                return false;
            } 
        }
        else{
            return true;
        }
    }
};

#endif /* graphlet_census_parametrization_hpp */
