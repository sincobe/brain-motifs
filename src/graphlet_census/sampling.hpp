#ifndef sampling_hpp
#define sampling_hpp

#include <chrono>
#include <memory>
#include <random>

struct SamplingStrategy{
    int strategy_label;

    std::unique_ptr<std::mt19937> eng;
    SamplingStrategy() : strategy_label{}{
        std::random_device seeder;
        auto seed = std::chrono::system_clock::now().time_since_epoch().count();
        eng = std::make_unique<std::mt19937>(static_cast<std::mt19937::result_type>(seed));
    }
    virtual ~SamplingStrategy() = default; 
    
    
};

#endif