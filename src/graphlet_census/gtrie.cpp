//  gtrie.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 3/30/22.
//

#include "gtrie.hpp"


std::string GTrie::get_label(int &i, const std::vector<int> &subg, int depth) const{
    std::string label{};

    for (int j = 0 ; j < depth ; j++){

        label += G.at(subg[j],i) ? '1' : '0';
        
        if (G.dir == EdgeType::directed){
            label += G.at(i,subg[j]) ? '1' : '0';
        }

    }
    return label;
}


void GTrie::extend_subg(int &gtrieNodeId,
                        std::vector<int> &subg,
                        int &&depth,
                        std::vector<int> &extension){
    
    auto currentGTrieNode =  nodes[gtrieNodeId];
    if (depth <= max_subg_size && depth >= min_subg_size){
       currentGTrieNode->gs->add(subg,depth);
    }
    
    while(extension.size()){
        
        int child{*extension.rbegin()},idx,j;
        extension.pop_back();

        bool extendGTrie;
        switch (state){
            case GTrieState::Deterministic:
                extendGTrie = true;
                break;
            case GTrieState::UniformSampling:{
                int size = currentGTrieNode->size() ;
                if (size > 1){
                    extendGTrie = samplingMethod->sample(size - 1);
                }
                else{
                    extendGTrie = true;
                }
                break;
            }
            default:
                extendGTrie = true;
                break;
        }

        if (extendGTrie){
            std::vector<int> extension_new;
            if (depth+1 < max_subg_size){
                extension_new = extension;
                auto it_neighbor{G.nodes[child]->neighborhood_begin()};
                while (it_neighbor != G.nodes[child]->neighborhood_end()){
                    j = *it_neighbor;
                    if (j > *subg.begin()){
     
                        
                        idx = 0;
                        std::vector<int>::iterator it_subg{subg.begin()};
                        
                        while (idx < depth){
                            if (G.at(*it_subg,j)  || G.at(j,*it_subg)){
                                break;
                            }
                            ++idx;
                            ++it_subg;
                        }
                        
                        if (idx == depth){
                            extension_new.push_back(j);
                        }
                    }
                    ++it_neighbor;
                }
            }
                    
            subg[depth] = child;
            int gtrieNodeIdChild;
            insert(gtrieNodeId, gtrieNodeIdChild, child, subg, depth);
            extend_subg(gtrieNodeIdChild,subg,depth+1,extension_new);
        }
    }
}


void GTrie::esu(){
    
    GraphletDistribution::totalFrequency = 0;   
    int root{};
    nodes.emplace_back(std::make_shared<GTrieLabel>());
    
    auto t0 = std::chrono::high_resolution_clock::now();

    int i,j;
    for (i = 0 ; i < G.sizeMat-1 ; i++){
        
        if (i % world.size() == world.rank()){
            std::vector<int> subg(max_subg_size);
            std::vector<int> extension{};
            subg[0] = i;
            auto it_neighbor = G.nodes[i]->neighborhood_begin();
            while (it_neighbor != G.nodes[i]->neighborhood_end()){
                j = *it_neighbor;
                if (j > i){
                    extension.push_back(j);
                }
                ++it_neighbor;
            }
            extend_subg(root,subg,1,extension);
        }
    }

    gDist->close_files();

    world.barrier();
    auto tf = std::chrono::high_resolution_clock::now();
    timer   = std::chrono::duration_cast<std::chrono::milliseconds>(tf - t0).count();
}
