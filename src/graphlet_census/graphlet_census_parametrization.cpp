//
//  graphlet_census_parametrization.cpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 4/17/22.
//


#include "graphlet_census_parametrization.hpp"
namespace mpi = boost::mpi;

void GraphletCensus::set_parameters() {
    max_subg_size     = cr.j["max_subg_size"];
    if (cr.j.find("min_subg_size") != cr.j.end()){
        min_subg_size = cr.j["min_subg_size"];
    }
    sampling_strategy = cr.j["sampling_strategy"];
    
    std::string graphlet_type = cr.j["graphlet_type"];
    if (graphlet_type == "topological"){
        gt = GraphletType::Topological;
    }
    else{
        std::cout << "Unknown graphlet type" << std::endl;
    }
}

std::filesystem::path GraphletCensus::get_output_path(){
    std::string pathname{output_folder("graphlet_census")};
    std::filesystem::create_directories(pathname + (static_cast<bool>(sampling_strategy["storing"]) ? "/store" : ""));
    return std::filesystem::path(pathname);
}



void GraphletCensus::make() {

    std::vector<int> adjMat = get_adjacency_matrix(); 
    Graph<bool> G = {EdgeType::directed, adjMat};

    bool keep_going;
    if (!world.rank()){
        keep_going = overwrite();
    }
    mpi::broadcast(world, keep_going, 0);
    

    if (keep_going){
        GTrie gtrie = {G,min_subg_size,max_subg_size};
        gtrie.set_gdist(sampling_strategy,gt);
        gtrie.set_path(get_output_path());
        if (!world.rank()){
            std::cout << gtrie.get_path() << std::endl;
        }
        gtrie.esu();
        gtrie.dump();

        if (world.size() > 1){
            world.barrier();
            gtrie.merge_files();
            
            if (static_cast<bool>(sampling_strategy["storing"])){
                world.barrier();
                gtrie.merge_subgraph_files();
            }
        }
    }
    else if (!world.rank()){
        std::cout << "Folder already exists and overwrite option is set to 'false'. To overwrite existing folder, set overwrite option to 'true'.";
    }
    
}
