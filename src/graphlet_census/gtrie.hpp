//
//  gtrie.hpp
//  TemplateGraph
//
//  Created by Benichou Alexis on 3/30/22.
//

#ifndef gtrie_hpp
#define gtrie_hpp



#include <random>
#include <memory>
#include <iomanip>
#include <graphlet.hpp>
#include <chrono>
#include "sampling.hpp"
#include <filesystem>

namespace mpi = boost::mpi;

enum class GTrieState{Deterministic,UniformSampling};

struct GTrieLabel{
    const int id;
    const int parent;
    const std::string label;
    std::map<std::string,int> children;

    std::shared_ptr<GraphletStore> gs;
    GTrieLabel() : id{},parent{-1},label{},gs{}{}

    GTrieLabel(int i,
               int j,
               const std::string &str,
               std::shared_ptr<GraphletStore> &_gs) : id{i},parent{j},label{str},gs{_gs}{}

    int size() const{
        if (id > 0){
            return gs->graphlet->size;
        }
        else if(!id){
            return 1;
        }
        else{
            return 0;
        }
    }
};

struct SubgSampling : SamplingStrategy{
    std::vector<double> vecProb;

    SubgSampling(std::vector<double> &_vecProb) : SamplingStrategy(),vecProb{_vecProb}{}

    virtual bool sample(const int &vecP_idx) = 0;
};

struct UniformSubgSampling : SubgSampling{
    std::uniform_real_distribution<double> genProb;
    UniformSubgSampling(std::vector<double> &_vecProb) : SubgSampling{_vecProb},genProb(0.,1.){}
    
    bool sample(const int &idx) override{

        if (genProb(*eng.get()) < vecProb[ idx ]){
            return true;
        }
        else{
            return false;
        }
        
    }
};

struct GTrie{
    
    mpi::communicator world;
    double timer; 
    const int max_subg_size;
    const int min_subg_size;
    std::unique_ptr<GraphletDistribution> gDist;

    GTrieState state;
    std::unique_ptr<SubgSampling> samplingMethod;

    const Graph<bool> G;
    std::vector<std::shared_ptr<GTrieLabel>> nodes;

    template<typename U>
    GTrie(const Graph<U> &_G, const int &_min_subg_size, const int &_max_subg_size) :
    min_subg_size{_min_subg_size},
    max_subg_size{_max_subg_size},
    G{_G},
    state{GTrieState::Deterministic}{}
    

    inline void set_gdist(nlohmann::json &j, GraphletType type = GraphletType::Topological){

        
        gDist = std::make_unique<GraphletDistribution>(j,min_subg_size,type);

        
        
        int idState = j["id"];
        switch (idState)
        {
        case 0:
            state = GTrieState::Deterministic;
            break;
        case 1:{
            state = GTrieState::UniformSampling;
            std::vector<double> vecProb = j["acceptance_rates"];
            samplingMethod = std::make_unique<UniformSubgSampling>(vecProb);
            break;
        }
            
        default:
            break;
        }
    }

    
    inline void set_path(std::string &&str){
        gDist->set_path(str);
    }

    inline std::string get_path() const{
        return gDist->path;
    }

    
    
    std::string get_label(int &i, const std::vector<int> &subg, int depth) const;
    std::string get_subg_label(int node_id) const{

        std::string label{};
        
        while (node_id != 0){
            auto node = nodes[node_id];
            label     = node->label + label;
            node_id   = node->parent;
        }

        return label;
    }
    

    
    void add_node(const std::string &label, std::shared_ptr<GraphletStore> &gs, std::shared_ptr<GTrieLabel> &parent){
        int id{static_cast<int>(nodes.size())};
        parent->children[label] = id;
        nodes.emplace_back(std::make_shared<GTrieLabel>(id,parent->id,label,gs));
    }
    
    void insert(int &gtrieNodeIdParent,
                int &gtrieNodeIdChild,
                int &graphNodeIdChild,
                const std::vector<int> &subg,
                int &depth){
        
        const std::string label{get_label(graphNodeIdChild,subg,depth)};
        
        auto parent   = nodes[gtrieNodeIdParent];
        auto it_child = parent->children.find(label);
        if (it_child == parent->children.end()){
            gtrieNodeIdChild = static_cast<int>(nodes.size());
          
            std::string subg_label{get_subg_label(gtrieNodeIdParent)+label};
            auto g = GraphletFactory::make(gDist->type,depth+1,G.dir,subg_label); 
            auto it_g = gDist->find(g->label);

            if (it_g == gDist->end()){
                auto gs = gDist->add(g);
                add_node(label,gs,parent);
            }
            else{
                add_node(label,it_g->second,parent);
            }
        }
        else{
            gtrieNodeIdChild = it_child->second;
        }
    }
    
    
    
    void extend_subg(int &gtrieNodeId, std::vector<int> &subg, int &&depth, std::vector<int> &extension);
    
    void esu();
    
    void dump(){

        nlohmann::json j;
        j["algorithmic_time"]          = timer;
        j["total_number_of_subgraphs"] = GraphletDistribution::totalFrequency;
        for (auto &gstore : gDist->graphlets){
            gstore.second->dump(j);
        }
        switch (state){
            case GTrieState::Deterministic:{
                j["sampling_strategy"]["id"] = 0;
                break;
            }
            case GTrieState::UniformSampling:{
                j["sampling_strategy"]["id"] = 1;
                j["sampling_strategy"]["acceptance_rates"] = samplingMethod->vecProb;
                break;
            }

            default: 
                break;
        }
        
        std::string filename{gDist->path + "gdist"};
        if (world.size() > 1){
            filename += "_"+std::to_string(world.rank());
        }
        filename += ".json";
            
        std::ofstream o{filename,std::ios::binary};
        o << std::setw(4) << j;
        o.close();

        
    }
    
    
    void merge_files(){
        
        if (!world.rank() && world.size() > 1){
            std::string ref_filename{gDist->path+"gdist_0.json"};
            std::ifstream ref{ref_filename};
            nlohmann::json j_ref;
            ref >> j_ref;
            ref.close();
            remove(ref_filename.c_str());
            for (int rank = 1 ; rank < world.size() ; rank++){

                std::string rank_filename{gDist->path+"gdist_"+std::to_string(rank)+".json"};
                std::ifstream input{rank_filename};
                nlohmann::json j;
                input >> j;
                input.close();

                std::size_t rankTotalFrequency = j["total_number_of_subgraphs"]; 
                
                GraphletDistribution::totalFrequency += rankTotalFrequency;
                for (auto &g : j["graphlets"].items()){
                    if (!j_ref["graphlets"].contains (g.key())){
                        j_ref["graphlets"][g.key()] = g.value();
                    }
                    else{
                        std::size_t n0 = j_ref["graphlets"][g.key()]["number_of_subgraphs"];
                        std::size_t n1 =   g.value()["number_of_subgraphs"];
                        j_ref["graphlets"][g.key()] ["number_of_subgraphs"] = n0 + n1;
                    }
                }
                remove(rank_filename.c_str());
            }

            j_ref["total_number_of_subgraphs"] = GraphletDistribution::totalFrequency;
            std::ofstream o{gDist->path+"gdist.json"};
            o << std::setw(4) << j_ref;
            o.close();
        }
    }
    
    void merge_subgraph_files(){
        
        if (!world.rank() && world.size() > 1){
            
            std::string ref_filename{gDist->path+"gdist.json"};
            std::ifstream ref{ref_filename};
            nlohmann::json j_ref;
            ref >> j_ref;
            
            int rank;
            for (auto &&g : j_ref["graphlets"].items()){

                std::string key{g.key()};
                std::ofstream o{gDist->path+"store/"+key+".bin",std::ios::out | std::ios::binary};
                std::size_t n{};
                for (rank = 0 ; rank < world.size() ; rank++){

                    std::string rank_filename{gDist->path+"store/"+key+"_"+std::to_string(rank)+".bin"};
                    
                    if (std::filesystem::exists(rank_filename)){
                        std::ifstream input{rank_filename,std::ios::in | std::ios::binary};
                        char buffer[1024];
                        while(!input.eof()){
                            input.read (buffer,sizeof(buffer));
                            o    .write(buffer,input.gcount());
                        } 
                        
                        input.close();
                        remove(rank_filename.c_str());
                    }
                }
                o.close();
                std::ifstream check(gDist->path+"store/"+key+".bin",std::ios::out | std::ios::binary);
                std::string unused;
                while ( std::getline(check, unused) )
                    ++n;
                if (n != static_cast<std::size_t>(g.value()["number_of_subgraphs"])){
                    std::cout << key << " is wrong of " << n << " vs " << static_cast<std::size_t>(g.value()["number_of_subgraphs"]) << std::endl; 
                }
                check.close();
            }
            ref.close();
        }
    }
};

#endif /* gtrie_hpp */
