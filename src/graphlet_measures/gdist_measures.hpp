#ifndef gdist_measures_hpp
#define gdist_measures_hpp 

#include <orbit_polynomial.hpp>
#include <json.hpp>

struct GDistMeasure{

    std::string gdist_path;
    nlohmann::json gdist;
    std::shared_ptr<Graph<bool>> G;
    std::map<int,std::map<std::string,std::map<int,std::size_t>>> graphlet_degree_distribution;
    std::map<std::string,std::map<Neighborhood,std::map<std::size_t,std::size_t>>> subgraph_degree_distribution;
    std::map<std::string,std::shared_ptr<OrbitPolynomialRoot<NetworkMeasure<bool>>>> symmetry_measures;
    std::vector<NetworkMeasureType> measure_types;

    GDistMeasure(const Graph<bool> &_G, const std::string &folderPath, const std::vector<NetworkMeasureType> &_measure_types) : 
    gdist_path{folderPath},G{std::make_shared<Graph<bool>>(_G)},measure_types{_measure_types}{
        std::ifstream gdist_stream{folderPath+"gdist.json"};
        gdist_stream >> gdist;
        gdist_stream.close();

        for (auto &&[graphlet_label,j] : gdist["graphlets"].items()){
            // set_graphlet_degree  (graphlet_label);
            // set_subgraph_degree  (graphlet_label);
            set_symmetry_measures(graphlet_label);
        }
        // save_graphlet_degree_distribution();
        // save_subg_degree_distribution();
        save_symmetry_measures();
        
    } 

    // void set_subgraph_degree(const std::string &label){
    //     std::ifstream graphlet_stream{gdist_path+"store/"+label+".bin"};
    //     std::string line{},node_id{};
    //     std::stringstream ss{};
    //     std::vector<int> subg;


    //     std::vector<Neighborhood> neighborhood_types = 
    //     {Neighborhood::in,Neighborhood::out,Neighborhood::single_in,Neighborhood::single_out,Neighborhood::mutual};
    //     while (std::getline(graphlet_stream,line,'\n')){
    //         ss.clear();
    //         subg.clear();
    //         ss << line;
    //         while (std::getline(ss,node_id,',')){
    //             subg.push_back(std::stoi(node_id));
    //         }
    //         for (auto &&neighborhood_type : neighborhood_types){
    //             std::size_t val = G->get_subg_degree(subg,neighborhood_type);
    //             subgraph_degree_distribution[label][neighborhood_type][val]++; 
    //         }   
    //     }
    // }

    // void save_subg_degree_distribution() {
    //     nlohmann::json sddist_output;
    //     std::vector<Neighborhood> neighborhood_types = 
    //     {Neighborhood::in,Neighborhood::out,Neighborhood::single_in,Neighborhood::single_out,Neighborhood::mutual};
    //     for (auto &&[label,sddist_all] : subgraph_degree_distribution){
    //         for (auto &&[neighborhood_type,sddist] : sddist_all){
    //             for (auto &&[freq,count] : sddist){
    //                 sddist_output[label][INode::get_neighborhood_type_string(neighborhood_type)][std::to_string(freq)] = count;
    //             }
    //         }
    //     }
    //     std::ofstream o{gdist_path+"subg_degree_distribution.json"};
    //     o << std::setw(4) << sddist_output;
    //     o.close();        
    // }

    // void set_graphlet_degree(const std::string &label) {
    //     std::ifstream graphlet_stream{gdist_path+"store/"+label+".bin"};
    //     std::string line{},node_id{};
    //     std::stringstream ss{};
    //     std::vector<int> subg;

    //     std::unique_ptr<NautyLabel> nl;
    //     std::vector<int> orbits_canonical;
    //     while (std::getline(graphlet_stream,line,'\n')){
    //         ss.clear();
    //         subg.clear();
    //         ss << line;
    //         while (std::getline(ss,node_id,',')){
    //             subg.push_back(std::stoi(node_id));
    //         }
    //         auto g = G->get_graph_obj_from_subg(subg);
            
    //         nl = std::make_unique<NautyLabel>(*g.get());
    //         nl->set_canonical_labelisation();
    //         int o;
    //         std::vector<int> lab (nl->lab,nl->lab+nl->n);
    //         if (orbits_canonical.empty()){
    //             orbits_canonical.resize(nl->n);
    //             std::vector<int> orbits(nl->orbits,nl->orbits+nl->n);
    //             o = 0;
    //             for (auto &&l : lab){
    //                 orbits_canonical[o] = orbits[l];
    //                 ++o;
    //             }
    //         }
    //         o = 0;
    //         for (auto &&l : lab){
    //             graphlet_degree_distribution[subg[l]][label][orbits_canonical[o]]++;
    //             o++;
                
    //         }
    //     }
    // }

    // void save_graphlet_degree_distribution() const{
    //     nlohmann::json gddist_output;
    //     for (auto &&[n,gdd] : graphlet_degree_distribution){
    //         for (auto &&[label,dist] : gdd){
    //             for (auto &&[orbit,count] : dist){
    //                 gddist_output[std::to_string(n)][label][std::to_string(orbit)] = count;
    //             }
    //         }
    //     }
    //     std::ofstream o{gdist_path+"graphlet_degree_distribution.json"};
    //     o << std::setw(4) << gddist_output;
    //     o.close(); 
    // }

    void set_symmetry_measures(const std::string &label){
        std::string gtrie_label{gdist["graphlets"][label]["gtrie_label"]};
        
        auto g = std::make_unique<TopologicalGraphlet>(G->dir,gtrie_label);
        symmetry_measures[label] = std::make_shared<OrbitPolynomialRoot<NetworkMeasure<bool>>>(g->adjMat);
    }

    void save_symmetry_measures(){
        nlohmann::json symmetry_measures_output;
        for (auto &&[label,measure] : symmetry_measures){
            symmetry_measures_output[label] = measure->get_output();
        }

        auto global = std::make_unique<OrbitPolynomialRoot<NetworkMeasure<bool>>>(G->adjMat);
        symmetry_measures_output["input_graph"] = global->get_output();
        
        std::ofstream o{gdist_path+"symmetry_measures.json"};
        o << std::setw(4) << symmetry_measures_output;
        o.close();
    }

};

#endif