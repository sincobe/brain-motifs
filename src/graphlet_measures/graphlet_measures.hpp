
#ifndef graphlet_measures_hpp
#define graphlet_measures_hpp

#include <task_interface.hpp>
#include <gdist_measures.hpp>
#include <planted_motif_model.hpp>
#include <json.hpp>
#include <fstream>

struct GraphletMeasures : ITask{

    GraphletMeasures(std::string &input, int job_id = -1) : ITask{input,job_id}{
        set_parameters();
    }

   

    void set_parameters() override{
       
    }



  
    std::string graphlet_census_folder() const {
        if (cr.j.find("artificial_data") == cr.j.end()){
            return output_folder_of_real_graph("graphlet_census");
        }
        else{
            return output_folder_of_rand_graph("graphlet_census");
        }   
    }



    void build_output(nlohmann::json &j){
        

    }

    void make() override{
        std::vector<int> adjMat = get_adjacency_matrix();
        Graph<bool> G = {EdgeType::directed, adjMat};

        // // auto pmm = PlantedMotifModel(G,)

        // const std::filesystem::path folderpath{output_folder("graph_encoding")+"planted_motif_model/configuration"};
        // for (auto const& dir_entry : std::filesystem::directory_iterator{folderpath}){
        //     if (dir_entry.path().extension() == ".json"){
        //         auto filename    = std::filesystem::absolute(dir_entry.path());
        //         std::ifstream file(filename);
        //         nlohmann::json j   = nlohmann::json::parse(file); 
        //         std::cout << filename << std::endl;
        //         auto pmm = PlantedMotifModel<ConfigurationModel>(G,j,graphlet_census_folder());
        //     } 
        // }
            
        

        std::vector<NetworkMeasureType> types = {NetworkMeasureType::AutomorphismGroupSize};
        auto measure = std::make_unique<GDistMeasure>(G,graphlet_census_folder(),types);
   
   
    }
};

#endif