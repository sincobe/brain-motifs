function Atot = adjMatFromSyns(allDataSyns)
% A = adjMatFromSyns(allDataSyns)
% Builds the directed adjacency matrix of neurons (columns->rows)
% weighted by number of synapses

Atot = zeros(length(allDataSyns));

for ii = 1:length(allDataSyns)
    for jj = 1:length(allDataSyns(ii).synsin.origind)
        if allDataSyns(ii).synsin.origind(jj) ~= -1
            Atot(ii,allDataSyns(ii).synsin.origind(jj)) = Atot(ii,allDataSyns(ii).synsin.origind(jj)) + 1;
        end
    end
end