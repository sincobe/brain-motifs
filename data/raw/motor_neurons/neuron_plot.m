function neuron_plot( neuron, varargin)
% Makes a 3d plot of the input neuron, with several options.
%     neuron_plot( neuron, 'withsyn' ) plots synapses with inputs as cyan and outputs as red.
%     neuron_plot( neuron, LineSpec ) uses LineSpec data for the skeletons.

d_syn = 1*10^3; % Sets the size of a synapse marker

set(gcf,'Renderer','OpenGL'); hold on;
if any(strcmp(varargin,'withsyn'))
    varargin(strcmp(varargin,'withsyn')) = [];
    x0 = neuron.xyz(neuron.synsin.treeinds,:);
    x1 = neuron.synsin.xyz;
    syn_len = sqrt(sum((x1-x0).^2,2));
    x1_scaled = x0 + d_syn * (x1-x0+0.0) ./ repmat(syn_len,1,3);
    for ii = 1:size(x0,1);
        plot3([x0(ii,1), x1_scaled(ii,1)],...
            [x0(ii,2), x1_scaled(ii,2)],...
            [x0(ii,3), x1_scaled(ii,3)],...
            'LineWidth',5,'Color','c');
    end
    
    x0 = neuron.xyz(neuron.synsout.treeinds,:);
    x1 = neuron.synsout.xyz;
    syn_len = sqrt(sum((x1-x0).^2,2));
    x1_scaled = x0 + d_syn * (x1-x0) ./ repmat(syn_len,1,3);
    for ii = 1:size(x0,1)
        plot3([x0(ii,1), x1_scaled(ii,1)],...
            [x0(ii,2), x1_scaled(ii,2)],...
            [x0(ii,3), x1_scaled(ii,3)],...
            'LineWidth',8,'Color','r');
    end
end

% Computes a minimal set of single towards-root paths covering the entire
% neuron. This lets us use a minimal number of separate lines.
partition_limit = Inf;
P = partition_skeleton( neuron.Adir );

for ii = 1:min(length(P),partition_limit)
    plot3(neuron.xyz(P{ii},1), neuron.xyz(P{ii},2), neuron.xyz(P{ii},3), varargin{:});
end

if any(strcmp(varargin,'Color'))
    clr = varargin{1+find(strcmp(varargin,'Color'))};
else
    clr = 'b';
end

axis image;

% Plots a sphere at the location tagged 'soma'.
if neuron.soma ~= -1
    [X, Y, Z] = sphere(8);
    X = 2500*X + neuron.xyz(neuron.soma(1),1);
    Y= 2500*Y + neuron.xyz(neuron.soma(1),2);
    Z = 2500*Z + neuron.xyz(neuron.soma(1),3);
    surf(X,Y,Z,'FaceColor',clr,'LineStyle','none')
end
