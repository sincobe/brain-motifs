function inds = matchIds(idsVec,allIds)

inds = zeros(size(idsVec));
for ii = 1:length(idsVec)
    relInd = find(allIds==idsVec(ii));
    if isempty(relInd)
        disp(['Cannot find ' num2str(idsVec(ii))]);
    else
        inds(ii) = find(allIds==idsVec(ii));
    end
end