Files related to the network analysis of the KC-to-KC network.

Code was written for use in MATLAB 2014b by Casey Schneider-Mizell.

'mushroom_body_neurons.mat'
  A .mat file containing the morphology and connectivity of all Kenyon Cells (KC_neurons) and a CSV file annotating the number of claws for each Kenyon Cell.

'KCKC_network_analysis.m'
  This file contains the scripts for analysis and plotting of the KC-KC network, starting from loading mushroom_body_neurons.mat. The analysis requires several accompanying functions written as part of the neuronal analysis tools here:
    neuron_plot.m: For 3d plotting of neurons
    matchIDs.m: A neuron id to index lookup.
    adjMatFromSyns.m: Generates an adjacency matrix from the KC_neurons data structure.
    partition_skeleton.m: Finds a minimal set of paths that span a skeleton, used for neuron_plot.
  The above code is distributed under an MIT License.

  The following functions and packages are required for complete analysis:
      Brain Connectivity Toolbox (https://sites.google.com/site/bctnet/) to compute network communities
      errorbarjitter.m (https://www.mathworks.com/matlabcentral/fileexchange/33658-errorbarjitter?focused=3820615&tab=function) for plotting.

Copyright 2017, Casey Schneider-Mizell

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
