%% Pull neurons and annotation map

load mushroom_body_neurons

%% updated to deal with CSVs intead of annotations (saved in KC_anno_fin)
% Had to manually tweak " KC #0 #29"
ck_LR = unique(KCnamesandclaws(:,1));
claw_groups = { [1 2], [3 4], [5 6], [7 8], [9 10], [11 12], [13 14] };

ids_csv = zeros(size(KCnamesandclaws,1),1);
for ii = 1:length(KCnamesandclaws(:,2))
   ind_hash = strfind( KCnamesandclaws{ii,2}, '#' );
   ids_csv(ii) = str2num( KCnamesandclaws{ii,2}( ind_hash+1:end) );
end

claw_num_str = cell(size(KCnamesandclaws,1),1);
for ii = 1:length( KCnamesandclaws(:,1) )
   space_inds = strfind( KCnamesandclaws{ii,1}, ' ');
   claw_num_str{ii} =  KCnamesandclaws{ii,1}( 1:space_inds(end)-1 );
end

claw_anno = {'1 claw KC', '2 claw KC', '3 claw KC', '4 claw KC', '5 claw KC', '6 claw KC', 'young KC'};
ids_c = cell(size(claw_anno));
for ii = 1:length(claw_anno)
    ids_c{ii} = ids_csv( strcmp( claw_num_str, claw_anno{ii} ) );
end
clawmap = containers.Map( claw_anno, ids_c );

ids_cLR = cell(size(ck_LR));
for ii = 1:length(ck_LR)
    ids_cLR{ii} = ids_csv( strcmp( KCnamesandclaws(:,1), ck_LR{ii} ) );
end
clawmap_LR = containers.Map( ck_LR, ids_cLR );

%% Organize KC_neurons into groups

A = adjMatFromSyns(KC_neurons);

names = {KC_neurons.name};
ids = [KC_neurons.id];

ids_kcl = [];
for ii = 1:2:11
    ids_kcl = [ids_kcl; clawmap_LR(ck_LR{ii}) ];
end
inds_kcl = matchIds( ids_kcl, ids);

ids_kcr = [];
for ii = 2:2:12
    ids_kcr = [ids_kcr; clawmap_LR(ck_LR{ii}) ];
end
inds_kcr = matchIds( ids_kcr, ids);

inds_kc = [inds_kcl; inds_kcr];

[Ci, Q] = community_louvain( A(inds_kc, inds_kc), 1.4 );
gr_id = unique(Ci);
grps = cell(size(gr_id));
for ii = 1:length(gr_id)
    grps{ii} = find(gr_id(ii)==Ci);
end

% Order by number of within-group synapses
grpsyn = zeros(size(grps));
for ii = 1:length(grps)
    grpsyn(ii) = sum(sum( A(inds_kc(grps{ii}),inds_kc(grps{ii}))));
end
[~,ord_grp] = sort(grpsyn,'descend');
grps = grps(ord_grp([2 3 1 4]));  % This was checked to put things into the order Left 1, Left 2, Right 1, Right 2.

ngrps = length(grps);

inds_kc_g = cell(size(grps));
for ii = 1:length(grps)
    inds_kc_g{ii} = inds_kc(grps{ii});
end

new_ord = cat(1,inds_kc_g{:});
A_ord = A(new_ord,new_ord);

%% Shorten names

for ii = 1:length(names)
    nmbrk = regexp(names{ii},';');
    if ~isempty(nmbrk)
        names{ii}(nmbrk:end) = [];
    end
end

%%

clrs = cbrewer('qual','Dark2',4);

g_clr = cell(1,4);
for ii = 1:4
    g_clr{ii} = clrs(ii,:);
end
g_clr{end+1} = 'k';

gnames = cell(1,ngrps);
for ii = 1:ngrps
    gnames{ii} = ['KC Group ' num2str(ii)];
end

%% Plot number of cells / class
figure('Color','w'); hold on;
for ii = 1:ngrps
    bar(ii,length(inds_kc_g{ii}),'FaceColor',g_clr{ii},'EdgeColor','none')
end
ylabel('Number of KCs')
set(gca,'XTick',1:ngrps,'XTickLabels',gnames,'FontSize',14)

%%
export_fig('1 Population size.pdf');

%% Properties of the two types of KCs

Nin_g = cell(size(inds_kc_g));
Nout_g = cell(size(inds_kc_g));

for ii = 1:length(inds_kc_g)
    for jj = 1:length(inds_kc_g{ii})
        Nin_g{ii}(end+1) = length(KC_neurons(inds_kc_g{ii}(jj)).synsin.treeinds);
        Nout_g{ii}(end+1) = sum(KC_neurons(inds_kc_g{ii}(jj)).synsout.numTargs);
    end
end

Nin_mat = nan(max(cellfun('length',Nin_g)),ngrps);
Nout_mat = nan(max(cellfun('length',Nout_g)),ngrps);

for ii = 1:ngrps
    Nin_mat(1:length(Nin_g{ii}),ii) = Nin_g{ii}';
    Nout_mat(1:length(Nout_g{ii}),ii) = Nout_g{ii}';
end
%clrs = {cmap2(2,:), cmap2(2,:), cmap2(1,:), cmap2(1,:)};
h = figure('Color','w','Position',[440   428   916   370]); hold on;
h1 = subplot(1,2,1);
errorbarjitter(Nin_mat,h1)
ylabel('KC input synapses')
set(h1,'XTickLabel',{'Group 1 Left','Group 2 Left','Group 1 Right','Group 2 Right'},'FontSize',14,'XTickLabelRotation',45,'YGrid','on');
ylim([0 450])

h2 = subplot(1,2,2);
errorbarjitter(Nout_mat,h2)

ylabel('KC output synapses')
set(h2,'XTickLabel',{'Group 1 Left','Group 2 Left','Group 1 Right','Group 2 Right'},'FontSize',14,'XTickLabelRotation',45,'YGrid','on');
ylim([0 700])
%%
export_fig('2 Synapse count.pdf')

%% Plot adjacency matrix
%% FIGURE FOR SI

cmap = cbrewer('seq','Greys',11);
figure('Color','w')
A2 = A; A2(A<2)=0;
imagesc(A(cat(1,inds_kc_g{:}),cat(1,inds_kc_g{:}) ))
set(gca,'YTick',[length(inds_kc_g{1})+0.5 length(inds_kc_g{2})+length(inds_kc_g{1})+0.5 length(inds_kc_g{3})+length(inds_kc_g{2})+length(inds_kc_g{1})+0.5],...
    'XTick',length(inds_kc_g{1})+0.5,'TickDir','out','XTickLabel',{},'YTickLabel',{})
caxis([-0.5 10.5])
colormap(cmap)
axis square;


%%
export_fig('3 Adjacency matrix.pdf')

%% Compute density of connections between/within groups
% FIGURE FOR SI
cmap2 = cbrewer('qual','Paired',4);

figure('Color','w'); hold on;
ax = bar([1 2 3 4],[ [dens_mat(1,1) dens_mat(1,2) dens_mat(2,1) dens_mat(2,2)];...
    [dens_mat(3,3) dens_mat(3,4) dens_mat(4,3) dens_mat(4,4)]]', 1);
ax(1).FaceColor = 'k'; ax(1).EdgeColor = 'w';
ax(2).FaceColor = 'k'; ax(2).EdgeColor = 'w';
set(gca,'XTick',1:4,'XTickLabel',{'Type 1->Type 1', 'Type 1->Type 2', 'Type 1->Type 2', 'Type 2->Type 2'},'FontSize',14,'XTickLabelRotation',45)
ylabel('Edge density (2+ synapses)')
ylim([0 0.7])
%%
export_fig('4 inter-KC density.pdf')

%% Plot connection strength for different groups
temp1 = A(inds_kc_g{1},inds_kc_g{1});
temp2 = A(inds_kc_g{3},inds_kc_g{3});
temp = [temp1(:); temp2(:)];
syns11 = temp(temp>0);

temp1 = A(inds_kc_g{1},inds_kc_g{2});
temp2 = A(inds_kc_g{3},inds_kc_g{4});
temp = [temp1(:); temp2(:)];
syns21 = temp(temp>0);
[y21 x21] = hist(syns21,0:max(syns21));

temp1 = A(inds_kc_g{2},inds_kc_g{1});
temp2 = A(inds_kc_g{4},inds_kc_g{3});
temp = [temp1(:); temp2(:)];
syns12 = temp(temp>0);
[y12 x12] = hist(syns12,0:max(syns12));


temp1 = A(inds_kc_g{2},inds_kc_g{2});
temp2 = A(inds_kc_g{4},inds_kc_g{4});
temp = [temp1(:); temp2(:)];
syns22 = temp(temp>0);
[y22 x22] = hist(syns22,0:max(syns22));

figure('Color','w'); hold on;
bplot(syns11,1,'outliers','whisker',5);
bplot(syns12,2,'outliers','whisker',5);
bplot(syns21,3,'outliers','whisker',5);
bplot(syns22,4,'outliers','whisker',5);
set(gca,'XTick',[1 2 3 4],'YScale','log','YGrid','on','XTickLabel',{'1->1','1->2','2->1','2->2'},'TickDir','out','YLim',[0.9 70])

%% Plot number of claws per group

inds_kc_g2 = cell(2,1);
inds_kc_g2{1} = [inds_kc_g{1}; inds_kc_g{3}];
inds_kc_g2{2} = [inds_kc_g{2}; inds_kc_g{4}];

grp_by_claw = zeros(length(inds_kc_g2),6);
ind_by_claw = cell(length(inds_kc_g2),6);

claw_inds = cell(1,6);

for ii = 1:length(claw_anno)
    rel_ids = clawmap(claw_anno{ii});
    claw_inds{ii} = matchIds(rel_ids,ids);
    for jj = 1:length(inds_kc_g2)
        ind_by_claw{jj,ii} = intersect(claw_inds{ii},inds_kc_g2{jj});
        grp_by_claw(jj,ii) = length(intersect(claw_inds{ii},inds_kc_g2{jj}));
    end
end

figure('Color','w')
b = bar(1:length(claw_anno),grp_by_claw',0.9,'EdgeColor','none');
for ii = 1:length(inds_kc_g2)
   b(ii).FaceColor = clrs{ii}; 
end

b(end).FaceColor = clrs{end}; 
set(gca,'Box','off','YGrid','on','FontSize',14,'TickDir','out')
ylabel('# KCs')
xlabel('# claws')

leg_text = cell(size(inds_kc_g2));
for ii = 1:size(grp_by_claw,1)
   leg_text{ii} = ['Total Claws = ' num2str( dot(grp_by_claw(ii,:),1:9))];
end
legend(leg_text)

%%
export_fig('8 Claws by group.pdf')

%% Plot anatomy

clr1 = [46 48 146]/255;
clr2 = [0 173 239]/255;
new_clrs = {clr1,clr2,clr1,clr2};

h = figure('Renderer','OpenGL','Color','w'); hold on;
for ii = 1:2
    for jj = 1:length(inds_kc_g{0+ii})
        neuron_plot(KC_neurons(inds_kc_g{0+ii}(jj)),'Color',new_clrs{2+ii})
    end
end

