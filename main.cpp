//
//  main.cpp
//  brain-motifs
//
//  Created by Benichou Alexis on 8/21/21.
//


#include <chrono>
#include <iomanip>
#include <boost/mpi.hpp>
#include <graphlet_census_parametrization.hpp>
#include <graph_encoding.hpp>
#include <graph_randomization.hpp> 
#include <test_non_null.hpp>
#include <graphlet_measures.hpp>




namespace mpi = boost::mpi;

int main(int argc, char * argv[]) {

    mpi::environment env(argc, argv);
    mpi::communicator world;

    std::string input        = argv[1];
    int job_id               = std::stoi(argv[2]);
    int number_of_executions = 1;


    std::unique_ptr<ITask> task;
    if (input == "graphlet_census"){
        task = std::make_unique<GraphletCensus>(input,job_id);
    }
    else if (input == "graphlet_measures"){
        task = std::make_unique<GraphletMeasures>(input,job_id);
    }
    else if (input == "graph_encoding"){
        task = std::make_unique<GraphEncoding>(input,job_id);
    }
    else if (input == "graph_randomization"){
        task = std::make_unique<GraphRandomization>(input,job_id);
    }
    else if (input == "test_non_null"){
        task = std::make_unique<TestNonNull>(input,job_id);
    }
    
    
    auto begin = std::chrono::high_resolution_clock::now();
    task->make();    
    auto end   = std::chrono::high_resolution_clock::now();
    
    if (world.rank() == 0){
        auto ms  = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
        std::cout <<  "Has lasted: " << ms <<" ms. \n" << std::endl;
    }



    
    return 0;
}
