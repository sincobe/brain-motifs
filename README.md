# Inference of network motif sets

This project proposes a modern tool for collecting _network motifs_ --- statistically significant structural patterns --- in static real-world networks. 
The code is the one used for the results displayed in the paper...
The main presented method relies on information-theoretic model selection, based on the Minimum Description Length (MDL) principle.
<!-- Additionally, classical mainstream frequentist network motif mining is also available.  -->

## Requirements & Installation

This project is meant for MacOs and Linux users. 

The necessary modules are the following: 
- GCC      (used version: 9.2.0), the common brick to compile C/C++ project (https://gcc.gnu.org).
- CMake    (used version: 3.27.7), for an automated building of the C++ project located in `src` (https://cmake.org).
- Open Mpi (used version: 4.0.5), to be able to run parallelized tasks (https://www.open-mpi.org).
- Boost    (used version: 1.81.0), A standard C++ library, that, here, alows for practically calling Open Mpi core functions (https://www.boost.org).
- Nauty    (used version: 27r3), a graph isomorphism tool, used for graphlet and subgraph census (https://pallini.di.uniroma1.it/index.html).
- The header file `json.hpp` (used version: 3.9.1), located in the `include` folder (it can also be downloaded from https://github.com/nlohmann/json). 

* For **Linux** users (e.g. on Ubuntu), installation commands in the terminal are:
  1. 
   ```
   sudo apt install wget cmake libboost-mpi-dev libnauty2
   ```
   2. Install Nauty again to get the `nauty.h` file:
   ```
   wget -c https://users.cecs.anu.edu.au/~bdm/nauty/nauty2_8_8.tar.gz -O - | tar -zx
   cd nauty2_8_8
   ./configure
   make
   sudo make install
   cd ..
   ```
* For **MacOs** users, installation commands (using Homebrew, https://formulae.brew.sh/)  are:
  ````
   brew install cmake boost-mpi nauty 
  ```` 

* For **Linux & MacOs** users, to clone and compile the project:
  ````
  git clone https://gitlab.pasteur.fr/sincobe/brain-motifs
  cd brain-motifs
  mkdir build
  cd build
  cmake ..
  ````


## First step: input graphs and algortihmic parametrization, the `param` folder

A set of JSON files that are made to 
- Find input data, where are edge lists of real-world networks;
- Define an output folder, where to store results;
- Set the parameters of the graphlet & subgraph census;
- Set the parameters of our motif-based inference.
<!-- - Set the parameters of the randomization graph procedures (for the special case of frequentist motif mining). -->

### Finding data & results, `path.json`

The `path.json` file, first, associates to the path of an input file a two-step alias: a *category* indicating a data folder and a *subcategory* pointing to a data file.
Secondly, it associates to data categories and their subcategories output folders for each algorithmic task.   

The shared root of `input` and `output` folders is a relative path with respect to the current folder or an absolute path given in the variable `"base"` (which is empty in the default version).

1. `"input"`: 
   1. `"base"`: a first complementary relative root path to the common root to find all dataset folders.
   2. `"dataset"`:
      1. `"base"`: a second complementary relative root path to find the dataset folders used by the algorithms; for instance to differiante raw from processed data. 
      2. `"category"`: a dictionary that maps string names of dataset folders to their respective relative folder paths with respect to the *processed* data folder. 
      3. `"subcategory"`: a dictionary that maps string  names of datapoints to their respective relative file paths with respect to the dataset folder path.
2. `"output"`:
   1. `"base"`: a first complementary relative root path to the common root to find all output folders produced by the algorithmic tasks.
   2. `"algorithm"`: a dictionary that maps to the string name of an algorithmic task to its corresponding output folder relative path within the output folder.
   3. `"category"`: a dictionary that maps to the string name of dataset its relative folder path within each output folder of the alogrithmic tasks.
   4. `"subcategory"`: a dictionary that maps to the string name of a datapoint its relative folder path within the category folder. 

Let us give an example. 
```json
{
   "base": "",
   "input": {
      "base":"data/",
      "dataset":{
         "category":{
            "celegans": "Celegans/",
            "drosophila_larva": "DrosophilaLarva/"
         },
         "subcategory":{
            "white_1986_whole": "White_1986/edge_list_whole.txt",
            "mushroom_body": "MB/edge_list_MB.txt"
         }
      }
   },
   "output":{
      "base": "output/",
      "algorithm":{
         "graphlet_census": "graphlet_census/",
         "graph_encoding": "graph_encoding/"
      },
      "dataset":{
         "category":{
            "celegans": "Celegans/",
            "drosophila_larva": "Drosophilalarva/"
         },
         "subcategory":{
            "white_1986_whole": "White_1986_Whole/",
            "mushroom_body": "MB/"
         }
      }
   }
}
```
Say that the `path.json` is deserialized as a simple dictionary in stored in a variable called `j_path`. 
Then, the edge list of *C. elegans*' complete connectome is located in `j_path["base"] + j_path["input"]["base"] + j_path["input"]["dataset"]["category"]["celegans"] + j_path["input"]["dataset"]["subcategory"]["white_1986_whole"]`.
After launching a graphlet census, the produced results are stored in `j_path["base"] + j_path["output"]["base"] + j_path["output"]["algorithm"]["graphlet_census"] + j_path["output"]["dataset"]["category"]["celegans"] + j_path["output"]["dataset"]["subcategory"]["white_1986_whole"]`.

In the default version, a `path.json` is proposed for all the datasets available from this repo. 

### Selection of input graphs, `datasets.json`

The `datasets.json` is a vector of dictionaries that serve as input of the algorithmic tasks. 
A single edge list constitutes the unique input of any algorithmic task.
To declare which input to read from the `path.json`, an element of `datasets.json` is given.
Let us give an example:
```json  
[
   {
      "category": "celegans",
      "subcategory": "white_1986_whole",
      "id":0
   },
   {
      "category": "drosophila_larva",
      "subcategory": "mushroom_body",
      "id":1
   }
]
```
In this case, one of the two edge lists may be read as input.
**The categories and subcategories must exist in the `path.json` file.** 
Remark: The datapoints' `id` must be orderly ranked, from the lowest to the highest line number of the JSON file. 

### Collecting subgraphs, `graphlet_census.json`

The `graphlet_census.json` parametrizes the algorithm that enumerates graphlet occurrences and stores them as lists of induced subgraphs.
We implemented our own paralellized-version of the Rand-Fase algorithm (cf. ...).
Let us the present the property members of the JSON file:
1. `"task"`: always set to `"graphlet_census"`.
2. `"max_subg_size"`: sets the maximal size of all stored subgraphs. As there is one subgraph list file per graphlet, **we do not advise going beyond five nodes**, to limit the impact on memory. Indeed, the maximal number of graphlets grows super-exponentially with the graphlet sizev (cf. https://oeis.org/A003085): 
   * For a maximal size of 3 nodes, there are 13 graphlets; 
   * For a maximal size of 4 nodes, there are 199 graphlets;    
   * For a maximal size of 5 nodes, there are 9364 graphlets, etc. 
3. `"min_subg_size"`: sets the maximal size of all stored subgraphs. Typically, this value is set to 3.
4. `"graphlet_type"`: always set to `"topological"` (this is the only kind of graphlet census currently implemented.)
5. `"sampling_strategy"`: is a dictionary that distinguishes the two available graphlet-census algorithms.
   1. If you anticipate that there is enough free memory to store **all** subgraphs from `"min_subg_size"` to `"max_subg_size"`, you may use the deterministic and full graphlet-census. It is selected by setting the property `"id"` to 0. 
   2. Otherwise, you may use a stochastic graphlet census algorithm that allows for a partial selection of subgraphs. To explain how to manipulate it, a basic understanding of the algorithm is required. The enumerating algorithm follows a tree-like search, that, based on a $`n`$-sized subgraph, looks for an $`(n+1)`$-sized subgraph, by exploring nodes that are neighbors to the root subgraph. The stochastic graphlet census fixes a set of acceptance rates, e.g. for 5-node subgraphs $`[p_2,p_3,p_4,p_5]`$, that defines the transition probability of the growing step from the $`n`$-sized to the $`(n+1)`$-sized subgraph. The strategy is selected by setting the property `"id"` to 1.
   3. `"storing"`:must be set to `true` to use the motif-based algorithm, otherwise , i.e. if set to `false`, this is only an enumerating task. 
6. `"number_of_datasets"`: permits to read a fraction of the datasets in `datasets.json`. In regular cases, it simply must indicate the number of elements of datapoints in `"datasets.json"`. 

Let us give an example, where *all* induced subgraphs of sizes 3 and 4 are collected from the above `datasets.json` file. 
`````json
{
  "task":"graphlet_census",
  "max_subg_size":4,
  "min_subg_size":3,
  "graphlet_type":"topological",
  "sampling_strategy":{
      "id":0,
      "storing":true
  },
  "number_of_datasets":2
}
``````
Let us give another example. If one wishes to *partially* observe subgraphs of sizes 3, 4 and 5 with the following fractions: all 3-node subgraphs, *half* of the 4-node subgraphs, and a *quarter* of the 5-node subgraphs, then the parameter file would be:
`````json
{
  "task":"graphlet_census",
  "max_subg_size":4,
  "min_subg_size":3,
  "graphlet_type":"topological",
  "sampling_strategy":{
      "id":0,
      "acceptance_rates": [1.0,1.0,0.5,0.5]
      "storing":true
  },
  "number_of_datasets":2
}
`````
The last value of the `acceptance_rate` is 0.5 because the fraction of the five-node subgraphs is equal to $`p_5 p_4 p_3 p_2`$.

### The motif set inference, `graph_encoding.json`

The `graph_encoding.json` parametrizes the inference of the motif-based model, which we refer to as the *planted motif model*. The inference of the motif distribution depends on a *dyadic base model* and on a greedy optimization reliying on sampling minibatches of subgraphs. Let us expand on the parameters.
1. `"task"`: is set to the string name `"graph_encoding"`.
2. `"method"`: is set to `"planted_motif_model"`.
3. `"models"`: is the vector of dyadic base models, whose attributes and functions are explained in ... . Currently, it should be set to `[ erdos-renyi, configuration, reciprocal_erdos-renyi, reciprocal_configuration ]`.
4. `"minibatch_strategy"`: sets the distribution of candidate subgraphs per graphlet and per time step of the optimization. There are two strategies:
   1. Equal-sized subgraph samples per graphlet, indicated with the property `"id"` set to 0. In this context, `"size"` is the *constant* number of sampled subgraphs per graphlet and per time step. A typical value for `"size"` is 10.
   2. Equilibrated subgraph samples per graphlet, indicated with the property `"id"` set to 1. In this context, `"size"` is the *weight* of the subgraph sample per graphlet and per time step. For instance, if a graphlet has a frequency $`f`$ and that the parameter of the minibatch strategy has a value $`s`$, then the number of sampled subgraphs for this graphlet is $` \lfloor f s \rfloor `$.   
   3. If the RAM is too small to perform a uniform sampling of subgraphs (which is often the case for graphs that are not sparse and/or larger than a thousand nodes), one may use an option that reads subgraph lists in chunks, instead of sampling uniformly over the complete subgraph collection. The associated parameter is called `"chunk_reading"` and should be set to `"true"` in these complicated cases. Further details can be found in ... .
5. `number_of_repetitions`: number of executions of the inference algorithm. The latter is stochastic and greedy: it should be applied several times.   
6. `"number_of_datasets"`: permits to read a fraction of the datasets in `datasets.json`. In regular cases, it simply must indicate the number of elements of datapoints in `"datasets.json"`. 
   
<!-- ### `graph_randomization.json` -->

## Launching algorithms & plots

### Obtaining raw results file

After setting up the parametric files of the `param` folder, the graphlet census and motif inference algortihms are ready to be run. 
Given a list of networks explicitely named in `datasets.json` which you intend to equally treat by sharing the same algorithmic parameters, a two-step procedure requires to run, *in this order*, the following commands

````bash
mpirun --use-hwthread-cpus ./build/brain-motifs graphlet_census $NETWORK_DATA_ID
mpirun --use-hwthread-cpus ./build/brain-motifs graph_encoding $NETWORK_DATA_ID
````
One sees that the executable has two parameters. 
First, the string name of the algorithmic task.
Second, the `id` of the input network, stored in the variable `$NETWORK_DATA_ID` above. 
The procedure should be repeated for all networks referenced in `datasets.json`.

## Plots

To plot results and get similar figures as the one found in ..., 
one can run the following commands 

````bash
python3 python/inference_processing.py $NETWORK_DATA_ID
python3 python/inference_plot.py       $NETWORK_DATA_ID
````


